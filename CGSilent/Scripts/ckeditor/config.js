/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';
     
};


CKEDITOR.editorConfig = function (config) {
    config.toolbar = 'MyToolbar';

    config.toolbar_MyToolbar =
	[
		{ name: 'document', items: [] },
		{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
		{ name: 'editing', items: [] },
		{
		    name: 'insert', items: [ 'Smiley']
		},
                '/',
		{ name: 'styles', items: [] },
		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
		{ name: 'paragraph', items: [] },
		{ name: 'links', items: [] },
		{ name: 'tools', items: [] }
	];
};
