﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Collections;
using MongoDB.Driver;
using System.Web.Mvc;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using MongoDB.Driver.GridFS;
using System.Threading;

namespace CGSilent.Models
{
    public class Articles : MongoDBModel<Articles>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Author;
        [Display(Name = "نویسنده")]
        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }


        private long _ArticleNumber;

        [Display(Name = "شماره خبر")]
        public long ArticleNumber
        {
            get { return _ArticleNumber; }
            set { _ArticleNumber = value; }
        }

        [StringLength(100, ErrorMessage = "The {0} must be at least {6} characters long.", MinimumLength = 6)]
        private string _Title;
        [Required]
        [Display(Name = "تیتر خبر")]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }


        [StringLength(300, ErrorMessage = "The {0} must be at least {6} characters long.", MinimumLength = 6)]
        private string _Summery;
        [AllowHtml]
        [Display(Name = "خلاصه خبر")]
        [UIHint("Html")]
        [Required]
        public string Summery
        {
            get { return _Summery; }
            set { _Summery = value; }
        }

        private string _Description;
        [AllowHtml]
        [Display(Name = "شرح خبر")]
        [Required]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }


        private int _IsDeleted;

        public int IsDeleted
        {
            get { return _IsDeleted; }
            set { _IsDeleted = value; }
        }


        private string _ImagePath;
        [Required]
        [Display(Name = "آدرس تصویر خبر")]
        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }


        private string _FileAddress;
        [Required]
        [Display(Name = "آدرس فایل")]
        public string FileAddress
        {
            get { return _FileAddress; }
            set { _FileAddress = value; }
        }

        private string _FileName;
        [Display(Name = "نام فایل")]
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }

        private string _FileSize;

        public string FileSize
        {
            get { return _FileSize; }
            set { _FileSize = value; }
        }


        private string _FileType;
        [Required]
        [Display(Name = "نوع فایل")]
        public string FileType
        {
            get { return _FileType; }
            set { _FileType = value; }
        }


        private string _VideoAddress;
        public string VideoAddress
        {
            get { return _VideoAddress; }
            set { _VideoAddress = value; }
        }

        private string _VideoName;
        [Display(Name = "")]
        public string VideoName
        {
            get { return _VideoName; }
            set { _VideoName = value; }
        }

        private string _Tag;
        [Display(Name = "تگ")]
        public string Tag
        {
            get { return _Tag; }
            set { _Tag = value; }
        }

        private string _KeyWords;
        [Display(Name = "کلمات کلیدی خبر")]
        public string KeyWords
        {
            get { return _KeyWords; }
            set { _KeyWords = value; }
        }

        private ObjectId _FilePermisionId;
        [Required]
        [Display(Name = "دسترسی به فایل")]
        public ObjectId FilePermisionId
        {
            get { return _FilePermisionId; }
            set { _FilePermisionId = value; }
        }

        private int _ViewCount;
        [Display(Name = "تعداد بازدید")]
        public int ViewCount
        {
            get { return _ViewCount; }
            set { _ViewCount = value; }
        }

        private string _CreatedDate;
        [Display(Name = "تاریخ ایجاد")]
        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;
        [Display(Name = "تاریخ به روز رسانی")]
        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }


        private int _Price;

        [Display(Name = "قیمت")]
        public int Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        #endregion Properties

        #region Events

        public List<Articles> LoadDataByObject()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            var article = collection.Find((Builders<Articles>.Filter.Eq("_id", Id)) &
                (Builders<Articles>.Filter.Eq("IsDeleted", 0))).ToList();
            for (int i = 0; i < article.Count; i++)
            {
                if ((article[i]).Author == null)
                {
                    article.RemoveAt(i);
                    i = i - 1;
                }
            }
            return article;
        }

        internal List<Articles> SearchArticle(string searchword)
        {
            List<Articles> searchResult = new List<Articles>();
            FilterDefinition<Articles> filter1 = Builders<Articles>.Filter.Text(searchword);
            var res = MongoConnection.instance.GetCollection<Articles>("Articles").FindAsync(filter1);
            searchResult = res.Result.ToList();

            return searchResult;
        }

        public List<Articles> SearchText(string searchword, int pageskip, int limitrecords, out long Total)
        {
            int skiprecord = 0;
            if (pageskip == 1)
            {
                skiprecord = 0;
            }
            else
            {
                skiprecord = (pageskip - 1) * 10;
            }

            FilterDefinition<Articles> filter1 = Builders<Articles>.Filter.Regex(x => x.Title, new BsonRegularExpression(searchword, "i")) |
                Builders<Articles>.Filter.Regex(x => x.Description, new BsonRegularExpression(searchword, "i")) |
                Builders<Articles>.Filter.Regex(x => x.FileName, new BsonRegularExpression(searchword, "i")) |
                Builders<Articles>.Filter.Regex(x => x.Summery, new BsonRegularExpression(searchword, "i"));
            Total = MongoConnection.instance.GetCollection<Articles>("Articles").Count(filter1);
            return MongoConnection.instance.GetCollection<Articles>("Articles").Find(filter1).Limit(limitrecords).Skip(skiprecord).ToList();
        }

        public List<Articles> SearchArticleNumber(long searcharticleNumber, int pageskip, int limitrecords, out long Total)
        {
            int skiprecord = 0;
            if (pageskip == 1)
            {
                skiprecord = 0;
            }
            else
            {
                skiprecord = (pageskip - 1) * 10;
            }

            //FilterDefinition<Articles> filter1 = Builders<Articles>.Filter.Regex(x => x.ArticleNumber, searcharticleNumber.ToString());
            //Total = MongoConnection.instance.GetCollection<Articles>("Articles").Count(filter1);
            //return MongoConnection.instance.GetCollection<Articles>("Articles").Find(filter1).Limit(limitrecords).Skip(skiprecord).ToList();
            List<Articles> list = MongoConnection.instance.GetCollection<Articles>("Articles").Find((Builders<Articles>.Filter.Eq("ArticleNumber", searcharticleNumber))).ToList();
            Total = list.Count;
            return list;
        }

        public List<Articles> SearchFullText(string searchword)
        {
            FilterDefinition<Articles> filter1 = Builders<Articles>.Filter.Text(searchword);
            List<Articles> res = FindNodes(filter1);
            return res;
        }

        List<Articles> FindNodes(FilterDefinition<Articles> filters)
        {
            List<Articles> result = null;
            try
            {
                // var res = MongoDbLayer.instance.GetCollection<Articles>().FindAsync(filters);
                var res = MongoConnection.instance.GetCollection<Articles>("Articles").FindAsync(filters);
                result = res.Result.ToList();
            }
            catch (Exception e)
            {
            }
            return result;
        }

        AdvanceSearchResult FindNodes(FilterDefinition<Articles> filterArticleType, FilterDefinition<Articles> filterArticleTime,
            FilterDefinition<Stores> filterStoreType, FilterDefinition<Stores> filterStoreTime, string sortorder)
        {
            AdvanceSearchResult result = new AdvanceSearchResult();
            Task<IAsyncCursor<Stores>> resStores = null;
            Task<IAsyncCursor<Articles>> resArticles = null;

            try
            {
                if (filterStoreType != null)
                {
                    if (filterStoreTime != null)
                        resStores = MongoConnection.instance.GetCollection<Stores>("Stores").FindAsync(filterStoreType & filterStoreTime);
                    else
                        resStores = MongoConnection.instance.GetCollection<Stores>("Stores").FindAsync(filterStoreType);
                    if (sortorder == "des")
                    {
                        result.StoreResult = resStores.Result.ToList().OrderBy(x => x.CreatedDate).ToList();
                    }
                    else
                    {
                        result.StoreResult = resStores.Result.ToList().OrderByDescending(x => x.CreatedDate).ToList();
                    }
                }
                if (filterArticleType != null)
                {

                    if (filterArticleTime != null)
                        resArticles = MongoConnection.instance.GetCollection<Articles>("Articles").FindAsync(filterArticleType & filterArticleTime);
                    else
                        resArticles = MongoConnection.instance.GetCollection<Articles>("Articles").FindAsync(filterArticleType);
                    if (sortorder == "dec")
                    {
                        result.ArticlesResult = resArticles.Result.ToList().OrderByDescending(x => x.CreatedDate).ToList();
                    }
                    else
                    {
                        result.ArticlesResult = resArticles.Result.ToList().OrderBy(x => x.CreatedDate).ToList();
                    }
                }
            }
            catch (Exception e)
            {
            }
            return result;
        }


        internal List<Articles> AdvanceSearchArticle(SelectedSearchItems selectedItemsForSearch)
        {
            List<Articles> searchResult = new List<Articles>();
            FilterDefinition<Articles> filter1 = Builders<Articles>.Filter.Text(selectedItemsForSearch.SearchWord);
            var res = MongoConnection.instance.GetCollection<Articles>("Articles").FindAsync(filter1);
            searchResult = res.Result.ToList();

            return searchResult;
        }

        internal List<Articles> AdvanceSearchArticle(long articlenumber)
        {
            List<Articles> searchResult = new List<Articles>();

            return MongoConnection.instance.GetCollection<Articles>("Articles")
                .Find((Builders<Articles>.Filter.Eq("ArticleNumber", articlenumber))).ToList();


        }

        public const string articlkey = "articlkey";
        internal AdvanceSearchResult AdvanceSearchArticle(SelectedSearchItems model, bool exactword)
        {
            List<FilterDefinition<Articles>> filterlist = new List<FilterDefinition<Articles>>();
            FilterDefinition<Articles> filter_articleType = null;
            FilterDefinition<Stores> filter_storeType = null;
            FilterDefinition<Stores> filter_storeSort = null;
            FilterDefinition<Articles> filter_articleSort = null;

            DateTime startdate = DateTime.Now;
            DateTime enddate = DateTime.Now;
            DateTime now = DateTime.Now;

            if (exactword)
            {
                model.SearchWord = "^" + model.SearchWord + "$";
            }
            switch (model.SearchByTypeSelected)
            {
                case "article":
                    {
                        filter_articleType = Builders<Articles>.Filter.Regex(x => x.Title, new BsonRegularExpression(model.SearchWord, "i")) |
                            Builders<Articles>.Filter.Regex(x => x.FileName, new BsonRegularExpression(model.SearchWord, "i")) |
                            Builders<Articles>.Filter.Regex(x => x.Summery, new BsonRegularExpression(model.SearchWord, "i")) |
                           Builders<Articles>.Filter.Regex(x => x.Description, new BsonRegularExpression(model.SearchWord, "i"));
                    }
                    break;
                case articlkey:
                    {
                        filter_articleType = Builders<Articles>.Filter.Regex(x => x.Title, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Articles>.Filter.Regex(x => x.Description, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Articles>.Filter.Regex(x => x.FileName, new BsonRegularExpression(model.SearchWord, "i")) |
                            Builders<Articles>.Filter.Regex(x => x.Summery, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Articles>.Filter.Regex(x => x.KeyWords, new BsonRegularExpression(model.SearchWord, "i"));
                    }
                    break;
                case "articletag":
                    {
                        filter_articleType = Builders<Articles>.Filter.Regex(x => x.Title, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Articles>.Filter.Regex(x => x.Description, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Articles>.Filter.Regex(x => x.FileName, new BsonRegularExpression(model.SearchWord, "i")) |
                            Builders<Articles>.Filter.Regex(x => x.Summery, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Articles>.Filter.Regex(x => x.Tag, new BsonRegularExpression(model.SearchWord, "i"));
                    }
                    break;
                case "store":
                    {
                        filter_storeType = Builders<Stores>.Filter.Regex(x => x.Title, new BsonRegularExpression(model.SearchWord, "i")) |
                            Builders<Stores>.Filter.Regex(x => x.Summery, new BsonRegularExpression(model.SearchWord, "i")) |
                           Builders<Stores>.Filter.Regex(x => x.Description, new BsonRegularExpression(model.SearchWord, "i"));
                    }
                    break;
                case "storekey":
                    {
                        filter_storeType = Builders<Stores>.Filter.Regex(x => x.Title, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Stores>.Filter.Regex(x => x.Description, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Stores>.Filter.Regex(x => x.Summery, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Stores>.Filter.Regex(x => x.KeyWords, new BsonRegularExpression(model.SearchWord, "i"));
                    }
                    break;
                case "storetag":
                    {
                        filter_storeType = Builders<Stores>.Filter.Regex(x => x.Title, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Stores>.Filter.Regex(x => x.Description, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Stores>.Filter.Regex(x => x.Summery, new BsonRegularExpression(model.SearchWord, "i")) |
                             Builders<Stores>.Filter.Regex(x => x.Tag, new BsonRegularExpression(model.SearchWord, "i"));
                    }
                    break;
            }
            switch (model.SearchTimeSelected)
            {
                case "today":
                    {
                        startdate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                        enddate = new DateTime(now.Year, now.Month, now.Day, 24, 0, 0);
                    }
                    break;

                case "yesterday":
                    {
                        startdate = now;
                        enddate = now.AddDays(-1);
                    }
                    break;
                case "week":
                    {
                        startdate = now;
                        enddate = now.AddDays(-7);
                    }
                    break;
                case "month":
                    {
                        startdate = DateTime.Now;
                        enddate = DateTime.Now.AddMonths(-1);
                    }
                    break;
                case "year":
                    {
                        startdate = DateTime.Now;
                        enddate = DateTime.Now.AddYears(-1);
                    }
                    break;

            }
            if (filter_storeType != null)
            {
                if (model.SearchTimeSelected == "all")
                    filter_storeSort = (Builders<Stores>.Filter.Lte(x => x.CreatedDate, startdate.ToString("s")));
            }
            if (filter_articleType != null)
            {
                if (model.SearchTimeSelected == "all")
                    filter_articleSort = (Builders<Articles>.Filter.Lte(x => x.CreatedDate, startdate.ToString("s")));
            }
            var res = FindNodes(filter_articleType, filter_articleSort, filter_storeType, filter_storeSort, model.SearchTimeOrderSelected);
            return res;
        }

        public List<Articles> LoadDataByUserId(ObjectId userID)
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.ArticleId);
            List<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", userID)) &
                (Builders<UserBasket>.Filter.Eq("IsDeleted", 0))).Project<UserBasket>(fields).ToList();

            IMongoCollection<Articles> collection_Articles = MongoConnection.instance.GetCollection<Articles>("Articles");

            List<Articles> article = new List<Articles>();
            ObjectId id_Articles = ObjectId.Empty;
            if (usersBasket != null)
            {
                if (usersBasket.Count > 0)
                {
                    if (usersBasket[0].ArticleId != null)
                    {
                        for (int i = 0; i < usersBasket[0].ArticleId.Count; i++)
                        {
                            id_Articles = usersBasket[0].ArticleId[i];
                            article.Add(collection_Articles.Find((Builders<Articles>.Filter.Eq("_id", id_Articles))).ToList()[0]);
                        }
                    }
                }
            }
            return article;
        }

        public static Articles LoadDataById(ObjectId id)
        {
            return MongoConnection.instance.GetCollection<Articles>("Articles").Find(Builders<Articles>.Filter.Eq("IsDeleted", 0) & Builders<Articles>.Filter.Eq("_id", id)).FirstOrDefault();
        }
        public static List<Articles> LoadArticlesByGroupId(ObjectId groupId)
        {
            List<Articles> finalArticle = new List<Articles>();
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            IMongoCollection<ArticleGroups> collectionGroups = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            var slideShowGroup = collectionGroups.Find(Builders<ArticleGroups>.Filter.Eq("GroupId", groupId)).ToList();
            foreach (var group in slideShowGroup)
            {
                var artic = LoadDataById(group.ArticleId);
                if (artic != null)
                {
                    finalArticle.Add(artic);
                }
            }
            return finalArticle;
        }

        public static List<Articles> LoadAllArticlesFromToday()
        {
            List<Articles> finalArticle = new List<Articles>();
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            var article = collection.Find((Builders<Articles>.Filter.Eq("IsDeleted", 0)) & (Builders<Articles>.Filter.Ne("_id", ObjectId.Empty))
                & (Builders<Articles>.Filter.Lte("CreatedDate", DateTime.Now.ToString("s")))).Sort(Builders<Articles>.Sort.Descending("CreatedDate")).ToList();

            return article;
        }

        public static long GetNumberOfArticles()
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<Articles>("Articles")
                .Count((Builders<Articles>.Filter.Eq("IsDeleted", 0)) &
                (Builders<Articles>.Filter.Ne("_id", ObjectId.Empty)) &
                (Builders<Articles>.Filter.Lte(x => x.CreatedDate, DateTime.Now.ToString("s")))
                );

            return count;
        }

        public static long GetNumberOfDeletedArticles()
        {
            long count = 0;
            DateTime setdate = new DateTime(2019, 03, 01);
            count = MongoConnection.instance.GetCollection<Articles>("Articles")
                .Count((Builders<Articles>.Filter.Eq("IsDeleted", 0)) &
                (Builders<Articles>.Filter.Ne("_id", ObjectId.Empty)) &
                (Builders<Articles>.Filter.Regex(x => x.FileAddress, new BsonRegularExpression("/^D:/i"))) &
                (Builders<Articles>.Filter.Lte(x => x.CreatedDate, DateTime.Now.ToString("s"))) &
                (Builders<Articles>.Filter.Lte(x => x.UpdateDate, setdate.ToString("s"))));
            return count;
        }
        public static List<Articles> LoadAllDeletedArticlesFromTodayByPageNumber(int pageskip)
        {
            int limitrecords = 10;
            int skiprecord = 0;
            if (pageskip == 1)
            {
                skiprecord = 0;
            }
            else
            {
                skiprecord = (pageskip - 1) * 10;
            }
            List<Articles> finalArticle = new List<Articles>();
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");

            //var article = collection.Find(Builders<Articles>.Filter.And(Builders<Articles>.Filter.Eq("IsDeleted", 0),
            //    Builders<Articles>.Filter.Regex(x => x.FileAddress, new BsonRegularExpression("/^D:/i")))).Sort(Builders<Articles>.Sort.Descending("CreatedDate")).Limit(limitrecords).Skip(skiprecord).ToList();
            var article = collection.Find(Builders<Articles>.Filter.And(Builders<Articles>.Filter.Eq("IsDeleted", 0),
               Builders<Articles>.Filter.Regex(x => x.FileAddress, new BsonRegularExpression("/^D:/i")))).Sort(Builders<Articles>.Sort.Descending("CreatedDate")).ToList();

            //DateTime setdate = new DateTime(2019, 03, 01);
            //for (int i = 0; i < article.Count; i++)
            //{
            //    if ((article[i]).Author != null)
            //    {
            //        if (DateTime.Parse((article[i]).CreatedDate) <= DateTime.Now &&
            //            DateTime.Parse((article[i]).UpdateDate) < DateTime.Parse(setdate.ToString("s")))
            //        {
            //            finalArticle.Add(article[i]);
            //        }
            //    }
            //}
            //return finalArticle;
            // return article; 
            int counter = 0;
            for (int i = 0; i < article.Count; i++)
            {
                if ((article[i]).Author != null)
                {
                    if (DateTime.Parse((article[i]).CreatedDate) <= DateTime.Now)
                    {
                        string[] fileaddress = article[i].FileAddress.Split(',');
                        for (int j = 0; j < fileaddress.Length; j++)
                        {
                            if (!System.IO.File.Exists(fileaddress[j]))
                            {
                                if (counter == skiprecord || (counter>skiprecord && counter < (skiprecord + limitrecords)))
                                {
                                    finalArticle.Add(article[i]);
                                }
                                counter = counter + 1;
                                break;
                            }
                        }
                    }
                }
            }

            return finalArticle;
        }

        public static long DeletedArticleCount()
        {

            long count = 0;
            //count = MongoConnection.instance.GetCollection<Articles>("Articles")
            //    .Count((Builders<Articles>.Filter.Eq("IsDeleted", 0)) &
            //    (Builders<Articles>.Filter.Ne("_id", ObjectId.Empty)) &
            //    (Builders<Articles>.Filter.Regex(x => x.FileAddress, new BsonRegularExpression("/^D:/i")))
            //    );
            List<Articles> finalArticle = new List<Articles>();
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");

            var article = collection.Find(Builders<Articles>.Filter.And(Builders<Articles>.Filter.Eq("IsDeleted", 0),
                Builders<Articles>.Filter.Regex(x => x.FileAddress, new BsonRegularExpression("/^D:/i")))).Sort(Builders<Articles>.Sort.Descending("CreatedDate")).ToList();


            for (int j = 0; j < article.Count; j++)
            {
                string[] fileaddress = article[j].FileAddress.Split(',');
                if (DateTime.Parse((article[j]).CreatedDate) <= DateTime.Now)
                {
                    for (int i = 0; i < fileaddress.Length; i++)
                    {
                        if (!System.IO.File.Exists(fileaddress[i]))
                        {
                            count = count + 1;
                            break;
                        }
                    }
                }
            }
            return count;
        }

        public static List<Articles> LoadAllArticlesFromTodayByPageNumber(int pageskip)
        {
            int limitrecords = 10;
            int skiprecord = 0;
            if (pageskip == 1)
            {
                skiprecord = 0;
            }
            else
            {
                skiprecord = (pageskip - 1) * 10;
            }
            List<Articles> finalArticle = new List<Articles>();
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");

            var article = collection.Find((Builders<Articles>.Filter.Eq("IsDeleted", 0))).Sort(Builders<Articles>.Sort.Descending("CreatedDate")).Limit(limitrecords).Skip(skiprecord).ToList();
            for (int i = 0; i < article.Count; i++)
            {
                if ((article[i]).Author != null)
                {
                    if (DateTime.Parse((article[i]).CreatedDate) <= DateTime.Now)
                    {
                        finalArticle.Add(article[i]);
                    }
                }
            }
            return finalArticle;
        }

        public static List<Articles> LoadDataAll()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            var article = collection.Find((Builders<Articles>.Filter.Eq("IsDeleted", 0))).Sort(Builders<Articles>.Sort.Descending("CreatedDate")).ToList();

            for (int i = 0; i < article.Count; i++)
            {
                if ((article[i]).Author == null)
                {
                    article.RemoveAt(i);
                    i = i - 1;
                }
            }
            return article;
        }

        public Articles GetLastArticleNumber()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            Articles article = collection.Find(x => true).SortByDescending(d => d.ArticleNumber).Limit(1).FirstOrDefault();
            if (article == null)
            {
                article = new Articles();
            }
            return article;
        }

        public ObjectId Insert()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            if (Author != null)
                collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            // var update = Builders<Articles>.Update.Set("Articles", ViewCount);
            collection.ReplaceOne(Builders<Articles>.Filter.Eq("_id", Id), this);
        }

        public void UpdateViewCount()
        {
            if (this.Id != ObjectId.Empty)
                if (this.Title != "")
                {
                    IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
                    collection.ReplaceOne(Builders<Articles>.Filter.Eq("_id", Id), this);
                }
        }

        public void DeleteById()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            var update = Builders<Articles>.Update.Set("IsDeleted", 1);
            collection.UpdateOne(Builders<Articles>.Filter.Eq("_id", Id), update);
        }

        public void Delete()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            collection.DeleteOne(Builders<Articles>.Filter.Eq("Id", Id));
        }

        public static Articles LoadArticleByNumber()
        {
            return MongoConnection.instance.GetCollection<Articles>("Articles")
                .Find((Builders<Articles>.Filter.Ne("_id", ObjectId.Empty))).ToList().OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }

        public static Articles LoadArticleByNumber(long articleNumber)
        {
            return MongoConnection.instance.GetCollection<Articles>("Articles")
                .Find((Builders<Articles>.Filter.Eq("ArticleNumber", articleNumber))).ToList().OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }

        internal List<Articles> LoadRelatedArticles()
        {
            IMongoCollection<ArticleGroups> collection_ArticleGroup = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            IMongoCollection<Articles> collection_Articles = MongoConnection.instance.GetCollection<Articles>("Articles");

            List<ArticleGroups> curentarticleGroups = collection_ArticleGroup.Find(Builders<ArticleGroups>.Filter.Eq(x => x.ArticleId, Id)).ToList();

            List<ArticleGroups> articlegroup = new List<ArticleGroups>();
            List<ObjectId> list_GroupId = new List<ObjectId>();
            for (int i = 0; i < curentarticleGroups.Count; i++)
            {
                list_GroupId.Add(curentarticleGroups[i].GroupId);
            }

            articlegroup = collection_ArticleGroup.Find(Builders<ArticleGroups>.Filter.In("GroupId", list_GroupId)).ToList();
            List<ObjectId> list_articleswithgroupsid = new List<ObjectId>();
            for (int i = 0; i < articlegroup.Count; i++)
            {
                list_articleswithgroupsid.Add(articlegroup[i].ArticleId);
            }
             
            List<Articles> result = collection_Articles.Find(Builders<Articles>.Filter.In("Id", list_articleswithgroupsid) &
                Builders<Articles>.Filter.Ne("ArticleNumber", 0)).
                Sort(Builders<Articles>.Sort.Descending("CreatedDate")).Limit(12).ToList();

            return result;
        }

        #endregion Events
    }

    public class linkInfo
    {

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        private string _Type;

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _Size;

        public string Size
        {
            get { return _Size; }
            set { _Size = value; }
        }



        private string _Link;

        public string Link
        {
            get { return _Link; }
            set { _Link = value; }
        }

    }

    public class SearchArticle
    {
        #region Properties

        private string _SearchWord;
        //[StringLength(100, ErrorMessage = " {0} باید لااقل {3} کارکتر داشته باشد.", MinimumLength = 3)]
        [Required]
        [Display(Name = "کلمه ی جستجو")]
        public string SearchWord
        {
            get { return _SearchWord; }
            set { _SearchWord = value; }
        }

        #endregion Properties
    }

    public class ArticleGalary : MongoDBModel<ArticleGalary>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        //private ObjectId _ImageId;

        //public ObjectId ImageId
        //{
        //    get { return _ImageId; }
        //    set { _ImageId = value; }
        //}

        private string _ImagePath;

        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }



        #endregion Properties

        #region Events

        public List<ArticleGalary> LoadDataByObject()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            var article = collection.Find((Builders<ArticleGalary>.Filter.Eq("_id", Id))).ToList();
            return article;
        }

        public List<ArticleGalary> LoadDataByArticleId()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            var article = collection.Find((Builders<ArticleGalary>.Filter.Eq("ArticleId", ArticleId))).ToList();
            return article;
        }

        public ArticleGalary LoadByArticleId(ObjectId aId)
        {
            return MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary")
               .Find((Builders<ArticleGalary>.Filter.Eq("ArticleId", aId))).FirstOrDefault();
        }

        public List<ArticleGalary> LoadDataAll()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            var articleGalary = collection.Find((Builders<ArticleGalary>.Filter.Empty)).ToList();
            return articleGalary;
        }

        public ObjectId Insert()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            collection.ReplaceOne(Builders<ArticleGalary>.Filter.Eq("_id", Id), this);
        }


        public void UpdateViewCount()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            collection.ReplaceOne(Builders<ArticleGalary>.Filter.Eq("_id", Id), this);
        }


        public void DeleteById()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            collection.DeleteOne(Builders<ArticleGalary>.Filter.Eq("_id", Id));
        }


        public void DeleteByArticleId()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            collection.DeleteMany(Builders<ArticleGalary>.Filter.Eq("ArticleId", ArticleId));
        }


        public void Delete()
        {
            IMongoCollection<ArticleGalary> collection = MongoConnection.instance.GetCollection<ArticleGalary>("ArticleGalary");
            collection.DeleteOne(Builders<ArticleGalary>.Filter.Eq("Id", Id));
        }


        public static ObjectId UploadFile(GridFSBucket fs, string filename, string path)
        {
            using (var s = File.OpenRead(path))
            {
                var t = Task.Run<ObjectId>(() =>
                {
                    return fs.UploadFromStreamAsync(filename, s);
                });
                return t.Result;
            }

        }

        public static byte[] DownloadFile(GridFSBucket fs, ObjectId id)//, string fileName)
        {
            byte[] x = fs.DownloadAsBytes(id);
            return x;
        }


        #endregion Events        
    }


    public class ArticleComplate
    {
        #region Properties
        private Articles _Art;

        public Articles Art
        {
            get { return _Art; }
            set { _Art = value; }
        }

        private List<SelectListItem> _GroupData;

        public List<SelectListItem> GroupData
        {
            get { return _GroupData; }
            set { _GroupData = value; }
        }

        public IEnumerable<SelectListItem> _RolesList;

        public IEnumerable<SelectListItem> RolesList
        {
            get { return _RolesList; }
            set { _RolesList = value; }
        }

        private LikeDislikeArticle _LikeDislike;

        public LikeDislikeArticle LikeDislike
        {
            get { return _LikeDislike; }
            set { _LikeDislike = value; }
        }

        private List<ArticleGalary> _Galary;

        public List<ArticleGalary> Galary
        {
            get { return _Galary; }
            set { _Galary = value; }
        }


        #endregion Properties
    }

    public class ArticlePermisions : MongoDBModel<ArticlePermisions>
    {
        #region Properties

        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private Object _RoleId;

        public Object RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }

        #endregion Properties

        #region Events
        public List<ArticlePermisions> LoadDataByObject()
        {
            throw new NotImplementedException();
        }

        public List<ArticlePermisions> LoadDataAll()
        {
            throw new NotImplementedException();
        }

        public ObjectId Insert()
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void DeleteById()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        #endregion Events 
    }

    public class Group : MongoDBModel<Group>
    {
        #region properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _ImageName;

        public string ImageName
        {
            get { return _ImageName; }
            set { _ImageName = value; }
        }


        private ObjectId _ParentId;

        public ObjectId ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        private int _GroupOrder;

        public int GroupOrder
        {
            get { return _GroupOrder; }
            set { _GroupOrder = value; }
        }


        #endregion Properties

        #region Events 

        public List<Group> LoadDataByObject()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            var article = collection.Find((Builders<Group>.Filter.Eq("_id", Id))).ToList();
            return article;
        }

        public List<Group> LoadParentData()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            var article = collection.Find((Builders<Group>.Filter.Eq("ParentId", ParentId))).ToList();
            return article;
        }

        public List<Group> LoadByParent(string parentId)
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            return collection.Find((Builders<Group>.Filter.Eq("ParentId", ParentId))).ToList().OrderBy(x => x.GroupOrder).ToList();
        }

        public List<Group> LoadDataAll()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            var group = collection.Find((Builders<Group>.Filter.Empty)).ToList().OrderBy(x => x.GroupOrder).ToList();

            return group;
        }

        public ObjectId Insert()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            List<Group> groupbyorder = collection.Find((Builders<Group>.Filter.Empty)).Sort(Builders<Group>.Sort.Descending("GroupOrder")).ToList();

            groupbyorder = groupbyorder.OrderByDescending(x => x.GroupOrder).ToList();
            int order = groupbyorder[0].GroupOrder;

            GroupOrder = order + 1;
            Id = ObjectId.Empty;
            collection = MongoConnection.instance.GetCollection<Group>("Group");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            //var update = Builders<Group>.Update.Set("Name", Name).Set("UpdateDate", UpdateDate).Set("ParentId", ParentId);
            //collection.UpdateOne(Builders<Group>.Filter.Eq("_id", Id), update);
            collection.ReplaceOne(Builders<Group>.Filter.Eq("_id", Id), this);
        }

        public void UpdateName()
        {
            UpdateDefinition<Group> update = Builders<Group>.Update.Set("UpdateDate", UpdateDate);
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            if (ImageName != null)
                update = update.Set("ImageName", ImageName);
            if (Name != null)
                update = update.Set("Name", Name);//= Builders<Group>.Update.Set("UpdateDate", UpdateDate).Set("ImageName", ImageName);

            collection.UpdateOne(Builders<Group>.Filter.Eq("_id", Id), update);
        }

        public void UpdateOrderBy(int order, bool isParent)
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            List<Group> groupbyorder = new List<Group>();
            if (isParent)
            {
                groupbyorder = collection.Find((Builders<Group>.Filter.Eq("ParentId", ObjectId.Empty))).ToList();
            }
            else
            {
                groupbyorder = collection.Find((Builders<Group>.Filter.Ne("ParentId", ObjectId.Empty))).ToList();
            }

            Group myGroup = groupbyorder.Find(x => x.Id == Id);

            Group nextGroup = new Group();
            Group prevGroup = new Group();
            groupbyorder = groupbyorder.OrderByDescending(x => x.GroupOrder).ToList();

            if (order < 0)
            {
                //group replace with next item
                nextGroup = groupbyorder.Find(x => x.GroupOrder < myGroup.GroupOrder);
                //Me
                if (nextGroup != null)
                {
                    collection = MongoConnection.instance.GetCollection<Group>("Group");
                    var update = Builders<Group>.Update.Set("GroupOrder", nextGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<Group>.Filter.Eq("_id", Id), update);
                    //Next
                    collection = MongoConnection.instance.GetCollection<Group>("Group");
                    update = Builders<Group>.Update.Set("GroupOrder", myGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<Group>.Filter.Eq("_id", nextGroup.Id), update);
                }
            }
            else
            {
                //group replace with previus item 
                prevGroup = groupbyorder.LastOrDefault(x => x.GroupOrder > myGroup.GroupOrder);
                //Me
                if (prevGroup != null)
                {
                    collection = MongoConnection.instance.GetCollection<Group>("Group");
                    var update = Builders<Group>.Update.Set("GroupOrder", prevGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<Group>.Filter.Eq("_id", myGroup.Id), update);
                    //Previus
                    collection = MongoConnection.instance.GetCollection<Group>("Group");
                    update = Builders<Group>.Update.Set("GroupOrder", myGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<Group>.Filter.Eq("_id", prevGroup.Id), update);
                }
            }

        }

        public void DeleteById()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            collection.DeleteOne(Builders<Group>.Filter.Eq("_id", Id));
            if (ParentId == ObjectId.Empty)
                collection.DeleteOne(Builders<Group>.Filter.Eq("ParentId", Id));
        }

        public void DeleteWithAllChildsById()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            collection.DeleteMany(Builders<Group>.Filter.Eq("ParentId", Id));
            collection.DeleteOne(Builders<Group>.Filter.Eq("_id", Id));
        }


        public void Delete()
        {
            IMongoCollection<Group> collection = MongoConnection.instance.GetCollection<Group>("Group");
            collection.DeleteOne(Builders<Group>.Filter.Eq("_id", Id));
        }


        #endregion Events
    }

    public class SelectedSearchItems
    {
        private string _SearchWord;

        public string SearchWord
        {
            get { return _SearchWord; }
            set { _SearchWord = value; }
        }

        private long _ArticleNumber;

        public long ArticleNumber
        {
            get { return _ArticleNumber; }
            set { _ArticleNumber = value; }
        }

        private string _GroupId;

        public string GroupId
        {
            get { return _GroupId; }
            set { _GroupId = value; }
        }

        private List<SelectListItem> _GroupItems;

        public List<SelectListItem> GroupItems
        {
            get { return _GroupItems; }
            set { _GroupItems = value; }
        }

        private string _SearchTimeSelected;

        public string SearchTimeSelected
        {
            get { return _SearchTimeSelected; }
            set { _SearchTimeSelected = value; }
        }


        private List<SelectListItem> _SearchTime;

        public List<SelectListItem> SearchTime
        {
            get { return _SearchTime; }
            set { _SearchTime = value; }
        }


        private string _SearchTimeOrderSelected;
        public string SearchTimeOrderSelected
        {
            get { return _SearchTimeOrderSelected; }
            set { _SearchTimeOrderSelected = value; }
        }


        private List<SelectListItem> _SearchTimeOrder;

        public List<SelectListItem> SearchTimeOrder
        {
            get { return _SearchTimeOrder; }
            set { _SearchTimeOrder = value; }
        }


        private string _SearchByTypeSelected;
        public string SearchByTypeSelected
        {
            get { return _SearchByTypeSelected; }
            set { _SearchByTypeSelected = value; }
        }


        private List<SelectListItem> _SearchByType;

        public List<SelectListItem> SearchByType
        {
            get { return _SearchByType; }
            set { _SearchByType = value; }
        }


        //private string _SearchByTypeOrderSelected;
        //public string SearchByTypeOrderSelected
        //{
        //    get { return _SearchByTypeOrderSelected; }
        //    set { _SearchByTypeOrderSelected = value; }
        //}

        //private List<SelectListItem> _SearchByTypeOrder;

        //public List<SelectListItem> SearchByTypeOrder
        //{
        //    get { return _SearchByTypeOrder; }
        //    set { _SearchByTypeOrder = value; }
        //}

    }

    public class AdvanceSearchResult
    {
        private List<Articles> _ArticlesResult;

        public List<Articles> ArticlesResult
        {
            get { return _ArticlesResult; }
            set { _ArticlesResult = value; }
        }

        private List<Stores> _StoreResult;

        public List<Stores> StoreResult
        {
            get { return _StoreResult; }
            set { _StoreResult = value; }
        }


    }

    public class ArticleGroups : MongoDBModel<ArticleGroups>
    {
        #region Properties
        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private ObjectId _GroupId;

        public ObjectId GroupId
        {
            get { return _GroupId; }
            set { _GroupId = value; }
        }

        private string _GroupName;

        public string GroupName
        {
            get { return _GroupName; }
            set { _GroupName = value; }
        }

        #endregion Properties

        #region Events 

        public List<ArticleGroups> LoadDataByObject()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            var article = collection.Find((Builders<ArticleGroups>.Filter.Eq("GroupId", GroupId))).ToList();
            return article;
        }

        public List<ArticleGroups> LoadDataByArticleId()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            var article = collection.Find((Builders<ArticleGroups>.Filter.Eq("ArticleId", ArticleId))).ToList();
            return article;
        }

        public List<ArticleGroups> LoadDataByArticleIdExceptSlideShow()
        {
            string groupid1 = "5a526a33e290b819dc102a35";
            ObjectId groupid = new ObjectId();
            groupid = ObjectId.Parse(groupid1);
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            var article = collection.Find((Builders<ArticleGroups>.Filter.Eq("ArticleId", ArticleId))
                & (Builders<ArticleGroups>.Filter.Ne("GroupId", groupid))
                ).ToList();
            return article;
        }

        public List<Group> LoadDataAllGroupsWithArticle()
        {
            IMongoCollection<Group> collection1 = MongoConnection.instance.GetCollection<Group>("Group");
            IMongoCollection<ArticleGroups> collection2 = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");

            List<Group> groupList = collection1.Find((Builders<Group>.Filter.Empty)).ToList();

            List<ObjectId> articlegroup = collection2.Distinct(new StringFieldDefinition<ArticleGroups, ObjectId>("GroupId"), FilterDefinition<ArticleGroups>.Empty).ToList();

            groupList.FindAll(x => x.ParentId != ObjectId.Empty).ToList().OrderBy(x => x.GroupOrder).ToList();
            groupList.OrderBy(x => x.GroupOrder);

            var query = from X in groupList
                        join Y in articlegroup
                             on X.Id equals Y
                        orderby X.GroupOrder
                        select X;

            List<Group> finalList = query.ToList<Group>();
            int count = finalList.Count;
            for (int i = 0; i < count; i++)
            {
                if (finalList[i].ParentId != ObjectId.Empty)
                {
                    var z = groupList.Find(x => x.Id == finalList[i].ParentId);
                    if (!finalList.Contains(z))
                        finalList.Add(z);
                }
            }

            return finalList;
        }

        public List<ArticleGroups> LoadArticlesByIdAndArticleId()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            var article = collection.Find(Builders<ArticleGroups>.Filter.Eq("GroupId", GroupId)).ToList();
            return article;
        }


        internal List<ArticleGroups> LoadDataByGroupId()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            IMongoCollection<Group> collection_Group = MongoConnection.instance.GetCollection<Group>("Group");

            List<Group> List_Group = collection_Group.Find(Builders<Group>.Filter.Eq("Id", GroupId) |
                Builders<Group>.Filter.Eq("ParentId", GroupId)).ToList();

            List<ArticleGroups> list_Articles = collection.Find(Builders<ArticleGroups>.Filter.Empty).ToList();
            List<ArticleGroups> final_List = new List<ArticleGroups>();
            bool haveArticle = false;
            foreach (Group item_group in List_Group)
            {
                haveArticle = false;
                foreach (ArticleGroups item in list_Articles)
                {
                    if (item_group.Id == item.GroupId)
                    {
                        haveArticle = true;
                        final_List.Add(item);
                    }
                }
                if (!haveArticle)
                {

                }
            }

            return final_List;
        }


        public List<ArticleGroups> LoadDataAll()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            List<ArticleGroups> group = collection.Find((Builders<ArticleGroups>.Filter.Empty)).ToList();
            return group;
        }


        public ObjectId Insert()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            //var update = Builders<Group>.Update.Set("Name", Name).Set("UpdateDate", UpdateDate).Set("ParentId", ParentId);
            //collection.UpdateOne(Builders<Group>.Filter.Eq("_id", Id), update);
            collection.ReplaceOne(Builders<ArticleGroups>.Filter.Eq("_id", Id), this);
        }

        public void DeleteById()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            collection.DeleteOne(Builders<ArticleGroups>.Filter.Eq("_id", Id));
        }

        public void DeleteWithArticleId()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            collection.DeleteMany(Builders<ArticleGroups>.Filter.Eq("ArticleId", ArticleId));
            collection.DeleteOne(Builders<ArticleGroups>.Filter.Eq("_id", Id));
        }


        public void Delete()
        {
            IMongoCollection<ArticleGroups> collection = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroups");
            collection.DeleteOne(Builders<ArticleGroups>.Filter.Eq("_id", Id));
        }



        #endregion Events
    }

    public class GroupForDropDown
    {
        #region Properties

        private Group _Group;

        public Group Group
        {
            get { return _Group; }
            set { _Group = value; }
        }

        private List<SelectListItem> _Groups;

        public List<SelectListItem> Groups
        {
            get { return _Groups; }
            set { _Groups = value; }
        }

        private string _ChangeOrder;

        public string ChangeOrder
        {
            get { return _ChangeOrder; }
            set { _ChangeOrder = value; }
        }


        #endregion Properties
    }


    public class LikeDislikeArticle : MongoDBModel<LikeDislikeArticle>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }


        private int _LikeCount;

        public int LikeCount
        {
            get { return _LikeCount; }
            set { _LikeCount = value; }
        }

        private int _DislikeCount;

        public int DislikeCount
        {
            get { return _DislikeCount; }
            set { _DislikeCount = value; }
        }

        private string _UserIp;

        public string UserIp
        {
            get { return _UserIp; }
            set { _UserIp = value; }
        }


        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        #endregion Properties

        #region Events

        public List<LikeDislikeArticle> LoadDataByObject()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            var article = collection.Find((Builders<LikeDislikeArticle>.Filter.Eq("_id", Id))).ToList();
            return article;
        }

        public List<LikeDislikeArticle> LoadDataByArticleId(ObjectId aId)
        {
            return MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle")
                .Find((Builders<LikeDislikeArticle>.Filter.Eq("ArticleId", aId))).ToList();
        }

        public List<LikeDislikeArticle> LoadDataByArticleId()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            var article = collection.Find((Builders<LikeDislikeArticle>.Filter.Eq("ArticleId", ArticleId))).ToList();
            return article;
        }


        public int FavoriteArticlesByUserIpCount()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            return collection.Find((Builders<LikeDislikeArticle>.Filter.Eq("UserIp", UserIp))).ToList().Count;
        }

        public List<LikeDislikeArticle> LoadFavoriteArticlesByUserIp()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            List<LikeDislikeArticle> list = collection.Find((Builders<LikeDislikeArticle>.Filter.Eq("UserIp", UserIp))).ToList();
            return list;
        }

        public List<LikeDislikeArticle> LoadDataAll()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            var user = collection.Find((Builders<LikeDislikeArticle>.Filter.Empty)).ToList();
            return user;
        }

        public ObjectId Insert()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            collection.ReplaceOne(Builders<LikeDislikeArticle>.Filter.Eq("_id", Id), this);
        }

        public void DeleteById()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            collection.DeleteOne(Builders<LikeDislikeArticle>.Filter.Eq("_id", Id));
        }

        public void DeleteByArticleId()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            collection.DeleteMany(Builders<LikeDislikeArticle>.Filter.Eq("ArticleId", ArticleId));
        }


        public void Delete()
        {
            IMongoCollection<LikeDislikeArticle> collection = MongoConnection.instance.GetCollection<LikeDislikeArticle>("LikeDislikeArticle");
            collection.DeleteOne(Builders<LikeDislikeArticle>.Filter.Eq("_id", Id));
        }

        #endregion Events
    }

    public class ArticleComment : MongoDBModel<ArticleComment>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private ObjectId _CommentId;

        public ObjectId CommentId
        {
            get { return _CommentId; }
            set { _CommentId = value; }
        }

        private bool _IsPublished;
        [Display(Name = "منتشر شود")]
        public bool IsPublished
        {
            get { return _IsPublished; }
            set { _IsPublished = value; }
        }


        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private string _UserID;

        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _UserName;
        [Display(Name = "نام کاربر")]
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }


        private string _CommentBody;
        [AllowHtml]
        [Display(Name = "متن نظر")]
        public string CommentBody
        {
            get { return _CommentBody; }
            set { _CommentBody = value; }
        }

        private string _Answer;
        [AllowHtml]
        [Display(Name = "پاسخ به نظر")]
        public string Answer
        {
            get { return _Answer; }
            set { _Answer = value; }
        }


        private string _AnswerBy;
        [Display(Name = "پاسخ توسط")]
        public string AnswerBy
        {
            get { return _AnswerBy; }
            set { _AnswerBy = value; }
        }


        private string _CreatedDate;
        [Display(Name = "تاریخ ثبت نظر")]
        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;
        [Display(Name = "تاریخ ویرایش نظر")]
        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        #endregion Properties

        #region Events 

        public List<ArticleComment> LoadDataByObject()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            var article = collection.Find((Builders<ArticleComment>.Filter.Eq("_id", Id))).ToList();

            return article;

        }


        public List<ArticleComment> LoadDataByArticleId()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            var article = collection.Find((Builders<ArticleComment>.Filter.Eq("ArticleId", ArticleId))).ToList();
            return article;
        }

        public List<ArticleComment> LoadDataByArticleIdAndUserId()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            var article = collection.Find((Builders<ArticleComment>.Filter.Eq("ArticleId", ArticleId)) &
                (Builders<ArticleComment>.Filter.Eq("UserID", UserID))).ToList();
            return article;
        }



        public List<ArticleComment> LoadDataAll()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            var comments = collection.Find((Builders<ArticleComment>.Filter.Empty)).ToList();
            return comments;
        }


        public List<RegisterViewModel> LoadCommentsWithUserInfo(ObjectId articleId)
        {
            IMongoCollection<RegisterViewModel> collection_Users = MongoConnection.instance.GetCollection<RegisterViewModel>("users");
            IMongoCollection<ArticleComment> collection_ArticleComments = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");

            var allregisterdUsers = collection_ArticleComments.Aggregate().Lookup("users", "UserId", "Id", "ArticleComment").ToList();
            return null;
        }

        public ObjectId Insert()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");

            var update = Builders<ArticleComment>.Update.Set("Answer", Answer).
                Set("AnswerBy", AnswerBy).Set("UpdateDate", UpdateDate).Set("IsPublished", IsPublished);
            collection.UpdateOne(Builders<ArticleComment>.Filter.Eq("_id", Id), update);
        }

        public void DeleteById()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            collection.DeleteOne(Builders<ArticleComment>.Filter.Eq("_id", Id));
        }

        public void DeleteByArticleId()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            collection.DeleteMany(Builders<ArticleComment>.Filter.Eq("ArticleId", ArticleId));
        }


        public void Delete()
        {
            IMongoCollection<ArticleComment> collection = MongoConnection.instance.GetCollection<ArticleComment>("ArticleComment");
            collection.DeleteOne(Builders<ArticleComment>.Filter.Eq("_id", Id));
        }


        #endregion Events
    }

    public class ArticleWithLikeDislikeAndComments
    {
        #region properties

        private Articles _Article;

        public Articles Article
        {
            get { return _Article; }
            set { _Article = value; }
        }

        private LikeDislikeArticle _LikeDislike;

        public LikeDislikeArticle LikeDislike
        {
            get { return _LikeDislike; }
            set { _LikeDislike = value; }
        }

        private ArticleComment _Comment;

        public ArticleComment Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private List<ArticleGroups> _ArticleGroupsData;

        public List<ArticleGroups> ArticleGroupsData
        {
            get { return _ArticleGroupsData; }
            set { _ArticleGroupsData = value; }
        }

        private List<ArticleComment> _ArticleComments;

        public List<ArticleComment> ArticleComments
        {
            get { return _ArticleComments; }
            set { _ArticleComments = value; }
        }


        private List<ArticleGalary> _ArticleGalary;

        public List<ArticleGalary> ArticleGalary
        {
            get { return _ArticleGalary; }
            set { _ArticleGalary = value; }
        }

        #endregion Properties
    }

    public class ArticleListComplete
    {
        #region properties

        private List<ArticleWithLikeDislikeAndComments> _ArticleList;

        public List<ArticleWithLikeDislikeAndComments> ArticleList
        {
            get { return _ArticleList; }
            set { _ArticleList = value; }
        }

        private List<StoreWithLikeDislikeAndComments> _StoreList;

        public List<StoreWithLikeDislikeAndComments> StoreList
        {
            get { return _StoreList; }
            set { _StoreList = value; }
        }

        private List<Group> _GroupList;

        public List<Group> GroupList
        {
            get { return _GroupList; }
            set { _GroupList = value; }
        }

        private List<Articles> _ArticleSlideShowList;

        public List<Articles> ArticleSlideShowList
        {
            get { return _ArticleSlideShowList; }
            set { _ArticleSlideShowList = value; }
        }


        private List<Advertizements> _AdvertizementTop;

        public List<Advertizements> AdvertizementTop
        {
            get { return _AdvertizementTop; }
            set { _AdvertizementTop = value; }
        }


        private List<Advertizements> _AdvertizementBottom;

        public List<Advertizements> AdvertizementBottom
        {
            get { return _AdvertizementBottom; }
            set { _AdvertizementBottom = value; }
        }

        private List<Group> _TreeGroup;

        public List<Group> TreeGroup
        {
            get { return _TreeGroup; }
            set { _TreeGroup = value; }
        }


        #endregion Properties
    }

}