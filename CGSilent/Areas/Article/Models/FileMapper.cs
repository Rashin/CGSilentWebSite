﻿

using System;
using System.Collections;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace CGSilent.Models
{

    public class FileMapper : MongoDBModel<FileMapper>
    {
        public ObjectId id;
        public string filePath;
        public string contentType;
        public string fileName;
        public long expireDate;

        public List<FileMapper> LoadDataByObject()
        {
            IMongoCollection<FileMapper> collection = MongoConnection.instance.GetCollection<FileMapper>("FileMapper");
            var article = collection.Find((Builders<FileMapper>.Filter.Eq("_id", id))).ToList();
            return article;
            
        }

        public List<FileMapper> LoadDataAll()
        {
            throw new NotImplementedException();
        }

        public ObjectId Insert()
        {
            IMongoCollection<FileMapper> collection = MongoConnection.instance.GetCollection<FileMapper>("FileMapper");
            collection.InsertOne(this);
            return id;
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void DeleteById()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }
    }
}