﻿using CGSilent.Models;
using System.Collections;
using System.Web.Mvc;
using MongoDB.Bson;
using System;
using System.Web;
using MongoDB.Driver.GridFS;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using Microsoft.AspNet.Identity;
using PagedList;
using AspNet.Identity.MongoDB;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Http;

namespace CGSilent.Areas.Article.Controllers
{
    public class ArticleAdminController : Controller
    {
        // GET: Article/Management
        enum ArticleimageType { Galary, Image };

        [HttpGet]
        public ActionResult Index(int? page)
        {
            // if no page was specified in the querystring, default to the first page (1)
            var pageNumber = page ?? 1;
            InsertSiteStatic(ObjectId.Empty, ObjectId.Empty, "Store");
            CreateArticleAllList(pageNumber);
            return View("ArticleList");
        }

        [HttpGet]
        private void InsertSiteStatic(ObjectId ArticleId, ObjectId storeId, string pageName)
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            UserRoles roles = new UserRoles();
            if (User.Identity.IsAuthenticated)
            {
                roles.Id = ObjectId.Parse(User.Identity.GetUserId());
                roles = roles.LoadDataByUserId();
            }
            else
            {
                roles.Id = ObjectId.Empty;
            }
            //افزودن آمار بازدید از سایت
            if (roles != null)
            {
                SiteStatics ss = new SiteStatics();
                ss.CreatedDate = DateTime.Now.ToString("s");
                ss.PageName = pageName;
                ss.ArticleId = ArticleId;
                ss.StoreId = storeId;
                if (User.Identity.IsAuthenticated)
                    ss.UserIdOrIp = User.Identity.GetUserId();
                else
                    ss.UserIdOrIp = Convert.ToString(ipEntry.AddressList[1]);

                if (roles.Roles != null)
                    for (int i = 0; i < roles.Roles.Length; i++)
                    {
                        ss.RoleName = roles.Roles[i];
                        ss.Insert();
                    }
                else
                {
                    ss.Insert();
                }
            }
        }


        [HttpGet]
        public ActionResult LoadArticlesByGroupId(ObjectId groupId)
        {
            InsertSiteStatic(ObjectId.Empty, ObjectId.Empty, "Store");
            return View("ArticleList", ArticlesByGroup(groupId));
            //return View("ArticleList", LoadAllArticles());
        }

        #region LayoutFunctions


        [ChildActionOnly]
        public ActionResult SlideShow()
        {
            Articles al = new Articles();
            string SlideId = "5a526a33e290b819dc102a35";
            return PartialView("~/Views/Shared/_SlidShow.cshtml", LoadSliderArticles(SlideId));
        }

        [ChildActionOnly]
        public ActionResult HomeArticles(int? page)
        {
            var pageNumber = page ?? 1;
            InsertSiteStatic(ObjectId.Empty, ObjectId.Empty, "Store");
            CreateArticleAllList(pageNumber);
            return PartialView("~/Views/Shared/_HomeArticles.cshtml");
        }

        [ChildActionOnly]
        public ActionResult ArticleImagesGalary(int? page)
        {
            var pageNumber = page ?? 1;
            CreateArticleAllList(pageNumber);
            return PartialView("~/Views/Shared/_ArticleImagesGalary.cshtml");
        }


        [ChildActionOnly]
        public ActionResult ArticleGroups()
        {
            Group gp = new Group();
            IList il_gp = gp.LoadDataAll();
            List<Group> list_Group = new List<Group>();
            for (int i = 0; i < il_gp.Count; i++)
            {
                list_Group.Add((Group)il_gp[i]);
            }
            return PartialView("~/Views/Shared/_ArticleGroups.cshtml", list_Group);
        }

        private List<Articles> LoadSliderArticles(string SliderId)
        {
            IList List_Article = Articles.LoadAllArticlesFromToday();

            ArticleGroups groups = new ArticleGroups();
            groups.GroupId = ObjectId.Parse(SliderId);
            IList ArticleGroups = groups.LoadDataByObject();

            List<Articles> SliderArticles = new List<Articles>();
            for (int i = 0; i < List_Article.Count; i++)
            {
                for (int j = 0; j < ArticleGroups.Count; j++)
                {
                    if (((Articles)List_Article[i]).Id == ((ArticleGroups)ArticleGroups[j]).ArticleId)
                        SliderArticles.Add((Articles)List_Article[i]);
                }
            }
            return SliderArticles;
        }

        [HttpGet]
        public ActionResult ArticlesByGroup(ObjectId groupid)
        {
            IList il = LoadAllArticlesByGroupId(groupid);
            CreateArticleListByGroupId(il, groupid);
            return View("ArticleListByGroup");
        }

        private IList LoadAllArticlesByGroupId(ObjectId groupid)
        {
            List<Articles> article = new List<Articles>();
            List<ArticleGroups> agList = new List<Models.ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            ag.GroupId = groupid;
            agList = ag.LoadDataByGroupId();
            if (agList.Count > 0)
            {
                var list = Articles.LoadAllArticlesFromToday();
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        for (int j = 0; j < agList.Count; j++)
                        {
                            if (((Articles)list[i]).Id == agList[j].ArticleId)
                                article.Add((Articles)list[i]);
                        }
                    }
                }
            }
            return article;
        }

        private void CreateArticleListByGroupId(IList ArticleList, ObjectId groupid)
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }
            List<Group> group_TreeView = new List<Group>();
            for (int r = 0; r < group_List.Count; r++)
            {
                if (group_List[r].Name != "تبلیغات" && group_List[r].Name != "اسلاید شو")
                {
                    group_TreeView.Add(group_List[r]);
                }
            }
            List<Articles> SlideShowData = new List<Articles>();

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist;
            ArticleGroupslist = articleGroup.LoadDataAll();

            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            for (int g = 0; g < allArticles.Count; g++)
            {
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[g].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[g]);
                        }
                    }
                }
                for (int j = 0; j < galaryList.Count; j++)
                {
                    if (galaryList[j].ArticleId == allArticles[g].Id)
                        articleGalaryList.Add(galaryList[j]);
                }
            }
            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();
            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            List<ArticleGroups> list_Group = ag.LoadDataAll();


            ArticleComment comment = new ArticleComment();
            List<ArticleComment> commentList = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();
            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = (Articles)ArticleList[i];
                    for (int j = 0; j < list_Group.Count; j++)
                    {
                        if (list_Group[j].ArticleId == al.Id)
                            articleGroup_List.Add(list_Group[j]);
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    if (articleGroup_List.Count > 0)
                    {
                        for (int k = 0; k < articleGroup_List.Count; k++)
                        {
                            if (al.Id == articleGroup_List[k].ArticleId)
                            {
                                for (int j = 0; j < list_likedislike.Count; j++)
                                {
                                    if (list_likedislike[j].ArticleId == al.Id)
                                    {
                                        likesCount = likesCount + list_likedislike[j].LikeCount;
                                        dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                                        if (list_likedislike[j].UserIp == ipaddress)
                                        {
                                            ip = list_likedislike[j].UserIp;
                                        }
                                        else { ip = ""; }
                                    }
                                }
                                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                                {
                                    ArticleId = ((Articles)ArticleList[i]).Id,
                                    DislikeCount = dislikesCount,
                                    LikeCount = likesCount,
                                    UserIp = ip
                                };
                                for (int j = 0; j < commentList.Count; j++)
                                {
                                    if (commentList[j].ArticleId == al.Id)
                                        articleComments.Add(commentList[j]);
                                }
                                commentsCount = articleComments.Count;
                                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                                {
                                    Article = al,
                                    LikeDislike = ldarticle,
                                    ArticleComments = articleComments,
                                    Comment = comment,
                                    ArticleGalary = articleGalaryList,
                                    ArticleGroupsData = articleGroup_List
                                };

                                aaList.Add(aa);
                            }
                        }
                    }
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = group_TreeView
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(1, 10); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;
            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
        }


        #endregion LayoutFunctions

        [HttpGet]
        public async Task<ActionResult> AddArticle()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                Group group = new Group();
                IList list_Group = group.LoadDataAll();
                List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
                SelectListItem sli;

                Group gp = new Group();
                IList gp_List = gp.LoadDataAll();

                for (int i = 0; i < gp_List.Count; i++)
                {
                    sli = new SelectListItem();
                    sli.Value = ((Group)list_Group[i]).Id.ToString();
                    sli.Text = ((Group)list_Group[i]).Name;
                    sli.Selected = false;
                    sli.Selected = false;
                    selectedlist_Group.Add(sli);
                }

                var roles = await IdentityContext.AllRolesAsync();
                Articles article = new Articles();
                article.CreatedDate = DateTime.Now.ToString("s");

                CreateArticleAllList(1);
                return View(new ArticleComplate()
                {
                    Art = article,
                    RolesList = roles.Select(x => new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Name
                    })
               ,
                    GroupData = selectedlist_Group
                });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        public ApplicationIdentityContext IdentityContext
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationIdentityContext>();
            }
        }


        //[HttpPost]
        //public ActionResult UploadImage(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor,
        //    string langCode)
        //{
        //    string vImagePath = String.Empty;
        //    string vMessage = String.Empty;
        //    string vFilePath = String.Empty;
        //    string vOutput = String.Empty;
        //    try
        //    {
        //        if (upload != null && upload.ContentLength > 0)
        //        {
        //            var vFileName = DateTime.Now.ToString("s") +
        //                            Path.GetExtension(upload.FileName).ToLower();
        //            var vFolderPath = Server.MapPath("/Upload/");
        //            if (!Directory.Exists(vFolderPath))
        //            {
        //                Directory.CreateDirectory(vFolderPath);
        //            }
        //            vFilePath = Path.Combine(vFolderPath, vFileName);
        //            upload.SaveAs(vFilePath);
        //            vImagePath = Url.Content("/Upload/" + vFileName);
        //            vMessage = "تصویر با موفقیت ذخیره شد";
        //        }
        //    }
        //    catch
        //    {
        //        vMessage = "There was an issue uploading";
        //    }
        //    vOutput = @"<html><body><mce:script type='text / javascript'><!--window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + vImagePath + "\", \"" + vMessage + "\")";
        //    // --></mce:script></body></html>";  
        //    return Content(vOutput);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> AddArticle(ArticleComplate al, HttpPostedFileBase GalaryUploader, FormCollection selectedGroup, params string[] selectedRole)
        {
            float filesize = 0;
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    Articles article = new Articles();
                    article = al.Art;
                    article.IsDeleted = 0;
                    Articles articleLastNumer = new Articles();
                    articleLastNumer = articleLastNumer.GetLastArticleNumber();
                    if (articleLastNumer.ArticleNumber == 0)
                        article.ArticleNumber = 1000;
                    else
                        article.ArticleNumber = articleLastNumer.ArticleNumber + 1;

                    //--------------------------------------------------------- GetFile File Addredd -------------------
                    string[] str_File = article.FileName.Split(',');
                    string fileAddress = article.FileAddress;
                    if (fileAddress.EndsWith("/"))
                    {
                        fileAddress = fileAddress.Substring(0, fileAddress.Length - 1);
                    }
                    if (fileAddress.EndsWith("\\"))
                    {
                        fileAddress = fileAddress.Substring(0, fileAddress.Length - 1);
                    }
                    article.FileAddress = "";

                    for (int i = 0; i < str_File.Length; i++)
                    {
                        float length = (new FileInfo(fileAddress + "\\" + str_File[i]).Length);
                        //saeed start 
                        if (length >= (1024 * 1024 * 1024))
                        {
                            article.FileSize += (length / (1024f * 1024 * 1024)).ToString("f2") + " Gb,";
                        }
                        else
                        {
                            if (length >= (1024 * 1024))
                            {
                                article.FileSize += (length / (1024f * 1024)).ToString("f2") + " Mb,";
                            }
                            else
                            {
                                article.FileSize += (length / (1024f)).ToString("f2") + " Kb,";
                            }
                        }
                        article.FileAddress += fileAddress + "\\" + str_File[i] + ",";
                    }
                    if (!string.IsNullOrEmpty(article.FileSize) && article.FileSize.Length > 0)
                        article.FileSize = article.FileSize.Substring(0, article.FileSize.Length - 1);
                    if (!string.IsNullOrEmpty(article.FileAddress) && article.FileAddress.Length > 0)
                        article.FileAddress = article.FileAddress.Substring(0, article.FileAddress.Length - 1);
                    //--------------------------------------------------------- GetFile Video Addredd -------------------
                    if (!string.IsNullOrEmpty(article.VideoName))
                    {
                        if (article.VideoName.Length > 0)
                        {
                            string[] str_Video = article.VideoName.Split(',');
                            string videoAddress = article.VideoAddress;

                            videoAddress = videoAddress.ToLower().Replace("\\", "/");
                            videoAddress = videoAddress.ToLower().Replace("d:/fileserver1/", "http://fileserver1.cgsilent.com/");
                            videoAddress = videoAddress.ToLower().Replace("e:/fileserver2/", "http://fileserver2.cgsilent.com/");
                            videoAddress = videoAddress.ToLower().Replace("f:/fileserver3/", "http://fileserver3.cgsilent.com/");

                            article.VideoAddress = "";
                            for (int i = 0; i < str_Video.Length; i++)
                            {
                                article.VideoAddress += videoAddress + "/" + str_Video[i];
                                //saeed
                                if (!article.VideoAddress.EndsWith("/"))
                                {
                                    article.VideoAddress += ",";//article.VideoAddress +
                                }
                                else
                                {
                                    article.VideoAddress += ",";//article.VideoAddress +
                                }
                            }
                            article.VideoAddress = article.VideoAddress.Substring(0, article.VideoAddress.Length - 1);
                        }
                    }
                    else
                    {
                        article.VideoAddress = "";
                        article.VideoName = "";
                    }
                    UploadFile(article);
                    if (article.ImagePath != null)
                    {
                        article.ViewCount = 0;
                        article.CreatedDate = DateTime.Parse(article.CreatedDate).ToString("s");
                        article.UpdateDate = article.CreatedDate;
                        article.Author = User.Identity.GetUserName();

                        var roles = await IdentityContext.AllRolesAsync();
                        string role = "";
                        if (selectedRole != null)
                        {
                            foreach (var item in roles)
                            {
                                if (item.Name == selectedRole[0].ToString())
                                    role = item.Id;
                            }
                            article.FilePermisionId = ObjectId.Parse(role);
                        }
                        else
                        {
                            foreach (var item in roles)
                            {
                                if (item.Name == "Public")
                                    role = item.Id;
                            }
                            article.FilePermisionId = ObjectId.Parse(role);
                        }

                        if (TempData["fa"] != null)
                        {
                            article.FileAddress = TempData["fa"].ToString();
                            TempData.Remove("fa");
                        }
                        if (!string.IsNullOrEmpty(article.FileAddress) &&
                            article.FileAddress.Length > 0 &&
                            article.FileAddress.LastIndexOf(",") == article.FileAddress.Length - 1)
                        {
                            article.FileAddress = article.FileAddress.Substring(0, article.FileAddress.Length - 1);
                        }
                        if (article.Tag == null)
                        {
                            article.Tag = "";
                        }
                        if (article.Price.ToString() == null)
                        {
                            article.Price = 0;
                        }
                        if (article.KeyWords == null)
                        {
                            article.KeyWords = "";
                        }
                        article.Insert();
                        ArticleGroups ag;
                        string[] GroupsSelected;
                        for (int i = 0; i < selectedGroup.Keys.Count; i++)
                        {
                            if (selectedGroup.Keys[i] == "SelectedGroup")
                            {
                                GroupsSelected = selectedGroup.GetValues("selectedGroup");
                                for (int j = 0; j < GroupsSelected.Length; j++)
                                {
                                    ag = new ArticleGroups();
                                    ag.ArticleId = article.Id;
                                    ag.GroupId = ObjectId.Parse(GroupsSelected[j]);
                                    ag.Insert();
                                }
                            }
                        }
                        if (GalaryUploader != null)
                        {
                            if (GalaryUploader.ContentLength > 0)
                            {
                                UploadGalary(article, GalaryUploader);
                            }
                        }
                    }
                    else
                    {
                        ViewBag.addArticleAlert = "عملیات درج خبر با شکست مواجه شد";
                    }
                }
            }
            catch (Exception excep)
            {
                Session.Add("ErrorFileSize", "  excep Message : " + excep.Message + " exception value : " + excep.Data.Values + " filesize: " + filesize);
            }
            return RedirectToAction("Index");
        }

        private void UploadGalary(Articles article, HttpPostedFileBase GalaryUploader)
        {
            if (article != null)
            {
                HttpPostedFileBase FileData;
                ObjectId fileId = ObjectId.Empty;

                if (GalaryUploader.ContentLength > 0)
                {
                    HttpFileCollectionBase files = Request.Files;
                    ArticleGalary gU = new ArticleGalary();
                    for (int i = 0; i < files.Count; i++)
                    {
                        if (files.Keys[i].Contains("GalaryUploader"))
                        {
                            FileData = files[i];
                            if (FileData.ContentLength > 0)
                            {
                                string fileName = Path.GetFileName(FileData.FileName);
                                if (fileName.Contains("#"))
                                {
                                    fileName = fileName.Replace("#", "sharp");
                                }
                                string path = Path.Combine(Server.MapPath("~/Content/Images/Article/Galary"), fileName);
                                if (System.IO.File.Exists(path))
                                {
                                    while (System.IO.File.Exists(path))
                                    {
                                        Guid newid = Guid.NewGuid();
                                        int index = fileName.LastIndexOf(".");
                                        string imagename = fileName.Substring(0, index);
                                        imagename = imagename + "_" + newid;
                                        fileName = imagename + fileName.Substring(index);
                                        path = Path.Combine(Server.MapPath("~/Content/Images/Article/Galary"), fileName);
                                    }
                                }
                                Image upImage = Image.FromStream(FileData.InputStream);

                                Image watermarkImage = Image.FromFile(Server.MapPath("~/Content/Images/Article/Galary/watermark_cg.png"));
                                {
                                    MemoryStream stream = new MemoryStream();
                                    upImage.Save(stream, ImageFormat.Bmp);
                                    upImage = Image.FromStream(stream);
                                    Bitmap bmp = bmp = new Bitmap(upImage);
                                    bmp.SetResolution(watermarkImage.HorizontalResolution, watermarkImage.VerticalResolution);
                                    upImage = bmp;
                                }

                                using (Graphics g = Graphics.FromImage(upImage))
                                {

                                    // Rectangle sRect = new Rectangle(0, 0, upImage.Width, upImage.Height);
                                    // Rectangle dRect = new Rectangle(0, 0, watermarkImage.Width, watermarkImage.Height);

                                    //  float hRes = ( upImage.HorizontalResolution / watermarkImage.HorizontalResolution);
                                    g.DrawImage(watermarkImage, 0, (upImage.Height - watermarkImage.Height));
                                    upImage.Save(Path.Combine(Server.MapPath("~/Content/Images/Article/Galary"), fileName));
                                }
                                //GridFSBucket fs = new GridFSBucket(MongoConnection.instance.database);
                                //fileId = Articles.UploadFile(fs, fileName, path);
                                //gU = new ArticleGalary();
                                gU.ArticleId = article.Id;
                                //gU.ImageId = fileId;
                                gU.ImagePath = "~/Content/Images/Article/Galary/" + fileName;
                                gU.Id = ObjectId.Empty;
                                gU.CreatedDate = DateTime.Now.ToString("s");
                                gU.Insert();
                            }
                        }
                    }
                }
            }
        }


        [HttpGet]
        public async Task<ActionResult> DetailArticle(ObjectId id)
        {
            InsertSiteStatic(id, ObjectId.Empty, "Store");
            Articles al = new Articles();
            al.Id = id;
            List<Articles> alList = al.LoadDataByObject();
            if (alList.Count > 0)
            {
                al = alList[0];
            }

            //---------------------------------------- Update View Count Of Article------------------------------
            al.ViewCount = al.ViewCount + 1;
            al.UpdateViewCount();

            //----------------------------------------------- Article Permision ---------------------------------

            if (User.Identity.IsAuthenticated)
            {
                //UserManagementVIPRole uservip = new UserManagementVIPRole();
                //uservip.Id = ObjectId.Parse(User.Identity.GetUserId());

                var uservip = UserManager.FindById(User.Identity.GetUserId().ToString());

                var roles = await IdentityContext.AllRolesAsync();

                IdentityRole[] UserRoleInfo = new IdentityRole[roles.Count];
                UserRoles userroles = new UserRoles();

                userroles.Id = ObjectId.Parse(User.Identity.GetUserId());
                userroles = userroles.LoadDataByUserId();

                IdentityRole articlePermision = new IdentityRole();
                foreach (IdentityRole role in roles)
                {
                    if (role.Id == al.FilePermisionId.ToString())
                    {
                        articlePermision = role;
                    }
                }
                if (articlePermision.Name == "Public" || User.IsInRole("Admin"))
                {
                    ViewBag.roleid = al.FilePermisionId.ToString();
                }
                else
                {
                    foreach (IdentityRole role in roles)
                    {
                        for (int i = 0; i < userroles.Roles.Count(); i++)
                        {
                            //if (userroles.Roles[i] == articlePermision.Name)
                            //{
                                if (uservip.VipExpireDateTime >= DateTime.Parse(DateTime.Now.ToString()))
                                {
                                    ViewBag.roleid = al.FilePermisionId.ToString();
                                    break;
                                }
                            //}
                        }
                    }

                    //---------------------------------------------- Check VIP Users ---------------------------

                    if (ViewBag.roleid == null)
                    {
                        al.FileAddress = "شما مجوز لازم برای مشاهده ی لینک های دانلود را ندارید";
                    }
                }
            }
            else
            {
                al.FileAddress = "شما مجوز لازم برای مشاهده ی لینک های دانلود را ندارید";
            }
            string[] names = null;
            if (!string.IsNullOrEmpty(al.FileName))
                names = al.FileName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] sizes = null;
            if (!string.IsNullOrEmpty(al.FileSize))

                sizes = al.FileSize.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] address = null;
            if (!string.IsNullOrEmpty(al.FileAddress))
                address = al.FileAddress.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] types = null;
            if (!string.IsNullOrEmpty(al.FileType))
                types = al.FileType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Dictionary<string, List<linkInfo>> result = new Dictionary<string, List<linkInfo>>();
            if (names != null && types != null && sizes != null && address != null &&
                    names.Length == types.Length && names.Length == sizes.Length
                    /*&& names.Length == address.Length*/)
            {
                for (int index = 0; index < types.Length; index++)
                {
                    if (result.ContainsKey(types[index]))
                    {
                        if (address[0] != "شما مجوز لازم برای مشاهده ی لینک های دانلود را ندارید")
                            result[types[index]].Add(new linkInfo() { Name = names[index], Type = types[index], Link = address[index], Size = sizes[index] });
                        else
                            result[types[index]].Add(new linkInfo() { Name = names[index], Type = types[index], Link = "", Size = sizes[index] });
                    }
                    else
                    {
                        List<linkInfo> temp = new List<linkInfo>();
                        if (address[0] != "شما مجوز لازم برای مشاهده ی لینک های دانلود را ندارید")
                            temp.Add(new linkInfo() { Name = names[index], Type = "", Link = address[index], Size = sizes[index] });
                        else
                            temp.Add(new linkInfo() { Name = names[index], Type = "", Link = "", Size = sizes[index] });
                        result.Add(types[index], temp);
                    }
                }
            }
            else
            {
                if (names != null && sizes != null && address != null &&
                     names.Length == sizes.Length && names.Length == address.Length)
                {
                    List<linkInfo> temp = new List<linkInfo>();
                    for (int k = 0; k < names.Length; k++)
                    {
                        if (address[0] == "شما مجوز لازم برای مشاهده ی لینک های دانلود را ندارید")
                            temp.Add(new linkInfo() { Name = names[k], Size = sizes[k], Link = "" });
                        else
                            temp.Add(new linkInfo() { Name = names[k], Size = sizes[k], Link = address[k] });
                    }
                    result.Add("", temp);
                }
                else
                {
                    if (names != null && sizes != null && address != null &&
                          names.Length == sizes.Length && names.Length != address.Length)
                    {
                        List<linkInfo> temp = new List<linkInfo>();
                        for (int k = 0; k < names.Length; k++)
                        {
                            if (address[0] == "شما مجوز لازم برای مشاهده ی لینک های دانلود را ندارید")
                                temp.Add(new linkInfo() { Name = names[k], Size = sizes[k], Link = "" });
                            else
                                temp.Add(new linkInfo() { Name = names[k], Size = sizes[k], Link = address[0] });
                        }
                        result.Add("", temp);

                    }
                }
            }
            ViewBag.fileType = result;
            return View("DetailArticle", CreateArticleListForDetail(alList));
        }


        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpPost]
        [Route("Article_Basket")]
        public ActionResult AddBasket(string id)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = "Model is not valid", Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };
            }
            else
            {
                ObjectId userid = ObjectId.Parse(User.Identity.GetUserId());
                List<ObjectId> articlesID = new List<ObjectId>();
                articlesID.Add(ObjectId.Parse(id));
                UserBasket basket = new UserBasket
                {
                    ArticleId = articlesID,
                    Id = userid
                };

                basket.ArticleId = new List<ObjectId>();
                basket.ArticleId.Add(ObjectId.Parse(id));
                basket.Id = ObjectId.Parse(User.Identity.GetUserId());
                basket.UpdateBasketForArticles();
                Articles al = new Articles();
                al.Id = ObjectId.Parse(id);
                List<Articles> alList = al.LoadDataByObject();
                return new JsonResult { Data = new { Success = true }, };
            }

        }

        [HttpGet]
        private void InsertSiteStatic(ObjectId storeId)
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            UserRoles roles = new UserRoles();

            string ip = Convert.ToString(ipEntry.AddressList[1]);
            string[] roleNames = roles.Roles;

            if (User.Identity.IsAuthenticated)
            {
                roles.Id = ObjectId.Parse(User.Identity.GetUserId());
                roles = roles.LoadDataByUserId();
            }
            else
            {
                roles.Id = ObjectId.Empty;
            }

            SiteStatics ss = new SiteStatics();
            ss.CreatedDate = DateTime.Now.ToString("s");
            ss.PageName = "Store";
            ss.ArticleId = storeId;

            if (User.Identity.IsAuthenticated)
                ss.UserIdOrIp = User.Identity.GetUserId();
            else
                ss.UserIdOrIp = ip;
            if (roleNames != null)
                for (int i = 0; i < roleNames.Length; i++)
                {
                    ss.RoleName = roleNames[i];
                    ss.Insert();
                }
            else
            {
                ss.Insert();
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Comments(string articleID, string comment)//(ArticleListComplete model)
        {
            ArticleComment artc = new ArticleComment();

            artc.CommentBody = comment;
            artc.ArticleId = ObjectId.Parse(articleID);
            artc.UpdateDate = artc.CreatedDate = DateTime.Now.ToString("s");
            artc.UserID = User.Identity.GetUserId();
            artc.IsPublished = false;
            artc.UserName = User.Identity.GetUserName();

            List<ArticleComment> commentInserted = artc.LoadDataByArticleIdAndUserId();

            string message = "";
            if (commentInserted.Count > 0)
            {
                // ViewBag.commentmessage = "نظر شما قبلا ثبت شده است.";
                message = "نظر شما قبلا ثبت شده است.";
            }
            else
            {
                artc.Insert();
                //  ViewBag.commentmessage = "نظر شما ثبت شد.";
                message = "نظر شما ثبت شد.";
            }
            return Json(message);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AnswerComments(string articleID, string commentId, string answer)//(ArticleListComplete model)
        {
            ArticleComment artc = new ArticleComment();
            artc.Answer = answer;
            artc.IsPublished = true;
            artc.Id = ObjectId.Parse(commentId);
            artc.UpdateDate = DateTime.Now.ToString("s");
            artc.UserID = User.Identity.GetUserId();
            artc.AnswerBy = User.Identity.GetUserName();
            artc.Update();

            string message = "نظر شما ثبت شد.";
            return Json(message);
        }


        [HttpGet]
        public ActionResult GetFile(string filePath, string fName)
        {
            int indexOfLastDot = filePath.LastIndexOf(".");
            // filePath = filePath.Substring(0, indexOfLastDot);
            string Type = filePath.Substring(indexOfLastDot + 1).ToLower();

            string contentType = "application/pdf";
            switch (Type)
            {
                case "pdf":
                    contentType = "application/pdf";
                    break;
                case "rar":
                    contentType = "application/rar";
                    break;
                case "zip":
                    contentType = "application/zip";
                    break;
                case "mp4":
                    contentType = "application/mp4";
                    break;
                case "mkv":
                    contentType = "application/mkv";
                    break;
            }
            // string fileName = "3D model.rar";

            FileMapper fm = new FileMapper()
            {
                expireDate = DateTime.Now.AddMonths(1).Ticks,
                filePath = filePath,
                //id = Guid.NewGuid().ToString(),
                contentType = contentType,
                fileName = fName
            };
            // fm = new FileMapper();

            fm.id = fm.Insert();
            string url = Request.Url.OriginalString.Remove(Request.Url.OriginalString.IndexOf(Request.Url.LocalPath));
            return Redirect(url + "/api/Files/" + fm.id.ToString());
            //  return RedirectToAction("Download", "Download", new { id = fm.id.ToString(), Area = "Article" });

        }

        [HttpGet]
        public async Task<ActionResult> EditArticle(ObjectId id)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                Articles al = new Articles();
                al.Id = id;
                IList il_Article = al.LoadDataByObject();

                if (il_Article.Count > 0)
                {
                    al = (Articles)il_Article[0];
                }

                string fileaddress = al.FileAddress;
                //int lastindex = fileaddress.LastIndexOf("\\");
                //al.FileAddress = fileaddress.Substring(0, lastindex);
                string[] str1 = al.FileAddress.Split(',');
                string Fileaddress = str1[0];
                //Fileaddress = Fileaddress.Substring(0, Fileaddress.Length - 1);
                if (str1.Length > 0)
                {
                    int lastfileindex = Fileaddress.LastIndexOf("\\");
                    al.FileAddress = Fileaddress.Substring(0, lastfileindex);
                }

                if (al.VideoName != null)
                {
                    if (al.VideoName != "")
                    {
                        string[] str = al.VideoAddress.Split(',');
                        string videoaddress = str[0];
                        videoaddress = videoaddress.Substring(0, videoaddress.Length - 1);
                        if (str.Length > 0)
                        {
                            int lastvideoindex = videoaddress.LastIndexOf("/");
                            al.VideoAddress = videoaddress.Substring(0, lastvideoindex);
                        }
                    }
                }

                ArticleGalary galary = new ArticleGalary();
                galary.ArticleId = al.Id;
                IList list_Galary = galary.LoadDataByArticleId();
                List<ArticleGalary> articleGalary_List = new List<ArticleGalary>();
                for (int i = 0; i < list_Galary.Count; i++)
                {
                    articleGalary_List.Add((ArticleGalary)list_Galary[i]);
                }

                var roles = await IdentityContext.AllRolesAsync();
                var myRole = await RoleManager.FindByIdAsync(al.FilePermisionId.ToString());
                ViewBag.RoleId = new SelectList(roles, "Name", "Name");


                Group group = new Group();
                IList list_Group = group.LoadDataAll();
                List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
                SelectListItem sli;

                ArticleGroups ag = new ArticleGroups();
                ag.ArticleId = al.Id;
                IList il = ag.LoadDataByArticleId();

                for (int i = 0; i < list_Group.Count; i++)
                {
                    sli = new SelectListItem();
                    sli.Value = ((Group)list_Group[i]).Id.ToString();
                    sli.Text = ((Group)list_Group[i]).Name;
                    sli.Selected = false;
                    if (il != null)
                    {
                        for (int j = 0; j < il.Count; j++)
                        {
                            if (((ArticleGroups)il[j]).GroupId == ((Group)list_Group[i]).Id)
                            {
                                sli.Selected = true;
                            }
                        }
                    }
                    selectedlist_Group.Add(sli);
                }

                ArticleComplate completeData = new ArticleComplate()
                {
                    Art = al,
                    RolesList = roles.Select(x => new SelectListItem()
                    {
                        Selected = x.Name == myRole.Name ? true : false,
                        Text = x.Name,
                        Value = x.Name
                    })
                    ,
                    GroupData = selectedlist_Group,
                    Galary = articleGalary_List
                };
                CreateArticleAllList(1);
                return View("EditArticle", completeData);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> EditArticle(ArticleComplate article, HttpPostedFileBase GalaryUploader,
            HttpPostedFileBase FileUploader, FormCollection selectedGroup, string deleteGalary, bool chb_DeleteGalary = false, params string[] selectedRole)
        {
            float filesize = 0;
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    article.Art.UpdateDate = DateTime.Now.ToString("s");
                    Articles arm = new Articles();

                    Articles editArticle = new Articles();
                    editArticle = article.Art;
                    editArticle.IsDeleted = 0;

                    //--------------------------------------------------------- GetFile File Addredd -------------------
                    if (editArticle.FileAddress != null)
                    {
                        string[] str_File = editArticle.FileName.Split(',');
                        string fileAddress = editArticle.FileAddress;
                        if (fileAddress.EndsWith("/"))
                        {
                            fileAddress = fileAddress.Substring(0, fileAddress.Length - 1);
                        }
                        if (fileAddress.EndsWith("\\"))
                        {
                            fileAddress = fileAddress.Substring(0, fileAddress.Length - 1);
                        }
                        editArticle.FileAddress = "";

                        for (int i = 0; i < str_File.Length; i++)
                        {
                            if (System.IO.File.Exists(fileAddress + "\\" + str_File[i]))
                            {
                                float length = (new FileInfo(fileAddress + "\\" + str_File[i]).Length);
                                //saeed start 
                                if (length >= (1024 * 1024 * 1024))
                                {
                                    editArticle.FileSize += (length / (1024f * 1024 * 1024)).ToString("f2") + " Gb,";
                                }
                                else
                                {
                                    if (length >= (1024 * 1024))
                                    {
                                        editArticle.FileSize += (length / (1024f * 1024)).ToString("f2") + " Mb,";
                                    }
                                    else
                                    {
                                        editArticle.FileSize += (length / (1024f)).ToString("f2") + " Kb,";
                                    }
                                }
                                //saeed end 
                                //filesize = length / 1024f / 1024f / 1024f;
                                //if (filesize < 1)
                                //{
                                //    filesize = length / 1024f / 1024f;
                                //    if (filesize < 1)
                                //    {
                                //        filesize = length / 1024f;
                                //        string[] str = filesize.ToString().Split('.');
                                //        if (str[1].Length >= 2)
                                //        {
                                //            filesize = float.Parse(str[0] + "." + str[1].Substring(0, 2));
                                //        }
                                //        else
                                //        {
                                //            filesize = float.Parse(str[0] + "." + str[1]);
                                //        }
                                //        editArticle.FileSize += filesize + " Kb,";
                                //    }
                                //    else
                                //    {
                                //        if (filesize.ToString().Contains("."))
                                //        {
                                //            string[] str = filesize.ToString().Split('.');
                                //            if (str[1].Length >= 2)
                                //            {
                                //                filesize = float.Parse(str[0] + "." + str[1].Substring(0, 2));
                                //            }
                                //            else
                                //            {
                                //                filesize = float.Parse(str[0] + "." + str[1]);
                                //            }
                                //        }
                                //        editArticle.FileSize += filesize + " Mb,";
                                //    }
                                //}
                                //else
                                //{
                                //    string[] str = filesize.ToString().Split('.');
                                //    if (str[1].Length >= 2)
                                //    {
                                //        filesize = float.Parse(str[0] + "." + str[1].Substring(0, 2));
                                //    }
                                //    else
                                //    {
                                //        filesize = float.Parse(str[0] + "." + str[1]);
                                //    }
                                //    editArticle.FileSize += filesize + " Gb,";
                                //}
                            }
                            else
                            {
                                editArticle.FileSize += "0.0kb,";
                            }
                            editArticle.FileAddress += fileAddress + "\\" + str_File[i] + ",";
                        }
                        if (!string.IsNullOrEmpty(editArticle.FileSize))
                        {
                            editArticle.FileSize = editArticle.FileSize.Substring(0, editArticle.FileSize.Length - 1);
                        }
                        if (!string.IsNullOrEmpty(editArticle.FileAddress))
                        {
                            editArticle.FileAddress = editArticle.FileAddress.Substring(0, editArticle.FileAddress.Length - 1);
                        }
                    }
                    //--------------------------------------------------------- GetFile Video Address -------------------
                    if (!string.IsNullOrEmpty(editArticle.VideoAddress))
                    {
                        string[] str_Video = editArticle.VideoName.Split(',');
                        string videoAddress = editArticle.VideoAddress;

                        videoAddress = videoAddress.ToLower().Replace("\\", "/");
                        videoAddress = videoAddress.ToLower().Replace("d:/fileserver1/", "http://fileserver1.cgsilent.com/");
                        videoAddress = videoAddress.ToLower().Replace("e:/fileserver2/", "http://fileserver2.cgsilent.com/");
                        videoAddress = videoAddress.ToLower().Replace("f:/fileserver3/", "http://fileserver3.cgsilent.com/");

                        editArticle.VideoAddress = "";
                        for (int i = 0; i < str_Video.Length; i++)
                        {
                            editArticle.VideoAddress += videoAddress + "/" + str_Video[i];
                            //saeed
                            if (!editArticle.VideoAddress.EndsWith("/"))
                            {
                                editArticle.VideoAddress += ",";//article.VideoAddress +
                            }
                            else
                            {
                                editArticle.VideoAddress += ",";//article.VideoAddress +
                            }
                        }
                        if (editArticle.VideoAddress.Length > 0)
                            editArticle.VideoAddress = editArticle.VideoAddress.Substring(0, editArticle.VideoAddress.Length - 1);
                    }
                    else
                    {
                        editArticle.VideoAddress = "";
                        editArticle.VideoName = "";
                    }

                    arm.Id = editArticle.Id;
                    IList articleList = arm.LoadDataByObject();

                    if (articleList.Count > 0)
                        DeleteFile(((Articles)articleList[0]).ImagePath);
                    UploadFile(arm);
                    if (arm.ImagePath != null)
                        editArticle.ImagePath = arm.ImagePath;

                    editArticle.Author = User.Identity.GetUserName();

                    var roles = await IdentityContext.AllRolesAsync();
                    string role = "";
                    foreach (var item in roles)
                    {
                        if (item.Name == selectedRole[0].ToString())
                            role = item.Id;
                    }
                    editArticle.FilePermisionId = ObjectId.Parse(role);
                    //saeed
                    if (FileUploader != null)
                    {
                        //editArticle.FileAddress = "";?????????????????????????
                        if (editArticle.FileAddress.LastIndexOf(",") == editArticle.FileAddress.Length - 1)
                        {
                            editArticle.FileAddress = editArticle.FileAddress.Substring(0, editArticle.FileAddress.Length - 1);
                        }
                    }
                    if (TempData["fa"] != null)
                    {
                        editArticle.FileAddress = TempData["fa"].ToString();
                        TempData.Remove("fa");
                    }
                    if (!string.IsNullOrEmpty(editArticle.FileAddress) &&
                        editArticle.FileAddress.LastIndexOf(",") == editArticle.FileAddress.Length - 1)
                    {
                        editArticle.FileAddress = editArticle.FileAddress.Substring(0, editArticle.FileAddress.Length - 1);
                    }
                    if (editArticle.Tag == null)
                    {
                        editArticle.Tag = "";
                    }
                    if (editArticle.Price.ToString() == null)
                    {
                        editArticle.Price = 0;
                    }
                    if (editArticle.KeyWords == null)
                    {
                        editArticle.KeyWords = "";
                    }
                    editArticle.Update();

                    ArticleGroups ag = new ArticleGroups();
                    string[] GroupsSelected;

                    ag.ArticleId = editArticle.Id;
                    ag.DeleteWithArticleId();
                    for (int i = 0; i < selectedGroup.Keys.Count; i++)
                    {
                        if (selectedGroup.Keys[i] == "SelectedGroup")
                        {
                            GroupsSelected = selectedGroup.GetValues("selectedGroup");

                            for (int j = 0; j < GroupsSelected.Length; j++)
                            {
                                ag = new ArticleGroups();
                                ag.ArticleId = editArticle.Id;
                                ag.GroupId = ObjectId.Parse(GroupsSelected[j]);
                                ag.Insert();
                            }
                        }
                    }

                    ArticleGalary galary = new ArticleGalary();
                    if (chb_DeleteGalary)
                    {
                        galary.ArticleId = editArticle.Id;
                        List<ArticleGalary> galaryList = galary.LoadDataByArticleId();
                        if (galaryList.Count > 0)
                        {
                            for (int i = 0; i < galaryList.Count; i++)
                                DeleteGalaryFile((galaryList[i]).ImagePath);
                            galary.DeleteByArticleId();
                        }
                    }


                    if (GalaryUploader != null)
                    {
                        if (GalaryUploader.ContentLength > 0)
                        {
                            UploadGalary(editArticle, GalaryUploader);
                        }
                        else
                        {
                            galary = new ArticleGalary();
                            galary.ArticleId = editArticle.Id;
                            List<ArticleGalary> list_Galary = new List<ArticleGalary>();
                            list_Galary = galary.LoadDataByArticleId();
                            for (int i = 0; i < list_Galary.Count; i++)
                                DeleteGalaryFile((list_Galary[i]).ImagePath);
                            galary.DeleteByArticleId();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                string error = " error Message : " + exp.Message + " StackTrace :" + exp.StackTrace;
                Response.Write(error);
                ViewBag.error = error + " file size : " + filesize;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteArticle(ObjectId id)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                Articles al = new Articles();
                al.Id = id;
                IList il_Article = al.LoadDataByObject();
                CreateArticleAllList(1);

                if (il_Article.Count > 0)
                    return View("DeleteArticle", il_Article[0]);
                else
                    return null;
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult DeleteArticle(Articles article)
        {
            article.DeleteById();
            CreateArticleAllList(1);

            return View("ArticleList");
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult LikeArticle(ObjectId Id)
        {
            string ipaddress = "";
            ipaddress = User.Identity.GetUserId();
            if (ipaddress == null)
            {
                string strHostName = "";
                strHostName = Dns.GetHostName();
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                ipaddress = Convert.ToString(ipEntry.AddressList[1]);
            }
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            if (Request.IsAjaxRequest())
            {
                LikeDislikeArticle ldc = new LikeDislikeArticle();
                ldc.ArticleId = Id;

                IList list_LDC = ldc.LoadDataByArticleId();
                int userWithSimilarIp = 0;
                for (int i = 0; i < list_LDC.Count; i++)
                {
                    if ((((LikeDislikeArticle)list_LDC[i]).UserIp == ipaddress))
                    {
                        userWithSimilarIp = userWithSimilarIp + 1;
                        ldc = (LikeDislikeArticle)list_LDC[i];
                        ldc.DislikeCount = 0;
                        ldc.LikeCount = 1;
                        ldc.UserIp = ipaddress;
                        ldc.UpdateDate = DateTime.Now.ToString("s");
                        ldc.Update();
                    }
                }

                if (ldc.Id == ObjectId.Empty)
                {
                    ldc = new LikeDislikeArticle
                    {
                        LikeCount = 1,
                        ArticleId = Id,
                        DislikeCount = 0,
                        CreatedDate = DateTime.Now.ToString("s"),
                        UpdateDate = DateTime.Now.ToString("s"),
                        UserIp = ipaddress
                    };
                    ldc.Insert();
                }
            }
            //var pageNumber = page ?? 1;

            List<Articles> il = Articles.LoadAllArticlesFromToday();
            //CreateArticleListForDetail(il);
            return View();// "DetailArticle", CreateArticleListForDetail(il));
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult DisLikeArticle(ObjectId Id)
        {
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            if (Request.IsAjaxRequest())
            {

                ObjectId userid = ObjectId.Empty;

                string strHostName = "";
                strHostName = Dns.GetHostName();
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

                if (Session["User"] != null)
                {
                    userid = ObjectId.Parse(Session["User"].ToString());
                }

                LikeDislikeArticle ldc = new LikeDislikeArticle();
                ldc.ArticleId = Id;
                IList list_LD = ldc.LoadDataByArticleId();
                for (int i = 0; i < list_LD.Count; i++)
                {
                    if ((((LikeDislikeArticle)list_LD[i]).UserIp == ipaddress))
                    {
                        ldc = (LikeDislikeArticle)list_LD[i];
                        ldc.DislikeCount = 1;
                        ldc.LikeCount = 0;
                        ldc.UpdateDate = DateTime.Now.ToString("s");
                        ldc.Update();
                    }
                }
                if (ldc.Id == ObjectId.Empty)
                {
                    ldc = new LikeDislikeArticle
                    {
                        LikeCount = 0,
                        ArticleId = Id,
                        DislikeCount = 1,
                        CreatedDate = DateTime.Now.ToString("s"),
                        UpdateDate = DateTime.Now.ToString("s"),
                        UserIp = ipaddress
                    };
                    ldc.Insert();
                }
            }
            // var pageNumber = page ?? 1;

            List<Articles> il = Articles.LoadAllArticlesFromTodayByPageNumber(1);
            //return View("ArticleList");
            return View();//"DetailArticle", CreateArticleListForDetail(il));
        }


        public void UploadFile(Articles article)
        {
            if (article != null)
            {
                HttpPostedFileBase FileData;
                ObjectId fileId = ObjectId.Empty;
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    FileData = Request.Files[0];
                    if (FileData.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(FileData.FileName);
                        if (fileName.Contains("#"))
                        {
                            fileName = fileName.Replace("#", "sharp");
                        }
                        string path = Path.Combine(Server.MapPath("~/Content/Images/Article"), fileName);
                        if (System.IO.File.Exists(path))
                        {
                            int i = 0;
                            while (System.IO.File.Exists(path))
                            {
                                Guid newid = Guid.NewGuid();

                                int index = fileName.LastIndexOf(".");
                                string imagename = fileName.Substring(0, index);
                                imagename = imagename + "_" + newid;
                                fileName = imagename + fileName.Substring(index);
                                path = Path.Combine(Server.MapPath("~/Content/Images/Article"), fileName);
                            }
                        }
                        FileData.SaveAs(path);
                        //GridFSBucket fs = new GridFSBucket(MongoConnection.instance.database);

                        //fileId = Articles.UploadFile(fs, fileName, path);
                        //article.ImageId = fileId;
                        article.ImagePath = "~/Content/Images/Article/" + fileName;
                    }
                }
            }
        }

        public static byte[] DownloadFile(GridFSBucket fs, ObjectId id)//, string fileName)
        {
            byte[] x = fs.DownloadAsBytes(id);
            return x;
        }

        private void DeleteFile(string filename)
        {
            if (filename != "")
            {
                HttpPostedFileBase FileData;
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    //Delete ArticleImage
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        FileData = Request.Files[i];
                        if (Request.Files.Keys[i].Contains("Image"))
                        {
                            if (FileData.ContentLength > 0)
                            {
                                string path = Path.Combine(Server.MapPath(filename));
                                if (System.IO.File.Exists(path))
                                {
                                    System.IO.File.Delete(path);
                                }
                                return;
                            }
                        }
                    }
                }
            }

        }


        private void DeleteGalaryFile(string filename)
        {
            if (filename != "")
            {
                string path = Path.Combine(Server.MapPath(filename));
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);
                return;
            }
        }
        public void DeleteAllFiles(string fileName)
        {
            if (fileName != "")
            {
                string path = Path.Combine(Server.MapPath(fileName));
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);
            }
        }


        [HttpGet]
        public ActionResult ArticlesByGroup(ObjectId groupid, int? page)
        {
            ViewBag.SelectedGroup = groupid;
            var pageNumber = page ?? 1;
            LoadAllArticlesByGroupId(groupid, pageNumber);
            return View("ArticleFilterGroup");
        }

        private void LoadAllArticlesByGroupId(ObjectId groupid, int pagenumber)
        {
            List<Articles> article = new List<Articles>();
            List<ArticleGroups> agList = new List<ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            ag.GroupId = groupid;

            agList = ag.LoadDataByGroupId();
            if (agList.Count > 0)
            {
                var list = Articles.LoadAllArticlesFromToday();
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        for (int j = 0; j < agList.Count; j++)
                        {
                            if ((list[i]).Id == agList[j].ArticleId)
                            {
                                article.Add(list[i]);
                                break;
                            }
                        }
                    }
                }
            }
            CreateArticleListByGroupId(article, agList, groupid, pagenumber);
        }

        private ArticleListComplete CreateArticleListForDetail(List<Articles> ArticleList)
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            Group group = new Group();
            List<Group> group_List = group.LoadDataAll();

            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();

            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleDetailGalaryList = new List<ArticleGalary>();

            List<ArticleGroups> ArticleGroupslist;

            // List<Stores> StoreSlideShow = Stores.LoadAllStoresFromToday();

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            ArticleGroups articleGroup = new ArticleGroups();
            for (int g = 0; g < allArticles.Count; g++)
            {
                articleGroup.ArticleId = allArticles[g].Id;
                ArticleGroupslist = articleGroup.LoadDataByArticleId();
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if ((ArticleGroupslist[r]).GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                    {
                        SlideShowData.Add(allArticles[g]);
                    }
                }
            }
            List<Articles> al_RelatedArticles = new List<Articles>();
            al_RelatedArticles = ArticleList[0].LoadRelatedArticles();
            ViewBag.RelatedArticles = al_RelatedArticles;
            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = ArticleList[i];


                    articleGroup_List = new List<ArticleGroups>();
                    ag = new ArticleGroups();
                    ag.ArticleId = al.Id;
                    articleGroup_List = ag.LoadDataByArticleIdExceptSlideShow();

                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";

                    for (int j = 0; j < list_likedislike.Count; j++)
                    {
                        if ((list_likedislike[j]).ArticleId == (ArticleList[i]).Id)
                        {
                            likesCount = likesCount + (list_likedislike[j]).LikeCount;
                            dislikesCount = dislikesCount + (list_likedislike[j]).DislikeCount;
                            if ((list_likedislike[j]).UserIp == ipaddress)
                            {
                                ip = (list_likedislike[j]).UserIp;
                            }
                            else
                                ip = "";
                        }
                    }
                    LikeDislikeArticle ldarticle = new LikeDislikeArticle
                    {
                        ArticleId = (ArticleList[i]).Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    ArticleComment comment = new ArticleComment();
                    comment.ArticleId = al.Id;
                    IList list = comment.LoadDataByArticleId();
                    List<ArticleComment> commentList = new List<ArticleComment>();
                    foreach (ArticleComment a in list)
                    {
                        commentList.Add(a);
                    }
                    commentsCount = list.Count;

                    ArticleGalary galary = new ArticleGalary();
                    galary.ArticleId = al.Id;
                    IList galaryList = galary.LoadDataByArticleId();

                    if (galaryList.Count > 0)
                    {
                        for (int j = 0; j < galaryList.Count; j++)
                        {
                            articleDetailGalaryList.Add((ArticleGalary)galaryList[j]);
                        }
                    }
                    ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    {
                        Article = al,
                        LikeDislike = ldarticle,
                        ArticleComments = commentList,
                        Comment = comment,
                        ArticleGalary = articleDetailGalaryList,
                        ArticleGroupsData = articleGroup_List
                    };
                    aaList.Add(aa);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(1, 10);
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;
            ViewBag.ArticleDetailImageGalary = articleDetailGalaryList;
            // ViewBag.StoreDataForSlideShow = StoreSlideShow;
            return articleList;
        }

        private void CreateArticleAllList(int page)
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();
            List<Articles> realArticles = Articles.LoadAllArticlesFromTodayByPageNumber(page);
            for (int i = 0; i < allArticles.Count; i++)
            {
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[i]);
                        }
                    }
                }
            }
            ArticleComment comment = new ArticleComment();
            //  List<ArticleComment> comment_list = comment.LoadDataAll();
            List<RegisterViewModel> li_users = new List<RegisterViewModel>();

            List<ArticleComment> articleComments = new List<ArticleComment>();
            articleComments = comment.LoadDataByArticleId();
            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();

            long articlecount = Articles.GetNumberOfArticles();

            int endpaging = page * 10;
            int startpaging = endpaging - 10;
            if (realArticles.Count < 10)
                endpaging = realArticles.Count;
            int filledIndex = 0;
            for (int i = 0; i < articlecount; i++)
            {
                if (startpaging <= i && i < endpaging && i < realArticles.Count)
                {
                    articleGroup.ArticleId = (realArticles[i]).Id;
                    for (int j = 0; j < galaryList.Count; j++)
                    {
                        if (galaryList[j].ArticleId == allArticles[i].Id)
                            articleGalaryList.Add(galaryList[j]);
                    }
                    Articles al = allArticles[filledIndex++];
                    List<ArticleGroups> articleGroups = new List<ArticleGroups>();
                    for (int j = 0; j < articleGroup_List.Count; j++)
                    {
                        if (articleGroup_List[j].ArticleId == al.Id)
                        {
                            articleGroups.Add(articleGroup_List[j]);
                        }
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    for (int j = 0; j < list_likedislike.Count; j++)
                    {
                        if (list_likedislike[j].ArticleId == al.Id)
                        {
                            likesCount = likesCount + list_likedislike[j].LikeCount;
                            dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                            if (list_likedislike[j].UserIp == ipaddress)
                            {
                                ip = list_likedislike[j].UserIp;
                            }
                            else { ip = ""; }
                        }
                    }
                    LikeDislikeArticle ldarticle = new LikeDislikeArticle
                    {
                        ArticleId = (allArticles[i]).Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    //for (int j = 0; j < comment_list.Count; j++)
                    //{
                    //    if (comment_list[j].ArticleId == al.Id)
                    //    {
                    //        articleComments.Add(comment_list[j]);
                    //    }
                    //}
                    commentsCount = articleComments.Count;


                    string[] names = null;
                    if (!string.IsNullOrEmpty(al.FileName))
                        al.FileName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] types = null;
                    if (!string.IsNullOrEmpty(al.FileType))
                        al.FileType.Split(new char[] { '%' }, StringSplitOptions.RemoveEmptyEntries);
                    Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
                    if (names != null && types != null &&
                            names.Length == types.Length)
                    {
                        for (int index = 0; index < types.Length; index++)
                        {
                            if (result.ContainsKey(types[index]))
                            {
                                result[types[index]].Add(names[index]);
                            }
                            else
                            {
                                List<string> temp = new List<string>();
                                temp.Add(names[index]);
                                result.Add(types[index], temp);
                            }
                        }
                    }
                    else
                    {
                        if (names != null && (types == null || types.Length == 0))
                        {
                            List<string> temp = new List<string>();
                            temp.AddRange(names);
                            result.Add("", temp);
                        }
                    }

                    ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    {
                        Article = al,
                        LikeDislike = ldarticle,
                        ArticleComments = articleComments,
                        Comment = comment,
                        ArticleGalary = articleGalaryList,
                        ArticleGroupsData = articleGroups
                    };
                    aaList.Add(aa);
                }
                else
                {
                    //ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    //{
                    //    //Article = null,
                    //    //LikeDislike = null,
                    //    //ArticleComments = null,
                    //    //Comment = null,
                    //    //ArticleGalary = null,
                    //    //ArticleGroupsData = null
                    //};
                    aaList.Add(null);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(page, 10); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;


        }


        private void CreateArticleListByGroupId(IList ArticleList, List<ArticleGroups> articleGroup_List, ObjectId groupid, int page)
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            ArticleGroups ag = new ArticleGroups();
            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            List<ArticleGroups> ArticleGroupslist = new List<ArticleGroups>();
            ArticleGroups articleGroup = new ArticleGroups();
            ArticleGroupslist = articleGroup.LoadDataAll();


            List<Articles> allArticles = Articles.LoadAllArticlesFromTodayByPageNumber(page);

            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            ArticleComment comment = new ArticleComment();
            List<ArticleComment> commentList = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();
            for (int g = 0; g < allArticles.Count; g++)
            {
                articleGroup.ArticleId = allArticles[g].Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[g].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[g]);
                        }
                    }
                }
                for (int j = 0; j < galaryList.Count; j++)
                {
                    if (galaryList[j].ArticleId == allArticles[g].Id)
                        articleGalaryList.Add(galaryList[j]);
                }
            }
            if (ArticleList.Count > 0)
            {
                ag = new ArticleGroups();
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = (Articles)ArticleList[i];
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    if (articleGroup_List.Count > 0)
                    {
                        for (int j = 0; j < list_likedislike.Count; j++)
                        {
                            if (list_likedislike[j].ArticleId == al.Id)
                            {
                                likesCount = likesCount + list_likedislike[j].LikeCount;
                                dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                                if (list_likedislike[j].UserIp == ipaddress)
                                {
                                    ip = list_likedislike[j].UserIp;
                                }
                                else { ip = ""; }
                            }
                        }
                    }
                    LikeDislikeArticle ldarticle = new LikeDislikeArticle
                    {
                        ArticleId = ((Articles)ArticleList[i]).Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    for (int j = 0; j < commentList.Count; j++)
                    {
                        if (commentList[j].ArticleId == al.Id)
                            articleComments.Add(commentList[j]);
                    }
                    commentsCount = articleComments.Count;
                    List<ArticleGroups> al_ArticleGroup = new List<ArticleGroups>();
                    for (int r = 0; r < articleGroup_List.Count; r++)
                    {
                        if (articleGroup_List[r].ArticleId == al.Id)
                        {
                            al_ArticleGroup.Add(articleGroup_List[r]);
                        }
                    }
                    ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    {
                        Article = al,
                        LikeDislike = ldarticle,
                        ArticleComments = articleComments,
                        Comment = comment,
                        ArticleGalary = articleGalaryList,
                        ArticleGroupsData = al_ArticleGroup
                    };

                    aaList.Add(aa);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(page, 10); // will only contain 25 products max because of the pageSizes
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;
            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
        }


        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }

    }

}