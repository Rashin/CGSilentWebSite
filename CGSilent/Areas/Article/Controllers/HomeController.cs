﻿using CGSilent.Models;
using MongoDB.Bson;
using MvcPaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CGSilent.Areas.Article.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        { 
            return View("Index"); 
        }

        [ChildActionOnly]
        public ActionResult SlideShow()
        {
            Articles al = new Articles();
            string SlideId = "5a526a33e290b819dc102a35";

            return PartialView("~/Views/Shared/_SlidShow.cshtml", LoadSliderArticles(SlideId));
        }

        [ChildActionOnly]
        public ActionResult HomeArticles()
        {
            Articles art = new Articles();
            IList il = art.LoadAllArticlesFromToday();
            ArticleListComplete List_Article = CreateArticleAllList(il);
            return PartialView("~/Views/Shared/_HomeArticles.cshtml", List_Article);
        }

        [ChildActionOnly]
        public ActionResult ArticleImagesGalary()
        {
            Articles art = new Articles();
            IList il = art.LoadAllArticlesFromToday();
            ArticleListComplete List_Article = CreateArticleAllList(il);
            return PartialView("~/Views/Shared/_ArticleImagesGalary.cshtml", List_Article);
        }


        [ChildActionOnly]
        public ActionResult ArticleGroups()
        {
            Group gp = new Group();
            IList il_gp = gp.LoadDataAll();
            List<Group> list_Group = new List<Group>();
            for (int i = 0; i < il_gp.Count; i++)
            {
                list_Group.Add((Group)il_gp[i]);
            }
            return PartialView("~/Views/Shared/_ArticleGroups.cshtml", list_Group);
        }

        private ArticleListComplete CreateArticleAllList(IList ArticleList)
        {
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            IList list_likedislike = likedislike.LoadDataAll();

            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = (Articles)ArticleList[i];

                    List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
                    ArticleGroups ag = new ArticleGroups();
                    ag.ArticleId = al.Id;
                    IList list_Group = ag.LoadDataByArticleId();
                    for (int j = 0; j < list_Group.Count; j++)
                    {
                        articleGroup_List.Add((ArticleGroups)list_Group[j]);
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";

                    for (int j = 0; j < list_likedislike.Count; j++)
                    {
                        if (((LikeDislikeArticle)list_likedislike[j]).ArticleId == ((Articles)ArticleList[i]).Id)
                        {
                            likesCount = likesCount + ((LikeDislikeArticle)list_likedislike[j]).LikeCount;
                            dislikesCount = dislikesCount + ((LikeDislikeArticle)list_likedislike[j]).DislikeCount;
                            if (((LikeDislikeArticle)list_likedislike[j]).UserIp == ipaddress)
                            {
                                ip = ((LikeDislikeArticle)list_likedislike[j]).UserIp;
                            }
                            else
                                ip = "";
                        }
                    }
                    LikeDislikeArticle ldarticle = new LikeDislikeArticle
                    {
                        ArticleId = ((Articles)ArticleList[i]).Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    ArticleComment comment = new ArticleComment();
                    comment.ArticleId = al.Id;
                    IList list = comment.LoadDataByArticleId();
                    // list.Add(comment);
                    List<ArticleComment> commentList = new List<ArticleComment>();
                    foreach (ArticleComment a in list)
                    {
                        commentList.Add(a);
                    }
                    commentsCount = list.Count;
                    ArticleGalary galary = new ArticleGalary();
                    galary.ArticleId = al.Id;
                    IList galaryList = galary.LoadDataByArticleId();
                    List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
                    for (int j = 0; j < galaryList.Count; j++)
                    {
                        articleGalaryList.Add((ArticleGalary)galaryList[j]);
                    }
                    ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    {
                        Article = al,
                        LikeDislike = ldarticle,
                        ArticleComments = commentList,
                        Comment = comment,
                        ArticleGalary = articleGalaryList,
                        ArticleGroupsData = articleGroup_List
                    };

                    aaList.Add(aa);
                }
            }
            Group group = new Group();
            IList g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            for (int i = 0; i < g_List.Count; i++)
                group_List.Add((Group)g_List[i]);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                GroupList = group_List
                // ArticleSlideShowList = null
            };
            return articleList;
        }


        private List<Articles> LoadSliderArticles(string SliderId)
        {
            Articles art = new Articles();
            IList List_Article = art.LoadAllArticlesFromToday();

            ArticleGroups groups = new ArticleGroups();
            groups.GroupId = ObjectId.Parse(SliderId);
            IList ArticleGroups = groups.LoadDataByObject();

            List<Articles> SliderArticles = new List<Articles>();
            for (int i = 0; i < List_Article.Count; i++)
            {
                for (int j = 0; j < ArticleGroups.Count; j++)
                {
                    if (((Articles)List_Article[i]).Id == ((ArticleGroups)ArticleGroups[j]).ArticleId)
                        SliderArticles.Add((Articles)List_Article[i]);
                }
            }
            return SliderArticles;
        }

        [HttpGet]
        public ActionResult ArticlesByGroup(ObjectId groupid)
        {
            //LoadByGroup
            Articles art = new Articles();
            IList il = art.LoadAllArticlesFromToday();
            // ArticleGroups groups = new ArticleGroups();

            return View("ArticleListByGroup", CreateArticleListByGroupId(il, groupid));
        }

        private ArticleListComplete CreateArticleListByGroupId(IList ArticleList, ObjectId groupid)
        {
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            IList list_likedislike = likedislike.LoadDataAll();

            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    ArticleGroups ag = new ArticleGroups();
                    List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
                    ag.GroupId = groupid;
                    IList list_Group = ag.LoadArticlesByIdAndArticleId();
                    for (int j = 0; j < list_Group.Count; j++)
                    {
                        articleGroup_List.Add((ArticleGroups)list_Group[j]);
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    if (articleGroup_List.Count > 0)
                    {
                        Articles al = (Articles)ArticleList[i];
                        for (int k = 0; k < articleGroup_List.Count; k++)
                        {
                            if (al.Id == articleGroup_List[k].ArticleId)
                            {
                                for (int j = 0; j < list_likedislike.Count; j++)
                                {
                                    if (((LikeDislikeArticle)list_likedislike[j]).ArticleId == ((Articles)ArticleList[i]).Id)
                                    {
                                        likesCount = likesCount + ((LikeDislikeArticle)list_likedislike[j]).LikeCount;
                                        dislikesCount = dislikesCount + ((LikeDislikeArticle)list_likedislike[j]).DislikeCount;
                                        if (((LikeDislikeArticle)list_likedislike[j]).UserIp == ipaddress)
                                        {
                                            ip = ((LikeDislikeArticle)list_likedislike[j]).UserIp;
                                        }
                                        else
                                            ip = "";
                                    }
                                }

                                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                                {
                                    ArticleId = ((Articles)ArticleList[i]).Id,
                                    DislikeCount = dislikesCount,
                                    LikeCount = likesCount,
                                    UserIp = ip
                                };

                                ArticleComment comment = new ArticleComment();
                                comment.ArticleId = al.Id;
                                IList list = comment.LoadDataByArticleId();
                                // list.Add(comment);
                                List<ArticleComment> commentList = new List<ArticleComment>();
                                foreach (ArticleComment a in list)
                                {
                                    commentList.Add(a);
                                }
                                commentsCount = list.Count;

                                ArticleGalary galary = new ArticleGalary();
                                galary.ArticleId = al.Id;
                                IList galaryList = galary.LoadDataByArticleId();
                                List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
                                for (int j = 0; j < galaryList.Count; j++)
                                {
                                    articleGalaryList.Add((ArticleGalary)galaryList[j]);
                                }

                                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                                {
                                    Article = al,
                                    LikeDislike = ldarticle,
                                    ArticleComments = commentList,
                                    Comment = comment,
                                    ArticleGalary = articleGalaryList,
                                    ArticleGroupsData = articleGroup_List
                                };

                                aaList.Add(aa);
                            }
                        }
                    }
                }
            }


            Group group = new Group();
            IList g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            for (int i = 0; i < g_List.Count; i++)
                group_List.Add((Group)g_List[i]);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                GroupList = group_List
                // ArticleSlideShowList = null
            };
            return articleList;
        }


        public class ArticleListComplete
        {
            #region properties

            private List<ArticleWithLikeDislikeAndComments> _ArticleList;

            public List<ArticleWithLikeDislikeAndComments> ArticleList
            {
                get { return _ArticleList; }
                set { _ArticleList = value; }
            }

            private List<Group> _GroupList;

            public List<Group> GroupList
            {
                get { return _GroupList; }
                set { _GroupList = value; }
            }

            #endregion Properties
        }

        private List<ArticleWithLikeDislikeAndComments> LoadAllArticles()
        {
            Articles art = new Articles();
            IList il = art.LoadAllArticlesFromToday();

            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();

            CreateArticleAllList(il, aaList);
            return aaList;
        }

        private void CreateArticleAllList(IList ArticleList, List<ArticleWithLikeDislikeAndComments> aaList)
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            IList list_likedislike = likedislike.LoadDataAll();

            for (int i = 0; i < ArticleList.Count; i++)
            {
                Articles al = (Articles)ArticleList[i];

                int likesCount = 0;
                int dislikesCount = 0;
                int commentsCount = 0;
                string ip = "0";

                for (int j = 0; j < list_likedislike.Count; j++)
                {
                    if (((LikeDislikeArticle)list_likedislike[j]).ArticleId == ((Articles)ArticleList[i]).Id)
                    {
                        likesCount = likesCount + ((LikeDislikeArticle)list_likedislike[j]).LikeCount;
                        dislikesCount = dislikesCount + ((LikeDislikeArticle)list_likedislike[j]).DislikeCount;
                        if (((LikeDislikeArticle)list_likedislike[j]).UserIp == ipaddress)
                        {
                            ip = ((LikeDislikeArticle)list_likedislike[j]).UserIp;
                        }
                        else
                            ip = "";
                    }
                }

                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                {
                    ArticleId = ((Articles)ArticleList[i]).Id,
                    DislikeCount = dislikesCount,
                    LikeCount = likesCount,
                    UserIp = ip
                };

                ArticleComment comment = new ArticleComment();
                comment.ArticleId = al.Id;
                IList list = comment.LoadDataByArticleId();
                // list.Add(comment);
                List<ArticleComment> commentList = new List<ArticleComment>();
                foreach (ArticleComment a in list)
                {
                    commentList.Add(a);
                }
                commentsCount = list.Count;


                ArticleGalary galary = new ArticleGalary();
                galary.ArticleId = al.Id;
                IList galaryList = galary.LoadDataByArticleId();
                List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
                for (int j = 0; j < galaryList.Count; j++)
                {
                    articleGalaryList.Add((ArticleGalary)galaryList[j]);
                }

                List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
                ArticleGroups ag = new ArticleGroups();
                ag.ArticleId = al.Id;
                IList list_Group = ag.LoadDataByArticleId();
                for (int j = 0; j < list_Group.Count; j++)
                {
                    articleGroup_List.Add((ArticleGroups)list_Group[j]);
                }

                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                {
                    Article = al,
                    LikeDislike = ldarticle,
                    ArticleComments = commentList,
                    Comment = comment,
                    ArticleGalary = articleGalaryList,
                    ArticleGroupsData = articleGroup_List
                };

                aaList.Add(aa);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}