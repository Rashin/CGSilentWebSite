﻿using System.Web.Mvc;
using CGSilent.Models;
using System.Collections;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Web;
using System.IO;

namespace CGSilent.Areas.Article.Controllers
{
    public class GroupController : Controller
    {
        // GET: Article/Group
        [HttpGet]
        public ActionResult Index()
        {
            return View("GroupList", LoadData());

        }

        private IList<GroupWithParent> LoadData()
        {
            Group groups = new Group();
            IList list_Group = groups.LoadDataAll();
            List<GroupWithParent> gwp_List = new List<GroupWithParent>();
            if (list_Group.Count > 0)
            {
                for (int i = 0; i < list_Group.Count; i++)
                {
                    GroupWithParent gwp = new GroupWithParent();
                    gwp.Id = ((Group)list_Group[i]).Id;
                    gwp.Name = ((Group)list_Group[i]).Name;
                    gwp.CreatedDate = ((Group)list_Group[i]).CreatedDate;
                    if (((Group)list_Group[i]).ParentId != ObjectId.Empty)
                    {
                        for (int j = 0; j < list_Group.Count; j++)
                        {
                            if (((Group)list_Group[i]).ParentId == ((Group)list_Group[j]).Id)
                                gwp.ParentName = ((Group)list_Group[j]).Name;
                        }
                    }
                    else
                    {
                        gwp.ParentName = "";
                    }
                    gwp_List.Add(gwp);
                }
            }
            return gwp_List;
        }

        public class GroupWithParent
        {
            private ObjectId _Id;

            public ObjectId Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Name;

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            private string _CreatedDate;

            public string CreatedDate
            {
                get { return _CreatedDate; }
                set { _CreatedDate = value; }
            }


            private string _ParentName;

            public string ParentName
            {
                get { return _ParentName; }
                set { _ParentName = value; }
            }


        }

        [HttpGet]
        public ActionResult AddGroup()
        {
            Group groups = new Group();
            IList list_Group = groups.LoadDataAll();
            List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
            SelectListItem sli;

            sli = new SelectListItem();
            sli.Value = ObjectId.Empty.ToString();
            sli.Text = "...";
            sli.Selected = true;
            selectedlist_Group.Add(sli);

            for (int i = 0; i < list_Group.Count; i++)
            {
                if (((Group)list_Group[i]).ParentId == ObjectId.Empty)
                {
                    sli = new SelectListItem();
                    sli.Value = ((Group)list_Group[i]).Id.ToString();
                    sli.Text = ((Group)list_Group[i]).Name;
                    selectedlist_Group.Add(sli);
                }
            }

            GroupForDropDown groupForView = new GroupForDropDown
            {
                Group = groups,
                Groups = selectedlist_Group
            };
            return View("AddGroup", groupForView);
        }


        [HttpPost]
        public ActionResult AddGroup(Group group, HttpPostedFileBase ImageUploader)
        {
            group.CreatedDate = group.UpdateDate = DateTime.Now.ToString();
            if (Request.Files != null && Request.Files.Count > 0)
            {
                HttpPostedFileBase FileData;
                FileData = Request.Files[0];
                if (FileData.ContentLength > 0)
                {
                    UploadFile(ImageUploader);
                    group.ImageName = ImageUploader.FileName;
                }
            }
            group.Insert();
            return View("GroupList", LoadData());

        }

        public void UploadFile(HttpPostedFileBase FileData)
        {

            FileData = Request.Files[0];
            if (FileData.ContentLength > 0)
            {
                string fileName = Path.GetFileName(FileData.FileName);
                if (fileName.Contains("#"))
                {
                    fileName = fileName.Replace("#", "sharp");
                }
                string path = Path.Combine(Server.MapPath("~/Content/Images/ArticleGroup"), fileName);
                if (System.IO.File.Exists(path))
                {
                    int i = 0;
                    while (System.IO.File.Exists(path))
                    {
                        Guid newid = Guid.NewGuid();

                        int index = fileName.LastIndexOf(".");
                        string imagename = fileName.Substring(0, index);
                        imagename = imagename + "_" + newid;
                        fileName = imagename + fileName.Substring(index);
                        path = Path.Combine(Server.MapPath("~/Content/Images/Article"), fileName);
                    }
                }
                FileData.SaveAs(path);

            }

        }

        [HttpGet]
        public ActionResult EditGroup(ObjectId id)
        {
            Group grp = new Group();
            grp.Id = id;
            IList list_Group = grp.LoadDataByObject();

            Group group = new Group();
            IList list_Groupddl = group.LoadDataAll();
            List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
            SelectListItem sli;

            sli = new SelectListItem();
            sli.Value = ObjectId.Empty.ToString();
            sli.Text = "...";

            selectedlist_Group.Add(sli);

            for (int i = 0; i < list_Groupddl.Count; i++)
            {
                if (((Group)list_Groupddl[i]).ParentId == ObjectId.Empty)
                {
                    sli = new SelectListItem();
                    sli.Value = ((Group)list_Groupddl[i]).Id.ToString();
                    sli.Text = ((Group)list_Groupddl[i]).Name;
                    for (int j = 0; j < list_Group.Count; j++)
                    {
                        if (((Group)list_Groupddl[i]).Id == ((Group)list_Group[0]).ParentId)
                            sli.Selected = true;
                    }
                    selectedlist_Group.Add(sli);
                }
            }

            GroupForDropDown groupForView = new GroupForDropDown
            {
                Group = (Group)list_Group[0],
                Groups = selectedlist_Group
            };


            if (groupForView.Group != null)
                return View("EditGroup", groupForView);
            else
                return View("EditGroup");
        }

        [HttpPost]
        public ActionResult EditGroup(Group group, HttpPostedFileBase ImageUploader)
        {
            group.UpdateDate = DateTime.Now.ToString();
            if (Request.Files != null && Request.Files.Count > 0)
            {
                HttpPostedFileBase FileData;
                FileData = Request.Files[0];
                if (FileData.ContentLength > 0)
                {
                    DeleteFile(group.ImageName);
                    UploadFile(ImageUploader);
                    group.ImageName = ImageUploader.FileName;
                }
            }
            group.Update();
            return View("GroupList", LoadData());
        }

        private void DeleteFile(string filename)
        {
            string path = Path.Combine(Server.MapPath(filename));
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return;
        }


        [HttpGet]
        public ActionResult DeleteGroup(ObjectId id)
        {
            Group group = new Group();
            group.Id = id;
            IList list_Group = group.LoadDataByObject();
            if (list_Group.Count > 0)
                return View("DeleteGroup", (Group)list_Group[0]);
            else
                return View("DeleteGroup");
        }

        [HttpPost]
        public ActionResult DeleteGroup(Group group)
        {
            group.DeleteWithAllChildsById();
            return View("GroupList", LoadData());
        }
    }
}