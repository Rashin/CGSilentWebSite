﻿using CGSilent.Models;
using MongoDB.Bson;
using System;
using System.Web.Mvc;

namespace CGSilent.Areas.Article.Controllers
{
    public class DownloadController : Controller
    {
        // GET: Article/Download
        public ActionResult Index()
        {
            return View();
        }

        //[Route("Download")]
        [HttpGet]
        public FileResult Download(string id)
        {
            //var x = Request.UrlReferrer;
            // ViewBag.Message = "Download?=" + id;
            ObjectId oid = ObjectId.Empty;
            if (ObjectId.TryParse(id, out oid))
            {
                if (oid != ObjectId.Empty)
                 {
                    FileMapper fm = new FileMapper() { id = oid };
                    var res = fm.LoadDataByObject();
                    if (res.Count > 0)
                        fm = res[0];
                    if (!string.IsNullOrEmpty(fm.filePath))
                    {                         
                        Response.Charset = "utf-8"; // or other encoding
                        if (fm.expireDate > DateTime.Now.Ticks && System.IO.File.Exists(fm.filePath))
                        {
                            Response.Clear();
                            Response.ClearHeaders();
                            Response.ClearContent();
                            Response.BufferOutput = false;
                            Response.ContentType = fm.contentType;
                            Response.AddHeader("Content-Type", fm.contentType);
                            Response.AddHeader("Content-Disposition", "attachment; filename=" + fm.fileName);
                            Response.AddHeader("Content-Length", new System.IO.FileInfo(fm.filePath).Length.ToString());
                            //Response.TransmitFile(fm.filePath);
                            //Response.Flush();
                            //Response.Close();
                            //Response.End();
                            //return View();
                            return File(fm.filePath, fm.contentType, fm.fileName);
                            // FilePathResult fpr = new FilePathResult(fm.filePath, fm.contentType) { FileDownloadName = fm.fileName };
                        }
                        else
                        {
                            //expire message
                        }
                    }
                }
            }
            //file not exist message
            return null;
        }

        //[HttpGet]
        //public FileResult Download(string id)
        //{
        //    // ViewBag.Message = "Download?=" + id;
        //    ObjectId oid = ObjectId.Empty;
        //    if (ObjectId.TryParse(id, out oid))
        //    {
        //        if (oid != ObjectId.Empty)
        //        {
        //            FileMapper fm = new FileMapper() { id = oid };
        //            var res = fm.LoadDataByObject();
        //            if (res.Count > 0)
        //                fm = (FileMapper)res[0];
        //            if (!string.IsNullOrEmpty(fm.filePath))
        //            {
        //                if (fm.expireDate > DateTime.Now.Ticks)
        //                    return new FilePathResult(fm.filePath, fm.contentType) { FileDownloadName = fm.fileName };
        //                else
        //                {
        //                    //expire message
        //                }
        //            }
        //        }
        //    }
        //    //file not exist message
        //    return null;
        //}


    }
}