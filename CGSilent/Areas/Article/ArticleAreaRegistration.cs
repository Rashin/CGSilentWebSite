﻿using System.Web.Mvc;

namespace CGSilent.Areas.Article
{
    public class ArticleAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Article";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Article_default",
                "Article/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
               "Article_Basket",
               "Article/{controller}/{action}/{id}",
               new { action = "AddBasket", id = UrlParameter.Optional }
           );

           // context.MapRoute(
           //     "GetFile",
           //     "Article/{controller}/{action}/{filepath}",
           //     new { action = "GetFile", filepath = UrlParameter.Optional }
           // );

           // context.MapRoute(
           //    "Download",
           //    "Article/{controller}/{action}/{id}",
           //    new { controller = "Download", action = "Download", id = UrlParameter. Optional}
           //);

            //  context.MapRoute(
            //    "DownloadFile",
            //    "Article/{controller}/{action}/{id}",
            //    new { action = "Download", id = UrlParameter.Optional }
            //);

        }
    }
}