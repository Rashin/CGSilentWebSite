﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CGSilent.Models;
using System.Web.Mvc;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace CGSilent.Areas.Advertizement.Controllers
{
    public class AdvertizementController : Controller
    {
        // GET: Advertizement/Advertizement
        public ActionResult Index()
        {
            List<Advertizements> list_adver = new List<Advertizements>();
            Advertizements ad = new Advertizements();
            list_adver = ad.LoadAllData();
            CreateArticleAllList();
            return View(list_adver);
        }

        [HttpGet]
        public ActionResult AddAdvertizement()
        {
            AdvertizmentWithType adtype = new AdvertizmentWithType();
            adtype.Type = fillTypeDropDown(1);
            adtype.Advertizement = null;
            CreateArticleAllList();
            return View(adtype);
        }

        private List<SelectListItem> fillTypeDropDown(int selecteditem)
        {
            List<SelectListItem> list_Type = new List<SelectListItem>();
            SelectListItem si = new SelectListItem();
            si.Text = "تبلیغات بالا";
            si.Value = "1";
            if (selecteditem == 1)
                si.Selected = true;
            list_Type.Add(si);

            si = new SelectListItem();
            si.Text = "تبلیغات پایین";
            si.Value = "2";
            if (selecteditem == 2)
                si.Selected = true;
            list_Type.Add(si);
            return list_Type;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAdvertizement(AdvertizmentWithType model)
        {
            UploadFile(model.Advertizement);
            model.Advertizement.CreatedDate = DateTime.Now.ToString("s");
            model.Advertizement.UpdateDate = DateTime.Now.ToString("s");
            if (!model.Advertizement.LinkAddress.Contains("http://"))
                model.Advertizement.LinkAddress = "http://" + model.Advertizement.LinkAddress;
            model.Advertizement.Insert();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditAdvertizement(ObjectId id)
        {
            AdvertizmentWithType ad = new AdvertizmentWithType();
            Advertizements adver = new Advertizements();
            adver.Id = id;

            ad.Advertizement = adver.LoadDataById();
            ad.Type = fillTypeDropDown(ad.Advertizement.Type);
            CreateArticleAllList();
            return View(ad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAdvertizement(AdvertizmentWithType model)
        {
            if (model.Advertizement != null)
                DeleteFile(model.Advertizement.ImageUrl);
            UploadFile(model.Advertizement);
            model.Advertizement.UpdateDate = DateTime.Now.ToString("s");
            if (!model.Advertizement.LinkAddress.Contains("http://"))
                model.Advertizement.LinkAddress = "http://" + model.Advertizement.LinkAddress;

            model.Advertizement.Update();
            CreateArticleAllList();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteAdvertizement(ObjectId id)
        {
            Advertizements ad = new Advertizements();
            ad.Id = id;
            ad = ad.LoadDataById();
            CreateArticleAllList();
            return View(ad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAdvertizement(Advertizements model)
        {
            DeleteFile(model.ImageUrl);
            model.DeleteById();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DetailAdvertizement(ObjectId id)
        {
            Advertizements advert = new Advertizements();
            advert.Id = id;
            advert = advert.LoadDataById();
            CreateArticleAllList();
            return View(advert);
        }

        private void CreateArticleAllList()
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<Models.ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll(); 
            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            long articlecount = Articles.GetNumberOfArticles();
            for (int i = 0; i < articlecount; i++)
            {
                articleGroup.ArticleId = (allArticles[i]).Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[i]);
                        }
                    }
                }
            }

            ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
            {
                Article = null,
                LikeDislike = null,
                ArticleComments = null,
                Comment = null,
                ArticleGalary = null,
                ArticleGroupsData = null
            };
            aaList.Add(aa);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }


        private void DeleteFile(string filename)
        {
            if (filename != "")
            {
                HttpPostedFileBase FileData;
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    //Delete ArticleImage
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        FileData = Request.Files[i];
                        if (Request.Files.Keys[i].Contains("Image"))
                        {
                            if (FileData.ContentLength > 0)
                            {
                                string path = Path.Combine(Server.MapPath(filename));
                                if (System.IO.File.Exists(path))
                                    System.IO.File.Delete(path);
                                return;
                            }
                        }
                    }
                }
            }

        }


        public void UploadFile(Advertizements model)
        {
            if (model != null)
            {
                HttpPostedFileBase FileData;
                ObjectId fileId = ObjectId.Empty;
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    FileData = Request.Files[0];
                    if (FileData.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(FileData.FileName);
                        if (fileName.Contains("#"))
                        {
                            fileName = fileName.Replace("#", "sharp");
                        }
                        string path = Path.Combine(Server.MapPath("~/Content/Images/Advertizement"), fileName);
                        if (System.IO.File.Exists(path))
                        {
                            int i = 0;
                            while (System.IO.File.Exists(path))
                            {
                                Guid newid = Guid.NewGuid();

                                int index = fileName.LastIndexOf(".");
                                string imagename = fileName.Substring(0, index);
                                imagename = imagename + "_" + newid;
                                fileName = imagename + fileName.Substring(index);
                                path = Path.Combine(Server.MapPath("~/Content/Images/Advertizement"), fileName);
                            }
                        }
                        FileData.SaveAs(path);
                        //GridFSBucket fs = new GridFSBucket(MongoConnection.instance.database);

                        //fileId = Articles.UploadFile(fs, fileName, path);
                        //model.ImageId = fileId;
                        model.ImageUrl = "/Content/Images/Advertizement/" + fileName;
                    }
                }
            }
        }


        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }


        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}