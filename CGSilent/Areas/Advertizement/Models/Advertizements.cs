﻿using CGSilent.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CGSilent.Models
{
    public class Advertizements
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Title;
        [DefaultValue("")]
        [Required]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _LinkAddress;
        [DefaultValue("")]
        [Required]
        public string LinkAddress
        {
            get { return _LinkAddress; }
            set { _LinkAddress = value; }
        }

        private string _ImageUrl;

        public string ImageUrl
        {
            get { return _ImageUrl; }
            set { _ImageUrl = value; }
        }

        //private ObjectId _ImageId;

        //public ObjectId ImageId
        //{
        //    get { return _ImageId; }
        //    set { _ImageId = value; }
        //}


        private int _Type;
        [DefaultValue(1)]
        [Required]
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private int _IsDeleted;
        [DefaultValue(0)]
        public int IsDeleted
        {
            get { return _IsDeleted; }
            set { _IsDeleted = value; }
        }


        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }


        #endregion Properties


        #region Events

        public List<Advertizements> LoadAllData()
        {
            List<Advertizements> list_adv = new List<Advertizements>();
            IMongoCollection<Advertizements> collection = MongoConnection.instance.GetCollection<Advertizements>("Advertizement");
            list_adv = collection.Find((Builders<Advertizements>.Filter.Empty) &
                (Builders<Advertizements>.Filter.Eq("IsDeleted", 0))).ToList();
            return list_adv;
        }

        internal Advertizements LoadDataById()
        {
            List<Advertizements> list_adv = new List<Advertizements>();
            IMongoCollection<Advertizements> collection = MongoConnection.instance.GetCollection<Advertizements>("Advertizement");
            list_adv = collection.Find((Builders<Advertizements>.Filter.Eq("Id", Id)) &
                (Builders<Advertizements>.Filter.Eq("IsDeleted", 0))).ToList();
            if (list_adv != null)
            {
                if (list_adv.Count > 0)
                {
                    return list_adv[0];
                }
                else
                    return null;
            }
            else
                return null;
        }

        public List<Advertizements> LoadTopFourData()
        {
            List<Advertizements> list_adv = new List<Advertizements>();
            IMongoCollection<Advertizements> collection = MongoConnection.instance.GetCollection<Advertizements>("Advertizement");
            list_adv = collection.Find((Builders<Advertizements>.Filter.Eq("Type", 1)) &
                (Builders<Advertizements>.Filter.Eq("IsDeleted", 0))).Sort(Builders<Advertizements>.Sort.Descending("CreatedDate")).Limit(4).ToList();

            return list_adv;
        }


        public List<Advertizements> LoadTopTwoData()
        {
            List<Advertizements> list_adv = new List<Advertizements>();
            IMongoCollection<Advertizements> collection = MongoConnection.instance.GetCollection<Advertizements>("Advertizement");
            list_adv = collection.Find((Builders<Advertizements>.Filter.Eq("Type", 2)) &
                (Builders<Advertizements>.Filter.Eq("IsDeleted", 0))).Sort(Builders<Advertizements>.Sort.Descending("CreatedDate")).Limit(2).ToList();
            return list_adv;
        }

        public ObjectId Insert()
        {
            IMongoCollection<Advertizements> collection = MongoConnection.instance.GetCollection<Advertizements>("Advertizement");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<Advertizements> collection = MongoConnection.instance.GetCollection<Advertizements>("Advertizement");
            collection.ReplaceOne(Builders<Advertizements>.Filter.Eq("_id", Id), this);
        }

        public void DeleteById()
        {
            IMongoCollection<Advertizements> collection = MongoConnection.instance.GetCollection<Advertizements>("Advertizement");
            var update = Builders<Advertizements>.Update.Set("IsDeleted", 1);
            collection.UpdateOne(Builders<Advertizements>.Filter.Eq("_id", Id), update);
        }

        public static ObjectId UploadFile(GridFSBucket fs, string filename, string path)
        {
            using (var s = File.OpenRead(path))
            {
                var t = Task.Run<ObjectId>(() =>
                {
                    return fs.UploadFromStreamAsync(filename, s);
                });
                return t.Result;
            }

        }

        #endregion Events
    }

    public class AdvertizmentWithType
    {

        private Advertizements _Advertizement;

        public Advertizements Advertizement
        {
            get { return _Advertizement; }
            set { _Advertizement = value; }
        }

        private List<SelectListItem> _Type;

        public List<SelectListItem> Type
        {
            get { return _Type; }
            set { _Type = value; }
        }


    }


}