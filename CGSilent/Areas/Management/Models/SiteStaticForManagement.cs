﻿
using System.Collections.Generic;
using MongoDB.Bson;
using System.Web.Mvc;
using System;
using MongoDB.Driver;
using System.Collections;
using System.Globalization;

namespace CGSilent.Models
{
    public class SiteStaticForManagement
    {
        #region Properties

        private SiteStatics _SiteStaticsInfo;

        public SiteStatics SiteStaticsInfo
        {
            get { return _SiteStaticsInfo; }
            set { _SiteStaticsInfo = value; }
        }

        private UserStatics _UserStaticsInfo;

        public UserStatics UserStaticsInfo
        {
            get { return _UserStaticsInfo; }
            set { _UserStaticsInfo = value; }
        }

        private RequestsStatics _RequestStaticInfo;

        public RequestsStatics RequestStaticInfo
        {
            get { return _RequestStaticInfo; }
            set { _RequestStaticInfo = value; }
        }

        private IncomeStatics _IncomStaticsInfo;

        public IncomeStatics IncomStaticsInfo
        {
            get { return _IncomStaticsInfo; }
            set { _IncomStaticsInfo = value; }
        }

        private ArticlesStatics _ArticleStaticsInfo;

        public ArticlesStatics ArticleStaticsInfo
        {
            get { return _ArticleStaticsInfo; }
            set { _ArticleStaticsInfo = value; }
        }

        private List<SelectListItem> _Time;

        public List<SelectListItem> Time
        {
            get { return _Time; }
            set { _Time = value; }
        }

        private List<SelectListItem> _UserRoles;

        public List<SelectListItem> UserRoles
        {
            get { return _UserRoles; }
            set { _UserRoles = value; }
        }

        private List<SelectListItem> _MaxMin;

        public List<SelectListItem> MaxMin
        {
            get { return _MaxMin; }
            set { _MaxMin = value; }
        }

        private int _MaxMinSelected;

        public int MaxMinSelected
        {
            get { return _MaxMinSelected; }
            set { _MaxMinSelected = value; }
        }

        private DateTime _StartDate;

        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }

        private DateTime _EndDate;

        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }


        private int _SiteViewBetweenDate;

        public int SiteViewBetweenDate
        {
            get { return _SiteViewBetweenDate; }
            set { _SiteViewBetweenDate = value; }
        }


        private string _UserRoleForSiteStatic;

        public string UserRoleForSiteStatic
        {
            get { return _UserRoleForSiteStatic; }
            set { _UserRoleForSiteStatic = value; }
        }

        private string _UserRoleForUserStatic;

        public string UserRoleForUserStatic
        {
            get { return _UserRoleForUserStatic; }
            set { _UserRoleForUserStatic = value; }
        }

        #endregion Propertis

        #region Events

        public IncomeStatics LoadIncomeInfo()
        {
            IncomeStatics ss = new IncomeStatics();
            return ss;
        }

        public RequestsStatics LoadBasketss()
        {
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");
            IList baskets = collection.Find((Builders<BasketModel>.Filter.Empty)).Sort(Builders<BasketModel>.Sort.Descending("CreatedDate")).ToList();

            RequestsStatics rs = new RequestsStatics();

            DateTime today = DateTime.Now;
            today = today.AddHours(23 - today.Hour);
            today = today.AddMinutes(59 - today.Minute);
            DayOfWeek todayX = today.DayOfWeek;

            int addDay = 0;
            if (todayX > DayOfWeek.Friday)
            {
                addDay = 6;
            }
            else
             if (todayX < DayOfWeek.Friday)
            {
                addDay = (DayOfWeek.Friday - todayX);
            }

            DateTime nextFriday = today.AddDays(addDay);
            DateTime prevFriday = nextFriday.AddDays(-7);

            for (int i = 0; i < baskets.Count; i++)
            {
                if (DateTime.Parse(((BasketModel)baskets[i]).CreatedDate) == DateTime.Now)
                    rs.TodayRequests += 1;
                if (DateTime.Parse(((BasketModel)baskets[i]).CreatedDate).Day == DateTime.Now.Day - 1)
                    rs.YesterdayRequests += 1;
                if (DateTime.Parse(((BasketModel)baskets[i]).CreatedDate).Month == DateTime.Now.Month)
                    rs.MonthlyRequests += 1;
                if (prevFriday < DateTime.Parse(((BasketModel)baskets[i]).CreatedDate) ||
                    DateTime.Parse(((BasketModel)baskets[i]).CreatedDate) < nextFriday)
                    rs.WeeklyRequests += 1;
                if (DateTime.Parse(((BasketModel)baskets[i]).CreatedDate).Year == DateTime.Now.Year)
                    rs.YearlyRequests += 1;

            }
            //BasketModel
            return null;
        }

        public UserStatics LoadUsersCount()
        {
            throw new NotImplementedException();
        }

        public SiteStatics LoadSiteViews()
        {
            throw new NotImplementedException();
        }

        public ArticlesStatics LoadArticlesView()
        {
            throw new NotImplementedException();
        }

        #endregion Events
    }

    public class SiteStaticCount
    {
        #region Properties

        private long _SiteViewCount;

        public long SiteViewCount
        {
            get { return _SiteViewCount; }
            set { _SiteViewCount = value; }
        }

        private long _AllUsersount;

        public long AllUsersount
        {
            get { return _AllUsersount; }
            set { _AllUsersount = value; }
        }

        private long _RequestsCount;

        public long RequestsCount
        {
            get { return _RequestsCount; }
            set { _RequestsCount = value; }
        }

        private long _IncomeCount;

        public long IncomeCount
        {
            get { return _IncomeCount; }
            set { _IncomeCount = value; }
        }

        private long _ArticleCount;

        public long ArticleCount
        {
            get { return _ArticleCount; }
            set { _ArticleCount = value; }
        }

        private long _StoreCount;

        public long StoreCount
        {
            get { return _StoreCount; }
            set { _StoreCount = value; }
        }


        private long _BasketCount;

        public long BasketCount
        {
            get { return _BasketCount; }
            set { _BasketCount = value; }
        }


        private long _StoreViewCount;

        public long StoreViewCount
        {
            get { return _StoreViewCount; }
            set { _StoreViewCount = value; }
        }

        private List<SelectListItem> _TimeLimit;

        public List<SelectListItem> TimeLimit
        {
            get { return _TimeLimit; }
            set { _TimeLimit = value; }
        }

        private string _TimeId;

        public string TimeId
        {
            get { return _TimeId; }
            set { _TimeId = value; }
        }

        #endregion Propertis

        #region Events

        public long LoadIncomeInfo()
        {
            long count = 0;
            IncomeStatics ss = new IncomeStatics();
            return count;
        }

        public long LoadIncomeInfo(DateTime startdate, DateTime enddate)
        {
            long count = 0;
            IncomeStatics ss = new IncomeStatics();
            return count;
        }

        public long LoadBasketsCount()
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<BasketModel>("Basket")
                .Count((Builders<BasketModel>.Filter.Empty));

            return count;
        }

        public long LoadBasketsCount(DateTime startdate, DateTime enddate)
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<BasketModel>("Basket")
                .Count((Builders<BasketModel>.Filter.Lte(x => x.CreatedDate, startdate.ToString("s"))) &
                (Builders<BasketModel>.Filter.Gte(x => x.CreatedDate, enddate.ToString("s"))) &
                (Builders<BasketModel>.Filter.Empty));

            return count;
        }

        public long LoadUsersCount()
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<ManageUser>("users")
                .Count((Builders<ManageUser>.Filter.Empty));

            return count;
        }

        public long LoadUsersCount(DateTime startdate, DateTime enddate)
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<RegisterViewModel>("users")
                .Count((Builders<RegisterViewModel>.Filter.Lte(x => x.CreatedDate, startdate.ToString("s"))) &
                (Builders<RegisterViewModel>.Filter.Gte(x => x.CreatedDate, enddate.ToString("s"))) & 
                (Builders<RegisterViewModel>.Filter.Empty));

            return count;
        }

        public long LoadSiteViews()
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics")
                .Count((Builders<SiteStatics>.Filter.Empty));

            return count;
        }

        public long LoadSiteViews(DateTime startdate, DateTime enddate)
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics")
                .Count((Builders<SiteStatics>.Filter.Lte(x => x.CreatedDate, startdate.ToString("s"))) &
                (Builders<SiteStatics>.Filter.Gte(x => x.CreatedDate, enddate.ToString("s"))) & 
                (Builders<SiteStatics>.Filter.Empty));

            return count;
        }

        public long LoadArticlesView()
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics")
                .Count((Builders<SiteStatics>.Filter.Eq(x => x.PageName, "Article")));

            return count;
        }

        public long LoadArticlesView(DateTime startdate, DateTime enddate)
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics")
                .Count((Builders<SiteStatics>.Filter.Lte(x => x.CreatedDate, startdate.ToString("s"))) &
                (Builders<SiteStatics>.Filter.Gte(x => x.CreatedDate, enddate.ToString("s"))) & 
                (Builders<SiteStatics>.Filter.Eq(x => x.PageName, "Article")));

            return count;
        }

        public long LoadStoresView()
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics")
                .Count((Builders<SiteStatics>.Filter.Eq(x => x.PageName, "Store")));

            return count;
        }

        public long LoadStoresView(DateTime startdate, DateTime enddate)
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics")
                .Count((Builders<SiteStatics>.Filter.Lte(x => x.CreatedDate, startdate.ToString("s"))) &
                (Builders<SiteStatics>.Filter.Gte(x => x.CreatedDate, enddate.ToString("s"))) &
                (Builders<SiteStatics>.Filter.Eq(x => x.PageName, "Store")));

            return count;
        }

        #endregion Events
    }

    public class SiteStaticsByRoles
    {
        #region Properties

        private int _TodayView;

        public int TodayView
        {
            get { return _TodayView; }
            set { _TodayView = value; }
        }

        private int _YesterdayView;

        public int YesterdayView
        {
            get { return _YesterdayView; }
            set { _YesterdayView = value; }
        }

        private int _WeeklyView;

        public int WeeklyView
        {
            get { return _WeeklyView; }
            set { _WeeklyView = value; }
        }

        private int _MonthlyView;

        public int MonthlyView
        {
            get { return _MonthlyView; }
            set { _MonthlyView = value; }
        }

        private int _YearView;

        public int YearView
        {
            get { return _YearView; }
            set { _YearView = value; }
        }

        #endregion Properties

    }

    public class UserStatics
    {
        #region Properties

        private int _TodayUserView;

        public int TodayUserView
        {
            get { return _TodayUserView; }
            set { _TodayUserView = value; }
        }

        private int _YesterdayUserView;

        public int YesterdayUserView
        {
            get { return _YesterdayUserView; }
            set { _YesterdayUserView = value; }
        }


        private int _MonthUserView;

        public int MonthUserView
        {
            get { return _MonthUserView; }
            set { _MonthUserView = value; }
        }

        private int _YearUserView;

        public int YearUserView
        {
            get { return _YearUserView; }
            set { _YearUserView = value; }
        }

        private int _AllUserView;

        public int AllUserView
        {
            get { return _AllUserView; }
            set { _AllUserView = value; }
        }

        private int _WeeklyUserView;

        public int WeeklyUserView
        {
            get { return _WeeklyUserView; }
            set { _WeeklyUserView = value; }
        }

        #endregion Properties
    }

    public class RequestsStatics
    {
        #region Properties

        private int _TodayRequests;

        public int TodayRequests
        {
            get { return _TodayRequests; }
            set { _TodayRequests = value; }
        }

        private int _YesterdayRequests;

        public int YesterdayRequests
        {
            get { return _YesterdayRequests; }
            set { _YesterdayRequests = value; }
        }


        private int _WeeklyRequests;

        public int WeeklyRequests
        {
            get { return _WeeklyRequests; }
            set { _WeeklyRequests = value; }
        }

        private int _MonthlyRequests;

        public int MonthlyRequests
        {
            get { return _MonthlyRequests; }
            set { _MonthlyRequests = value; }
        }


        private int _YearlyRequests;

        public int YearlyRequests
        {
            get { return _YearlyRequests; }
            set { _YearlyRequests = value; }
        }

        private int _AllRequests;

        public int AllRequests
        {
            get { return _AllRequests; }
            set { _AllRequests = value; }
        }

        #endregion Peoperties
    }

    public class IncomeStatics
    {
        #region Properties

        private int _TodayIncom;

        public int TodayIncom
        {
            get { return _TodayIncom; }
            set { _TodayIncom = value; }
        }


        private int _YesterdayIncom;

        public int YesterdayIncom
        {
            get { return _YesterdayIncom; }
            set { _YesterdayIncom = value; }
        }

        private int _WeeklyIncom;

        public int WeeklyIncom
        {
            get { return _WeeklyIncom; }
            set { _WeeklyIncom = value; }
        }

        private int _MonthlyIncom;

        public int MonthlyIncom
        {
            get { return _MonthlyIncom; }
            set { _MonthlyIncom = value; }
        }

        private int _YearlyIncom;

        public int YearlyIncom
        {
            get { return _YearlyIncom; }
            set { _YearlyIncom = value; }
        }

        private int _AllIncom;

        public int AllIncom
        {
            get { return _AllIncom; }
            set { _AllIncom = value; }
        }

        #endregion Properties
    }

    public class ArticlesStatics
    {
        #region Properties
        private List<Articles> _ArticleViews;

        public List<Articles> ArticleViews
        {
            get { return _ArticleViews; }
            set { _ArticleViews = value; }
        }

        #endregion Properties
    }

    public class UserRolesStatic
    {
        #region Properties

        private ObjectId _RoleId;

        public ObjectId RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }

        private string _RoleName;

        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }

        #endregion Properties
    }
    public class TimeRange
    {
        #region Properties

        private string _Caption;

        public string Caption
        {
            get { return _Caption; }
            set { _Caption = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        #endregion Properties
    }

    public class MaxMin
    {
        #region Properties

        private int _Caption;

        public int Caption
        {
            get { return _Caption; }
            set { _Caption = value; }
        }

        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        #endregion Properties
    }



}