﻿using System.Web.Mvc;

namespace CGSilent.Areas.Management
{
    public class ManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Management";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Management_default",
                "Management/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "EditVipTime",
                "Management/{controller}/{action}/{id,vip,createddate}",
                new { action = "EditVipTime", id = UrlParameter.Optional, vip = UrlParameter.Optional, createddate = UrlParameter.Optional }
            );


            context.MapRoute(
                "DeleteAcount",
                "Management/{controller}/{action}/{id}",
                new { action = "DeleteAcount", id = UrlParameter.Optional }
            );
           

        }
    }
}