﻿using CGSilent.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Net;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace CGSilent.Areas.Management.Controllers
{
    public class AdminController : Controller
    {
        //----------------------------------------------------- Article Management ---------------------------------------
        #region ArticleManagement

        // GET: Management/Admin
        public ActionResult Index()
        {
            CreateArticleAllList();
            return View("Index");
        }


        private void CreateArticleAllList()
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<Models.ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            long articlecount = Articles.GetNumberOfArticles();
            for (int i = 0; i < articlecount; i++)
            {
                articleGroup.ArticleId = (allArticles[i]).Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[i]);
                        }
                    }
                }
            }

            ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
            {
                Article = null,
                LikeDislike = null,
                ArticleComments = null,
                Comment = null,
                ArticleGalary = null,
                ArticleGroupsData = null
            };
            aaList.Add(aa);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };

            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(1, 10); // will only contain 25 products max because of the pageSize
                                                                        //ViewBag.UsersPageingAndView = onePageOfUsers;
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }

        [HttpGet]
        public ActionResult ArticleByNumber()
        {
            CreateArticleAllList();

            Articles model = new Articles();
            return View("FindArticle", model);
        }

        [HttpGet]
        public ActionResult FindArticleByArticleNumber(Articles model)
        {
            CreateArticleAllList();

            model.ArticleNumber = model.ArticleNumber;
            model = Articles.LoadArticleByNumber();
            return View("FindArticle", model);
        }

        public ActionResult EditArticle(string id)
        {
            CreateArticleAllList();

            Articles article = new Articles();
            article.Id = ObjectId.Parse(id);
            IList list = article.LoadDataByObject();
            if (list.Count > 0)
                article = (Articles)list[0];
            return RedirectToAction("EditArticle", "ArticleAdmin", new { area = "Article", id = article.Id });
        }
        public ActionResult DeleteArticle(string id)
        {
            CreateArticleAllList();

            Articles article = new Articles();
            article.Id = ObjectId.Parse(id);
            IList list = article.LoadDataByObject();
            if (list.Count > 0)
                article = (Articles)list[0];
            return RedirectToAction("DeleteArticle", "ArticleAdmin", new { area = "Article", id = article.Id });
        }

        #endregion ArticleManagement

        // ------------------------------------------------------- Group Admin -------------------------------------------
        #region GroupAdmin
        public ActionResult GroupManagement()
        {
            CreateArticleAllList();
            return View("GroupManagement", LoadGropData());
        }

        public GroupForDropDown LoadGropData()
        {
            Group groups = new Group();
            IList list_Group = groups.LoadDataAll();
            List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
            SelectListItem sli;

            sli = new SelectListItem();
            sli.Value = ObjectId.Empty.ToString();
            sli.Text = "...";
            sli.Selected = true;
            selectedlist_Group.Add(sli);

            for (int i = 0; i < list_Group.Count; i++)
            {
                if (((Group)list_Group[i]).ParentId == ObjectId.Empty)
                {
                    sli = new SelectListItem();
                    sli.Value = ((Group)list_Group[i]).Id.ToString();
                    sli.Text = ((Group)list_Group[i]).Name;
                    selectedlist_Group.Add(sli);
                }
            }

            GroupForDropDown groupForView = new GroupForDropDown
            {
                Group = groups,
                Groups = selectedlist_Group
            };
            CreateArticleAllList();
            return groupForView;
        }

        [HttpPost]
        public ActionResult AddParentGroup(GroupForDropDown groupdata, HttpPostedFileBase ImageUploader)
        {
            groupdata.Group.CreatedDate = groupdata.Group.UpdateDate = DateTime.Now.ToString();
            groupdata.Group.ParentId = ObjectId.Empty;

            if (Request.Files != null && Request.Files.Count > 0)
            {
                HttpPostedFileBase FileData;
                FileData = Request.Files[0];
                if (FileData.ContentLength > 0)
                {
                    UploadFile(ImageUploader);
                    groupdata.Group.ImageName = ImageUploader.FileName;
                }
            }
            groupdata.Group.Insert();

            CreateArticleAllList();
            return RedirectToAction("GroupManagement");
        }


        public void UploadFile(HttpPostedFileBase FileData)
        {

            FileData = Request.Files[0];
            if (FileData.ContentLength > 0)
            {
                string fileName = Path.GetFileName(FileData.FileName);
                if (fileName.Contains("#"))
                {
                    fileName = fileName.Replace("#", "sharp");
                }
                string path = Path.Combine(Server.MapPath("~/Content/Images/ArticleGroup"), fileName);
                if (System.IO.File.Exists(path))
                {
                    int i = 0;
                    while (System.IO.File.Exists(path))
                    {
                        Guid newid = Guid.NewGuid();

                        int index = fileName.LastIndexOf(".");
                        string imagename = fileName.Substring(0, index);
                        imagename = imagename + "_" + newid;
                        fileName = imagename + fileName.Substring(index);
                        path = Path.Combine(Server.MapPath("~/Content/Images/Article"), fileName);
                    }
                }
                FileData.SaveAs(path);

            }

        }


        private void DeleteFile(string filename)
        {
            string path = Path.Combine(Server.MapPath("~/Content/Images/Article"), filename);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            return;
        }


        [HttpPost]
        public ActionResult EditParentGroup(GroupForDropDown groupdata, HttpPostedFileBase ImageUploader1, bool chb_DeleteParentIcon = false)
        {
            Group gp = new Group();
            gp.Id = groupdata.Group.Id;
            gp = gp.LoadDataByObject()[0];
            if (Request.Files != null && Request.Files.Count > 0)
            {
                HttpPostedFileBase FileData;
                FileData = Request.Files[0];
                if (FileData.ContentLength > 0)
                {
                    if (gp.ImageName != null)
                        DeleteFile(gp.ImageName);
                    UploadFile(ImageUploader1);
                    groupdata.Group.ImageName = ImageUploader1.FileName;
                }
                else
                {
                    if (chb_DeleteParentIcon)
                    {
                        DeleteFile(gp.ImageName);
                        groupdata.Group.ImageName = "";
                    }
                }
            }

            groupdata.Group.UpdateDate = DateTime.Now.ToString("s");
            groupdata.Group.UpdateName();
            CreateArticleAllList();

            return RedirectToAction("GroupManagement");
        }

        [HttpPost]
        public ActionResult DeleteParentGroup(GroupForDropDown model, string submitButton)
        {
            if (model.ChangeOrder != null)
            {
                if (model.ChangeOrder == "increase")
                {
                    Group gropChangeOrder = new Group();
                    gropChangeOrder.Id = model.Group.Id;
                    gropChangeOrder.UpdateOrderBy(1, true);
                    return Redirect(Url.Action("GroupManagement"));
                }
                if (model.ChangeOrder == "decrease")
                {
                    Group gropChangeOrder = new Group();
                    gropChangeOrder.Id = model.Group.Id;
                    gropChangeOrder.UpdateOrderBy(-1, true);
                    return Redirect(Url.Action("GroupManagement"));
                }
            }
            else
            {
                List<Group> list_gfdd = new List<Group>();
                Group gfdd = new Group();
                gfdd.Id = model.Group.Id;
                list_gfdd = gfdd.LoadDataByObject();
                if (list_gfdd != null)
                    if (list_gfdd[0].ImageName != null)
                    {
                        string path = Path.Combine(Server.MapPath("~/Content/Images/Article"), list_gfdd[0].ImageName);
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);
                    }
                model.Group.DeleteById();
            }
            CreateArticleAllList();
            return RedirectToAction("GroupManagement");
        }

        public void IncreaseOrder(string id)
        {
            Group gropChangeOrder = new Group();
            gropChangeOrder.Id = ObjectId.Parse(id);
            gropChangeOrder.UpdateOrderBy(1, true);
        }

        public void DecreaseOrder(string id)
        {
            Group gropChangeOrder = new Group();
            gropChangeOrder.Id = ObjectId.Parse(id);
            gropChangeOrder.UpdateOrderBy(-1, true);
        }

        public ActionResult IncreaseOrder(string id, string submitButton)
        {
            switch (submitButton)
            {
                case "up":
                    DecreaseOrder(id);
                    break;
                case "down":
                    IncreaseOrder(id);
                    break;
            }
            return Redirect(Url.Action("GroupManagement"));
        }

        public class GroupWithParent
        {
            private ObjectId _Id;

            public ObjectId Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private string _Name;

            public string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            private DateTime _CreatedDate;

            public DateTime CreatedDate
            {
                get { return _CreatedDate; }
                set { _CreatedDate = value; }
            }


            private string _ParentName;

            public string ParentName
            {
                get { return _ParentName; }
                set { _ParentName = value; }
            }


        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GETGroupsChild(string parentId)
        {
            Group childgroups = new Group();
            childgroups.ParentId = ObjectId.Parse(parentId);
            List<Group> groupList = childgroups.LoadByParent(parentId);

            List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
            SelectListItem sli;
            sli = new SelectListItem();
            sli = new SelectListItem();
            sli.Value = ObjectId.Empty.ToString();
            sli.Text = "...";
            selectedlist_Group.Add(sli);

            for (int i = 0; i < groupList.Count; i++)
            {
                sli = new SelectListItem();
                sli.Value = groupList[i].Id.ToString();
                sli.Text = groupList[i].Name;
                selectedlist_Group.Add(sli);
            }
            return Json(selectedlist_Group, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddGroup(GroupForDropDown groupdata, HttpPostedFileBase ImageUploader2)
        {
            CreateArticleAllList();

            groupdata.Group.CreatedDate = groupdata.Group.UpdateDate = DateTime.Now.ToString();
            groupdata.Group.ParentId = groupdata.Group.ParentId;
            if (Request.Files != null && Request.Files.Count > 0)
            {
                HttpPostedFileBase FileData;
                FileData = Request.Files[0];
                if (FileData.ContentLength > 0)
                {
                    UploadFile(ImageUploader2);
                    groupdata.Group.ImageName = ImageUploader2.FileName;
                }
            }
            groupdata.Group.Insert();
            return RedirectToAction("GroupManagement");
        }

        [HttpPost]
        public ActionResult EditGroup(GroupForDropDown groupdata, HttpPostedFileBase ImageUploader3, bool chb_DeleteIcon = false)
        {
            Group gp = new Group();
            gp.Id = groupdata.Group.Id;
            gp = gp.LoadDataByObject()[0];
            CreateArticleAllList();
            if (Request.Files != null && Request.Files.Count > 0)
            {
                HttpPostedFileBase FileData;
                FileData = Request.Files[0];
                if (FileData.ContentLength > 0)
                {
                    if (gp.ImageName != null)
                        DeleteFile(gp.ImageName);
                    UploadFile(ImageUploader3);
                    groupdata.Group.ImageName = ImageUploader3.FileName;
                }
                else
                {
                    if (chb_DeleteIcon)
                    {
                        DeleteFile(gp.ImageName);
                        groupdata.Group.ImageName = "";
                    }
                }
            }

            groupdata.Group.UpdateDate = DateTime.Now.ToString("s");
            groupdata.Group.UpdateName();
            return RedirectToAction("GroupManagement");
        }

        [HttpPost]
        public ActionResult DeleteGroup(GroupForDropDown groupdata)
        {
            if (groupdata.ChangeOrder != null)
            {
                if (groupdata.ChangeOrder == "increase")
                {
                    Group gropChangeOrder = new Group();
                    gropChangeOrder.Id = groupdata.Group.Id;
                    gropChangeOrder.UpdateOrderBy(1, false);
                    return Redirect(Url.Action("GroupManagement"));
                }
                if (groupdata.ChangeOrder == "decrease")
                {
                    Group gropChangeOrder = new Group();
                    gropChangeOrder.Id = groupdata.Group.Id;
                    gropChangeOrder.UpdateOrderBy(-1, false);
                    return Redirect(Url.Action("GroupManagement"));
                }
            }
            else
            {
                groupdata.Group.DeleteById();
            }
            CreateArticleAllList();
            return RedirectToAction("GroupManagement");
        }

        #endregion GroupManagement

        //------------------------------------------------------- Site Management ----------------------------------------
        #region SiteManagement

        [HttpGet]
        public ActionResult SiteStatics()
        {
            CreateArticleAllList();
            SiteStaticCount steStaticsInfo = LoadDataForStaticsInfo();
            PutDataToDropDowns(steStaticsInfo);
            return View("SiteStatics", steStaticsInfo);
        }

        private void PutDataToDropDowns(SiteStaticCount model)
        {
            List<SelectListItem> list_sli = new List<SelectListItem>();
            SelectListItem slitime = new SelectListItem();

            slitime.Text = "همه";
            slitime.Value = "0";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "امروز";
            slitime.Value = "1";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "هفته ی گذشته";
            slitime.Value = "2";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "ماه گذشته";
            slitime.Value = "3";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "یک سال گذشته";
            slitime.Value = "4";
            list_sli.Add(slitime);

            model.TimeLimit = list_sli;
            model.TimeId = "0";
            model.TimeId = "0";
        }

        private SiteStaticCount LoadDataForStaticsInfo()
        {
            SiteStaticCount ss = new SiteStaticCount();
            ss.SiteViewCount = ss.LoadSiteViews();
            ss.AllUsersount = ss.LoadUsersCount();
            ss.BasketCount = ss.LoadBasketsCount();
            ss.IncomeCount = ss.LoadIncomeInfo();
            ss.ArticleCount = ss.LoadArticlesView();
            ss.StoreViewCount = ss.LoadStoresView();
            return ss;
        }

        private SiteStaticCount LoadDataForStaticsInfoByTime(string TimeId)
        {
            SiteStaticCount ss = new SiteStaticCount();
            ss.TimeId = TimeId;

            DateTime startdate = DateTime.Now;
            DateTime enddate = DateTime.Now;
            DateTime now = DateTime.Now;

            switch (TimeId)
            {
                case "1":
                    {
                        startdate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
                        enddate = new DateTime(now.Year, now.Month, now.Day, 24, 0, 0);
                    }
                    break;
                case "2":
                    {
                        startdate = now;
                        enddate = now.AddDays(-7);
                    }
                    break;
                case "3":
                    {
                        startdate = DateTime.Now;
                        enddate = DateTime.Now.AddMonths(-1);
                    }
                    break;
                case "4":
                    {
                        startdate = DateTime.Now;
                        enddate = DateTime.Now.AddYears(-1);
                    }
                    break;

            }
            ss.SiteViewCount = ss.LoadSiteViews(startdate, enddate);
            ss.AllUsersount = ss.LoadUsersCount(startdate, enddate);
            ss.BasketCount = ss.LoadBasketsCount(startdate, enddate);
            ss.IncomeCount = ss.LoadIncomeInfo(startdate, enddate);
            ss.ArticleCount = ss.LoadArticlesView(startdate, enddate);
            ss.StoreViewCount = ss.LoadStoresView(startdate, enddate);
            return ss;
        }


        [HttpPost]
        public ActionResult FilterSiteStatics(SiteStaticCount model)
        {
            CreateArticleAllList();
            SiteStaticCount steStaticsInfo;
            if (model.TimeId != "0")
                steStaticsInfo = LoadDataForStaticsInfoByTime(model.TimeId);
            else
                steStaticsInfo = LoadDataForStaticsInfo();
            PutDataToDropDowns(steStaticsInfo);
            return View("SiteStatics", steStaticsInfo);
        }

        private List<SelectListItem> LoadMaxMinDropDown()
        {
            List<SelectListItem> selectedlist_MaxMin = new List<SelectListItem>();
            SelectListItem sli_MaxMin;
            sli_MaxMin = new SelectListItem();
            sli_MaxMin.Value = "1";
            sli_MaxMin.Text = "بیشترین";
            sli_MaxMin.Selected = true;
            selectedlist_MaxMin.Add(sli_MaxMin);

            sli_MaxMin = new SelectListItem();
            sli_MaxMin.Value = "2";
            sli_MaxMin.Text = "کمترین";
            selectedlist_MaxMin.Add(sli_MaxMin);
            return selectedlist_MaxMin;
        }

        private List<SelectListItem> LoadDateDropDow()
        {
            List<SelectListItem> selectedlist_Date = new List<SelectListItem>();
            SelectListItem sli_Date;

            sli_Date = new SelectListItem();
            sli_Date.Value = "1";
            sli_Date.Text = "همه";
            sli_Date.Selected = true;
            selectedlist_Date.Add(sli_Date);

            sli_Date = new SelectListItem();
            sli_Date.Value = "2";
            sli_Date.Text = "امروز";
            selectedlist_Date.Add(sli_Date);

            sli_Date = new SelectListItem();
            sli_Date.Value = "3";
            sli_Date.Text = "دیروز";
            selectedlist_Date.Add(sli_Date);

            sli_Date = new SelectListItem();
            sli_Date.Value = "4";
            sli_Date.Text = "این هفته";
            selectedlist_Date.Add(sli_Date);

            sli_Date = new SelectListItem();
            sli_Date.Value = "5";
            sli_Date.Text = "این ماه";
            selectedlist_Date.Add(sli_Date);

            sli_Date = new SelectListItem();
            sli_Date.Value = "6";
            sli_Date.Text = "امسال";
            selectedlist_Date.Add(sli_Date);

            return selectedlist_Date;
        }

        private ArticlesStatics LoadArticlesView()
        {
            //بدست آوردن آمار بازدید از اخبار
            //FilterDefinition<Node> filter2 = Builders<Node>.Filter.Ne("Id", ObjectId.Empty);
            //var result = MongoDbLayer.instance.GetCollection<Node>().Aggregate<Node>()
            //      .Match(filter2)
            //                 .Group(
            //                     x => x.TagsKey,
            //                     g => new
            //                     {
            //                         Id = g.Select(x => x.Id),
            //                         Count = g.Select(
            //                                    x => x.TagsKey
            //                                    ).Count()
            //                     }
            //                 ).ToList();

            //Articles articles= new Articles();
            //IList il_articles = articles.LoadAllArticlesFromToday();
            //List<Articles> artList = new List<Articles>();
            //foreach (Articles art in il_articles)
            //{
            //    artList.Add(art);
            //}

            SiteStatics ss = new SiteStatics();

            // List<SiteStatics> list_siteStatics = ss.LoadArticleViewByDateAndOrder("desc","All");
            ArticlesStatics astatics = new ArticlesStatics();
            return astatics;
        }



        #endregion SiteManagement

        //------------------------------------------------------- User Management ----------------------------------------
        #region ManageVIPUser

        public ActionResult ManageVIPUsers()
        {
            return View("ManageVIPUsers", LoadAllUsersFullInfo());
        }

        public UserVIP LoadAllUsersFullInfo()
        {
            UserVIP vip = new UserVIP();
            UserManagementVIPRole UserManagement = new UserManagementVIPRole();
            List<UserManagementVIPRole> userList = UserManagement.GetAllUsersFullInfo();
            vip.AllData = userList;
            CreateArticleAllList();
            return vip;
        }


        //[HttpPost] 
        //[ValidateAntiForgeryToken]
        //public ActionResult AddVipTime(UserVIP model)//(string id, string vipTime)
        ////public ActionResult AddVipTime(ObjectId id, int vipTime)
        //{
        //    if (model.AllData != null)
        //    {
        //        UserManagementVIPRole UserManagement = new UserManagementVIPRole();
        //        UserManagement.Id = model.Id;//ObjectId.Parse()
        //        //int viptimeEntered;
        //        //int.TryParse(model.VipTime, out viptimeEntered);

        //        UserManagement.VipTime = model.VipTime;
        //        UserManagement.UpdateVipTime();
        //    }
        //    return View("ManageVIPUsers", LoadAllUsersFullInfo());
        //}

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult EditVipTime(ObjectId id, int vip, DateTime createddate)
        {
            UserManagementVIPRole UserManagement = new UserManagementVIPRole();
            UserManagement.Id = id;// ObjectId.Parse(id);
            UserManagement.VipTime = vip;
            UserManagement.CreatedDate = createddate.ToString();
            UserManagement.UpdateVipTime();

            return View("ManageVIPUsers", LoadAllUsersFullInfo());
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteAcount(ObjectId id)
        {
            UserManagementVIPRole UserManagement = new UserManagementVIPRole();
            UserManagement.Id = id;// ObjectId.Parse(id);
            UserManagement.DeleteAcount();
            return View("ManageVIPUsers", LoadAllUsersFullInfo());
        }


        public class UserVIP
        {
            private List<UserManagementVIPRole> _AllData;

            public List<UserManagementVIPRole> AllData
            {
                get { return _AllData; }
                set { _AllData = value; }
            }

            private ObjectId _Id;

            public ObjectId Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private int _VipTime;

            public int VipTime
            {
                get { return _VipTime; }
                set { _VipTime = value; }
            }


        }

        #endregion ManageVIPUser

        //------------------------------------------------------- Basket Management---------------------------------------
        #region BasketManagement

        enum Status { done = 1, doing = 2, notdone = 3 }
        public ActionResult BasketManagement()
        {
            return LoadBasketData();
        }

        private ActionResult LoadBasketData()
        {
            BasketModel info = new BasketModel();
            List<BasketModel> list_Basket = new List<BasketModel>();
            list_Basket = info.LoadAllBaskets();

            //List<BasketModel> list_BasketAccepted = info.LoadBasketsById();

            LoadDetailsOfRequests(info, list_Basket);
            info.UpdateDate = DateTime.Now.ToString();
            CreateArticleAllList();
            info.UpdateLastView();
            return View("BasketManagement", list_Basket);
        }


        public void LoadDetailsOfRequests(BasketModel info, List<BasketModel> list_Basket)
        {
            int basketNotDone = 0;
            int basketDone = 0;
            int newBasket = 0;
            int allBaskets = list_Basket.Count;
            for (int i = 0; i < list_Basket.Count; i++)
            {
                if (list_Basket[i].LastView != null)
                {
                    if (DateTime.Parse(list_Basket[i].LastView) < DateTime.Parse(list_Basket[i].UpdateDate))
                    {
                        newBasket = newBasket + 1;
                    }
                }
                if (list_Basket[i].Status == 0)
                {
                    basketNotDone = basketNotDone + 1;
                }
                if (list_Basket[i].Status == 1)
                {
                    basketDone = basketDone + 1;
                }
            }

            List<SelectListItem> sl = new List<SelectListItem>();
            SelectListItem sli = new SelectListItem();

            sli.Text = "همه";
            sli.Value = "0";
            sli.Selected = true;
            sl.Add(sli);

            sli = new SelectListItem();
            sli.Text = "جدید";
            sli.Value = "1";
            sli.Selected = false;
            sl.Add(sli);

            sli = new SelectListItem();
            sli.Text = "انجام شده";
            sli.Value = "2";
            sli.Selected = false;
            sl.Add(sli);

            sli = new SelectListItem();
            sli.Text = "انجام نشده";
            sli.Value = "3";
            sli.Selected = false;
            sl.Add(sli);

            sli = new SelectListItem();
            sli.Text = "در حال انجام";
            sli.Value = "4";
            sli.Selected = false;
            sl.Add(sli);

            ViewBag.DropDownItems = sl;

            ViewBag.NotDone = basketNotDone;
            ViewBag.Done = basketDone;
            ViewBag.NewBasket = newBasket;
            ViewBag.All = allBaskets;
        }

        public ActionResult LoadByDropDown(string itemSelect)
        {
            BasketFullInfo info = new BasketFullInfo();

            BasketModel basket = new BasketModel();
            List<BasketModel> list_Basket = new List<BasketModel>();
            LoadDetailsOfRequests(basket, list_Basket);

            list_Basket = basket.LoadAllNewBaskets();
            List<BasketModel> Final = new List<BasketModel>();
            if (itemSelect == "0")
            {
                Final = list_Basket;
            }
            else
            {
                for (int i = 0; i < list_Basket.Count; i++)
                {
                    if (itemSelect == "1")
                    {
                        if (DateTime.Parse(list_Basket[i].LastView) < DateTime.Parse(list_Basket[i].UpdateDate))
                        {
                            Final.Add(list_Basket[i]);
                        }
                    }
                    if (itemSelect == "2")
                    {
                        if (list_Basket[i].Status == 1)
                        {
                            Final.Add(list_Basket[i]);
                        }
                    }
                    if (itemSelect == "3")
                    {
                        if (list_Basket[i].Status == 2)
                        {
                            Final.Add(list_Basket[i]);
                        }
                    }
                    if (itemSelect == "4")
                    {
                        if (list_Basket[i].Status == 0)
                        {
                            Final.Add(list_Basket[i]);
                        }
                    }
                }
            }
            CreateArticleAllList();
            return View("BasketManagement", Final);
        }

        [HttpPost]
        public ActionResult EditBasket(string id, int status)
        {
            BasketModel ub = new BasketModel();
            ub.Id = ObjectId.Parse(id);
            ub.Status = status;
            ub.UpdateBasketStatus();
            return LoadBasketData();
        }

        [HttpPost]
        public ActionResult LoadByStatus(string status)
        {
            return LoadByDropDown(status);
        }

        #endregion BasketManagement

        //----------------------------------------------------- Manage Comments ------------------------------------------
        #region Manage Comments

        [HttpGet]
        public ActionResult ManageComment()
        {
            CreateArticleAllList();
            ArticleComment artcom = new ArticleComment();
            List<ArticleComment> arComments = artcom.LoadDataAll();
            return View("ManageComment", arComments);
        }

        [HttpGet]
        public ActionResult EditComment(ObjectId id)
        {
            CreateArticleAllList();
            ArticleComment artcom = new ArticleComment();
            artcom.Id = id;
            List<ArticleComment> arComments = artcom.LoadDataByObject();
            if (arComments != null)
                return View("EditComment", arComments[0]);
            else
                return View("EditComment", null);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditComment(ArticleComment model)
        {
            ArticleComment artcom = new ArticleComment();
            model.UpdateDate = DateTime.Now.ToString("s");
            model.AnswerBy = User.Identity.Name;
            model.Update();
            return RedirectToAction("ManageComment");
        }

        #endregion Manage Comments

        //----------------------------------------------------- MAnage Deleted Articles
        #region Manage Deleted Articles

        [HttpGet]
        public ActionResult DeletedArticles(int? page)
        {
            var pageNumber = page ?? 1;

            Articles art = new Articles();
            CreateArticleAllList(page ?? 1);

            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            UserRoles roles = new UserRoles();
            if (User.Identity.IsAuthenticated)
            {
                roles.Id = ObjectId.Parse(User.Identity.GetUserId());
                roles = roles.LoadDataByUserId();
            }
            else
            {
                roles.Id = ObjectId.Empty;
            }

            //افزودن آمار بازدید از سایت
            if (roles != null)
            {
                //یافتن نام نقش کاربری بریا نمایش در صفحات
                if (User.Identity.IsAuthenticated)
                {
                    for (int i = 0; i < roles.Roles.Length; i++)
                    {
                        if (i == roles.Roles.Length - 1)
                            ViewBag.UserRoles += roles.Roles[i].ToString();
                        else
                            ViewBag.UserRoles += roles.Roles[i].ToString() + ",";
                    }
                }
            }

            LikeDislikeArticle ld = new LikeDislikeArticle();
            if (User.Identity.GetUserId() != "")
                ld.UserIp = User.Identity.GetUserId();
            else
            {
                ld.UserIp = Convert.ToString(ipEntry.AddressList[1]);
            }
            int likesCount = 0;
            likesCount = ld.FavoriteArticlesByUserIpCount();
            ViewBag.LikeCount = likesCount;

            SearchArticle searchmodel = new SearchArticle();
            ViewBag.searchModel = searchmodel;

            if (User.Identity.IsAuthenticated)
            {
                ObjectId userID = ObjectId.Parse(User.Identity.GetUserId());
                UserBasket info = new UserBasket();
                info.Id = userID;
                info.ArticleId = info.LoadArticleBasketByUserId();
                ViewBag.Basket = info.ArticleId.Count + "";

            }
            else
            {
                ViewBag.Basket = "0";
            }
            return View();
        }

        private void CreateArticleAllList(int page)
        {
            CheckVIPDate();
            List<Articles> list_EledtedArticle = Articles.LoadAllDeletedArticlesFromTodayByPageNumber(page);
            //Articles.GetNumberOfDeletedArticles()
            CreateArticleAllList(page, list_EledtedArticle, Articles.DeletedArticleCount());
        }


        private void CreateArticleAllList(int page, List<Articles> displayArticles, long articlecount)
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
            Articles articleAll = new Articles();
            ArticleComment comment = new ArticleComment();
            List<ArticleComment> comment_list = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();
            ArticleGalary galary = new ArticleGalary();


            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            List<Articles> allSlideShowArticle = Articles.LoadArticlesByGroupId(new ObjectId("5a526a33e290b819dc102a35"));
            //Guid newid = Guid.NewGuid();
            //string path = Path.Combine(Server.MapPath("~/Content/Logs"), "fileLOG" + newid);
            //System.IO.File.WriteAllText(path, " ArticleCount : " + displayArticles.Count);

            for (int r = 0; r < displayArticles.Count; r++)
            {
                Articles al = displayArticles[r];

                var gal = galary.LoadByArticleId(al.Id);
                if (gal != null)
                    articleGalaryList.Add(gal);

                List<ArticleGroups> articleGroups = new List<ArticleGroups>();
                for (int j = 0; j < articleGroup_List.Count; j++)
                {
                    if (articleGroup_List[j].ArticleId == al.Id)
                    {
                        articleGroups.Add(articleGroup_List[j]);
                    }
                }
                int likesCount = 0;
                int dislikesCount = 0;
                foreach (var item in likedislike.LoadDataByArticleId(al.Id))
                {
                    likesCount += item.LikeCount;
                    dislikesCount += item.DislikeCount;
                }

                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                {
                    ArticleId = al.Id,
                    DislikeCount = dislikesCount,
                    LikeCount = likesCount,
                    UserIp = ""
                };
                for (int j = 0; j < comment_list.Count; j++)
                {
                    if (comment_list[j].ArticleId == al.Id)
                    {
                        articleComments.Add(comment_list[j]);
                    }
                }
                int commentsCount = articleComments.Count;
                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                {
                    Article = al,
                    LikeDislike = ldarticle,
                    ArticleComments = articleComments,
                    Comment = comment,
                    ArticleGalary = articleGalaryList,
                    ArticleGroupsData = articleGroups
                };
                aaList.Add(aa);
            }

            int articlePerPage = 10;
            for (int i = 0; i < articlecount; i++)
            {
                if (i < (page * articlePerPage))
                {
                    if (i >= ((page - 1) * articlePerPage))
                    {
                    }
                    else
                    {
                        aaList.Insert(0, null);
                    }
                }
                else
                {
                    aaList.Add(null);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = allSlideShowArticle,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(page, articlePerPage); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }


        #endregion Manage Deleted Articles


        //----------------------------------------------------- Manage VIP Plan
        public ActionResult VIPPlan()
        {
            VIPPlan vip = new VIPPlan();
            List<VIPPlan> list_VIP = vip.LoadDataAll();
            CreateArticleAllList();
            return View("ManageVIPPlan", list_VIP);
        }

        [HttpGet]
        public ActionResult AddVIPPlan()
        {
            VIPPlan vip = new VIPPlan();
            CreateArticleAllList();
            return View("AddVIPPlan");
        }

        [HttpPost]
        public ActionResult AddVIPPlan(VIPPlan vipModel)
        {
            vipModel.CreatedDate = vipModel.UpdateDate = DateTime.Now.ToString("s");
            vipModel.Insert();
            vipModel = new VIPPlan();
            List<VIPPlan> list_VIP = vipModel.LoadDataAll();
            CreateArticleAllList();
            return View("ManageVIPPlan", list_VIP);
        }


        public ActionResult DeleteVIPPlan(string id)
        {
            CreateArticleAllList();
            return View("ManageVIPPlan");
        }


        [HttpGet]
        public ActionResult EditVIPPlan(string id)
        {
            VIPPlan vipModel = new VIPPlan();
            vipModel.Id = ObjectId.Parse(id);
            vipModel = vipModel.LoadDataById();
            CreateArticleAllList();
            return View("EditVIPPlan", vipModel);
        }


        [HttpPost]
        public ActionResult EditVIPPlan(VIPPlan vipModel)
        {
            vipModel.UpdateDate = DateTime.Now.ToString("s");
            vipModel.Update();
            CreateArticleAllList();
            List<VIPPlan> list_VIP = vipModel.LoadDataAll();
            return View("ManageVIPPlan", list_VIP);
        }


        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

    }
}