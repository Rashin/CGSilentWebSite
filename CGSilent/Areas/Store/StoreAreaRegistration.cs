﻿using System.Web.Mvc;

namespace CGSilent.Areas.Store
{
    public class StoreAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Store";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Store_default",
                "Store/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
               "Store_Basket",
               "Store/{controller}/{action}/{id}",
               new { action = "AddBasket", id = UrlParameter.Optional }
           );
               
        }
    }
}