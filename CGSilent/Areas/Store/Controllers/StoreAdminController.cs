﻿using CGSilent.Models;
using System.Collections;
using System.Web.Mvc;
using MongoDB.Bson;
using System;
using System.Web;
using MongoDB.Driver.GridFS;
using System.IO;
using System.Collections.Generic;
using System.Net; 
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using Microsoft.AspNet.Identity;
using PagedList;
using System.Drawing;
using System.Drawing.Imaging;

namespace CGSilent.Areas.Store.Controllers
{
    public class StoreAdminController : Controller
    {
        // GET: Article/Management
        enum StoreimageType { Galary, Image };

        [HttpGet]
        public ActionResult Index(int? page)
        {
            InsertSiteStatic(ObjectId.Empty, ObjectId.Empty, "Store");
            var pageNumber = page ?? 1;
            LoadArticlesAndStoresData(pageNumber);
            return View("StoreList");
        }


        [HttpGet]
        private void InsertSiteStatic(ObjectId ArticleId, ObjectId storeId, string pageName)
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            UserRoles roles = new UserRoles();
            if (User.Identity.IsAuthenticated)
            {
                roles.Id = ObjectId.Parse(User.Identity.GetUserId());
                roles = roles.LoadDataByUserId();
            }
            else
            {
                roles.Id = ObjectId.Empty;
            }
            //افزودن آمار بازدید از سایت
            if (roles != null)
            {
                SiteStatics ss = new SiteStatics();
                ss.CreatedDate = DateTime.Now.ToString("s");
                ss.PageName = pageName;
                ss.ArticleId = ArticleId;
                ss.StoreId = storeId;
                if (User.Identity.IsAuthenticated)
                    ss.UserIdOrIp = User.Identity.GetUserId();
                else
                    ss.UserIdOrIp = Convert.ToString(ipEntry.AddressList[1]);

                if (roles.Roles != null)
                    for (int i = 0; i < roles.Roles.Length; i++)
                    {
                        ss.RoleName = roles.Roles[i];
                        ss.Insert();
                    }
                else
                {
                    ss.Insert();
                }
            }
        }

        #region LayoutFunctions

        [ChildActionOnly]
        public ActionResult SlideShow()
        {
            Articles al = new Articles();
            string SlideId = "5a526a33e290b819dc102a35";
            return PartialView("~/Views/Shared/_SlidShow.cshtml", LoadSliderArticles(SlideId));
        }

        [ChildActionOnly]
        public ActionResult HomeArticles(int? page)
        {
            InsertSiteStatic(ObjectId.Empty, ObjectId.Empty, "HomeStore");
            var pageNumber = page ?? 1;
            LoadArticlesAndStoresData(pageNumber);
            return PartialView("~/Views/Shared/_HomeArticles.cshtml");
        }
        //private void CreateArticleAllList(int page,   long articlecount)
        //{
        //    List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
        //    string strHostName = "";
        //    strHostName = Dns.GetHostName();
        //    IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
        //    string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

        //    LikeDislikeArticle likedislike = new LikeDislikeArticle();

        //    Group group = new Group();
        //    List<Group> g_List = group.LoadDataAll();
        //    List<Group> group_List = new List<Group>();

        //    ArticleGroups ag = new ArticleGroups();
        //    List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
        //    articleGroup_List = ag.LoadDataAll();

        //    List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
        //    for (int i = 0; i < list_Group.Count; i++)
        //    {
        //        if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
        //        {
        //            list_Group.RemoveAt(i);
        //            i = i - 1;
        //        }
        //    }
        //    for (int i = 0; i < g_List.Count; i++)
        //    {
        //        group_List.Add(g_List[i]);
        //    }

        //    List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
        //    Articles articleAll = new Articles();
        //    ArticleComment comment = new ArticleComment();
        //    List<ArticleComment> comment_list = comment.LoadDataAll();
        //    List<ArticleComment> articleComments = new List<ArticleComment>();
        //    ArticleGalary galary = new ArticleGalary();


        //    //------------------------------ تبلیغات  ------------------
        //    Advertizements advertizement = new Advertizements();
        //    List<Advertizements> advertizementTopData = new List<Advertizements>();
        //    advertizementTopData = advertizement.LoadTopFourData();
        //    List<Advertizements> advertizementBottomData = new List<Advertizements>();
        //    advertizementBottomData = advertizement.LoadTopTwoData();
        //    //----------------------------------------------------------

        //    List<Articles> allSlideShowArticle = Articles.LoadArticlesByGroupId(new ObjectId("5a526a33e290b819dc102a35"));

        //    for (int r = 0; r < displayArticles.Count; r++)
        //    {
        //        Articles al = displayArticles[r];

        //        var gal = galary.LoadByArticleId(al.Id);
        //        if (gal != null)
        //            articleGalaryList.Add(gal);

        //        List<ArticleGroups> articleGroups = new List<ArticleGroups>();
        //        for (int j = 0; j < articleGroup_List.Count; j++)
        //        {
        //            if (articleGroup_List[j].ArticleId == al.Id)
        //            {
        //                articleGroups.Add(articleGroup_List[j]);
        //            }
        //        }
        //        int likesCount = 0;
        //        int dislikesCount = 0;
        //        foreach (var item in likedislike.LoadDataByArticleId(al.Id))
        //        {
        //            likesCount += item.LikeCount;
        //            dislikesCount += item.DislikeCount;
        //        }

        //        LikeDislikeArticle ldarticle = new LikeDislikeArticle
        //        {
        //            ArticleId = al.Id,
        //            DislikeCount = dislikesCount,
        //            LikeCount = likesCount,
        //            UserIp = ""
        //        };
        //        for (int j = 0; j < comment_list.Count; j++)
        //        {
        //            if (comment_list[j].ArticleId == al.Id)
        //            {
        //                articleComments.Add(comment_list[j]);
        //            }
        //        }
        //        int commentsCount = articleComments.Count;
        //        ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
        //        {
        //            Article = al,
        //            LikeDislike = ldarticle,
        //            ArticleComments = articleComments,
        //            Comment = comment,
        //            ArticleGalary = articleGalaryList,
        //            ArticleGroupsData = articleGroups
        //        };
        //        aaList.Add(aa);
        //    }
        //    int articlePerPage = 10;
        //    for (int i = 0; i < articlecount; i++)
        //    {
        //        if (i < (page * articlePerPage))
        //        {
        //            if (i >= ((page - 1) * articlePerPage))
        //            {
        //            }
        //            else
        //            {
        //                aaList.Insert(0, null);
        //            }
        //        }
        //        else
        //        {
        //            aaList.Add(null);
        //        }
        //    }
        //    ArticleListComplete articleList = new ArticleListComplete
        //    {
        //        ArticleList = aaList,
        //        AdvertizementTop = advertizementTopData,
        //        AdvertizementBottom = advertizementBottomData,
        //        GroupList = group_List,
        //        ArticleSlideShowList = allSlideShowArticle,
        //        TreeGroup = list_Group
        //    };
        //    IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
        //    var onePageOfProducts = IQ_List_Article.ToPagedList(page, articlePerPage); // will only contain 25 products max because of the pageSize
        //    ViewBag.ArticleDataForPageingAndView = onePageOfProducts;

        //    Stores st = new Stores();
        //    List<Stores> StoreSlideShow = new List<Stores>();
        //    StoreSlideShow = st.loadTopTwentyStoriesForSlide();
        //    ViewBag.StoreDataForSlideShow = StoreSlideShow;
        //    ViewBag.AllData = articleList;
        //}

        private void LoadArticlesAndStoresData(int pageNumber)
        {
            StoreListComplete storeComplete = new StoreListComplete();
            ArticleListComplete articleList = new ArticleListComplete();
            List<StoreWithLikeDislikeAndComments> storeWithCommentsData = new List<StoreWithLikeDislikeAndComments>();
           // List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            List<Stores> list_StoresForSlideShow = new List<Stores>();

            LayoutLoader.CreateArticlesAndStoresData( 
                out storeWithCommentsData,
              
                out articleList,
                out list_StoresForSlideShow
                , Stores.LoadAllStoresFromTodayByPageNumber(pageNumber), Stores.GetNumberOfStores());

           // IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Articles = aaList.AsQueryable();
            IQueryable<StoreWithLikeDislikeAndComments> IQ_List_Stores = storeWithCommentsData.AsQueryable();
            // var onePageOfArticles = IQ_List_Articles.ToPagedList(pageNumber, 10); // will only contain 25 products max because of the pageSize
            var onePageOfProducts = IQ_List_Stores.ToPagedList(pageNumber, 10); // will only contain 25 products max because of the pageSize

            // ViewBag.ArticleDataForPageingAndView = onePageOfArticles;
            ViewBag.StoreDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;
            ViewBag.StoreDataForSlideShow = list_StoresForSlideShow;


            // CreateArticleAllList(pageNumber, Stores.LoadAllStoresFromTodayByPageNumber(pageNumber), Stores.GetNumberOfStores()  );
        }

        //private void CreateArticleAllList(int page, List<Stores> displayArticles, long articlecount)
        //{
        //    List<StoreWithLikeDislikeAndComments> aaList = new List<StoreWithLikeDislikeAndComments>();
        //    string strHostName = "";
        //    strHostName = Dns.GetHostName();
        //    IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
        //    string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

        //    LikeDislikeStore likedislike = new LikeDislikeStore();

        //    Group group = new Group();
        //    List<Group> g_List = group.LoadDataAll();
        //    List<Group> group_List = new List<Group>();

        //    StoreGroups ag = new StoreGroups();
        //    List<StoreGroups> articleGroup_List = new List<StoreGroups>();
        //    articleGroup_List = ag.LoadDataAll();

        //    List<Group> list_Group = ag.LoadDataAllGroups();
        //    for (int i = 0; i < list_Group.Count; i++)
        //    {
        //        if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
        //        {
        //            list_Group.RemoveAt(i);
        //            i = i - 1;
        //        }
        //    }
        //    for (int i = 0; i < g_List.Count; i++)
        //    {
        //        group_List.Add(g_List[i]);
        //    }

        //    List<StoreGalary> articleGalaryList = new List<StoreGalary>();
        //    Stores articleAll = new Stores();
        //    StoreComment comment = new StoreComment();
        //    List<StoreComment> comment_list = comment.LoadDataAll();
        //    List<StoreComment> articleComments = new List<StoreComment>();
        //    StoreGalary galary = new StoreGalary();


        //    //------------------------------ تبلیغات  ------------------
        //    Advertizements advertizement = new Advertizements();
        //    List<Advertizements> advertizementTopData = new List<Advertizements>();
        //    advertizementTopData = advertizement.LoadTopFourData();
        //    List<Advertizements> advertizementBottomData = new List<Advertizements>();
        //    advertizementBottomData = advertizement.LoadTopTwoData();
        //    //----------------------------------------------------------

        //    List<Articles> allSlideShowArticle = Articles.LoadArticlesByGroupId(new ObjectId("5a526a33e290b819dc102a35"));

        //    for (int r = 0; r < displayArticles.Count; r++)
        //    {
        //        Stores al = displayArticles[r];

        //        var gal = galary.LoadById(al.Id);
        //        if (gal != null)
        //            articleGalaryList.Add(gal);

        //        List<StoreGroups> articleGroups = new List<StoreGroups>();
        //        for (int j = 0; j < articleGroup_List.Count; j++)
        //        {
        //            if (articleGroup_List[j].Id  == al.Id)
        //            {
        //                articleGroups.Add(articleGroup_List[j]);
        //            }
        //        }
        //        int likesCount = 0;
        //        int dislikesCount = 0;
        //        foreach (var item in likedislike.LoadDataByStoreId(al.Id))
        //        {
        //            likesCount += item.LikeCount;
        //            dislikesCount += item.DislikeCount;
        //        }

        //        LikeDislikeStore ldarticle = new LikeDislikeStore
        //        {
        //            Id = al.Id,
        //            DislikeCount = dislikesCount,
        //            LikeCount = likesCount,
        //            UserIp = ""
        //        };
        //        for (int j = 0; j < comment_list.Count; j++)
        //        {
        //            if (comment_list[j].Id == al.Id)
        //            {
        //                articleComments.Add(comment_list[j]);
        //            }
        //        }
        //        int commentsCount = articleComments.Count;
        //        StoreWithLikeDislikeAndComments aa = new StoreWithLikeDislikeAndComments
        //        {
        //            Store = al,
        //            LikeDislike = ldarticle,
        //            StoreComments = articleComments,
        //            Comment = comment,
        //            StoreGalary = articleGalaryList,
        //            StoreGroupsData = articleGroups
        //        };
        //        aaList.Add(aa);
        //    }
        //    int articlePerPage = 10;
        //    for (int i = 0; i < articlecount; i++)
        //    {
        //        if (i < (page * articlePerPage))
        //        {
        //            if (i >= ((page - 1) * articlePerPage))
        //            {
        //            }
        //            else
        //            {
        //                aaList.Insert(0, null);
        //            }
        //        }
        //        else
        //        {
        //            aaList.Add(null);
        //        }
        //    }
        //    StoreListComplete articleList = new StoreListComplete
        //    {
        //        StoreList = aaList,
        //        AdvertizementTop = advertizementTopData,
        //        AdvertizementBttom = advertizementBottomData,
        //        GroupList = group_List,
        //        ArticleSlideShowList = allSlideShowArticle,
        //        TreeGroup = list_Group
        //    };
        //    IQueryable<StoreWithLikeDislikeAndComments> IQ_List_Article = articleList.StoreList.AsQueryable();
        //    var onePageOfProducts = IQ_List_Article.ToPagedList(page, articlePerPage); // will only contain 25 products max because of the pageSize
        //    //ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
        //    ViewBag.StoreDataForPageingAndView = onePageOfProducts;

        //    Stores st = new Stores();
        //    List<Stores> StoreSlideShow = new List<Stores>();
        //    StoreSlideShow = st.loadTopTwentyStoriesForSlide();
        //    ViewBag.StoreDataForSlideShow = StoreSlideShow;
        //    ViewBag.AllData = articleList;
        //}

        
        [ChildActionOnly]
        public ActionResult ArticleImagesGalary(int? page)
        {
            var pageNumber = page ?? 1;
            LoadArticlesAndStoresData(pageNumber);
            return PartialView("~/Views/Shared/_ArticleImagesGalary.cshtml");
        }


        [ChildActionOnly]
        public ActionResult ArticleGroups()
        {
            Group gp = new Group();
            IList il_gp = gp.LoadDataAll();
            List<Group> list_Group = new List<Group>();
            for (int i = 0; i < il_gp.Count; i++)
            {
                list_Group.Add((Group)il_gp[i]);
            }
            return PartialView("~/Views/Shared/_ArticleGroups.cshtml", list_Group);
        }

        private List<Articles> LoadSliderArticles(string SliderId)
        { 
            IList List_Article = Articles.LoadAllArticlesFromToday();

            ArticleGroups groups = new ArticleGroups();
            groups.GroupId = ObjectId.Parse(SliderId);
            IList ArticleGroups = groups.LoadDataByObject();

            List<Articles> SliderArticles = new List<Articles>();
            for (int i = 0; i < List_Article.Count; i++)
            {
                for (int j = 0; j < ArticleGroups.Count; j++)
                {
                    if (((Articles)List_Article[i]).Id == ((ArticleGroups)ArticleGroups[j]).ArticleId)
                        SliderArticles.Add((Articles)List_Article[i]);
                }
            }
            return SliderArticles;
        }

        [HttpGet]
        public ActionResult ArticlesByGroup(ObjectId groupid)
        {
            List<Articles> il = LoadAllArticlesByGroupId(groupid);
            ArticleListComplete articleComplete = new ArticleListComplete();
            List<ArticleWithLikeDislikeAndComments> aalist = new List<ArticleWithLikeDislikeAndComments>();
            List<Stores> list_StoresForSlideShow = new List<Stores>();
            LayoutLoader.CreateArticleListByGroupId(il, groupid, articleComplete, aalist, out list_StoresForSlideShow);
            ViewBag.StoreDataForSlideShow = list_StoresForSlideShow;
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = aalist.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(1, 10); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleComplete;
            return View("ArticleListByGroup");
        }

        private List<Articles> LoadAllArticlesByGroupId(ObjectId groupid)
        {
            List<Articles> article = new List<Articles>();
            List<ArticleGroups> agList = new List<Models.ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            ag.GroupId = groupid;
            agList = ag.LoadDataByGroupId();
            if (agList.Count > 0)
            { 
                var list = Articles.LoadAllArticlesFromToday();
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        for (int j = 0; j < agList.Count; j++)
                        {
                            if (((Articles)list[i]).Id == agList[j].ArticleId)
                                article.Add((Articles)list[i]);
                        }
                    }
                }
            }
            return article;
        }


        #endregion LayoutFunctions

        private StoreListComplete CreateStoreListForDetail(IList storeList)
        {
            CheckVIPDate();
            List<StoreWithLikeDislikeAndComments> aaList = new List<StoreWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            IList list_likedislike = likedislike.LoadDataAll();


            Group group = new Group();
            IList g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            List<StoreGroups> storeGroup_List = new List<StoreGroups>();
            StoreGroups sg = new StoreGroups();
            List<GroupStore> list_Group = sg.LoadDataAllGroupsWithStore();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add((Group)g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<StoreGalary> storeDetailGalaryList = new List<StoreGalary>();
            List<StoreGalary> storeGalaryList = new List<StoreGalary>();

            IList ArticleGroupslist;
             
            IList allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            ArticleGroups articleGroup = new ArticleGroups();
            for (int g = 0; g < allArticles.Count; g++)
            {
                articleGroup.ArticleId = ((Articles)allArticles[g]).Id;
                ArticleGroupslist = articleGroup.LoadDataByArticleId();

                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (((ArticleGroups)ArticleGroupslist[r]).GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                    {
                        SlideShowData.Add((Articles)allArticles[g]);
                    } 
                }

                ArticleGalary galary = new ArticleGalary();
                galary.ArticleId = ((Articles)allArticles[g]).Id;
                IList galaryList = galary.LoadDataByArticleId();
                if (galaryList.Count > 0)
                {
                    for (int j = 0; j < galaryList.Count; j++)
                    {
                        storeGalaryList.Add((StoreGalary)galaryList[j]);
                    }
                }
            }

            List<Stores> al_RelatedArticles = new List<Stores>();

            if (storeList.Count > 0)
            {
                for (int i = 0; i < storeList.Count; i++)
                {
                    Stores al = (Stores)storeList[i];
                    string[] str_RelatedStores = al.Tag.Split('،');
                    for (int g = 0; g < str_RelatedStores.Length; g++)
                    {
                        for (int j = 0; j < allArticles.Count; j++)
                        {
                            if (str_RelatedStores[g] == ((Stores)allArticles[j]).Title)
                            {
                                al_RelatedArticles.Add((Stores)allArticles[j]);
                            }
                        }
                    }
                    ViewBag.RelatedStores = al_RelatedArticles;

                    storeGroup_List = new List<StoreGroups>();
                    sg = new StoreGroups();
                    sg.StoreId = al.Id;
                    storeGroup_List = sg.LoadDataByStoreId();

                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";

                    for (int j = 0; j < list_likedislike.Count; j++)
                    {
                        if (((LikeDislikeStore)list_likedislike[j]).StoreId == ((Stores)storeList[i]).Id)
                        {
                            likesCount = likesCount + ((LikeDislikeStore)list_likedislike[j]).LikeCount;
                            dislikesCount = dislikesCount + ((LikeDislikeStore)list_likedislike[j]).DislikeCount;
                            if (((LikeDislikeStore)list_likedislike[j]).UserIp == ipaddress)
                            {
                                ip = ((LikeDislikeStore)list_likedislike[j]).UserIp;
                            }
                            else
                                ip = "";
                        }
                    }
                    LikeDislikeStore ldarticle = new LikeDislikeStore
                    {
                        StoreId = ((Stores)storeList[i]).Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    StoreComment comment = new StoreComment();
                    comment.StoreId = al.Id;
                    IList list = comment.LoadDataBystoreId();
                    List<StoreComment> commentList = new List<StoreComment>();
                    foreach (StoreComment a in list)
                    {
                        commentList.Add(a);
                    }
                    commentsCount = list.Count;

                    StoreGalary galary = new StoreGalary();
                    galary.StoreId = al.Id;
                    IList galaryList = galary.LoadDataByStoreId();

                    if (galaryList.Count > 0)
                    {
                        for (int j = 0; j < galaryList.Count; j++)
                        {
                            storeDetailGalaryList.Add((StoreGalary)galaryList[j]);
                        }
                    }
                    StoreWithLikeDislikeAndComments aa = new StoreWithLikeDislikeAndComments
                    {
                        Store = al,
                        LikeDislike = ldarticle,
                        StoreComments = commentList,
                        Comment = comment,
                        StoreGalary = storeGalaryList
                    };
                    aaList.Add(aa);
                }
            }
            StoreListComplete storesListFinal = new StoreListComplete
            {
                StoreList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBttom=advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<StoreWithLikeDislikeAndComments> IQ_List_Article = storesListFinal.StoreList.AsQueryable<StoreWithLikeDislikeAndComments>();

            var onePageOfProducts = IQ_List_Article.ToPagedList(1, 10);
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = storesListFinal;
            ViewBag.ArticleDetailImageGalary = storeDetailGalaryList;
            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;

            return storesListFinal;
        }


        private IList LoadStoresByIdAndUpdateViewCout(ObjectId id, int? page)
        {
            Stores srt = new Stores();
            srt.Id = id;
            List<Stores> il = srt.LoadDataByObject();
            if (il.Count > 0)
            {
                srt.ViewCount = (il[0]).ViewCount + 1;
                srt.Update();
            }
            List<StoreWithLikeDislikeAndComments> aaList = new List<StoreWithLikeDislikeAndComments>();
            StoreListComplete ac = new StoreListComplete();
            var pageNumber = page ?? 1;
            LoadArticlesAndStoresData(pageNumber);
            return ac.StoreList;
        }

        [HttpGet]
        public ActionResult AddStore()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                // var roles = await IdentityContext.AllRolesAsync();
                Stores ss = new Stores();
                ss.CreatedDate = DateTime.Now.ToString("s");
                LoadArticlesAndStoresData(1);
                return View(new StoreComplate()
                {
                    store = ss,
                });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ApplicationIdentityContext IdentityContext
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationIdentityContext>();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult AddStore(StoreComplate al_Store, HttpPostedFileBase GalaryUploader)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                Stores store = new Stores();
                store = al_Store.store;
                store.IsDeleted = 0;
                Stores storeLastNumer = new Stores();
                storeLastNumer = storeLastNumer.GetLastStoreNumber();
                if (storeLastNumer.StoreNumber == 0)
                    store.StoreNumber = 1000;
                else
                    store.StoreNumber = storeLastNumer.StoreNumber + 1;

                //--------------------------------------------------------- GetFile Video Addredd -------------------
                if (store.VideoName != null)
                {
                    if (store.VideoName.Length > 0)
                    {
                        string[] str_Video = store.VideoName.Split(',');
                        string videoAddress = store.VideoAddress;

                        videoAddress = videoAddress.ToLower().Replace("\\", "/");
                        videoAddress = videoAddress.ToLower().Replace("d:/fileserver1/", "http://fileserver1.cgsilent.com/");
                        videoAddress = videoAddress.ToLower().Replace("e:/fileserver2/", "http://fileserver2.cgsilent.com/");

                        store.VideoAddress = "";
                        for (int i = 0; i < str_Video.Length; i++)
                        {
                            store.VideoAddress += videoAddress + "/" + str_Video[i];
                            if (!store.VideoAddress.EndsWith("/"))
                            {
                                store.VideoAddress += ",";//article.VideoAddress +
                            }
                            else
                            {
                                store.VideoAddress += ",";//article.VideoAddress +
                            }
                        }
                        store.VideoAddress = store.VideoAddress.Substring(0, store.VideoAddress.Length - 1);
                    }
                }
                else
                {
                    store.VideoAddress = "";
                    store.VideoName = "";
                }
                UploadStoreFile(store);
                if (store.ImagePath != null)
                {
                    store.ViewCount = 0;
                    store.CreatedDate = DateTime.Parse(store.CreatedDate).ToString("s");
                    store.UpdateDate = store.CreatedDate;
                    store.Author = User.Identity.GetUserName();

                    if (store.Tag == null)
                    {
                        store.Tag = "";
                    }
                    if (store.Price.ToString() == null)
                    {
                        store.Price = 0;
                    }
                    if (store.KeyWords == null)
                    {
                        store.KeyWords = "";
                    }
                    store.Insert();
                    GroupStore group = new GroupStore();
                    List<GroupStore> list_GroupStore = group.LoadDataAll();

                    StoreGroups storesGroup = new StoreGroups();
                    if (list_GroupStore.Count > 0)
                    {
                        group = list_GroupStore[0];
                        storesGroup.GroupStoreName ="";
                        storesGroup.GroupStoreId =ObjectId.Empty;
                        storesGroup.StoreId = store.Id;
                        storesGroup.Insert();
                    }
                    if (GalaryUploader != null)
                    {
                        if (GalaryUploader.ContentLength > 0)
                        {
                            UploadStoreGalary(store, GalaryUploader);
                        }
                    }
                }
                else
                {
                    ViewBag.addArticleAlert = "عملیات درج محصول با شکست مواجه شد";
                }
            }
            return RedirectToAction("Index");
        }

        private void UploadStoreGalary(Stores stores, HttpPostedFileBase GalaryUploader)
        {
            if (stores != null)
            {
                HttpPostedFileBase FileData;
                ObjectId fileId = ObjectId.Empty;

                if (GalaryUploader.ContentLength > 0)
                {
                    HttpFileCollectionBase files = Request.Files;
                    StoreGalary gU = new StoreGalary();
                    for (int i = 0; i < files.Count; i++)
                    {
                        if (files.Keys[i].Contains("GalaryUploader"))
                        {
                            FileData = files[i];
                            if (FileData.ContentLength > 0)
                            {
                                string fileName = Path.GetFileName(FileData.FileName);
                                if (fileName.Contains("#"))
                                {
                                    fileName = fileName.Replace("#", "sharp");
                                }
                                string path = Path.Combine(Server.MapPath("~/Content/Images/Store/Galary"), fileName);
                                if (System.IO.File.Exists(path))
                                {
                                    while (System.IO.File.Exists(path))
                                    {
                                        Guid newid = Guid.NewGuid();
                                        int index = fileName.LastIndexOf(".");
                                        string imagename = fileName.Substring(0, index);
                                        imagename = imagename + "_" + newid;
                                        fileName = imagename + fileName.Substring(index);
                                        path = Path.Combine(Server.MapPath("~/Content/Images/Store/Galary"), fileName);
                                    }
                                }
                                Image upImage = Image.FromStream(FileData.InputStream);

                                Image watermarkImage = Image.FromFile(Server.MapPath("~/Content/Images/Article/Galary/watermark_cg.png"));
                                {
                                    MemoryStream stream = new MemoryStream();
                                    upImage.Save(stream, ImageFormat.Bmp);
                                    upImage = Image.FromStream(stream);
                                    Bitmap bmp = bmp = new Bitmap(upImage);
                                    bmp.SetResolution(watermarkImage.HorizontalResolution, watermarkImage.VerticalResolution);
                                    upImage = bmp;
                                }

                                using (Graphics g = Graphics.FromImage(upImage))
                                {
                                    g.DrawImage(watermarkImage, 0, (upImage.Height - watermarkImage.Height));
                                    upImage.Save(Path.Combine(Server.MapPath("~/Content/Images/Store/Galary"), fileName));
                                }
                                //GridFSBucket fs = new GridFSBucket(MongoConnection.instance.database);
                                //fileId = Stores.UploadFile(fs, fileName, path);
                                //gU = new StoreGalary();
                                //gU.ImageId = fileId;

                                gU.StoreId = stores.Id;                                
                                gU.ImagePath = "~/Content/Images/Store/Galary/" + fileName;

                                gU.CreatedDate = DateTime.Now.ToString("s");
                                gU.Insert();
                            }
                        }
                    }
                }
            }
        }


        [HttpGet]
        public ActionResult DetailStore(ObjectId id)
        {
            InsertSiteStatic(ObjectId.Empty, id, "StoreDetail");
            Stores al = new Stores();
            al.Id = id;
            List<Stores> strList = al.LoadDataByObject();
            if (strList.Count > 0)
            {
                al = strList[0];
            }

            //---------------------------------------- Update View Count Of Article------------------------------
            al.ViewCount = al.ViewCount + 1;
            al.Update();
            List<ArticleWithLikeDislikeAndComments> articleList = new List<ArticleWithLikeDislikeAndComments>();
            List<StoreWithLikeDislikeAndComments> storeList = new List<StoreWithLikeDislikeAndComments>();
             List<StoreGalary> storeGalary = new List<StoreGalary>();
            List<Stores> relatedStore = new List<Stores>();
            ArticleListComplete articleListComplete = new ArticleListComplete();
            List<Stores> list_Storeslide = new List<Stores>();

            StoreListComplete storeComplete = new StoreListComplete();
            LayoutLoader.CreateStoreListForDetail(al,out articleList, out storeComplete, out storeGalary, 
                out relatedStore, out storeList,out articleListComplete,out list_Storeslide);
            ViewBag.RelatedStores = relatedStore;
            ViewBag.StoreDataForSlideShow = list_Storeslide;
            // IQueryable<StoreWithLikeDislikeAndComments> IQ_List_Store = storeList.AsQueryable();
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.AsQueryable();
           
           // var onePageOfProducts = IQ_List_Store.ToPagedList(1, 10);
            var onePageOfArticles = IQ_List_Article.ToPagedList(1, 10);

           // ViewBag.StoreDataForPageingAndView = onePageOfProducts;
            ViewBag.ArticleDataForPageingAndView = onePageOfArticles;


            ViewBag.AllData = articleListComplete;
            ViewBag.StoreDetailImageGalary = storeGalary;

            return View("DetailStore", storeComplete);//, CreateStoreListForDetail(alList));
        }


        [HttpPost]
        public ActionResult AddBasket(string id)
        {
            UserBasket basket = new UserBasket();
            basket.StoreId = new List<ObjectId>();
            basket.StoreId.Add(ObjectId.Parse(id));
            basket.Id = ObjectId.Parse(User.Identity.GetUserId());
            basket.UpdateStore();
            ViewBag.Result = "Success";
            Articles al = new Articles();
            al.Id = ObjectId.Parse(id);
            IList alList = al.LoadDataByObject();

            return View("DetailArticle", CreateStoreListForDetail(alList));
        }
 
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Comments(string storeId, string comment)//(ArticleListComplete model)
        {
            StoreComment artc = new StoreComment();

            artc.CommentBody = comment;
            artc.StoreId = ObjectId.Parse(storeId);
            artc.UpdateDate = DateTime.Now.ToString("s");
            artc.UserID = User.Identity.GetUserId();
            artc.UserName = User.Identity.GetUserName();
            List<StoreComment> commentInserted = artc.LoadDataByStoreIdAndUserId();

            string message = "";
            if (commentInserted.Count > 0)
            {
                // ViewBag.commentmessage = "نظر شما قبلا ثبت شده است.";
                message = "نظر شما قبلا ثبت شده است.";
            }
            else
            {
                artc.Insert();
                //  ViewBag.commentmessage = "نظر شما ثبت شد.";
                message = "نظر شما ثبت شد.";
            }
            return Json(message);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AnswerComments(string storeId, string commentId, string answer)//(ArticleListComplete model)
        {
            StoreComment artc = new StoreComment();
            artc.Answer = answer;
            artc.Id = ObjectId.Parse(commentId);
            artc.CreatedDate = DateTime.Now.ToString("s");
            artc.UserID = User.Identity.GetUserId();
            artc.AnswerBy = User.Identity.GetUserName();
            artc.Update();

            Stores al = new Stores();
            al.Id = ObjectId.Parse(storeId);
            IList alList = al.LoadDataByObject();
            if (alList.Count > 0)
            {
                al = (Stores)alList[0];
            }
            return View("DetailStore", CreateStoreListForDetail(alList));
        }


        [HttpGet]
        public ActionResult EditStore(ObjectId id)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                Stores al = new Stores();
                al.Id = id;
                IList il_Article = al.LoadDataByObject();

                if (il_Article.Count > 0)
                {
                    al = (Stores)il_Article[0];
                }
                if (al.VideoName != null)
                {
                    if (al.VideoName != "")
                    {
                        string[] str = al.VideoAddress.Split(',');
                        string videoaddress = str[0];
                        videoaddress = videoaddress.Substring(0, videoaddress.Length - 1);
                        if (str.Length > 0)
                        {
                            int lastvideoindex = videoaddress.LastIndexOf("/");
                            al.VideoAddress = videoaddress.Substring(0, lastvideoindex);
                        }
                    }
                }

                StoreGalary galary = new StoreGalary();
                galary.StoreId = al.Id;
                IList list_Galary = galary.LoadDataByStoreId();
                List<StoreGalary> storeGalary_List = new List<StoreGalary>();
                for (int i = 0; i < list_Galary.Count; i++)
                {
                    storeGalary_List.Add((StoreGalary)list_Galary[i]);
                }
                StoreComplate completeData = new StoreComplate()
                {
                    store = al,
                    Galary = storeGalary_List
                };
                LoadArticlesAndStoresData(1);
                return View("EditStore", completeData);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditStore(StoreComplate model, HttpPostedFileBase GalaryUploader,
           string deleteGalary, bool chb_DeleteGalary = false)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                model.store.UpdateDate = DateTime.Now.ToString("s");
                Stores arm = new Stores();

                Stores editStore = new Stores();
                editStore = model.store;
                editStore.IsDeleted = 0;
                //--------------------------------------------------------- GetFile Video Addredd -------------------
                if (editStore.VideoAddress != null)
                {
                    string[] str_Video = editStore.VideoName.Split(',');
                    string videoAddress = editStore.VideoAddress;

                    videoAddress = videoAddress.ToLower().Replace("\\", "/");
                    videoAddress = videoAddress.ToLower().Replace("d:/fileserver1/", "http://fileserver1.cgsilent.com/");
                    videoAddress = videoAddress.ToLower().Replace("e:/fileserver2/", "http://fileserver2.cgsilent.com/");

                    editStore.VideoAddress = "";
                    for (int i = 0; i < str_Video.Length; i++)
                    {
                        editStore.VideoAddress += videoAddress + "/" + str_Video[i];
                        if (!editStore.VideoAddress.EndsWith("/"))
                        {
                            editStore.VideoAddress += ",";//article.VideoAddress +
                        }
                        else
                        {
                            editStore.VideoAddress += ",";//article.VideoAddress +
                        }
                    }
                    editStore.VideoAddress = editStore.VideoAddress.Substring(0, editStore.VideoAddress.Length - 1);
                }
                else
                {
                    editStore.VideoAddress = "";
                    editStore.VideoName = "";
                }

                arm.Id = editStore.Id;
                IList storeList = arm.LoadDataByObject();

                if (storeList.Count > 0)
                    DeleteFile(((Stores)storeList[0]).ImagePath);
                UploadStoreFile(arm);
                if (arm.ImagePath != null)
                {
                    editStore.ImagePath = arm.ImagePath;
                }

                editStore.Author = User.Identity.GetUserName();

                if (editStore.Tag == null)
                {
                    editStore.Tag = "";
                }
                if (editStore.Price.ToString() == null)
                {
                    editStore.Price = 0;
                }
                if (editStore.KeyWords == null)
                {
                    editStore.KeyWords = "";
                }
                editStore.Update();
                StoreGalary galary = new StoreGalary();
                if (chb_DeleteGalary)
                {
                    galary.StoreId = editStore.Id;
                    List<StoreGalary> galaryList = galary.LoadDataByStoreId();
                    if (galaryList.Count > 0)
                    {
                        for (int i = 0; i < galaryList.Count; i++) 
                            DeleteGalaryFile((galaryList[i]).ImagePath); 
                        galary.DeleteByStoreId();
                    }
                }

                if (GalaryUploader != null)
                {
                    if (GalaryUploader.ContentLength > 0)
                    {
                        UploadStoreGalary(editStore, GalaryUploader);
                    }
                    else
                    {
                        galary = new StoreGalary();
                        galary.StoreId = editStore.Id;
                        List<StoreGalary> list_Galary = new List<StoreGalary>();
                        list_Galary = galary.LoadDataByStoreId(); 
                        for (int i = 0; i < list_Galary.Count; i++) 
                            DeleteGalaryFile( (list_Galary[i]).ImagePath); 
                        galary.DeleteByStoreId();
                    }
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DeleteStore(ObjectId id)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                Stores al = new Stores();
                al.Id = id;
                IList il_Store = al.LoadDataByObject();
                // CreateArticleAllList(null, null, 1);
                LoadArticlesAndStoresData(1);
                if (il_Store.Count > 0)
                    return View("DeleteStore", il_Store[0]);
                else
                    return null;
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult DeleteStore(Stores store)
        {
            store.DeleteById();
            LoadArticlesAndStoresData(1);
            return View("StoreList");
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult LikeStore(ObjectId Id)
        {
            string ipaddress = "";
            if (User.Identity.GetUserId() != "")
            {
                ipaddress = User.Identity.GetUserId();
            }
            else
            {
                string strHostName = "";
                strHostName = Dns.GetHostName();
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                ipaddress = Convert.ToString(ipEntry.AddressList[1]);
            }
            List<StoreWithLikeDislikeAndComments> aaList = new List<StoreWithLikeDislikeAndComments>();
            if (Request.IsAjaxRequest())
            {
                LikeDislikeStore ldc = new LikeDislikeStore();
                ldc.StoreId = Id;

                IList list_LDC = ldc.LoadDataByStoreId();
                int userWithSimilarIp = 0;
                for (int i = 0; i < list_LDC.Count; i++)
                {
                    if ((((LikeDislikeStore)list_LDC[i]).UserIp == ipaddress))
                    {
                        userWithSimilarIp = userWithSimilarIp + 1;
                        ldc = (LikeDislikeStore)list_LDC[i];
                        ldc.DislikeCount = 0;
                        ldc.LikeCount = 1;
                        ldc.UpdateDate = DateTime.Now.ToString("s");
                        ldc.Update();
                    }
                }

                if (ldc.Id == ObjectId.Empty)
                {
                    ldc = new LikeDislikeStore
                    {
                        LikeCount = 1,
                        StoreId = Id,
                        DislikeCount = 0,
                        CreatedDate = DateTime.Now.ToString("s"),
                        UpdateDate = DateTime.Now.ToString("s"),
                        UserIp = ipaddress
                    };
                    ldc.Insert();
                }
            }
            //var pageNumber = page ?? 1;
            
            IList il = Stores.LoadAllStoresFromToday();
            return View("DetailStore", CreateStoreListForDetail(il));
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult DisLikeArticle(ObjectId Id)
        {
            List<StoreWithLikeDislikeAndComments> aaList = new List<StoreWithLikeDislikeAndComments>();
            if (Request.IsAjaxRequest())
            {
                ObjectId userid = ObjectId.Empty;
                string strHostName = "";
                strHostName = Dns.GetHostName();
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

                if (Session["User"] != null)
                {
                    userid = ObjectId.Parse(Session["User"].ToString());
                }

                LikeDislikeStore ldc = new LikeDislikeStore();
                ldc.StoreId = Id;
                IList list_LD = ldc.LoadDataByStoreId();
                for (int i = 0; i < list_LD.Count; i++)
                {
                    if ((((LikeDislikeStore)list_LD[i]).UserIp == ipaddress))
                    {
                        ldc = (LikeDislikeStore)list_LD[i];
                        ldc.DislikeCount = 1;
                        ldc.LikeCount = 0;
                        ldc.UpdateDate = DateTime.Now.ToString("s");
                        ldc.Update();
                    }
                }
                if (ldc.Id == ObjectId.Empty)
                {
                    ldc = new LikeDislikeStore
                    {
                        LikeCount = 0,
                        StoreId = Id,
                        DislikeCount = 1,
                        CreatedDate = DateTime.Now.ToString("s"),
                        UpdateDate = DateTime.Now.ToString("s"),
                        UserIp = ipaddress
                    };
                    ldc.Insert();
                }
            }
            // var pageNumber = page ?? 1;
            
            IList il = Articles.LoadAllArticlesFromToday();
            return View("DetailArticle", CreateStoreListForDetail(il));
        }


        public void UploadStoreFile(Stores store)
        {
            if (store != null)
            {
                HttpPostedFileBase FileData;
                ObjectId fileId = ObjectId.Empty;
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    FileData = Request.Files[0];
                    if (FileData.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(FileData.FileName);
                        if (fileName.Contains("#"))
                        {
                            fileName = fileName.Replace("#", "sharp");
                        }
                        string path = Path.Combine(Server.MapPath("~/Content/Images/Store"), fileName);
                        if (System.IO.File.Exists(path))
                        {
                            int i = 0;
                            while (System.IO.File.Exists(path))
                            {
                                Guid newid = Guid.NewGuid();

                                int index = fileName.LastIndexOf(".");
                                string imagename = fileName.Substring(0, index);
                                imagename = imagename + "_" + newid;
                                fileName = imagename + fileName.Substring(index);
                                path = Path.Combine(Server.MapPath("~/Content/Images/Store"), fileName);
                            }
                        }
                        FileData.SaveAs(path);
                        //GridFSBucket fs = new GridFSBucket(MongoConnection.instance.database);

                        //fileId = Articles.UploadFile(fs, fileName, path);
                        //store.ImageId = fileId;
                        store.ImagePath = "~/Content/Images/Store/" + fileName;
                    }
                }
            }
        }

        public static byte[] DownloadFile(GridFSBucket fs, ObjectId id)//, string fileName)
        {
            byte[] x = fs.DownloadAsBytes(id);
            return x;
        }

        private void DeleteFile(string filename)
        {
            if (filename != "")
            {
                HttpPostedFileBase FileData;
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    //Delete ArticleImage
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        FileData = Request.Files[i];
                        if (Request.Files.Keys[i].Contains("Image"))
                        {
                            if (FileData.ContentLength > 0)
                            {
                                string path = Path.Combine(Server.MapPath(filename));
                                if (System.IO.File.Exists(path))
                                    System.IO.File.Delete(path);
                                return;
                            }
                        }
                    }
                }
            }

        }


        private void DeleteGalaryFile(string filename)
        {
            if (filename != "")
            {
                string path = Path.Combine(Server.MapPath(filename));
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);
                return;
            }
        }
        public void DeleteAllFiles(string fileName)
        {
            if (fileName != "")
            {
                string path = Path.Combine(Server.MapPath(fileName));
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);
            }
        }


        [HttpGet]
        public ActionResult ArticlesByGroup(ObjectId groupid, int? page)
        {
            ViewBag.SelectedGroup = groupid;
            var pageNumber = page ?? 1;
            LoadAllArticlesByGroupId(groupid, pageNumber);
            return View("ArticleFilterGroup");
        }

        private void LoadAllArticlesByGroupId(ObjectId groupid, int pagenumber)
        {
            List<Articles> article = new List<Articles>();
            List<ArticleGroups> agList = new List<ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            ag.GroupId = groupid;

            agList = ag.LoadDataByGroupId();
            if (agList.Count > 0)
            { 
                var list = Articles.LoadAllArticlesFromToday();
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        for (int j = 0; j < agList.Count; j++)
                        {
                            if ((list[i]).Id == agList[j].ArticleId)
                            {
                                article.Add(list[i]);
                                break;
                            }
                        }
                    }
                }
            }
            CreateArticleListByGroupId(article, agList, groupid, pagenumber);
        }

        private void CreateArticleListByGroupId(IList ArticleList, List<ArticleGroups> articleGroup_List, ObjectId groupid, int page)
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            ArticleGroups ag = new ArticleGroups();
            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add((Group)g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();  
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            List<ArticleGroups> ArticleGroupslist = new List<ArticleGroups>();
            ArticleGroups articleGroup = new ArticleGroups();
            ArticleGroupslist = articleGroup.LoadDataAll();
             
            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            ArticleComment comment = new ArticleComment();
            List<ArticleComment> commentList = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();
            for (int g = 0; g < allArticles.Count; g++)
            {
                articleGroup.ArticleId = allArticles[g].Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[g].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[g]);
                        } 
                    }
                }
                for (int j = 0; j < galaryList.Count; j++)
                {
                    if (galaryList[j].ArticleId == allArticles[g].Id)
                        articleGalaryList.Add(galaryList[j]);
                }
            }
            if (ArticleList.Count > 0)
            {
                ag = new ArticleGroups();
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = (Articles)ArticleList[i];
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    if (articleGroup_List.Count > 0)
                    {
                        for (int j = 0; j < list_likedislike.Count; j++)
                        {
                            if (list_likedislike[j].ArticleId == al.Id)
                            {
                                likesCount = likesCount + list_likedislike[j].LikeCount;
                                dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                                if (list_likedislike[j].UserIp == ipaddress)
                                {
                                    ip = list_likedislike[j].UserIp;
                                }
                                else { ip = ""; }
                            }
                        }
                    }
                    LikeDislikeArticle ldarticle = new LikeDislikeArticle
                    {
                        ArticleId = ((Articles)ArticleList[i]).Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    for (int j = 0; j < commentList.Count; j++)
                    {
                        if (commentList[j].ArticleId == al.Id)
                            articleComments.Add(commentList[j]);
                    }
                    commentsCount = articleComments.Count;
                    List<ArticleGroups> al_ArticleGroup = new List<ArticleGroups>();
                    for (int r = 0; r < articleGroup_List.Count; r++)
                    {
                        if (articleGroup_List[r].ArticleId == al.Id)
                        {
                            al_ArticleGroup.Add(articleGroup_List[r]);
                        }
                    }
                    ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    {
                        Article = al,
                        LikeDislike = ldarticle,
                        ArticleComments = articleComments,
                        Comment = comment,
                        ArticleGalary = articleGalaryList,
                        ArticleGroupsData = al_ArticleGroup
                    };

                    aaList.Add(aa);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom=advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(page, 10); // will only contain 25 products max because of the pageSizes
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
        }

        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}