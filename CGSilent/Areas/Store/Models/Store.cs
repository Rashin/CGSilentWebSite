﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Collections;
using MongoDB.Driver;
using System.Web.Mvc;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using MongoDB.Driver.GridFS;

namespace CGSilent.Models
{
    public class Stores : MongoDBModel<Stores>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Author;
        [Display(Name = "نویسنده")]
        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }


        private long _StoreNumber;

        [Display(Name = "شماره محصول")]
        public long StoreNumber
        {
            get { return _StoreNumber; }
            set { _StoreNumber = value; }
        }

        [StringLength(100, ErrorMessage = "The {0} must be at least {6} characters long.", MinimumLength = 6)]
        private string _Title;
        [Required]
        [Display(Name = "تیتر محصول")]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        [StringLength(300, ErrorMessage = "The {0} must be at least {6} characters long.", MinimumLength = 6)]
        private string _Summery;
        [AllowHtml]
        [Display(Name = "خلاصه محصول")]
        [UIHint("Html")]
        [Required]
        public string Summery
        {
            get { return _Summery; }
            set { _Summery = value; }
        }


        private string _Description;
        [AllowHtml]
        [Display(Name = "شرح محصول")]
        [Required]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        private int _IsDeleted;

        public int IsDeleted
        {
            get { return _IsDeleted; }
            set { _IsDeleted = value; }
        }


        //private ObjectId _ImageId;
        //[Required]
        //public ObjectId ImageId
        //{
        //    get { return _ImageId; }
        //    set { _ImageId = value; }
        //}

        private string _ImagePath;
        [Required]
        [Display(Name = "آدرس تصویر خبر")]
        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }

        private string _VideoAddress;
        public string VideoAddress
        {
            get { return _VideoAddress; }
            set { _VideoAddress = value; }
        }

        private string _VideoName;
        [Display(Name = "")]
        public string VideoName
        {
            get { return _VideoName; }
            set { _VideoName = value; }
        }

        private string _Tag;
        [Display(Name = "تگ")]
        public string Tag
        {
            get { return _Tag; }
            set { _Tag = value; }
        }

        private string _KeyWords;
        [Display(Name = "کلمات کلیدی ")]
        public string KeyWords
        {
            get { return _KeyWords; }
            set { _KeyWords = value; }
        }

        private int _ViewCount;
        [Display(Name = "تعداد بازدید")]
        public int ViewCount
        {
            get { return _ViewCount; }
            set { _ViewCount = value; }
        }

        private string _CreatedDate;
        [Display(Name = "تاریخ ایجاد")]
        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;
        [Display(Name = "تاریخ به روز رسانی")]
        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }


        private int _Price;

        [Display(Name = "قیمت")]
        public int Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        #endregion Properties

        #region Events

        public List<Stores> LoadDataByObject()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            var store = collection.Find((Builders<Stores>.Filter.Eq("_id", Id)) &
                (Builders<Stores>.Filter.Eq("IsDeleted", 0))).ToList();
            for (int i = 0; i < store.Count; i++)
            {
                if ((store[i]).Author == null)
                {
                    store.RemoveAt(i);
                    i = i - 1;
                }
            }
            return store;
        }

        public List<Stores> LoadRelatedStores()
        {
            List<Stores> stores_Rel = new List<Stores>();
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            string[] str_Tags;
             
            if (Tag.Contains(','))
            {
                str_Tags = Tag.Split(',');
                stores_Rel = collection.Find((Builders<Stores>.Filter.ElemMatch(b => str_Tags, Title))).ToList();                
            }
            else
                stores_Rel = collection.Find((Builders<Stores>.Filter.Eq("Tag", Title))).ToList();

            for (int i = 0; i < stores_Rel.Count; i++)
            {
                if ((stores_Rel[i]).Author == null)
                {
                    stores_Rel.RemoveAt(i);
                    i = i - 1;
                }
            }

            return stores_Rel;
        }



        public List<Stores> LoadDataByUserId(ObjectId userID)
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.StoreId);
            List<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", userID)) &
                (Builders<UserBasket>.Filter.Eq("IsDeleted", 0))).Project<UserBasket>(fields).ToList();

            IMongoCollection<Stores> collection_Articles = MongoConnection.instance.GetCollection<Stores>("Stores");

            List<Stores> store = new List<Stores>();
            ObjectId id_Stores = ObjectId.Empty;
            if (usersBasket != null)
            {
                if (usersBasket.Count > 0)
                {
                    if (usersBasket[0].ArticleId != null)
                    {
                        for (int i = 0; i < usersBasket[0].StoreId.Count; i++)
                        {
                            id_Stores = usersBasket[0].ArticleId[i];
                            store.Add(collection_Articles.Find((Builders<Stores>.Filter.Eq("_id", id_Stores))).ToList()[0]);
                        }
                    }
                }
            }
            return store;
        }

        internal List<Stores> loadTopTwentyStoriesForSlide()
        { 
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            var c = collection.Count((Builders<Stores>.Filter.Empty));
            var store = collection.Find((Builders<Stores>.Filter.Empty) & (Builders<Stores>.Filter.Eq("IsDeleted", 0)))
                .Sort(Builders<Stores>.Sort.Descending("CreatedDate")).Limit(28).ToList();
            
            return store;
        }

        public static List<Stores> LoadAllStoresFromToday()
        {
            List<Stores> finalStores = new List<Stores>();
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            var c = collection.Count((Builders<Stores>.Filter.Empty));
            var store = collection.Find((Builders<Stores>.Filter.Empty) & (Builders<Stores>.Filter.Eq("IsDeleted", 0)) &
                (Builders<Stores>.Filter.Empty)).Sort(Builders<Stores>.Sort.Descending("CreatedDate")).Limit(6).ToList();
            for (int i = 0; i < store.Count; i++)
            {
                if ((store[i]).Author != null)
                {
                    if (DateTime.Parse((store[i]).CreatedDate) <= DateTime.Now)
                    {
                        finalStores.Add(store[i]);
                    }
                }
            }
            return finalStores;
        }

        public static List<Stores> LoadAllStoresFromTodayByPageNumber(int pageskip)
        {
            int limitrecords = 9;
            int skiprecord = 0;
            if (pageskip == 1)
            {
                skiprecord = 0;
            }
            else
            {
                skiprecord = (pageskip - 1) * 10;
            }
            List<Stores> finalStores = new List<Stores>();
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            var c = collection.Count((Builders<Stores>.Filter.Empty));
            var store = collection.Find((Builders<Stores>.Filter.Empty) & (Builders<Stores>.Filter.Eq("IsDeleted", 0)) &
                (Builders<Stores>.Filter.Empty)).Sort(Builders<Stores>.Sort.Descending("CreatedDate")).Limit(limitrecords).Skip(skiprecord).ToList();
            for (int i = 0; i < store.Count; i++)
            {
                if ((store[i]).Author != null)
                {
                    if (DateTime.Parse((store[i]).CreatedDate) <= DateTime.Now)
                    {
                        finalStores.Add(store[i]);
                    }
                }
            }
            return finalStores;
        }


        public List<Stores> LoadDataAll()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            var store = collection.Find((Builders<Stores>.Filter.Eq("IsDeleted", 0))).Sort(Builders<Stores>.Sort.Descending("CreatedDate")).ToList();

            for (int i = 0; i < store.Count; i++)
            {
                if ((store[i]).Author == null)
                {
                    store.RemoveAt(i);
                    i = i - 1;
                }
            }
            return store;
        }

        public Stores GetLastStoreNumber()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            Stores store = collection.Find(x => true).SortByDescending(d => d.StoreNumber).Limit(1).FirstOrDefault();
            if (store == null)
            {
                store = new Stores();
            }
            return store;
        }

        public ObjectId Insert()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            if (Author != null)
                collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            collection.ReplaceOne(Builders<Stores>.Filter.Eq("_id", Id), this);
        }


        public void UpdateViewCount()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            collection.ReplaceOne(Builders<Stores>.Filter.Eq("_id", Id), this);
        }

        public void DeleteById()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            var update = Builders<Stores>.Update.Set("IsDeleted", 1);
            collection.UpdateOne(Builders<Stores>.Filter.Eq("_id", Id), update);

        }

        public void Delete()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            collection.DeleteOne(Builders<Stores>.Filter.Eq("Id", Id));
        }

        //public List<StoreComplate> LoadStoresLikeDislikeComment(ObjectId articleID)
        //{
        //    List<StoreComplate> awld = new List<StoreComplate>();

        //    IMongoCollection<Stores> collection_Articles = MongoConnection.instance.GetCollection<Stores>("Stores");

        //    IMongoCollection<GroupStore> collection_ArticleGroup = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
        //    IMongoCollection<StoreComplate> collection_Complete = MongoConnection.instance.GetCollection<StoreComplate>("StoreComplate");

        //    IMongoCollection<LikeDislikeStore> collection_Articlelikedislike = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");

        //    var articleWithLikeDislike = collection_Articlelikedislike.Aggregate().Lookup("Store", "Id", "StoreId", "LikeDislikeStore");
        //    var list_ALD = articleWithLikeDislike.ToList();

        //    var articelGroup = collection_Complete.Aggregate().Lookup("ArticleGroup", "GroupId", "Id", "StoreGroups");
        //    var list_AG = articelGroup.ToList();

        //    return awld;
        //}

        public static ObjectId UploadFile(GridFSBucket fs, string filename, string path)
        {
            using (var s = File.OpenRead(path))
            {
                var t = Task.Run<ObjectId>(() =>
                {
                    return fs.UploadFromStreamAsync(filename, s);
                });
                return t.Result;
            }

        }

        public static byte[] DownloadFile(GridFSBucket fs, ObjectId id)//, string fileName)
        {
            byte[] x = fs.DownloadAsBytes(id);
            return x;
        }

        public Stores LoadArticleByNumber()
        {
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            List<Stores> store = collection.Find((Builders<Stores>.Filter.Ne("_id", ObjectId.Empty))).ToList().OrderByDescending(x => x.CreatedDate).ToList();

            if (store.Count > 0)
                return store[0];
            else
                return null;
        }

        public static long GetNumberOfStores()
        {
            long count = 0;
            count = MongoConnection.instance.GetCollection<Stores>("Stores")
                .Count((Builders<Stores>.Filter.Eq("IsDeleted", 0)) &
                (Builders<Stores>.Filter.Ne("_id", ObjectId.Empty)) &
                (Builders<Stores>.Filter.Lte(x => x.CreatedDate, DateTime.Now.ToString("s")))
                );

            return count;
        }

        public static List<Stores> LoadStoresByGroupId(ObjectId groupId)
        {
            List<Stores> finalArticle = new List<Stores>();
            IMongoCollection<Stores> collection = MongoConnection.instance.GetCollection<Stores>("Stores");
            IMongoCollection<StoreGroups> collectionGroups = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            var slideShowGroup = collectionGroups.Find(Builders<StoreGroups>.Filter.Eq("GroupStoreId", groupId)).ToList();
            foreach (var group in slideShowGroup)
            {
                var artic = LoadDataById(group.Id);
                if (artic != null)
                {
                    finalArticle.Add(artic);
                }
            }
            return finalArticle;
        }

        public static Stores LoadDataById(ObjectId id)
        {
            return MongoConnection.instance.GetCollection<Stores>("Stores").Find(Builders<Stores>.Filter.Eq("IsDeleted", 0) & Builders<Stores>.Filter.Eq("_id", id)).FirstOrDefault();
        }
        #endregion Events
    }

    public class SearchStore
    {
        #region Properties

        private string _SearchWord;
        //[StringLength(100, ErrorMessage = " {0} باید لااقل {3} کارکتر داشته باشد.", MinimumLength = 3)]
        [Required]
        [Display(Name = "کلمه ی جستجو")]
        public string SearchWord
        {
            get { return _SearchWord; }
            set { _SearchWord = value; }
        }

        #endregion Properties
    }

    public class StoreGalary : MongoDBModel<StoreGalary>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        private ObjectId _StoreId;

        public ObjectId StoreId
        {
            get { return _StoreId; }
            set { _StoreId = value; }
        }

        //private ObjectId _ImageId;

        //public ObjectId ImageId
        //{
        //    get { return _ImageId; }
        //    set { _ImageId = value; }
        //}

        private string _ImagePath;

        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }



        #endregion Properties

        #region Events

        public List<StoreGalary> LoadDataByObject()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            var store = collection.Find((Builders<StoreGalary>.Filter.Eq("_id", Id))).ToList();
            return store;
        }

        public List<StoreGalary> LoadDataByStoreId()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            var store = collection.Find((Builders<StoreGalary>.Filter.Eq("StoreId", StoreId))).ToList();
            return store;
        }


        public List<StoreGalary> LoadDataAll()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            var articleGalary = collection.Find((Builders<StoreGalary>.Filter.Empty)).ToList();
            return articleGalary;
        }

        public ObjectId Insert()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            collection.ReplaceOne(Builders<StoreGalary>.Filter.Eq("_id", Id), this);
        }


        public void UpdateViewCount()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            collection.ReplaceOne(Builders<StoreGalary>.Filter.Eq("_id", Id), this);
        }


        public void DeleteById()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            collection.DeleteOne(Builders<StoreGalary>.Filter.Eq("_id", Id));
        }


        public void DeleteByStoreId()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            collection.DeleteMany(Builders<StoreGalary>.Filter.Eq("StoreId", StoreId));
        }


        public void Delete()
        {
            IMongoCollection<StoreGalary> collection = MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary");
            collection.DeleteOne(Builders<StoreGalary>.Filter.Eq("Id", Id));
        }
        public StoreGalary LoadById(ObjectId aId)
        {
            return MongoConnection.instance.GetCollection<StoreGalary>("StoreGalary")
               .Find((Builders<StoreGalary>.Filter.Eq("Id", aId))).FirstOrDefault();
        }

        public static ObjectId UploadFile(GridFSBucket fs, string filename, string path)
        {
            using (var s = File.OpenRead(path))
            {
                var t = Task.Run<ObjectId>(() =>
                {
                    return fs.UploadFromStreamAsync(filename, s);
                });
                return t.Result;
            }

        }

        public static byte[] DownloadFile(GridFSBucket fs, ObjectId id)//, string fileName)
        {
            byte[] x = fs.DownloadAsBytes(id);
            return x;
        }


        #endregion Events        
    }


    public class StoreComplate
    {
        #region Properties
        private Stores _store;

        public Stores store
        {
            get { return _store; }
            set { _store = value; }
        }

        //private List<SelectListItem> _GroupData;

        //public List<SelectListItem> GroupData
        //{
        //    get { return _GroupData; }
        //    set { _GroupData = value; }
        //}

        //public IEnumerable<SelectListItem> _RolesList;

        //public IEnumerable<SelectListItem> RolesList
        //{
        //    get { return _RolesList; }
        //    set { _RolesList = value; }
        //}

        private LikeDislikeStore _LikeDislike;

        public LikeDislikeStore LikeDislike
        {
            get { return _LikeDislike; }
            set { _LikeDislike = value; }
        }

        private List<StoreGalary> _Galary;

        public List<StoreGalary> Galary
        {
            get { return _Galary; }
            set { _Galary = value; }
        }


        #endregion Properties
    }

    public class StorePermisions
    {
        #region Properties

        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private Object _RoleId;

        public Object RoleId
        {
            get { return _RoleId; }
            set { _RoleId = value; }
        }

        #endregion Properties

    }

    public class GroupStore
    {
        #region properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private ObjectId _ParentId;

        public ObjectId ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        private int _GroupOrder;

        public int GroupOrder
        {
            get { return _GroupOrder; }
            set { _GroupOrder = value; }
        }


        #endregion Properties

        #region Events 

        public List<GroupStore> LoadDataByObject()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            var groupstore = collection.Find((Builders<GroupStore>.Filter.Eq("_id", Id))).ToList();
            return groupstore;
        }

        public List<GroupStore> LoadParentData()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            var groupstore = collection.Find((Builders<GroupStore>.Filter.Eq("ParentId", ParentId))).ToList();
            return groupstore;
        }

        public List<GroupStore> LoadByParent(string parentId)
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            return collection.Find((Builders<GroupStore>.Filter.Eq("ParentId", ParentId))).ToList().OrderBy(x => x.GroupOrder).ToList();
        }

        public List<GroupStore> LoadDataAll()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            var group = collection.Find((Builders<GroupStore>.Filter.Empty)).ToList().OrderBy(x => x.GroupOrder).ToList();

            return group;
        }

        public ObjectId Insert()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            List<GroupStore> groupbyorder = collection.Find((Builders<GroupStore>.Filter.Empty)).Sort(Builders<GroupStore>.Sort.Descending("GroupOrder")).ToList();

            groupbyorder = groupbyorder.OrderByDescending(x => x.GroupOrder).ToList();
            int order = groupbyorder[0].GroupOrder;

            GroupOrder = order + 1;
            Id = ObjectId.Empty;
            collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            collection.ReplaceOne(Builders<GroupStore>.Filter.Eq("_id", Id), this);
        }

        public void UpdateName()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            var update = Builders<GroupStore>.Update.Set("Name", Name).Set("UpdateDate", UpdateDate);
            collection.UpdateOne(Builders<GroupStore>.Filter.Eq("_id", Id), update);
        }

        public void UpdateOrderBy(int order, bool isParent)
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            List<GroupStore> groupbyorder = new List<GroupStore>();
            if (isParent)
            {
                groupbyorder = collection.Find((Builders<GroupStore>.Filter.Eq("ParentId", ObjectId.Empty))).ToList();
            }
            else
            {
                groupbyorder = collection.Find((Builders<GroupStore>.Filter.Ne("ParentId", ObjectId.Empty))).ToList();
            }

            GroupStore myGroup = groupbyorder.Find(x => x.Id == Id);

            GroupStore nextGroup = new GroupStore();
            GroupStore prevGroup = new GroupStore();
            groupbyorder = groupbyorder.OrderByDescending(x => x.GroupOrder).ToList();

            if (order < 0)
            {
                //group replace with next item
                nextGroup = groupbyorder.Find(x => x.GroupOrder < myGroup.GroupOrder);
                //Me
                if (nextGroup != null)
                {
                    collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
                    var update = Builders<GroupStore>.Update.Set("GroupOrder", nextGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<GroupStore>.Filter.Eq("_id", Id), update);
                    //Next
                    collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
                    update = Builders<GroupStore>.Update.Set("GroupOrder", myGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<GroupStore>.Filter.Eq("_id", nextGroup.Id), update);
                }
            }
            else
            {
                //group replace with previus item 
                prevGroup = groupbyorder.LastOrDefault(x => x.GroupOrder > myGroup.GroupOrder);
                //Me
                if (prevGroup != null)
                {
                    collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
                    var update = Builders<GroupStore>.Update.Set("GroupOrder", prevGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<GroupStore>.Filter.Eq("_id", myGroup.Id), update);
                    //Previus
                    collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
                    update = Builders<GroupStore>.Update.Set("GroupOrder", myGroup.GroupOrder).Set("UpdateDate", UpdateDate);
                    collection.UpdateOne(Builders<GroupStore>.Filter.Eq("_id", prevGroup.Id), update);
                }
            }

        }

        public void DeleteById()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            collection.DeleteOne(Builders<GroupStore>.Filter.Eq("_id", Id));
            if (ParentId == ObjectId.Empty)
                collection.DeleteOne(Builders<GroupStore>.Filter.Eq("ParentId", Id));
        }

        public void DeleteWithAllChildsById()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            collection.DeleteMany(Builders<GroupStore>.Filter.Eq("ParentId", Id));
            collection.DeleteOne(Builders<GroupStore>.Filter.Eq("_id", Id));
        }


        public void Delete()
        {
            IMongoCollection<GroupStore> collection = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            collection.DeleteOne(Builders<GroupStore>.Filter.Eq("_id", Id));
        }


        #endregion Events
    }

    public class SelectedStoreSearchItems
    {
        private string _SearchWord;

        public string SearchWord
        {
            get { return _SearchWord; }
            set { _SearchWord = value; }
        }


        private string _GroupId;

        public string GroupId
        {
            get { return _GroupId; }
            set { _GroupId = value; }
        }

        private List<SelectListItem> _GroupItems;

        public List<SelectListItem> GroupItems
        {
            get { return _GroupItems; }
            set { _GroupItems = value; }
        }

        private string _SearchTimeSelected;

        public string SearchTimeSelected
        {
            get { return _SearchTimeSelected; }
            set { _SearchTimeSelected = value; }
        }


        private List<SelectListItem> _SearchTime;

        public List<SelectListItem> SearchTime
        {
            get { return _SearchTime; }
            set { _SearchTime = value; }
        }


        private string _SearchTimeOrderSelected;
        public string SearchTimeOrderSelected
        {
            get { return _SearchTimeOrderSelected; }
            set { _SearchTimeOrderSelected = value; }
        }


        private List<SelectListItem> _SearchTimeOrder;

        public List<SelectListItem> SearchTimeOrder
        {
            get { return _SearchTimeOrder; }
            set { _SearchTimeOrder = value; }
        }


        private string _SearchByTypeSelected;
        public string SearchByTypeSelected
        {
            get { return _SearchByTypeSelected; }
            set { _SearchByTypeSelected = value; }
        }


        private List<SelectListItem> _SearchByType;

        public List<SelectListItem> SearchByType
        {
            get { return _SearchByType; }
            set { _SearchByType = value; }
        }
    }

    public class StoreGroups : MongoDBModel<StoreGroups>
    {
        #region Properties
        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        private ObjectId _StoreId;

        public ObjectId StoreId
        {
            get { return _StoreId; }
            set { _StoreId = value; }
        }

        private ObjectId _GroupStoreId;

        public ObjectId GroupStoreId
        {
            get { return _GroupStoreId; }
            set { _GroupStoreId = value; }
        }

        private string _GroupStoreName;

        public string GroupStoreName
        {
            get { return _GroupStoreName; }
            set { _GroupStoreName = value; }
        }

        #endregion Properties

        #region Events 

        public List<Group> LoadDataAllGroups ()
        {
            IMongoCollection<Group> collection1 = MongoConnection.instance.GetCollection<Group>("Group");
            IMongoCollection<StoreGroups> collection2 = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");

            List<Group> groupList = collection1.Find((Builders<Group>.Filter.Empty)).ToList();

            List<ObjectId> articlegroup = collection2.Distinct(new StringFieldDefinition<StoreGroups, ObjectId>("GroupId"), FilterDefinition<StoreGroups>.Empty).ToList();

            groupList.FindAll(x => x.ParentId != ObjectId.Empty).ToList().OrderBy(x => x.GroupOrder).ToList();
            groupList.OrderBy(x => x.GroupOrder);

            var query = from X in groupList
                        join Y in articlegroup
                             on X.Id equals Y
                        orderby X.GroupOrder
                        select X;

            List<Group> finalList = query.ToList<Group>();
            int count = finalList.Count;
            for (int i = 0; i < count; i++)
            {
                if (finalList[i].ParentId != ObjectId.Empty)
                {
                    var z = groupList.Find(x => x.Id == finalList[i].ParentId);
                    if (!finalList.Contains(z))
                        finalList.Add(z);
                }
            }

            return finalList;
        }

        public List<StoreGroups> LoadDataByObject()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            var store = collection.Find((Builders<StoreGroups>.Filter.Eq("GroupId", GroupStoreId))).ToList();
            return store;
        }

        public List<StoreGroups> LoadDataByStoreId()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            var store = collection.Find((Builders<StoreGroups>.Filter.Eq("ArticleId", StoreId))).ToList();
            return store;
        }

        public List<GroupStore> LoadDataAllGroupsWithStore()
        {
            IMongoCollection<GroupStore> collection1 = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");
            IMongoCollection<StoreGroups> collection2 = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");

            List<GroupStore> groupList = collection1.Find((Builders<GroupStore>.Filter.Empty)).ToList();
            List<ObjectId> articlegroup = collection2.Distinct(new StringFieldDefinition<StoreGroups, ObjectId>("GroupId"), FilterDefinition<StoreGroups>.Empty).ToList();
            groupList.FindAll(x => x.ParentId != ObjectId.Empty);
            var query = from X in groupList
                        join Y in articlegroup
                             on X.Id equals Y
                        select X;

            List<GroupStore> finalList = query.ToList<GroupStore>();
            int count = finalList.Count;
            for (int i = 0; i < count; i++)
            {
                if (finalList[i].ParentId != ObjectId.Empty)
                {
                    var z = groupList.Find(x => x.Id == finalList[i].ParentId);
                    if (!finalList.Contains(z))
                        finalList.Add(z);
                }
            }

            return finalList;
        }

        public List<StoreGroups> LoadStoresByIdAndStoreId()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            var storegroup = collection.Find(Builders<StoreGroups>.Filter.Eq("GroupId", GroupStoreId)).ToList();
            return storegroup;
        }


        internal List<StoreGroups> LoadDataByGroupId()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            IMongoCollection<GroupStore> collection_Group = MongoConnection.instance.GetCollection<GroupStore>("GroupStore");

            List<GroupStore> List_Group = collection_Group.Find(Builders<GroupStore>.Filter.Eq("Id", GroupStoreId) |
                Builders<GroupStore>.Filter.Eq("ParentId", GroupStoreId)).ToList();

            List<StoreGroups> list_Articles = collection.Find(Builders<StoreGroups>.Filter.Empty).ToList();
            List<StoreGroups> final_List = new List<StoreGroups>();
            bool haveArticle = false;
            foreach (GroupStore item_group in List_Group)
            {
                haveArticle = false;
                foreach (StoreGroups item in list_Articles)
                {
                    if (item_group.Id == item.GroupStoreId)
                    {
                        haveArticle = true;
                        final_List.Add(item);
                    }
                }
                if (!haveArticle)
                {

                }
            }

            return final_List;
        }


        public List<StoreGroups> LoadDataAll()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            List<StoreGroups> storegroup = collection.Find((Builders<StoreGroups>.Filter.Empty)).ToList();
            return storegroup;
        }

        public ObjectId Insert()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            collection.ReplaceOne(Builders<StoreGroups>.Filter.Eq("_id", Id), this);
        }

        public void DeleteById()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            collection.DeleteOne(Builders<StoreGroups>.Filter.Eq("_id", Id));
        }

        public void DeleteWithStoreId()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            collection.DeleteMany(Builders<StoreGroups>.Filter.Eq("StoreId", StoreId));
            collection.DeleteOne(Builders<StoreGroups>.Filter.Eq("_id", Id));
        }


        public void Delete()
        {
            IMongoCollection<StoreGroups> collection = MongoConnection.instance.GetCollection<StoreGroups>("StoreGroups");
            collection.DeleteOne(Builders<StoreGroups>.Filter.Eq("_id", Id));
        }



        #endregion Events
    }

    public class GroupStoreForDropDown
    {
        #region Properties

        private GroupStore _Group;

        public GroupStore Group
        {
            get { return _Group; }
            set { _Group = value; }
        }

        private List<SelectListItem> _Groups;

        public List<SelectListItem> Groups
        {
            get { return _Groups; }
            set { _Groups = value; }
        }

        private string _ChangeOrder;

        public string ChangeOrder
        {
            get { return _ChangeOrder; }
            set { _ChangeOrder = value; }
        }


        #endregion Properties
    }

    public class LikeDislikeStore : MongoDBModel<LikeDislikeStore>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private ObjectId _StoreId;

        public ObjectId StoreId
        {
            get { return _StoreId; }
            set { _StoreId = value; }
        }


        private int _LikeCount;

        public int LikeCount
        {
            get { return _LikeCount; }
            set { _LikeCount = value; }
        }

        private int _DislikeCount;

        public int DislikeCount
        {
            get { return _DislikeCount; }
            set { _DislikeCount = value; }
        }

        private string _UserIp;

        public string UserIp
        {
            get { return _UserIp; }
            set { _UserIp = value; }
        }


        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        #endregion Properties

        #region Events

        public List<LikeDislikeStore> LoadDataByStoreId(ObjectId aId)
        {
            return MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore")
                .Find((Builders<LikeDislikeStore>.Filter.Eq("_id", aId))).ToList();
        }

        public List<LikeDislikeStore> LoadDataByObject()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            var store = collection.Find((Builders<LikeDislikeStore>.Filter.Eq("_id", Id))).ToList();
            return store;
        }

        public List<LikeDislikeStore> LoadDataByStoreId()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            var store = collection.Find((Builders<LikeDislikeStore>.Filter.Eq("StoreId", StoreId))).ToList();
            return store;
        }


        public int FavoriteArticlesByUserIpCount()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            return collection.Find((Builders<LikeDislikeStore>.Filter.Eq("UserIp", UserIp))).ToList().Count;
        }

        public List<LikeDislikeStore> LoadFavoriteArticlesByUserIp()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            List<LikeDislikeStore> list = collection.Find((Builders<LikeDislikeStore>.Filter.Eq("UserIp", UserIp))).ToList();
            return list;
        }

        public List<LikeDislikeStore> LoadDataAll()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            var user = collection.Find((Builders<LikeDislikeStore>.Filter.Empty)).ToList();
            return user;
        }

        public ObjectId Insert()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            collection.ReplaceOne(Builders<LikeDislikeStore>.Filter.Eq("_id", Id), this);
        }

        public void DeleteById()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            collection.DeleteOne(Builders<LikeDislikeStore>.Filter.Eq("_id", Id));
        }

        public void DeleteByArticleId()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            collection.DeleteMany(Builders<LikeDislikeStore>.Filter.Eq("StoreId", StoreId));
        }


        public void Delete()
        {
            IMongoCollection<LikeDislikeStore> collection = MongoConnection.instance.GetCollection<LikeDislikeStore>("LikeDislikeStore");
            collection.DeleteOne(Builders<LikeDislikeStore>.Filter.Eq("_id", Id));
        }

        #endregion Events
    }

    public class StoreComment : MongoDBModel<StoreComment>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private ObjectId _CommentId;

        public ObjectId CommentId
        {
            get { return _CommentId; }
            set { _CommentId = value; }
        }

        private ObjectId _StoreId;

        public ObjectId StoreId
        {
            get { return _StoreId; }
            set { _StoreId = value; }
        }

        private string _UserID;

        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }


        private string _CommentBody;
        [AllowHtml]
        public string CommentBody
        {
            get { return _CommentBody; }
            set { _CommentBody = value; }
        }

        private string _Answer;
        [AllowHtml]
        public string Answer
        {
            get { return _Answer; }
            set { _Answer = value; }
        }


        private string _AnswerBy;

        public string AnswerBy
        {
            get { return _AnswerBy; }
            set { _AnswerBy = value; }
        }


        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        #endregion Properties

        #region Events 

        public List<StoreComment> LoadDataByObject()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            var store = collection.Find((Builders<StoreComment>.Filter.Eq("_id", Id))).ToList();
            return store;
        }

        public List<StoreComment> LoadDataBystoreId()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            var store = collection.Find((Builders<StoreComment>.Filter.Eq("StoreId", StoreId))).ToList();
            return store;
        }

        public List<StoreComment> LoadDataByStoreIdAndUserId()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            var store = collection.Find((Builders<StoreComment>.Filter.Eq("StoreId", StoreId)) &
                (Builders<StoreComment>.Filter.Eq("UserID", UserID))).ToList();
            return store;
        }



        public List<StoreComment> LoadDataAll()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            var comments = collection.Find((Builders<StoreComment>.Filter.Empty)).ToList();
            return comments;
        }


        public List<RegisterViewModel> LoadCommentsWithUserInfo(ObjectId articleId)
        {
            IMongoCollection<RegisterViewModel> collection_Users = MongoConnection.instance.GetCollection<RegisterViewModel>("users");
            IMongoCollection<StoreComment> collection_StoreComments = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");

            var allregisterdUsers = collection_StoreComments.Aggregate().Lookup("users", "UserId", "Id", "StoreComment").ToList();
            return null;
        }

        public ObjectId Insert()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");

            var update = Builders<StoreComment>.Update.Set("Answer", Answer).Set("AnswerBy", AnswerBy).Set("UpdateDate", UpdateDate);
            collection.UpdateOne(Builders<StoreComment>.Filter.Eq("_id", Id), update);
        }

        public void DeleteById()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            collection.DeleteOne(Builders<StoreComment>.Filter.Eq("_id", Id));
        }

        public void DeleteByArticleId()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            collection.DeleteMany(Builders<StoreComment>.Filter.Eq("StoreId", StoreId));
        }


        public void Delete()
        {
            IMongoCollection<StoreComment> collection = MongoConnection.instance.GetCollection<StoreComment>("StoreComment");
            collection.DeleteOne(Builders<StoreComment>.Filter.Eq("_id", Id));
        }


        #endregion Events
    }

    public class StoreWithLikeDislikeAndComments
    {
        #region properties

        private Stores _Store;

        public Stores Store
        {
            get { return _Store; }
            set { _Store = value; }
        }

        private LikeDislikeStore _LikeDislike;

        public LikeDislikeStore LikeDislike
        {
            get { return _LikeDislike; }
            set { _LikeDislike = value; }
        }

        private StoreComment _Comment;

        public StoreComment Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private List<StoreComment> _StoreComments;

        public List<StoreComment> StoreComments
        {
            get { return _StoreComments; }
            set { _StoreComments = value; }
        }


        private List<StoreGalary> _StoreGalary;

        public List<StoreGalary> StoreGalary
        {
            get { return _StoreGalary; }
            set { _StoreGalary = value; }
        }

        public List<StoreGroups> _StoreGroupsData;
        public List<StoreGroups> StoreGroupsData
        {
            get { return _StoreGroupsData; }
            set {
                _StoreGroupsData = value;
            }
        }


        #endregion Properties
    }

    public class StoreListComplete
    {
        #region properties

        private List<StoreWithLikeDislikeAndComments> _StoreList;

        public List<StoreWithLikeDislikeAndComments> StoreList
        {
            get { return _StoreList; }
            set { _StoreList = value; }
        }

        private List<Group> _GroupList;

        public List<Group> GroupList
        {
            get { return _GroupList; }
            set { _GroupList = value; }
        }


        private List<Articles> _ArticleSlideShowList;

        public List<Articles> ArticleSlideShowList
        {
            get { return _ArticleSlideShowList; }
            set { _ArticleSlideShowList = value; }
        }


        private List<Advertizements> _AdvertizementTop;

        public List<Advertizements> AdvertizementTop
        {
            get { return _AdvertizementTop; }
            set { _AdvertizementTop = value; }
        }

        private List<Advertizements> _AdvertizementBttom;

        public List<Advertizements> AdvertizementBttom
        {
            get { return _AdvertizementBttom; }
            set { _AdvertizementBttom = value; }
        }

        private List<GroupStore> _TreeGroup;

        public List<GroupStore> TreeGroup
        {
            get { return _TreeGroup; }
            set { _TreeGroup = value; }
        }


        #endregion Properties
    }

}