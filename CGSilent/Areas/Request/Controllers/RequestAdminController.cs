﻿
using CGSilent.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MongoDB.Bson;
using MvcPaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CGSilent.Areas.Request.Controllers
{
    public class RequestAdminController : Controller
    {
        enum Status { notDone = 0, Doing = 1, Done = 2, };

        // GET: Requests/RequestAdmin
        [HttpGet]
        public ActionResult RequestList()
        {
            string roles = "";
            ManageUser userdata = new ManageUser();
            RequestModel req = new RequestModel();          

            if (User.Identity.IsAuthenticated)
            {
                userdata.Id = ObjectId.Parse(User.Identity.GetUserId());
                userdata = userdata.GetUserById();
                for (int i = 0; i < userdata.Roles.Length; i++)
                {
                    roles += userdata.Roles[i].ToString() + ",";
                }
                if (roles.Length > 0)
                    roles = roles.Substring(0, roles.Length - 1);
                ViewBag.UserRoles = roles;
                ViewBag.Email = userdata.Email;

                req.UserId = ObjectId.Parse(User.Identity.GetUserId());
                return View("RequestList", req);
            }
            else
            { 
                List<Articles> il = Articles.LoadAllArticlesFromToday();
                CreateArticleAllList(il);
                req.UserId = ObjectId.Empty;
                return View("RequestNotAllowd");
            }            
        }


        private void CreateArticleAllList(IList ArticleList)
        {

            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<Models.ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();
             
            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            ArticleComment comment = new ArticleComment();
            List<ArticleComment> comment_list = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();
            for (int g = 0; g < allArticles.Count; g++)
            {
                articleGroup.ArticleId = (allArticles[g]).Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[g].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[g]);
                        } 
                    }
                }
                for (int j = 0; j < galaryList.Count; j++)
                {
                    if (galaryList[j].ArticleId == allArticles[g].Id)
                        articleGalaryList.Add(galaryList[j]);
                }
            }
            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = (Articles)ArticleList[i];
                    List<ArticleGroups> articleGroups = new List<ArticleGroups>();
                    for (int j = 0; j < articleGroup_List.Count; j++)
                    {
                        if (articleGroup_List[j].ArticleId == al.Id)
                        {
                            articleGroups.Add(articleGroup_List[j]);
                        }
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    for (int j = 0; j < list_likedislike.Count; j++)
                    {
                        if (list_likedislike[j].ArticleId == al.Id)
                        {
                            likesCount = likesCount + list_likedislike[j].LikeCount;
                            dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                            if (list_likedislike[j].UserIp == ipaddress)
                            {
                                ip = list_likedislike[j].UserIp;
                            }
                            else { ip = ""; }
                        }
                    }
                    LikeDislikeArticle ldarticle = new LikeDislikeArticle
                    {
                        ArticleId = ((Articles)ArticleList[i]).Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    for (int j = 0; j < comment_list.Count; j++)
                    {
                        if (comment_list[j].ArticleId == al.Id)
                        {
                            articleComments.Add(comment_list[j]);
                        }
                    }
                    commentsCount = articleComments.Count;
                    ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    {
                        Article = al,
                        LikeDislike = ldarticle,
                        ArticleComments = articleComments,
                        Comment = comment,
                        ArticleGalary = articleGalaryList,
                        ArticleGroupsData = articleGroups
                    };
                    aaList.Add(aa);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementBottomData,
                AdvertizementBottom=advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(1, 10); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
        }


        [ChildActionOnly]
        [HttpGet]
        public ActionResult LoadBuyExtraData()
        {
            List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
            SelectListItem sli;

            for (int i = 0; i < 10; i++)
            {
                sli = new SelectListItem();
                sli.Value = (i + 1).ToString();
                sli.Text = "اعتبار " + (i + 1);
                sli.Selected = true;
                selectedlist_Group.Add(sli);
            }
            for (int i = 20; i < 60; i = i + 10)
            {
                sli = new SelectListItem();
                sli.Value = i.ToString();
                sli.Text = "اعتبار " + (i);
                sli.Selected = true;
                selectedlist_Group.Add(sli);
            }

            sli = new SelectListItem();
            sli.Value = "100";
            sli.Text = "اعتبار 100";
            sli.Selected = true;
            selectedlist_Group.Add(sli);
            BuyExtra be = new BuyExtra();
            be.DropDownListItems = selectedlist_Group;

            return PartialView("_BuyExtra", be);
        }


        [ChildActionOnly]
        [HttpGet]
        public ActionResult AddRequest()
        {
            RequestWithSize rws = new RequestWithSize();
            rws.Requst = new RequestModel();

            List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
            SelectListItem sli;

            RequestSize rs = new RequestSize();
            rs.Id = "1";
            rs.Size = "1GB - 5GB";

            sli = new SelectListItem();
            sli.Text = "1GB - 5GB";
            sli.Value = "1GB - 5GB";
            sli.Selected = true;
            selectedlist_Group.Add(sli);

            sli = new SelectListItem();
            sli.Text = "5GB - 10GB";
            sli.Value = "5GB - 10GB";
            sli.Selected = true;
            selectedlist_Group.Add(sli);

            sli = new SelectListItem();
            sli.Text = "10B - 15GB";
            sli.Value = "10B - 15GB";
            sli.Selected = true;
            selectedlist_Group.Add(sli);

            sli = new SelectListItem();
            sli.Text = "15GB - 20GB";
            sli.Value = "15GB - 20GB";
            sli.Selected = true;
            selectedlist_Group.Add(sli);

            rws.RequestSizeList = selectedlist_Group;

            return PartialView("_AddRequest", rws);
        }


        [ChildActionOnly]
        [HttpGet]
        public ActionResult ViewRequests()
        {
            RequestModel req = new RequestModel();
            req.UserId = ObjectId.Parse(User.Identity.GetUserId());
            IList<RequestModel> requestList = req.LoadDataByUserId();

            return PartialView("_ViewRequests", requestList);
        }

        [HttpPost]
        public ActionResult AddRequest(RequestWithSize model)
        {
            model.Requst.CreatedDate = DateTime.Now.ToString();
            model.Requst.UpdatedDate = DateTime.Now.ToString();
            model.Requst.UserId = ObjectId.Parse(User.Identity.GetUserId());
            model.Requst.Status = Status.notDone.ToString();

            model.Requst.Insert();

            return RedirectToAction("Index");
        }


        [ChildActionOnly]
        [HttpGet]
        public ActionResult AddLearnArticles()
        {
            LearningArticles learnArticle = new LearningArticles();
            List<Articles> Data = learnArticle.LoadAllLearningArticls();

            List<SelectListItem> selectedlist_Group = new List<SelectListItem>();
            SelectListItem sli;
            for (int i = 0; i < Data.Count; i++)
            {
                sli = new SelectListItem();
                sli.Text = Data[i].Title;
                sli.Value = Data[i].Id.ToString();
                if (i == 0)
                    sli.Selected = true;
                selectedlist_Group.Add(sli);
            }

            learnArticle.DropDownListItems = selectedlist_Group;

            return PartialView("_PurchaseFarsiContent", learnArticle);
        }

        [HttpPost]
        public ActionResult AddLearnArticles(LearningArticles model)
        { 
            return RedirectToAction("Index");
        }


        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}