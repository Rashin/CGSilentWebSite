﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Collections;
using MongoDB.Driver;
using System.Web.Mvc;
using MongoDB.Driver.GridFS;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;
using static MongoDB.Driver.WriteConcern;

namespace CGSilent.Models
{
    public class RequestModel
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private ObjectId _UserId;
        public ObjectId UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        private string _UserName;
        [Display(Name = "کاربر درخواست کننده")]
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }


        private string _RequestName;
        [Display(Name = "نام درخواست")]
        public string RequestName
        {
            get { return _RequestName; }
            set { _RequestName = value; }
        }

        private string _RequestLink;
        [Display(Name = "لینک درخواست")]
        public string RequestLink
        {
            get { return _RequestLink; }
            set { _RequestLink = value; }
        }

        private string _Requestsize;
        [Display(Name = "حجم درخواست")]
        public string Requestsize
        {
            get { return _Requestsize; }
            set { _Requestsize = value; }
        }

        private string _CreatedDate;
        [Display(Name = "تاریخ درخواست")]
        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }


        private string _UpdatedDate;
        [Display(Name = "تاریخ به روز رسانی")]
        public string UpdatedDate
        {
            get { return _UpdatedDate; }
            set { _UpdatedDate = value; }
        }

        private string _Status;
        [Display(Name = "وضعیت")]
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        #endregion Properties

        #region Events

        public IList LoadDataByObject()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            IList req = collection.Find((Builders<RequestModel>.Filter.Eq("_id", Id))).ToList();
            return req;
        }

        public IList<RequestModel> LoadDataByUserId()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            IList<RequestModel> req = collection.Find((Builders<RequestModel>.Filter.Eq("UserId", UserId))).ToList();
            return req;
        }

        public IList LoadAllArticlesFromToday()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            IList req = collection.Find((Builders<RequestModel>.Filter.Empty) & (Builders<RequestModel>.Filter.Empty)).Sort(Builders<RequestModel>.Sort.Descending("UpdateDate")).ToList();
            for (int i = 0; i < req.Count; i++)
            {
                if (DateTime.Parse(((RequestModel)req[i]).CreatedDate) > DateTime.Now)
                {
                    req.RemoveAt(i);
                    i = i - 1;
                }
            }

            return req;
        }

        public IList LoadDataAll()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            IList req = collection.Find((Builders<RequestModel>.Filter.Empty)).Sort(Builders<RequestModel>.Sort.Descending("UpdateDate")).ToList();
            return req;
        }

        public ObjectId Insert()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            collection.ReplaceOne(Builders<RequestModel>.Filter.Eq("_id", Id), this);
        }


        public void UpdateViewCount()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            collection.ReplaceOne(Builders<RequestModel>.Filter.Eq("_id", Id), this);
        }


        public void DeleteById()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            collection.DeleteOne(Builders<RequestModel>.Filter.Eq("_id", Id));
        }

        public void Delete()
        {
            IMongoCollection<RequestModel> collection = MongoConnection.instance.GetCollection<RequestModel>("RequestModel");
            collection.DeleteOne(Builders<RequestModel>.Filter.Eq("Id", Id));
        }
        #endregion Events
    }


    public class RequestSize
    {
        private string _Id;

        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Size;

        public string Size
        {
            get { return _Size; }
            set { _Size = value; }
        }

    }

    public class RequestWithSize
    {
        private RequestModel _Requst;

        public RequestModel Requst
        {
            get { return _Requst; }
            set { _Requst = value; }
        }

        private RequestSize _RequestSize;

        public RequestSize RequestSize
        {
            get { return _RequestSize; }
            set { _RequestSize = value; }
        }


        private IEnumerable<SelectListItem> _RequestSizeList;

        public IEnumerable<SelectListItem> RequestSizeList
        {
            get { return _RequestSizeList; }
            set { _RequestSizeList = value; }
        }

    }

    public class BuyExtra
    {
        private int _Id;

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Text;

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        private IEnumerable<SelectListItem> _DropDownListItems;

        public IEnumerable<SelectListItem> DropDownListItems
        {
            get { return _DropDownListItems; }
            set { _DropDownListItems = value; }
        }
    }


    public class LearningArticles
    {
        #region Properties

        private string _ArticleTitle;

        public string ArticleTitle
        {
            get { return _ArticleTitle; }
            set { _ArticleTitle = value; }
        }

        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private IEnumerable<SelectListItem> _DropDownListItems;

        public IEnumerable<SelectListItem> DropDownListItems
        {
            get { return _DropDownListItems; }
            set { _DropDownListItems = value; }
        }

        #endregion Properties

        #region Events

        internal List<Articles> LoadAllLearningArticls()
        {
            IMongoCollection<Articles> collection = MongoConnection.instance.GetCollection<Articles>("Articles");
            List<Articles> article = collection.Find(Builders<Articles>.Filter.Empty).Sort(Builders<Articles>.Sort.Descending("UpdateDate")).ToList();
            for (int i = 0; i < article.Count; i++)
            {
                if (article[i].CreatedDate != null)
                {
                    if (DateTime.Parse(article[i].CreatedDate) > DateTime.Parse(DateTime.Now.ToString("s")))
                    {
                        article.RemoveAt(i);
                        i = i - 1;
                    }
                }
                else
                {
                    article.RemoveAt(i);
                    i = i - 1;
                }
            }
            IMongoCollection<ArticleGroups> collection_ArticleGroup = MongoConnection.instance.GetCollection<ArticleGroups>("ArticleGroup");
            List<ArticleGroups> groupLearning = collection_ArticleGroup.Find((Builders<ArticleGroups>.Filter.Eq("_id", "5a526a38e290b819dc102a36")) &
                (Builders<ArticleGroups>.Filter.Eq("ParentId", "5a526a38e290b819dc102a36"))).ToList();

            List<Articles> FinalArticles = new List<Articles>();
            for (int i = 0; i < groupLearning.Count; i++)
            {
                for (int j = 0; j < article.Count; j++)
                {
                    if (article[j].Id == groupLearning[i].ArticleId)
                    {
                        FinalArticles.Add(article[j]);
                        break;
                    }
                }
            }

            return article;
        }

        #endregion Events

    }

}