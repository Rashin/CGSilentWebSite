﻿using CGSilent.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MongoDB.Bson;
using MvcPaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CGSilent.Controllers
{
    public class ManageSiteController : Controller
    {
        // GET: ManageSite
        public ActionResult Index()
        {
            return View();
        }
        //------------------------------------------------------- User Management --------------------------
        public ActionResult ManageVIPUsers()
        { 
            CreateArticleAllList();
            return View("ManageSite", LoadAllUsersFullInfo());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult AddVipTime(ObjectId id, int vipTime)
        public ActionResult AddVipTime(UserManagementVIPRole model)
        {
            if (model.Id != null && model.VipTime != 0)
            {
                UserManagementVIPRole UserManagement = new UserManagementVIPRole();
                UserManagement.Id = model.Id;
                UserManagement.VipTime = model.VipTime;
                UserManagement.UpdateVipTime();
            }
            return View("ManageSite", LoadAllUsersFullInfo());
        }

        public UserVIP LoadAllUsersFullInfo()
        {
            UserVIP vip = new UserVIP();
            UserManagementVIPRole UserManagement = new UserManagementVIPRole();
            List<UserManagementVIPRole> userList = UserManagement.GetAllUsersFullInfo();

            vip.AllData = userList;

            IQueryable<UserManagementVIPRole> IQ_List_Article = userList.AsQueryable<UserManagementVIPRole>();
            var onePageOfUsers = IQ_List_Article.ToPagedList(1, 10); // will only contain 25 products max because of the pageSize
            ViewBag.UsersPageingAndView = onePageOfUsers;
             
            CreateArticleAllList();

            return vip;
        }

        private void CreateArticleAllList()
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>(); 
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<Models.ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();
             
            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            long articlecount = Articles.GetNumberOfArticles();
            for (int i = 0; i < articlecount; i++)
            {
                articleGroup.ArticleId = (allArticles[i]).Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[i]);
                        } 
                    }
                }
            }

            ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
            {
                Article = null,
                LikeDislike = null,
                ArticleComments = null,
                Comment = null,
                ArticleGalary = null,
                ArticleGroupsData = null
            };
            aaList.Add(aa);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom=advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }

        public class UserVIP
        {
            private List<UserManagementVIPRole> _AllData;

            public List<UserManagementVIPRole> AllData
            {
                get { return _AllData; }
                set { _AllData = value; }
            }

            private ObjectId _Id;

            public ObjectId Id
            {
                get { return _Id; }
                set { _Id = value; }
            }

            private int _VipTime;

            public int VipTime
            {
                get { return _VipTime; }
                set { _VipTime = value; }
            }


        }


        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }


        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}