﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CGSilent.Models;
using System.Web.Security;
using System.Net;
using Newtonsoft.Json.Linq;
using MongoDB.Bson;
using System.IO;
using MongoDB.Driver.GridFS;
using System.Web.UI.WebControls;
using System;
using System.Collections;
using PagedList;
using System.Collections.Generic;
using CaptchaMvc.HtmlHelpers;
using System.Net.Mail;
using System.Collections.Concurrent;

namespace CGSilent.Controllers
{
    [Authorize]

    public class AccountController : Controller
    {
        public AccountController()
        {
        }



        [HttpPost]
        public bool FormSubmit()
        {
            var response = Request["g-recaptcha-response"];
            string secretKey = "your secret key here";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");
            ViewBag.Message = status ? "Google reCaptcha validation success" : "Google reCaptcha validation failed";
            if (status)
                return true;
            else
                return false;
        }


        public AccountController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            CreateArticleAllList();
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        private SignInHelper _helper;

        private SignInHelper SignInHelper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new SignInHelper(UserManager, AuthenticationManager);
                }
                return _helper;
            }
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(string UserName, string Password, bool RememberMe, string returnUrl)
        {
            CreateArticleAllList();
            if (!ModelState.IsValid)
            {
                return View();
            }
            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var result = await SignInHelper.PasswordSignIn(UserName, Password, RememberMe, shouldLockout: false, isLogin: true);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresTwoFactorAuthentication:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "نام کاربری یا کلمه ورود اشتباه است. لطفا مجددا تلاش نمایید");
                    return View();
            }
        }

        private void CreateArticleAllList()
        {
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<Models.ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            long articlecount = Articles.GetNumberOfArticles();
            for (int i = 0; i < articlecount; i++)
            {
                articleGroup.ArticleId = (allArticles[i]).Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[i]);
                        }
                    }
                }
            }

            ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
            {
                Article = null,
                LikeDislike = null,
                ArticleComments = null,
                Comment = null,
                ArticleGalary = null,
                ArticleGroupsData = null
            };
            aaList.Add(aa);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }



        [ChildActionOnly]
        public ActionResult UserImage()
        {
            string userid = User.Identity.GetUserId();
            UserImage image = new Models.UserImage();
            image.id = ObjectId.Parse(userid);
            image.ImagePath = image.GetUserImageById();

            return PartialView("_UserImage", image);
        }

        //
        // GET: /Account/VerifyCode

        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInHelper.HasBeenVerified())
            {
                return View("Error");
            }
            var user = await UserManager.FindByIdAsync(await SignInHelper.GetVerifiedUserIdAsync());
            if (user != null)
            {
                // To exercise the flow without actually sending codes, uncomment the following line
                ViewBag.Status = "For DEMO purposes the current " + provider + " code is: " + await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl });
        }

        //
        // POST: /Account/VerifyCode

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInHelper.TwoFactorSignIn(model.Provider, model.Code, isPersistent: false, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            CreateArticleAllList();
            return View();
        }

        public ApplicationIdentityContext IdentityContext
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationIdentityContext>();
            }
        }


        [AllowAnonymous]
        [HttpPost]
        public JsonResult EmailExist(string email)
        {
            var t = UserManager.FindByEmail(email);
            ViewBag.Error = "ایمیل وارد شده قبلا در سیستم ثبت شده است.";
            //if (t.Id != "")
            //    return Json(true, JsonRequestBehavior.AllowGet);// Json(1);  
            //else
            //    return Json(false, JsonRequestBehavior.AllowGet);// Json(0);
            return Json(t == null);
        }



        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            CreateArticleAllList();
            if (ModelState.IsValid)
            {
                if (CaptchaHelper.IsCaptchaValid(this, "Captcha is valid"))
                {
                    List<string> userrole = new List<string>();
                    userrole.Add("Public");
                    if (model.ImagePath == null)
                    {
                        model.ImagePath = "";
                    }
                    else
                    {
                        UploadFile(model);
                    }
                    if (model.Address == null)
                        model.Address = "";
                    if (model.City == null)
                        model.City = "";
                    if (model.Phone == null)
                        model.Phone = "";
                    var aUser = new ApplicationUser
                    {
                        UserName = model.UserName,
                        Email = model.Email,
                        Address = model.Address,
                        City = model.City,
                        CreatedDate = DateTime.Now.ToString(),
                        ExpireDate = (new DateTime(DateTime.Now.Year - 1, DateTime.Now.Month, DateTime.Now.Day,
                        DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTimeKind.Local)).ToString(),
                        LastName = model.LastName,
                        Roles = userrole,
                        ImageId = model.ImageId,
                        ImagePath = model.ImagePath,
                        Name = model.Name,
                        PhoneNumber = model.Phone
                    };
                    var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var result = await manager.CreateAsync(aUser, model.Password);
                    if (result.Succeeded)
                    {
                        var code = await UserManager.GenerateEmailConfirmationTokenAsync(aUser.Id);
                        var callbackUrl = Url.Action("ConfirmEmailAccepted", "Account", new { userId = aUser.Id, code = code }, protocol: Request.Url.Scheme);

                        SmtpClient client = new SmtpClient("mail.cgsilent.com", 25);

                        MailMessage mailMessage = new MailMessage();
                        mailMessage.From = new MailAddress("info@cgsilent.com");
                        mailMessage.To.Add(aUser.Email);
                        mailMessage.Subject = "تایید حساب کاربری";

                        mailMessage.Body = MakeEmailBody(callbackUrl);
                        // mailMessage.Body = "<div style='direction:rtl; text-align:right;'>با کلیک بر روی :" + "<a href = '" + callbackUrl + "'> این لینک</a >" + " حساب کاربری خود را فعال نمایید.</div>";

                        // mailMessage.Body = "با کلیک بر روی لینک: <a href=\"" + callbackUrl + " حساب کاربری خود را فعال نمایید.\">link</a>";
                        mailMessage.IsBodyHtml = true;
                        client.Send(mailMessage);
                        //var signinResult = await SignInHelper.PasswordSignIn(model.UserName, model.Password, true, shouldLockout: false, isLogin: false);

                        ViewBag.Link = callbackUrl;
                        // if (signinResult == SignInStatus.Success)
                        return View("ConfirmEmail");
                        //else
                        //{
                        //    AuthenticationManager.SignOut();
                        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        //    FormsAuthentication.SignOut();
                        //    Session.Abandon();
                        //    return View();
                        //}

                    }
                    else
                    {
                        ModelState.AddModelError("", "");// result.Errors.FirstOrDefault());
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError("CaptchaError", "فیلد کپچا را صحیح وارد نمایدد");
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        private string MakeEmailBody(string callbackUrl)
        {
            string body = "";
            body += "<body>";
            body += "<div style='width: 100%; height: 144px; background-color:#101a22;'>";
            body += "<div style='float:left; width: 30%; '>";
            body += "<img src='http://www.cgsilent.com/cgsilentLogo.png' style='float:left;' />";
            body += "</div>";
            body += "<div style='float:right; padding-top:30px; padding-right:43px; padding-bottom:30px; width: 60%; line-height:25px;'>";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += "کاربر گرامی, اطلاعات شما با موفقیت ثبت گردید.";
            body += "</span>";
            body += "<br />";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += "برای فعال کردن حساب کاربری خود بر روی";
            body += " </span>";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#def84d;padding-left:2px; padding-right:2px;'>";
            body += "<a href='" + callbackUrl + "' style='text-decoration: none; color:#def84d;' >این لینک</a> ";
            body += "</span> ";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += "کلیک نمایید. ";
            body += "</span>";
            body += "<br />";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += "با سپاس از عضویت شما";
            body += "</span>";
            body += "</div>";
            body += "</div>";
            body += "</body>";
            return body;
        }

        public void UploadFile(RegisterViewModel model)
        {
            if (model != null)
            {
                HttpPostedFileBase FileData;
                ObjectId fileId = ObjectId.Empty;
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    FileData = Request.Files[0];
                    if (FileData.ContentLength > 0)
                    {
                        string fileName = Path.GetFileName(FileData.FileName);
                        if (fileName.Contains("#"))
                        {
                            fileName = fileName.Replace("#", "sharp");
                        }
                        string path = Path.Combine(Server.MapPath("~/Content/Images/Users"), fileName);

                        if (System.IO.File.Exists(path))
                        {
                            int i = 0;
                            while (System.IO.File.Exists(path))
                            {
                                if (i <= 100)
                                {
                                    int index = fileName.LastIndexOf(".");
                                    string imagename = fileName.Substring(0, index);
                                    imagename = imagename + "_" + i;
                                    fileName = imagename + fileName.Substring(index);

                                    path = Path.Combine(Server.MapPath("~/Content/Images/Users"), fileName);
                                    i = i + 1;
                                }
                                else
                                {
                                    Guid newid = new Guid();
                                    int index = fileName.LastIndexOf(".");
                                    string imagename = fileName.Substring(0, index);
                                    imagename = imagename + "_" + newid;
                                    fileName = imagename + fileName.Substring(index);

                                    path = Path.Combine(Server.MapPath("~/Content/Images/Users"), fileName);
                                }
                            }
                        }

                        FileData.SaveAs(path);
                        //GridFSBucket fs = new GridFSBucket(MongoConnection.instance.database);

                        //fileId = Articles.UploadFile(fs, fileName, path);
                        //model.ImageId = fileId;
                        model.ImagePath = "~/Content/Images/Users/" + fileName;
                    }
                }
            }
        }

        //
        // GET: /Account/ConfirmEmail

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmailAccepted(string userId, string code)
        {
            CreateArticleAllList();
            //return View("ConfirmEmail");

            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return View("ConfirmEmailAccepted");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ForgotPassword

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            CreateArticleAllList();
            return View();
        }

        //
        // POST: /Account/ForgotPassword

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            CreateArticleAllList();
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);

                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    return View("ForgotPasswordConfirmation");
                }
                string code_Password = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                string newPassword = Guid.NewGuid().ToString();
                string[] str = newPassword.Split('-');
                string finalPass = str[0] + "@A";
                var res = UserManager.ResetPassword(user.Id, code_Password, finalPass);

                var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl = Url.Action("Login", "Account", routeValues: null, protocol: Request.Url.Scheme);

                SmtpClient client = new SmtpClient("mail.cgsilent.com", 25);
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("info@cgsilent.com");
                mailMessage.To.Add(user.Email);
                mailMessage.Subject = "cgsilent.com رمز عبور جدید";
                mailMessage.Body = "<div style='direction:rtl; text-align:right;'> کاربر عزیز : " + user.UserName + " رمز عبور جدید شما " + finalPass + " می باشد. برای تغییر رمز عبور بر روی <a href=\"" + callbackUrl + " \">بازگشت به سایت </a></div>";
                mailMessage.IsBodyHtml = true;
                client.Send(mailMessage);
                //-------------------------------------------------------------------------------------            

                ViewBag.Link = callbackUrl;
                return View("ResetPasswordConfirmation");
                //return View("ForgotPasswordConfirmation");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        readonly ConcurrentQueue<SmtpClient> _clients = new ConcurrentQueue<SmtpClient>();

        private SmtpClient GetOrCreateSmtpClient()
        {
            SmtpClient client = null;
            if (_clients.TryDequeue(out client))
            {
                return client;
            }

            client = new SmtpClient();
            return client;
        }

        // GET: /Account/ForgotPasswordConfirmation

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            CreateArticleAllList();
            return View();
        }

        //
        // GET: /Account/ResetPassword

        [AllowAnonymous]
        public ActionResult ResetPassword()//string code)
        {
            CreateArticleAllList();
            //return code == null ? View("Error") : View();
            return View("ResetPassword");
        }

        //
        // POST: /Account/ResetPassword

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            CreateArticleAllList();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            CreateArticleAllList();
            return View();
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            CreateArticleAllList();
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode

        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl)
        {
            CreateArticleAllList();
            var userId = await SignInHelper.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl });
        }

        //
        // POST: /Account/SendCode

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            CreateArticleAllList();
            // Generate the token and send it
            if (!ModelState.IsValid)
            {
                return View();
            }

            if (!await SignInHelper.SendTwoFactorCode(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl });
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            CreateArticleAllList();
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInHelper.ExternalSignIn(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresTwoFactorAuthentication:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            CreateArticleAllList();
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInHelper.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            CreateArticleAllList();
            AuthenticationManager.SignOut();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            CreateArticleAllList();
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;

            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}