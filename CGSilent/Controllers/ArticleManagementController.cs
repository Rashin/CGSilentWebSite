﻿using CGSilent.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CGSilent.Controllers
{
    public class ArticleManagementController : Controller
    {
        // GET: SiteManagement
        public ActionResult Index()
        { 
            return View("ArticleManagement");
        }


        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }

}