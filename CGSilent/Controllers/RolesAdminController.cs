﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AspNet.Identity.MongoDB;
using MongoDB.Driver;
using CGSilent.Models;
using MvcPaging;
using System.Collections.Generic;
using System.Collections;
using System;

namespace CGSilent.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class RolesAdminController : Controller
    {
        public RolesAdminController()
        {
        }

        public RolesAdminController(ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public ApplicationIdentityContext IdentityContext
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationIdentityContext>();
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //
        // GET: /Roles/
        public async Task<ActionResult> Index()
        {
            var roles = await IdentityContext.AllRolesAsync();
            CreateArticleAllList();

            return View(roles);
        }

        //
        // GET: /Roles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);

            // Get the list of Users in this Role
            var users = await IdentityContext.Users.Find(u => u.Roles.Contains(role.Name)).ToListAsync();

            ViewBag.Users = users;
            ViewBag.UserCount = users.Count(); 
            CreateArticleAllList();

            return View(role);
        }

        //
        // GET: /Roles/Create
        public ActionResult Create()
        { 
            CreateArticleAllList();
            return View();
        }

        //
        // POST: /Roles/Create
        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel roleViewModel)
        {
            if (ModelState.IsValid)
            {
                var role = new IdentityRole(roleViewModel.Name);
                var roleresult = await RoleManager.CreateAsync(role);
                if (!roleresult.Succeeded)
                {
                    ModelState.AddModelError("", roleresult.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            } 
            CreateArticleAllList();

            return View();
        }

        //
        // GET: /Roles/Edit/Admin
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            RoleViewModel roleModel = new RoleViewModel { Id = role.Id, Name = role.Name };       
            CreateArticleAllList();

            return View(roleModel);
        }

        //
        // POST: /Roles/Edit/5
        [HttpPost]

        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Name,Id")] RoleViewModel roleModel)
        {
            if (ModelState.IsValid)
            {
                var role = await RoleManager.FindByIdAsync(roleModel.Id);
                role.Name = roleModel.Name;
                await RoleManager.UpdateAsync(role);
                return RedirectToAction("Index");
            } 
            CreateArticleAllList();

            return View();
        }

        //
        // GET: /Roles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                return HttpNotFound();
            } 
            CreateArticleAllList();

            return View(role);
        }

        //
        // POST: /Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id, string deleteUser)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var role = await RoleManager.FindByIdAsync(id);
                if (role == null)
                {
                    return HttpNotFound();
                }
                IdentityResult result;
                if (deleteUser != null)
                {
                    result = await RoleManager.DeleteAsync(role);
                }
                else
                {
                    result = await RoleManager.DeleteAsync(role);
                }
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            } 
            CreateArticleAllList();

            return View();
        }


        private void CreateArticleAllList()
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>(); 
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<Models.ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();
             
            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            long articlecount = Articles.GetNumberOfArticles();
            for (int i = 0; i < articlecount; i++)
            {
                articleGroup.ArticleId = (allArticles[i]).Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[i]);
                        } 
                    }
                }
            }

            ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
            {
                Article = null,
                LikeDislike = null,
                ArticleComments = null,
                Comment = null,
                ArticleGalary = null,
                ArticleGroupsData = null
            };
            aaList.Add(aa);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom=advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }


        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }


    }
}

