﻿using CGSilent.Models;
using Microsoft.AspNet.Identity;
using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using PagedList;
using System.Linq;
using MongoDB.Driver;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.IO;

namespace CGSilent.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        { }
        public HomeController(ApplicationUserManager userManager)
        {
            UserManager = userManager;


        }
        public ApplicationIdentityContext IdentityContext
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationIdentityContext>();
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index(int? page)
        {


            #region Comment For Edit DateTime ToString(s)
            //var collection = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics");
            //var article = collection.Find(MongoDB.Driver.Builders<SiteStatics>.Filter.Empty).ToList();
            //foreach (var item in article)
            //{
            //    var update1 = MongoDB.Driver.Builders<SiteStatics>.Update.Set("CreatedDate", DateTime.Parse(item.CreatedDate).ToString("s"));
            //    collection.UpdateOne(MongoDB.Driver.Builders<SiteStatics>.Filter.Eq("_id", item.Id), update1);
            //    //if (item.UpdateDate != null)
            //    //{
            //    //    var update2 = MongoDB.Driver.Builders<SiteStatics>.Update.Set("UpdateDate", DateTime.Parse(item.UpdateDate).ToString("s"));
            //    //    collection.UpdateOne(MongoDB.Driver.Builders<SiteStatics>.Filter.Eq("_id", item.Id), update2);
            //    //}
            //}
            #endregion  Comment For Edit DateTime ToString(s)
            // if no page was specified in the querystring, default to the first page (1)
            var pageNumber = page ?? 1;

            Articles art = new Articles();
            CreateArticleAllList(page ?? 1);

            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            UserRoles roles = new UserRoles();
            if (User.Identity.IsAuthenticated)
            {
                roles.Id = ObjectId.Parse(User.Identity.GetUserId());
                roles = roles.LoadDataByUserId();
            }
            else
            {
                roles.Id = ObjectId.Empty;
            }

            //افزودن آمار بازدید از سایت
            if (roles != null)
            {
                InsertSiteStatic(Convert.ToString(ipEntry.AddressList[1]), roles.Roles);
                //یافتن نام نقش کاربری بریا نمایش در صفحات
                if (User.Identity.IsAuthenticated)
                {
                    for (int i = 0; i < roles.Roles.Length; i++)
                    {
                        if (i == roles.Roles.Length - 1)
                            ViewBag.UserRoles += roles.Roles[i].ToString();
                        else
                            ViewBag.UserRoles += roles.Roles[i].ToString() + ",";
                    }
                }
            }

            LikeDislikeArticle ld = new LikeDislikeArticle();
            if (User.Identity.GetUserId() != "")
                ld.UserIp = User.Identity.GetUserId();
            else
            {
                ld.UserIp = Convert.ToString(ipEntry.AddressList[1]);
            }
            int likesCount = 0;
            likesCount = ld.FavoriteArticlesByUserIpCount();
            ViewBag.LikeCount = likesCount;
           
            SearchArticle searchmodel = new SearchArticle();
            ViewBag.searchModel = searchmodel;

            if (User.Identity.IsAuthenticated)
            {
                ObjectId userID = ObjectId.Parse(User.Identity.GetUserId());
                UserBasket info = new UserBasket();
                info.Id = userID;
                info.ArticleId = info.LoadArticleBasketByUserId();
                //List<Articles> list_Basketmodel = info.LoadArticleDataByUserId();
                //IList list_Basket = art.LoadDataByUserId(userID);
                ViewBag.Basket = info.ArticleId.Count + "";

            }
            else
            {
                ViewBag.Basket = "0";
            }
            return View("Index");
        }

 
        private void CheckVIPDate()
        {
             if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }

        [HttpGet]
        public ActionResult SiteManagement()
        {
            return PartialView("_SiteManagement");
        }

        [HttpGet]
        private void InsertSiteStatic(string ip, string[] RoleNames)
        {
            SiteStatics ss = new SiteStatics();
            ss.CreatedDate = DateTime.Now.ToString("s");
            ss.PageName = "Home";
            ss.ArticleId = ObjectId.Empty;
            ss.StoreId = ObjectId.Empty;
            if (User.Identity.IsAuthenticated)
                ss.UserIdOrIp = User.Identity.GetUserId();
            else
                ss.UserIdOrIp = ip;
            if (RoleNames != null)
                for (int i = 0; i < RoleNames.Length; i++)
                {
                    ss.RoleName = RoleNames[i];
                    ss.Insert();
                }
            else
            {
                ss.Insert();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Search(string SearchWord, int? page)
        {
            if (Session["SearchWord"] != null)
                Session.Remove("SearchWord");
            Session.Add("SearchWord", SearchWord);
            if (ModelState.IsValid)
            {
                SelectedSearchItems searchItems = new SelectedSearchItems();
                searchItems.SearchWord = SearchWord;
                if (Session["SearchText"] != null)
                {
                    searchItems.SearchWord = Session["SearchText"].ToString();
                    Session.Remove("SearchText");
                }
                //-------------------------- Search Word -------------------------------------------
                Articles art = new Articles();
                List<Articles> result = new List<Articles>();
                int pageNumber = page ?? 1;
                long TotalCount;

                long outSearchword;
                if (long.TryParse(SearchWord, out outSearchword))
                    result = art.SearchArticleNumber(long.Parse(SearchWord), pageNumber, 10, out TotalCount);
                else
                    result = art.SearchText(SearchWord, pageNumber, 10, out TotalCount);

                CreateArticleAllList(page ?? 1, result, TotalCount);
                //-------------------------- Create DropDownLists ForAdvance Search ----------------

                searchItems = PutDataToDropDowns(searchItems);
                return View("ArticleListFilter", searchItems);

            }
            else
            {
                return View(SearchWord);
            }
        }
        [HttpGet]
        public ActionResult Search(int? page)
        {
            string SearchWord = "";
            if (Session["SearchWord"] != null)
                SearchWord = Session["SearchWord"].ToString();
            if (ModelState.IsValid)
            {
                SelectedSearchItems searchItems = new SelectedSearchItems();
                searchItems.SearchWord = SearchWord;
                if (Session["SearchText"] != null)
                {
                    searchItems.SearchWord = Session["SearchText"].ToString();
                    Session.Remove("SearchText");
                }
                //-------------------------- Search Word -------------------------------------------
                Articles art = new Articles();
                List<Articles> result = new List<Articles>();
                int pageIndex = page ?? 1;
                long TotalCount;
                result = art.SearchText(SearchWord, pageIndex, 10, out TotalCount);
                CreateArticleAllList(pageIndex, result, TotalCount);
                //List<Articles> allArticles = art.SearchArticle(SearchWord);

                // CreateArticleAllListByArticleData(result, 1);
                //-------------------------- Create DropDownLists ForAdvance Search ----------------
                var pageNumber = page ?? 1;
                searchItems = PutDataToDropDowns(searchItems);
                ViewBag.SearchText = SearchWord;
                return View("ArticleListFilter", searchItems);

            }
            else
            {
                return View(SearchWord);
            }
        }

        private SelectedSearchItems PutDataToDropDowns(SelectedSearchItems SearchListItems)
        {
            #region ddl Groups For Search 

            Group ddl_Group = new Group();
            List<SelectListItem> list_sli = new List<SelectListItem>();
            List<Group> list_Group = ddl_Group.LoadDataAll();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name != "تبلیغات" || list_Group[i].Name != "اسلاید شو" || list_Group[i].Name != "گروه تست")
                {
                    SelectListItem sli = new SelectListItem();
                    sli.Text = list_Group[i].Name;
                    sli.Value = list_Group[i].Id.ToString();
                    list_sli.Add(sli);
                }
            }

            SearchListItems.GroupItems = list_sli;
            SearchListItems.GroupId = ObjectId.Empty.ToString();

            #endregion ddl Groups For Search 

            #region ddl For Search Time

            list_sli = new List<SelectListItem>();
            SelectListItem slitime = new SelectListItem();

            slitime.Text = "همه";
            slitime.Value = "all";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "امروز";
            slitime.Value = "today";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "دیروز";
            slitime.Value = "yesterday";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "هفت روز گذشته";
            slitime.Value = "week";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "یک ماه گذشته";
            slitime.Value = "month";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "یک سال گذشته";
            slitime.Value = "year";
            list_sli.Add(slitime);

            SearchListItems.SearchTime = list_sli;
            SearchListItems.SearchTimeSelected = "";

            list_sli = new List<SelectListItem>();
            slitime = new SelectListItem();

            slitime.Text = "جدیدترین";
            slitime.Value = "dec";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "قدیمی ترین";
            slitime.Value = "asc";
            list_sli.Add(slitime);

            SearchListItems.SearchTimeOrder = list_sli;
            SearchListItems.SearchTimeOrderSelected = "";

            #endregion ddl For Search Time

            #region ddl For Search Type

            list_sli = new List<SelectListItem>();
            slitime = new SelectListItem();
            slitime.Text = "مطالب";
            slitime.Value = "article";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "مطالب و کلمات کلیدی";
            slitime.Value = "articlkey";
            list_sli.Add(slitime);


            slitime = new SelectListItem();
            slitime.Text = "مطالب و تگ ها";
            slitime.Value = "articletag";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "فروشگاه";
            slitime.Value = "store";
            list_sli.Add(slitime);

            slitime = new SelectListItem();
            slitime.Text = "فروشگاه و کلمات کلیدی";
            slitime.Value = "storekey";
            list_sli.Add(slitime);


            slitime = new SelectListItem();
            slitime.Text = "فروشگاه و تگ ها";
            slitime.Value = "storetag";
            list_sli.Add(slitime);


            SearchListItems.SearchByType = list_sli;
            SearchListItems.SearchByTypeSelected = "";

            //slitime.Text = "نزولی";
            //slitime.Value = "desc";
            //list_sli.Add(slitime);

            //slitime = new SelectListItem();
            //slitime.Text = "صعودی";
            //slitime.Value = "asce";
            //list_sli.Add(slitime);

            //SearchListItems.SearchByTypeOrder = list_sli;
            //SearchListItems.SearchByTypeOrderSelected = "";

            #endregion ddl For Search Type

            return SearchListItems;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AdvanceSearch(SelectedSearchItems model, int? page, string exactword, bool chb_exactword = false)
        {
            if (ModelState.IsValid)
            {
                AdvanceSearchResult allArticles = new AdvanceSearchResult();
                var pageNumber = page ?? 1;

                Articles art = new Articles();
                List<Articles> art_List = new List<Articles>();
                if (model.ArticleNumber > 0)
                {
                    int pageIndex = page ?? 1;
                    long TotalCount;
                    art_List = art.SearchArticleNumber(model.ArticleNumber, pageIndex, 10, out TotalCount);
                    CreateArticleAllList(pageIndex, art_List, TotalCount);
                }
                else
                {

                    if (model.SearchTimeSelected == null)
                    {
                        model.SearchTimeSelected = model.SearchTime[0].Value;
                    }
                    if (model.SearchTimeOrderSelected == null)
                    {
                        model.SearchTimeOrderSelected = model.SearchTimeOrder[0].Value;
                    }
                    //جستجوی کلمه در آرایه ی allArticles
                    allArticles = art.AdvanceSearchArticle(model, chb_exactword);
                    if (allArticles != null)
                    {
                        if (allArticles.ArticlesResult != null)
                            CreateArticleAllList(1, allArticles.ArticlesResult, allArticles.ArticlesResult.Count);

                        //TODO نمایش نتیجه جستجو بر اساس فروشگاه
                        //if (allArticles.StoreResult != null)
                        //    CreateArticleAllList(1, allArticles.StoreResult, allArticles.StoreResult.Count); 
                    }
                }
                PutDataToDropDowns(model);
                return View("ArticleListFilter", model);
            }
            else
            {
                return View(model.SearchWord);
            }
        }


        [HttpGet]
        public ActionResult FavoritArticles()
        {
            LikeDislikeArticle ld = new LikeDislikeArticle();
            if (User.Identity.GetUserId() != "")
                ld.UserIp = User.Identity.GetUserId();
            else
            {
                string strHostName = "";
                strHostName = Dns.GetHostName();
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                ld.UserIp = Convert.ToString(ipEntry.AddressList[1]);
            }
            IList il = Articles.LoadAllArticlesFromToday();
            List<Articles> il_FavoritArticles = new List<Articles>();
            IList<LikeDislikeArticle> favorit_List = ld.LoadFavoriteArticlesByUserIp();
            if (favorit_List.Count > 0)
            {
                for (int i = 0; i < favorit_List.Count; i++)
                {
                    for (int j = 0; j < il.Count; j++)
                    {
                        if (favorit_List[i].LikeCount > 0 && favorit_List[i].ArticleId == ((Articles)il[j]).Id)
                        {
                            il_FavoritArticles.Add((Articles)il[j]);
                            break;
                        }
                    }
                }
            }
            CreateArticleAllList(1, il_FavoritArticles, il_FavoritArticles.Count);
            //  CreateArticleAllListByArticleData(il_FavoritArticles, 1);
            return View("ArticleListFilter");
        }

        private void CreateArticleAllList(int page)
        {
            CheckVIPDate();
            CreateArticleAllList(page, Articles.LoadAllArticlesFromTodayByPageNumber(page), Articles.GetNumberOfArticles());
        }

        private void CreateArticleAllList(int page, List<Articles> displayArticles, long articlecount)
        {
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
            Articles articleAll = new Articles();
            ArticleComment comment = new ArticleComment();
            List<ArticleComment> comment_list = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();
            ArticleGalary galary = new ArticleGalary();


            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            List<Articles> allSlideShowArticle = Articles.LoadArticlesByGroupId(new ObjectId("5a526a33e290b819dc102a35"));

            for (int r = 0; r < displayArticles.Count; r++)
            {
                Articles al = displayArticles[r];

                var gal = galary.LoadByArticleId(al.Id);
                if (gal != null)
                    articleGalaryList.Add(gal);

                List<ArticleGroups> articleGroups = new List<ArticleGroups>();
                for (int j = 0; j < articleGroup_List.Count; j++)
                {
                    if (articleGroup_List[j].ArticleId == al.Id)
                    {
                        articleGroups.Add(articleGroup_List[j]);
                    }
                }
                int likesCount = 0;
                int dislikesCount = 0;
                foreach (var item in likedislike.LoadDataByArticleId(al.Id))
                {
                    likesCount += item.LikeCount;
                    dislikesCount += item.DislikeCount;
                }

                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                {
                    ArticleId = al.Id,
                    DislikeCount = dislikesCount,
                    LikeCount = likesCount,
                    UserIp = ""
                };
                for (int j = 0; j < comment_list.Count; j++)
                {
                    if (comment_list[j].ArticleId == al.Id)
                    {
                        articleComments.Add(comment_list[j]);
                    }
                }
                int commentsCount = articleComments.Count;
                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                {
                    Article = al,
                    LikeDislike = ldarticle,
                    ArticleComments = articleComments,
                    Comment = comment,
                    ArticleGalary = articleGalaryList,
                    ArticleGroupsData = articleGroups
                };
                aaList.Add(aa);
            }
            int articlePerPage = 10;
            for (int i = 0; i < articlecount; i++)
            {
                if (i < (page * articlePerPage))
                {
                    if (i >= ((page - 1) * articlePerPage))
                    {
                    }
                    else
                    {
                        aaList.Insert(0, null);
                    }
                }
                else
                {
                    aaList.Add(null);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = allSlideShowArticle,
                TreeGroup = list_Group
            };

            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(page, articlePerPage); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }


        [HttpGet]
        private List<Articles> LoadSliderArticles(string SliderId)
        {
            IList List_Article = Articles.LoadAllArticlesFromToday();

            ArticleGroups groups = new ArticleGroups();
            groups.GroupId = ObjectId.Parse(SliderId);
            IList ArticleGroups = groups.LoadDataByObject();

            List<Articles> SliderArticles = new List<Articles>();
            for (int i = 0; i < List_Article.Count; i++)
            {
                for (int j = 0; j < ArticleGroups.Count; j++)
                {
                    if (((Articles)List_Article[i]).Id == ((ArticleGroups)ArticleGroups[j]).ArticleId)
                        SliderArticles.Add((Articles)List_Article[i]);
                }
            }
            return SliderArticles;
        }


        [HttpGet]
        //  [Route("ArticlesByGroupgroupid)]
        public ActionResult ArticlesByGroup(ObjectId groupid, int? page)
        {
            ViewBag.SelectedGroup = groupid;
            var pageNumber = page ?? 1;
            LoadAllArticlesByGroupId(groupid, pageNumber);
            return View("ArticleFilterGroup");
        }

        private void LoadAllArticlesByGroupId(ObjectId groupid, int pagenumber)
        {
            List<Articles> article = new List<Articles>();
            List<ArticleGroups> agList = new List<ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            ag.GroupId = groupid;

            agList = ag.LoadDataByGroupId();
            if (agList.Count > 0)
            {
                var list = Articles.LoadAllArticlesFromToday();
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        for (int j = 0; j < agList.Count; j++)
                        {
                            if ((list[i]).Id == agList[j].ArticleId)
                            {
                                article.Add(list[i]);
                                break;
                            }
                        }
                    }
                }
            }
            CreateArticleListByGroupId(article, agList, groupid, pagenumber);
        }


        private void CreateArticleListByGroupId(IList ArticleList, List<ArticleGroups> articleGroup_List, ObjectId groupid, int page)
        {
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            Group group = new Group();
            IList g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            ArticleGroups ag = new ArticleGroups();
            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add((Group)g_List[i]);
            }
            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = articleGroup.LoadDataAll();

            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();

            ArticleComment comment = new ArticleComment();
            List<ArticleComment> list_Comments = comment.LoadDataAll();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();
            ag = new ArticleGroups();

            for (int i = 0; i < allArticles.Count; i++)
            {
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add((Articles)allArticles[i]);
                        }
                    }
                }
            }
            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = (Articles)ArticleList[i];
                    for (int j = 0; j < galaryList.Count; j++)
                    {
                        if (galaryList[j].ArticleId == al.Id)
                            articleGalaryList.Add(galaryList[j]);
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    if (articleGroup_List.Count > 0)
                    {
                        for (int j = 0; j < list_likedislike.Count; j++)
                        {
                            if (list_likedislike[j].ArticleId == al.Id)
                            {
                                likesCount = likesCount + list_likedislike[j].LikeCount;
                                dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                                if (list_likedislike[j].UserIp == ipaddress)
                                {
                                    ip = list_likedislike[j].UserIp;
                                }
                                else { ip = ""; }
                            }
                        }
                        LikeDislikeArticle ldarticle = new LikeDislikeArticle
                        {
                            ArticleId = al.Id,
                            DislikeCount = dislikesCount,
                            LikeCount = likesCount,
                            UserIp = ip
                        };

                        List<ArticleComment> articleComments = new List<ArticleComment>();
                        for (int j = 0; j < list_Comments.Count; j++)
                        {
                            if (list_Comments[j].ArticleId == al.Id)
                                articleComments.Add(list_Comments[j]);
                        }
                        commentsCount = articleComments.Count;
                        List<ArticleGroups> al_ArticleGroup = new List<ArticleGroups>();
                        for (int r = 0; r < articleGroup_List.Count; r++)
                        {
                            if (articleGroup_List[r].ArticleId == al.Id)
                            {
                                al_ArticleGroup.Add(articleGroup_List[r]);
                            }
                        }
                        ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                        {
                            Article = al,
                            LikeDislike = ldarticle,
                            ArticleComments = articleComments,
                            Comment = comment,
                            ArticleGalary = articleGalaryList,
                            ArticleGroupsData = al_ArticleGroup
                        };
                        aaList.Add(aa);
                    }
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementBottom = advertizementBottomData,
                AdvertizementTop = advertizementTopData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(page, 10); // will only contain 25 products max because of the pageSizes
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }


        private List<ArticleWithLikeDislikeAndComments> LoadAllArticles()
        {
            IList il = Articles.LoadAllArticlesFromToday();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            CreateArticleAllListReturnLikeDislike(il, aaList);
            return aaList;
        }

        private void CreateArticleAllListReturnLikeDislike(IList ArticleList, List<ArticleWithLikeDislikeAndComments> aaList)
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            Group group = new Group();
            IList g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add((Group)g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<Articles> AdvertizementData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            List<ArticleGroups> ArticleGroupslist;

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            ArticleGroups articleGroup = new ArticleGroups();
            ArticleGroupslist = articleGroup.LoadDataAll();

            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();

            ArticleComment comment = new ArticleComment();
            List<ArticleComment> list_Comments = comment.LoadDataAll();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            for (int g = 0; g < allArticles.Count; g++)
            {
                articleGroup.ArticleId = allArticles[g].Id;

                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[g].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add((Articles)allArticles[g]);
                        }
                    }
                }
                for (int j = 0; j < galaryList.Count; j++)
                {
                    if (galaryList[j].ArticleId == allArticles[g].Id)
                        articleGalaryList.Add(galaryList[j]);
                }
            }
            for (int i = 0; i < ArticleList.Count; i++)
            {
                Articles al = (Articles)ArticleList[i];

                int likesCount = 0;
                int dislikesCount = 0;
                int commentsCount = 0;
                string ip = "0";

                for (int j = 0; j < list_likedislike.Count; j++)
                {
                    if (list_likedislike[j].ArticleId == al.Id)
                    {
                        likesCount = likesCount + list_likedislike[j].LikeCount;
                        dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                        if (list_likedislike[j].UserIp == ipaddress)
                        {
                            ip = list_likedislike[j].UserIp;
                        }
                        else
                            ip = "";
                    }
                }
                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                {
                    ArticleId = ((Articles)ArticleList[i]).Id,
                    DislikeCount = dislikesCount,
                    LikeCount = likesCount,
                    UserIp = ip
                };

                List<ArticleComment> articleComments = new List<ArticleComment>();
                for (int j = 0; j < list_Comments.Count; j++)
                {
                    if (list_Comments[j].ArticleId == al.Id)
                        articleComments.Add(list_Comments[j]);
                }
                commentsCount = articleComments.Count;

                List<ArticleGroups> articlegroups = new List<ArticleGroups>();
                for (int j = 0; j < articleGroup_List.Count; j++)
                {
                    articlegroups.Add(articleGroup_List[j]);
                }
                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                {
                    Article = al,
                    LikeDislike = ldarticle,
                    ArticleComments = articleComments,
                    Comment = comment,
                    ArticleGalary = articleGalaryList,
                    ArticleGroupsData = articlegroups
                };
                aaList.Add(aa);
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(1, 10); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;
            ViewBag.AllData = articleList;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }


        [HttpGet]
        [ValidateInput(false)]
        public ActionResult About()
        {
            HtmlContent hc = new HtmlContent();
            hc.Type = "About";
            IList hc_List = hc.LoadDataByType();
            if (hc_List.Count > 0)
                ViewBag.Content = ((HtmlContent)hc_List[0]).Content;
            else
                ViewBag.Content = "";

            CreateArticleAllList(1);
            return View("About");

        }


        [HttpGet]
        public ActionResult Rules()
        {
            CreateArticleAllList(1);
            return View("Rules");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditAbout(HtmlContent model)
        {
            model.UpdatedDate = DateTime.Now.ToString();
            model.Update();

            CreateArticleAllList(1);
            return View("About", model);
        }



        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Contact()
        {
            HtmlContent hc = new HtmlContent();
            hc.Type = "Contact";
            IList hc_List = hc.LoadDataByType();
            if (hc_List.Count > 0)
                ViewBag.Content = ((HtmlContent)hc_List[0]).Content;
            else
                ViewBag.Content = "";

            CreateArticleAllList(1);
            return View("Contact");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditContact(HtmlContent model)
        {
            model.UpdatedDate = DateTime.Now.ToString();
            model.Update();

            CreateArticleAllList(1);
            return View("Contact", model);
        }



        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Rendering()
        {
            HtmlContent hc = new HtmlContent();
            hc.Type = "Rendering";

            IList hc_List = hc.LoadDataByType();
            if (hc_List.Count > 0)
                ViewBag.Content = ((HtmlContent)hc_List[0]).Content;
            else
                ViewBag.Content = "";

            IList il = Articles.LoadAllArticlesFromToday();
            CreateArticleAllList(1);
            return View("Rendering");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditRendering(HtmlContent model)
        {
            model.UpdatedDate = DateTime.Now.ToString();
            model.Update();

            CreateArticleAllList(1);
            return View("Rendering", model);
        }


        [HttpGet]
        [ValidateInput(false)]
        public ActionResult Project()
        {
            HtmlContent hc = new HtmlContent();
            hc.Type = "Project";

            IList hc_List = hc.LoadDataByType();
            if (hc_List.Count > 0)
                ViewBag.Content = ((HtmlContent)hc_List[0]).Content;
            else
                ViewBag.Content = "";

            CreateArticleAllList(1);
            return View("Project");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditProject(HtmlContent model)
        {
            model.UpdatedDate = DateTime.Now.ToString();
            model.Update();

            CreateArticleAllList(1);
            return View("Project", model);
        }
    }
}