﻿using CGSilent.Models;
using MvcPaging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using MongoDB.Driver;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using CGSilent.Helpers;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using MongoDB.Bson;
using System.IO;
using System.Net.Mail;

namespace CGSilent.Controllers
{
    public class VIPaccountController : Controller
    {
        public VIPaccountController()
        {


        }
        public VIPaccountController(ApplicationUserManager userManager)
        {
            UserManager = userManager;


        }
        public ApplicationIdentityContext IdentityContext
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationIdentityContext>();
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: VIPaccount
        public ActionResult Index()
        {
            return View();
        }



        private int uniqueGenerator()
        {
            Random num = new Random();
            int id = num.Next(1, 500000000);
            IMongoCollection<PaymentModel> collection = MongoConnection.instance.GetCollection<PaymentModel>("Payment");
            var allpayments = collection.Find(_ => true).ToList();
            var payment = allpayments.Where(x => x.PaymentId == id).FirstOrDefault();
            if (payment == null)
            {
                return id;
            }
            else
            {
                uniqueGenerator();
            }
            return id;

        }
        private int InsertPayment(long amount, string bankName, int? IsVip)
        {
            int paymentId = 0;
            var payment = new PaymentModel()
            {
                Amount = amount,
                BankName = bankName,
                PaymentStatus = "-100",
                SaleReferenceId = 0,
                PaymentIsFinished = false,
                UserId = User.Identity.GetUserId(),
                PaymentId = uniqueGenerator(),
                IsVipPayment = IsVip != null && IsVip.Value == 1 ? true : false
            };

            IMongoCollection<PaymentModel> collection = MongoConnection.instance.GetCollection<PaymentModel>("Payment");
            collection.InsertOne(payment);

            paymentId = payment.PaymentId;
            return paymentId;
        }


        private void UpdatePayment(int paymentId, string vresult, long saleReferenceId, string refId, bool paymentFinished = false)
        {
            IMongoCollection<PaymentModel> collection = MongoConnection.instance.GetCollection<PaymentModel>("Payment");
            var allpayments = collection.Find(_ => true).ToList();
            var payment = allpayments.Where(x => x.PaymentId == paymentId).FirstOrDefault();
            //var payment = Collation..Find(paymentId);

            if (payment != null)
            {
                payment.PaymentStatus = vresult;
                payment.SaleReferenceId = saleReferenceId;
                payment.PaymentIsFinished = paymentFinished;
                payment.BuyDateTime = DateTime.Parse(DateTime.Now.ToString("s"));

                if (refId != null)
                {
                    payment.ReferenceNumber = refId;
                }
                var filter = Builders<PaymentModel>.Filter.Eq(s => s.PaymentId, payment.PaymentId);
                var result = collection.ReplaceOne(filter, payment);
                //db.Entry(payment).State = EntityState.Modified;
                //db.SaveChanges();
            }
            else
            {
                // اطلاعاتی از دیتابیس پیدا نشد
            }
        }


        [HttpGet]
        public ActionResult BuyVipAcount()
        {
            CreateArticleAllList();
            VIPDropDown vipdd = LoadVIPPlaneDropDow();

            return View("BuyVipAcount", vipdd);
        }

        private VIPDropDown LoadVIPPlaneDropDow()
        {
            VIPPlan vipmodel = new VIPPlan();
            List<VIPPlan> list_vipPlan = new List<VIPPlan>();
            list_vipPlan = vipmodel.LoadDataActiveAll();
            VIPDropDown vipPlan = new VIPDropDown();
            List<SelectListItem> list_sli = new List<SelectListItem>();
            SelectListItem sli = new SelectListItem();

            for (int i = 0; i < list_vipPlan.Count; i++)
            {
                if (!(((VIPPlan)list_vipPlan[i]).IsDeleted))
                {
                    sli = new SelectListItem();
                    sli.Value = ((VIPPlan)list_vipPlan[i]).Id.ToString();
                    sli.Text = ((VIPPlan)list_vipPlan[i]).Caption;
                    if (i == 0)
                        sli.Selected = true;
                    list_sli.Add(sli);
                }
            }

            vipPlan.Plans = list_sli;
            return vipPlan;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BuyVipAcount(VIPPlan vipPlan, string dargahpardakht, int? IsVip)
        {
            VIPDropDown vipdd = LoadVIPPlaneDropDow();
            VIPPlan plan = new VIPPlan();
            plan.Id = vipPlan.Id;
            vipPlan = plan.LoadDataById();

            #region اکشن اتصال به درگاه
            // GET: Home
            // if(!User.Identity.IsAuthenticated)(User.IsInRole("Admin") || User.IsInRole("TestVIP"))
            //{
                string path = Path.Combine(Server.MapPath("~/Content/Images/Article"), "fileLOG" + new Guid());
                try
                {
                    long result;
                    //System.IO.File.WriteAllText(path, " vipPlan Price : " + vipPlan.Price.ToString() + " ***********");
                    bool success = long.TryParse(vipPlan.Price.ToString(), out result);

                    // System.IO.File.WriteAllText(path, " success : " + success.ToString() + " * **********");
                    if (success)
                    {

                        // آدرس برگشت از درگاه
                        string redirectPage = "https://cgsilent.ir/VIPaccount/return";
                        // ثبت اطلاعات اولیه
                        int paymentId = InsertPayment(result, dargahpardakht, 1);
                        //  System.IO.File.WriteAllText(path, " paymentId : " + paymentId.ToString() + " * **********");
                        if (paymentId > 0)
                        {
                            switch (dargahpardakht)
                            {
                                case "سامان":
                                    SamanPayment(result, redirectPage, paymentId);
                                    break;
                                default:
                                    ViewBag.Message = "درگاهی برای پرداخت انتخاب نشده است";
                                    break;
                            }
                        }
                        else
                        {
                            ViewBag.Message = "پاسخی از درگاه دریافت نشد";
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewBag.Message = "پاسخی از درگاه دریافت نشد";
                    System.IO.File.WriteAllText(path, " Exception : " + e.Message + " ***********");
                }
            //}
            //else
            //{
            //    ViewBag.PaymentForVIP = "اتصال به درگاه پرداخت میسر نمی باشد.";
            //}
            #endregion


            CreateArticleAllList();
            return View(vipdd);
        }



        public ActionResult Return()
        {
            #region اکشن برگشت از درگاه
            ViewBag.BankName = "درگاه بانک سامان";
            SamanReturn();
            CreateArticleAllList(1);
            return View();
            #endregion
        }

        private void CreateArticleAllList(int page)
        {
            CheckVIPDate();
            CreateArticleAllList(page, Articles.LoadAllArticlesFromTodayByPageNumber(page), Articles.GetNumberOfArticles());
        }

        private void CreateArticleAllList(int page, List<Articles> displayArticles, long articlecount)
        {
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeArticle likedislike = new LikeDislikeArticle();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
            Articles articleAll = new Articles();
            ArticleComment comment = new ArticleComment();
            List<ArticleComment> comment_list = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();
            ArticleGalary galary = new ArticleGalary();


            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            List<Articles> allSlideShowArticle = Articles.LoadArticlesByGroupId(new ObjectId("5a526a33e290b819dc102a35"));

            for (int r = 0; r < displayArticles.Count; r++)
            {
                Articles al = displayArticles[r];

                var gal = galary.LoadByArticleId(al.Id);
                if (gal != null)
                    articleGalaryList.Add(gal);

                List<ArticleGroups> articleGroups = new List<ArticleGroups>();
                for (int j = 0; j < articleGroup_List.Count; j++)
                {
                    if (articleGroup_List[j].ArticleId == al.Id)
                    {
                        articleGroups.Add(articleGroup_List[j]);
                    }
                }
                int likesCount = 0;
                int dislikesCount = 0;
                foreach (var item in likedislike.LoadDataByArticleId(al.Id))
                {
                    likesCount += item.LikeCount;
                    dislikesCount += item.DislikeCount;
                }

                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                {
                    ArticleId = al.Id,
                    DislikeCount = dislikesCount,
                    LikeCount = likesCount,
                    UserIp = ""
                };
                for (int j = 0; j < comment_list.Count; j++)
                {
                    if (comment_list[j].ArticleId == al.Id)
                    {
                        articleComments.Add(comment_list[j]);
                    }
                }
                int commentsCount = articleComments.Count;
                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                {
                    Article = al,
                    LikeDislike = ldarticle,
                    ArticleComments = articleComments,
                    Comment = comment,
                    ArticleGalary = articleGalaryList,
                    ArticleGroupsData = articleGroups
                };
                aaList.Add(aa);
            }
            int articlePerPage = 10;
            for (int i = 0; i < articlecount; i++)
            {
                if (i < (page * articlePerPage))
                {
                    if (i >= ((page - 1) * articlePerPage))
                    {
                    }
                    else
                    {
                        aaList.Insert(0, null);
                    }
                }
                else
                {
                    aaList.Add(null);
                }
            }
            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = allSlideShowArticle,
                TreeGroup = list_Group
            };
            IQueryable<ArticleWithLikeDislikeAndComments> IQ_List_Article = articleList.ArticleList.AsQueryable();
            var onePageOfProducts = IQ_List_Article.ToPagedList(page, articlePerPage); // will only contain 25 products max because of the pageSize
            ViewBag.ArticleDataForPageingAndView = onePageOfProducts;

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }



        /********************************************/

        private int InsertPayment(long price, string bankName)
        {
            #region ثبت اطلاعات اولیه پرداخت در دیتابیس

            //    int paymentId = 0;
            //    try
            //    {

            //        var payment = new Payment();

            //        // قیمت پرداخت
            //        payment.Amount = price;

            //        // وضعیت پرداخت در حالت پیش فرض 100- است
            //        payment.StatusPayment = "-100";

            //        // شماره پیگیری را در ثبت اولیه 0 ثبت می کنیم
            //        payment.SaleReferenceId = 0;

            //        // نام بانک انتخاب شده برای پرداخت
            //        payment.BankName = bankName;

            //        // فقط در صورتی که این فید ترو باشد پرداخت موفق بوده است
            //        payment.PaymentFinished = false;

            //        // آی دی کاربر درحال پرداخت که ما یک در نظر گرفتیم و شما باید آی دی کاربری که پرداخت را انجام می دهد ثبت کنید
            //        payment.UserId = 1;

            //        // ثبت اطلاعات در دیتابیس
            //        db.Payments.Add(payment);
            //        db.SaveChanges();

            //        // شماره پرداخت که همان آی دی جدول می باشد که به بانک ارسال می کنیم و هنگام بازگشت اطلاعات را از دیتابیس پیدا می کنیم
            //        paymentId = payment.PaymentId;

            //    }
            //    catch (Exception ex)
            //    {
            //    }

            //    return paymentId;
            return 0;
            #endregion
        }




        private void UpdatePayment(long paymentId, string vresult, long saleReferenceId, string refId, bool paymentFinished = false)
        {
            #region متد ویرایش پرداخت 
            //    var payment = db.Payments.Find(paymentId);

            //    if (payment != null)
            //    {
            //        payment.StatusPayment = vresult;
            //        payment.SaleReferenceId = saleReferenceId;
            //        payment.PaymentFinished = paymentFinished;

            //        if (refId != null)
            //        {
            //            payment.ReferenceNumber = refId;
            //        }

            //        db.Entry(payment).State = EntityState.Modified;
            //        db.SaveChanges();
            //    }
            //    else
            //    {
            //        // اطلاعاتی از دیتابیس پیدا نشد
            //    }
            #endregion
        }



        private long FindAmountPayment(long paymentId)
        {
            #region پیدا کردن مبلغ خرید
            IMongoCollection<PaymentModel> collection = MongoConnection.instance.GetCollection<PaymentModel>("Payment");
            var allpayments = collection.Find(_ => true).ToList();
            var payment = allpayments.Where(x => x.PaymentId == paymentId).FirstOrDefault();
            long amount = payment.Amount;

            return amount;
            #endregion
        }


        /********************************************/

        private void SamanPayment(long price, string redirectPage, int paymentId)
        {
            #region متد پرداخت از درگاه سامان
            try
            {
                // شماره ترمینال
                string termId = "11522320";

                // ایجاد یک شی از 
                //SepInitPayment
                //برای پرداخت
                var initPayment = new SamanBank.PaymentIFBinding();

                // ارسال اطلاعات به درگاه بانک به روش توکن
                // 1- شماره ترمینال
                // 2- شماره پرداخت
                // 3- مبلغ
                // 5- مبلغ
                // 6-مبلغ
                // 7-مبلغ
                // 8-مبلغ
                // 9-مبلغ
                // 10- اطلاعات اضافی
                // 11- اطلاعات اضافی
                // 12- 0
                string token = initPayment.RequestToken(termId, paymentId.ToString(), long.Parse(price.ToString()), 0, 0, 0, 0, 0, 0, "", "", 0);
                // token = "bQh2kM5QBYzk3pgPXMOM+a4AM2P0ifIqIi4NBmVtBy";

                // بررسی وجود توکن ارسال شده از درگاه بانک
                if (!String.IsNullOrEmpty(token))
                {
                    if (String.IsNullOrEmpty(PaymentResult.Saman(token)))
                    {
                        // اگر عدد برگشت مانند -18 آی پی بسته است و.. به کلاس 
                        //SepResult
                        //مراجعه شود

                        // ثبت توکن در دیتابیس
                        UpdatePayment(paymentId, "-100", 0, token, false);

                        // ایجاد یک شی از نیم ولیو کالکشن
                        NameValueCollection datacollection = new NameValueCollection();

                        // اضافه کردن توکن به شی ساخت شده از نیم ولیو کالکشن
                        datacollection.Add("Token", token);

                        // اضافه کردن آدرس برگشت از درگاه در به شی ساخت شده از نیم ولیو کالکشن
                        datacollection.Add("RedirectURL", redirectPage);

                        // ارسال اطلاعات به درگاه
                        Response.Write(HttpHelper.PreparePOSTForm("https://sep.shaparak.ir/payment.aspx", datacollection));
                    }
                    else
                    {
                        // فرا خوانی متد آپدیت پی منت برای ویرایش اطلاعات ارسالی از درگاه در صورت عدم اتصال
                        UpdatePayment(paymentId, token, 0, null, false);

                        // نمایش خطا به کاربر
                        ViewBag.Message = PaymentResult.Saman(token);
                    }
                }
                else
                {
                    // نمایش خطا به کاربر
                    ViewBag.Message = "در حال حاضر امکان پرداخت وجود ندارد";
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = "در حال حاظر امکان اتصال به این درگاه وجود ندارد ";
            }
            #endregion
        }



        private void SamanReturn()
        {
            #region متد برگشت از درگاه سامان
            try
            {
                // بررسی وجود استیت ارسالی از درگاه
                // در صورت عدم وجود خطا را نمایش می دهیم
                if (Request.Form["state"].ToString().Equals(string.Empty))
                {
                    //ViewBag.Message = "خريد شما توسط بانک تاييد شده است اما رسيد ديجيتالي شما تاييد نگشت! مشکلي در فرايند رزرو خريد شما پيش آمده است";
                    ViewBag.Message = "پاسخی از درگاه بانکی دریافت نشد";
                    ViewBag.SaleReferenceId = "**************";
                    ViewBag.Image = "~/Images/notaccept.png";
                }

                // بررسی وجود رف نام ارسالی از درگاه
                // در صورت عدم وجود خطا را نمایش می دهیم
                // RefNum همان paymentId است
                else if (Request.Form["RefNum"].ToString().Equals(string.Empty) && Request.Form["state"].ToString().Equals(string.Empty))
                {
                    ViewBag.Message = "فرايند انتقال وجه با موفقيت انجام شده است اما فرايند تاييد رسيد ديجيتالي با خطا مواجه گشت";
                    ViewBag.SaleReferenceId = "**************";
                    ViewBag.Image = "~/Images/notaccept.png";
                }

                // بررسی وجود رس نام ارسالی از درگاه
                // در صورت عدم وجود خطا را نمایش می دهیم
                else if (Request.Form["ResNum"].ToString().Equals(string.Empty) && Request.Form["state"].ToString().Equals(string.Empty))
                {
                    ViewBag.Message = "خطا در برقرار ارتباط با بانک";
                    ViewBag.SaleReferenceId = "**************";
                    ViewBag.Image = "~/Images/notaccept.png";
                }
                else
                {
                    // تغییر های مورد تعریف شده برای قرار دادن اطلاعات دریافتی از درگاه
                    string refrenceNumber = string.Empty;

                    // این همان PaymentId است
                    string reservationNumber = string.Empty;
                    string transactionState = string.Empty;
                    string traceNumber = string.Empty;


                    // کد سفارش که به صورت عدد و حروف می باشد
                    refrenceNumber = Request.Form["RefNum"].ToString();

                    // کد ارسالی از طرف سایت که شماره آی دی همان اطلاعات ثبتی در هنگام اتصال به درگاه
                    // این همان PaymentId است
                    reservationNumber = Request.Form["ResNum"].ToString();

                    // وضعیت پرداخت
                    transactionState = Request.Form["state"].ToString();

                    // شماره پیگیری
                    traceNumber = Request.Form["TraceNo"].ToString();

                    int paymentId = Convert.ToInt32(reservationNumber);

                    if (transactionState.Equals("OK"))
                    {
                        // در صورت نیاز می توانیم بررسی کنیم که 
                        // refrenceNumber
                        // که بانک ارسال کرد در دیتابیس تکراری نباشد
                        ///////////////////////////////////////////////////////////////////////////////////
                        //   *** IMPORTANT  ****   ATTENTION
                        // Here you should check refrenceNumber in your DataBase tp prevent double spending
                        ///////////////////////////////////////////////////////////////////////////////////

                        // جلوگیری از خطای اس اس ال
                        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                        // ایجاد یک شی از 
                        //PaymentIFBindingSoapClient
                        //برای پرداخت
                        var srv = new SamanBankVerify.PaymentIFBinding();

                        // تایید اطلاعات پرداخت و دریافت نتیجه
                        var result = srv.verifyTransaction(Request.Form["RefNum"], Request.Form["MID"]);

                        // بررسی نتیجه برگشتی از درگاه  
                        if (result > 0)
                        {
                            // پیدا کردن مبلغ پرداختی از درگاه
                            long amount = FindAmountPayment(paymentId);

                            // تبدیل مبلغ به ریال
                            //amount = amount * 10;

                            // چک کردن مبلغ بازگشتی از سرویس با مبلغ تراکنش
                            if ((long)result == amount)
                            {
                                // در اینجا خرید موفق بوده و اطلاعات دریافتی از درگاه را در دیتابیس ذخیره می کنیم
                                UpdatePayment(paymentId, transactionState, Convert.ToInt64(traceNumber), refrenceNumber, true);

                                IMongoCollection<PaymentModel> collection = MongoConnection.instance.GetCollection<PaymentModel>("Payment");
                                var allpayments = collection.Find(_ => true).ToList();
                                var payment = allpayments.Where(x => x.PaymentId == paymentId).FirstOrDefault();
                                //var payment = Collation..Find(paymentId);                                

                                var user = UserManager.FindById(payment.UserId);

                                VIPPlan vip = new VIPPlan();
                                List<VIPPlan> list_VIP = vip.LoadDataAll();
                                string error = "";

                                error += " paymentId : " + paymentId;
                                error += " TIME : " + DateTime.Now;
                                error += " USERID : " + user.Id;
                                ManageUser mu = new ManageUser();
                                mu.Email = user.Email;
                                mu.UserName = user.UserName;
                                mu.Id = ObjectId.Parse(user.Id);
                                if (payment != null)
                                {
                                    for (int i = 0; i < list_VIP.Count; i++)
                                    {
                                        if (amount == list_VIP[i].Price)
                                        {
                                            error += "amount : " + amount + " Price " + list_VIP[i].Price;
                                            if (user.VipExpireDateTime != null)
                                            {
                                                if (user.VipExpireDateTime > DateTime.Parse(DateTime.Now.ToString()))
                                                {
                                                    mu.VipExpireDateTime = user.VipExpireDateTime.Value.AddDays(list_VIP[i].DateLong);
                                                }
                                                else
                                                {
                                                    mu.VipExpireDateTime = DateTime.Parse(DateTime.Now.ToString()).AddDays(list_VIP[i].DateLong);
                                                }
                                            }
                                            else
                                            {
                                                mu.VipExpireDateTime = DateTime.Parse(DateTime.Now.ToString()).AddDays(list_VIP[i].DateLong);
                                            }
                                            //user.Roles.Add("VIP");
                                            error += " VipExpireDateTime : " + mu.VipExpireDateTime;
                                            mu.VipPlan = list_VIP[i].Caption; 
                                            mu.IsVip = true;
                                        }
                                    }

                                    //UserManager.Update(user); 
                                    error += mu.UpdateUserVIPPlan();

                                    Guid newid = Guid.NewGuid();
                                    string path = Path.Combine(Server.MapPath("~/Content/Logs"), "fileLOG" + newid);
                                    System.IO.File.WriteAllText(path, error);

                                    SendEmail(mu, amount.ToString());
                                    #region old
                                    //switch (amount)
                                    //{
                                    //    case 10000:
                                    //        maage_User.VipExpireDateTime = System.DateTime.Now.AddMonths(1);
                                    //       // user.VipExpireDateTime = System.DateTime.Now.AddMonths(1);
                                    //        break;

                                    //    case 180000:
                                    //        maage_User.VipExpireDateTime = System.DateTime.Now.AddMonths(2);
                                    //       // user.VipExpireDateTime = System.DateTime.Now.AddMonths(2);

                                    //        break;
                                    //    case 250000:
                                    //        maage_User.VipExpireDateTime = System.DateTime.Now.AddMonths(3);
                                    //       // user.VipExpireDateTime = System.DateTime.Now.AddMonths(3);

                                    //        break;
                                    //    case 500000:
                                    //        maage_User.VipExpireDateTime = System.DateTime.Now.AddMonths(6);
                                    //        //user.VipExpireDateTime = System.DateTime.Now.AddMonths(6);

                                    //        break;
                                    //    case 980000:
                                    //        maage_User.VipExpireDateTime = System.DateTime.Now.AddMonths(6);
                                    //       // user.VipExpireDateTime = System.DateTime.Now.AddYears(1);

                                    //        break;
                                    //}

                                    #endregion old
                                }
                                // قرار دادن اطلاعات پرداخت در ویوبگ ها
                                //ViewBag.Message = "بانک صحت رسيد ديجيتالي شما را تصديق نمود. فرايند خريد تکميل گشت";
                                ViewBag.Message = "پرداخت با موفقیت انجام شد.";
                                ViewBag.SaleReferenceId = traceNumber;
                                ViewBag.RefrenceNumber = refrenceNumber;
                                ViewBag.Image = "~/Images/accept.png";
                            }
                            // عدم یکسان بودن مبلغ پرداختی با مبلغ موجود در دیتابیس
                            else
                            {
                                //نام کاربری همان ام آی دی است
                                string userName = Request.Form["MID"];

                                // رمز عبور برای شما توسط سامان کیش ایمیل شده است
                                string pass = "148000261";

                                // فراخوانی متد ریورس ترنزاکشن برای بازگشت دادن مبلغ به حساب خریدار
                                srv.reverseTransaction(Request.Form["RefNum"], Request.Form["MID"], userName, pass);

                                // پرداخت ناموفق بوده و اطلاعات دریافتی را در دیتابیس ثبت می کنیم
                                UpdatePayment(paymentId, result.ToString(), 0, refrenceNumber, false);

                                // نمایش اطلاعات خرید و وضعیت خرید به خریدار
                                ViewBag.Message = PaymentResult.Saman(transactionState);
                                ViewBag.SaleReferenceId = "**************";
                                ViewBag.Image = "~/Images/notaccept.png";

                            }
                        }
                        // بعد از وریفای کردن خرید نتیجه بزرگتر از صفر نبود این قسمت اجرا می شود
                        else
                        {
                            // پرداخت ناموفق بوده و اطلاعات دریافتی را در دیتابیس ثبت می کنیم
                            UpdatePayment(paymentId, result.ToString(), 0, refrenceNumber, false);

                            // نمایش اطلاعات خرید و وضعیت خرید به خریدار
                            ViewBag.Message = PaymentResult.Saman(transactionState);
                            ViewBag.SaleReferenceId = "**************";
                            ViewBag.Image = "~/Images/notaccept.png";
                        }
                    }
                    // در صورتی که 
                    // transactionState
                    // برابر 
                    // ok
                    // نبود این قسمت اجرا می شود
                    else
                    {
                        // پرداخت ناموفق بوده و اطلاعات دریافتی را در دیتابیس ثبت می کنیم
                        UpdatePayment(paymentId, transactionState, 0, refrenceNumber, false);

                        if (!String.IsNullOrEmpty(PaymentResult.Saman(transactionState)))
                        {
                            ViewBag.Message = PaymentResult.Saman(transactionState);
                        }
                        else
                        {
                            ViewBag.Message = "متاسفانه بانک خريد شما را تاييد نکرده است";
                        }
                        ViewBag.SaleReferenceId = "**************";
                        ViewBag.Image = "~/Images/notaccept.png";

                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.SaleReferenceId = "**************";
                ViewBag.Image = "~/Images/notaccept.png";
                ViewBag.Message = ex.Message + " ******** " + "مشکلی در پرداخت به وجود آمده است ، در صورتیکه وجه پرداختی از حساب بانکی شما کسر شده است آن مبلغ به صورت خودکار برگشت داده خواهد شد";

                Guid newid = Guid.NewGuid();
                string path = Path.Combine(Server.MapPath("~/Content/Images/Article"), "fileLOG" + newid);
                System.IO.File.WriteAllText(path, ViewBag.Message);
            }

            #endregion
        }

        private void SendEmail(ManageUser user, string price)
        {

            SmtpClient client = new SmtpClient("mail.cgsilent.com", 25);

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("info@cgsilent.com");
            mailMessage.To.Add(user.Email);
            mailMessage.Subject = "تایید پرداخت اینترنتی";

            mailMessage.Body = MakeEmailBody(price, user);
            mailMessage.IsBodyHtml = true;
            client.Send(mailMessage);
        }


        private string MakeEmailBody(string price, ManageUser user)
        {
            string body = "";
            body += "<body>";
            body += "<div style='width: 100%; height: 144px; background-color:#101a22;'>";
            body += "<div style='float:left; width: 30%; '>";
            body += "<img src='http://www.cgsilent.com/cgsilentLogo.png' style='float:left;' />";
            body += "</div>";
            body += "<div style='float:right; padding-top:30px; padding-right:43px; padding-bottom:30px; width: 60%; line-height:25px;'>";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += "کاربر گرامی " + user.UserName + " , پرداخت شما با موفقیت انجام شد";
            body += "<br />";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += " مبلغ  " + price + " ریال بابت خرید پلن " + user.VipPlan + " ";
            body += " </span>";
            body += "<br />";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += "اعتبار تا تاریخ : " + user.VipExpireDateTime;
            body += " </span>";
            body += "<br />";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#ffffff;'>";
            body += "با سپاس از خرید شما";
            body += "</span>";
            body += "<span style='float:right; text-align:right; direction: rtl; color:#def84d;padding-left:2px; padding-right:2px;'>";
            body += "<a href='http://www.cgsilent.ir' style='text-decoration: none; color:#def84d;' >بازگشت به سایت</a> ";
            body += "</span> ";
            body += "</div>";
            body += "</div>";
            body += "</body>";
            return body;
        }


        private void CreateArticleAllList()
        {
            CheckVIPDate();
            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<Models.ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();

            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            long articlecount = Articles.GetNumberOfArticles();
            for (int i = 0; i < articlecount; i++)
            {
                articleGroup.ArticleId = (allArticles[i]).Id;
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[i].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[i]);
                        }
                    }
                }
            }

            ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
            {
                Article = null,
                LikeDislike = null,
                ArticleComments = null,
                Comment = null,
                ArticleGalary = null,
                ArticleGroupsData = null
            };
            aaList.Add(aa);

            ArticleListComplete articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };

            Stores st = new Stores();
            List<Stores> StoreSlideShow = new List<Stores>();
            StoreSlideShow = st.loadTopTwentyStoriesForSlide();
            ViewBag.StoreDataForSlideShow = StoreSlideShow;
            ViewBag.AllData = articleList;
        }

        private void CheckVIPDate()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = UserManager.FindById(User.Identity.GetUserId().ToString());
                if (user.VipExpireDateTime != null && user.VipExpireDateTime > DateTime.Now)
                {
                    long vipdate = 0;
                    vipdate = DateTime.Parse(user.VipExpireDateTime.ToString()).Ticks - DateTime.Now.Ticks;
                    double day = TimeSpan.FromTicks(vipdate).TotalDays;
                    ViewBag.VipDays = Math.Round(day);
                }
                if (user.VipExpireDateTime != null && user.VipExpireDateTime < DateTime.Now)
                {
                    user.IsVip = false;
                    user.VipExpireDateTime = null;
                }
                UserManager.Update(user);
            }
        }

    }
}