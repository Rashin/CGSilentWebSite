﻿using CGSilent.Models;
using MongoDB.Bson;
using System;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
//using System.Web.Http; 

namespace CGSilent
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
            ModelBinders.Binders.Add(typeof(ObjectId), new ObjectIdBinder());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            // WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ///For HTTPS
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.RequireSsl = true;
        }

        ///For HTTPS
        protected void Application_BeginRequest()
        {
            if (!Context.Request.IsSecureConnection &&
               !Request.Url.Host.Contains("localhost") &&
               Request.Url.AbsolutePath.Contains("Account/Login"))
            {
                Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }
            if (!Context.Request.IsSecureConnection)
            {
                // This is an insecure connection, so redirect to the secure version
                UriBuilder uri = new UriBuilder(Context.Request.Url);
                if (!uri.Host.Equals("localhost"))
                {
                    uri.Port = 443;
                    uri.Scheme = "https";
                    Response.Redirect(uri.ToString());
                }
            }
        }



    }

}
