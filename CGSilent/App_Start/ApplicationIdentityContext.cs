﻿namespace CGSilent
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AspNet.Identity.MongoDB;
    using Models;
    using MongoDB.Driver;

    public class ApplicationIdentityContext : IDisposable
    {
        public static ApplicationIdentityContext Create()
        {
            // todo add settings where appropriate to switch server & database in your own application
            var client = new MongoClient("mongodb://localhost:27017");

            var database = client.GetDatabase("DB_CGSilentWebDB");

            MongoConnection.instance.GetCollection<Articles>("Articles").Indexes.CreateOne(Builders<Articles>.IndexKeys.Combine(
                   Builders<Articles>.IndexKeys.Text(x => x.Summery), Builders<Articles>.IndexKeys.Text(x => x.Title),
                  Builders<Articles>.IndexKeys.Text(x => x.Tag), Builders<Articles>.IndexKeys.Text(x => x.KeyWords), 
                  Builders<Articles>.IndexKeys.Text(x => x.Description)));



            MongoConnection.instance.GetCollection<Stores>("Stores").Indexes.CreateOne(Builders<Stores>.IndexKeys.Combine(
                   Builders<Stores>.IndexKeys.Text(x => x.Summery), Builders<Stores>.IndexKeys.Text(x => x.Title),                   
                  Builders<Stores>.IndexKeys.Text(x => x.Tag), Builders<Stores>.IndexKeys.Text(x => x.KeyWords),
                  Builders<Stores>.IndexKeys.Text(x => x.Description)));

            var users = database.GetCollection<ApplicationUser>("users");
            var roles = database.GetCollection<IdentityRole>("roles");
            return new ApplicationIdentityContext(users, roles);
        }

        private ApplicationIdentityContext(IMongoCollection<ApplicationUser> users, IMongoCollection<IdentityRole> roles)
        {
            Users = users;
            Roles = roles;
        }

        public IMongoCollection<IdentityRole> Roles { get; set; }

        public IMongoCollection<ApplicationUser> Users { get; set; }

        public Task<List<IdentityRole>> AllRolesAsync()
        {
            return Roles.Find(r => true).ToListAsync();
        }

        public void Dispose()
        {
        }
    }
}