﻿using CGSilent.Models;
using System.Web;
using System.Web.Mvc;

namespace CGSilent
{
    public class FilterConfig
    {
        [AuthorizeAttribute]
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ElmahHandledErrorLoggerFilter());
            filters.Add(new HandleErrorAttribute());

            ///For HTTPS
            filters.Add(new RequireHttpsAttribute());
        }
    }
}
