﻿using MongoDB.Bson;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

namespace CGSilent.Models
{
    public interface MongoDBModelNew
    {
        List<BsonDocument> LoadDataByObject();
        List<XDocument> LoadDataAll();
        ObjectId Insert();
        void Update();
        void DeleteById();
        void Delete();
    }
}