﻿using MongoDB.Bson;
using System.Collections;
using System.Collections.Generic;

namespace CGSilent.Models
{
    public interface MongoDBModel<T>
    {
        List<T> LoadDataByObject();
       // static List<T> LoadDataAll();
        ObjectId Insert();
        void Update();
        void DeleteById();
        void Delete();
    }
}