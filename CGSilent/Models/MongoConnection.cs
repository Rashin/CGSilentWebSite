﻿using MongoDB.Driver;

namespace CGSilent.Models
{
    public class MongoConnection
    {
        public static MongoConnection instance;

        public IMongoDatabase database;

        public MongoConnection()
        {
            Init();
        }
        private void Init()
        {

            instance = this;
            database = new MongoClient().GetDatabase("DB_CGSilentWebDB");
        }

        public IMongoCollection<TDocument> GetCollection<TDocument>(string name, MongoCollectionSettings settings = null)
        {
            return instance.database.GetCollection<TDocument>(name, settings);
        }
    }
}