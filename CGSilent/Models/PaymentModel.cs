﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace CGSilent.Models
{
  
    public class PaymentModel {

        public PaymentModel()
        {
            PaymentNumber = Guid.NewGuid();
            BuyDateTime = DateTime.Parse(DateTime.Now.ToString("s"));
        }
        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        public int PaymentId { get; set; }
        public Guid PaymentNumber { get; set; }
        //shomare peigiri
        public string ReferenceNumber{ get; set; }
        //shomare pardakht
        public long SaleReferenceId { get; set; }
        public string PaymentStatus { get; set; }
        public bool PaymentIsFinished { get; set; }
        public long Amount { get; set; }

        public string BankName { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public DateTime BuyDateTime { get; set; }
        public bool IsVipPayment { get; set; }

    }

}