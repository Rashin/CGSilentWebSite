﻿using AspNet.Identity.MongoDB;
using Microsoft.AspNet.Identity;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CGSilent.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim("Email", Email));
            userIdentity.AddClaim(new Claim("Name", Name));
            userIdentity.AddClaim(new Claim("LastName", LastName));
            userIdentity.AddClaim(new Claim("ImagePath", ImagePath));
            userIdentity.AddClaim(new Claim("ImageId", ImageId.ToString()));
            userIdentity.AddClaim(new Claim("Address", Address));
            userIdentity.AddClaim(new Claim("City", City));
            userIdentity.AddClaim(new Claim("CreatedDate", CreatedDate.ToString()));
            userIdentity.AddClaim(new Claim("ExpireDate", ExpireDate.ToString()));
            userIdentity.AddClaim(new Claim("VipExpireDateTime", VipExpireDateTime.ToString()));
            userIdentity.AddClaim(new Claim("VipPlan", VipPlan == null ? "" : VipPlan));
            userIdentity.AddClaim(new Claim("IsVip", IsVip.ToString()));

            // userIdentity.AddClaim(new Claim("ArticleId", ""));

            return userIdentity;
        }


        public string Name { get; set; }
        public string LastName { get; set; }
        public string ImagePath { get; set; }
        public ObjectId ImageId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string CreatedDate { get; set; }

        //public string ExpireDate { get; set; }

        private string _ExpireDate;

        public string ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }

        //public DateTime? VipExpireDateTime { get; set; }
        //public string VipPlan { get; set; }
        private DateTime? _VipExpireDateTime;

        public DateTime? VipExpireDateTime
        {
            get { return _VipExpireDateTime; }
            set { _VipExpireDateTime = value; }
        }


        private string _VipPlan;

        public string VipPlan
        {
            get { return _VipPlan; }
            set { _VipPlan = value; }
        }


        // public bool  { get; set; }
        private bool _IsVip;

        public bool IsVip
        {
            get { return _IsVip; }
            set { _IsVip = value; }
        }


        public List<ObjectId> ArticleId { get; set; }
    }

}