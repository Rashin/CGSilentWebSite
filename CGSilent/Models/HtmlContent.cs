﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using MongoDB.Driver;

namespace CGSilent.Models
{
    public class HtmlContent : MongoDBModel<HtmlContent>
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Content;

        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private string _Type;

        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdatedDate;

        public string UpdatedDate
        {
            get { return _UpdatedDate; }
            set { _UpdatedDate = value; }
        }

        #endregion Properties

        #region Events

        public   List<HtmlContent> LoadDataByObject()
        {
            IMongoCollection<HtmlContent> collection = MongoConnection.instance.GetCollection<HtmlContent>("HtmlContent");
            var hcontent = collection.Find((Builders<HtmlContent>.Filter.Eq("_id", Id))).ToList();
            return hcontent;
        }

        public  List<HtmlContent> LoadDataByType()
        {
            IMongoCollection<HtmlContent> collection = MongoConnection.instance.GetCollection<HtmlContent>("HtmlContent");
            var hcontent = collection.Find((Builders<HtmlContent>.Filter.Eq("Type", Type))).ToList();
            return hcontent;
        }

        public  List<HtmlContent> LoadDataAll()
        {
            IMongoCollection<HtmlContent> collection = MongoConnection.instance.GetCollection<HtmlContent>("HtmlContent");
            var hcontent = collection.Find((Builders<HtmlContent>.Filter.Empty)).ToList();
            return hcontent;
        }

        public ObjectId Insert()
        {
            IMongoCollection<HtmlContent> collection = MongoConnection.instance.GetCollection<HtmlContent>("HtmlContent");
            collection.InsertOne(this);
            return Id;
        }

        public void Update()
        {
            IMongoCollection<HtmlContent> collection = MongoConnection.instance.GetCollection<HtmlContent>("HtmlContent");
            collection.ReplaceOne(Builders<HtmlContent>.Filter.Eq("_id", Id), this);
        }


        public void DeleteById()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        #endregion Events
    }
}