﻿using MongoDB.Bson;
using System.Web.Mvc;

namespace CGSilent.Models
{
    public class ObjectIdBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var result = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (result != null)
                if (result.AttemptedValue != "")
                {
                    if (result.AttemptedValue.Contains(","))
                    {
                        if (result.AttemptedValue.IndexOf(",") == 0)
                            return new ObjectId(result.AttemptedValue.Substring(result.AttemptedValue.IndexOf(",") + 1));
                        else
                        {
                            if (result.AttemptedValue.Substring(0, result.AttemptedValue.IndexOf(",")) != "")
                            {
                                return new ObjectId(result.AttemptedValue.Substring(result.AttemptedValue.IndexOf(",") + 1));
                            }
                            else
                            {
                                //return new ObjectId(result.AttemptedValue.Substring(0, result.AttemptedValue.IndexOf(",")));
                                return null;
                            }
                        }
                    }
                    else
                    {
                        if (result.AttemptedValue != ObjectId.Empty.ToString())
                            return new ObjectId(result.AttemptedValue);
                        else
                        {
                            return (null);
                        }
                    }
                }
                else
                {
                    return null;
                }
            else
                return null;
        }
    }

}