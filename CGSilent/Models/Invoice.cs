﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGSilent.Models
{
    public class Invoice
    {
        private int v1;
        private string v2;

        public Invoice()
        {
                
        }

        public Invoice(int orderNumber, int amount, string verifyUrl)
        {
            OrderNumber = OrderNumber;
            Amount = amount;
            this.VerifyUrl = VerifyUrl;
        }

        public int OrderNumber { get; set; }
        public int Amount { get; set; }
        public string VerifyUrl { get; set; }

    }
}