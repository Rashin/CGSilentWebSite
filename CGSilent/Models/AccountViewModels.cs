﻿using CGSilent.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CGSilent.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "کد")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "به خاطر سپردن در مرورگر?")]
        public bool RememberBrowser { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
    }

    public class UserImage
    {
        private ObjectId _id;

        public ObjectId id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _ImagePath;

        public string ImagePath
        {
            get { return _ImagePath; }
            set { _ImagePath = value; }
        }


        public string GetUserImageById()
        {
            IMongoCollection<UserImage> collection = MongoConnection.instance.GetCollection<UserImage>("users");
            var condition = Builders<UserImage>.Filter.Eq(p => p.id, id);
            var fields = Builders<UserImage>.Projection.Include(p => p.ImagePath);
            var results = collection.Find(condition).Project<UserImage>(fields).ToList().AsQueryable();
            var res = results.FirstOrDefault();
            if (res != null)
                return res.ImagePath;
            return "";
        }
    }

    public class ManageUser
    {

        #region Properties

        private ObjectId _id;

        public ObjectId Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string[] _Roles;

        public string[] Roles
        {
            get { return _Roles; }
            set { _Roles = value; }
        }
        private DateTime? _VipExpireDateTime;

        public DateTime? VipExpireDateTime
        {
            get { return _VipExpireDateTime; }
            set { _VipExpireDateTime = value; }
        }


        private string _VipPlan;

        public string VipPlan
        {
            get { return _VipPlan; }
            set { _VipPlan = value; }
        }


        // public bool  { get; set; }
        private bool _IsVip;

        public bool IsVip
        {
            get { return _IsVip; }
            set { _IsVip = value; }
        }


        #endregion Properties

        #region Events

        public ManageUser GetUserById()
        {
            IMongoCollection<ManageUser> collection = MongoConnection.instance.GetCollection<ManageUser>("users");

            var condition = Builders<ManageUser>.Filter.Eq(p => p.Id, Id);
            var fields = Builders<ManageUser>.Projection.Include(p => p.Id).Include(p => p.Email).Include(p => p.UserName).Include(p => p.Roles);

            var results = collection.Find(condition).Project<ManageUser>(fields).ToList().AsQueryable();
            ManageUser res = results.FirstOrDefault();
            // ManageUser user = new ManageUser();
            return res;
        }
        public string UpdateUserVIPPlan()
        {
            IMongoCollection<UserManagementVIPRole> collection = MongoConnection.instance.GetCollection<UserManagementVIPRole>("users");

            var update = Builders<UserManagementVIPRole>.Update.Set("VipExpireDateTime",VipExpireDateTime).Set("IsVip", IsVip).Set("VipPlan", VipPlan);
            collection.UpdateOne(Builders<UserManagementVIPRole>.Filter.Eq("_id", Id), update);
            return " true ";
        }

        #endregion Events

    }

    public class UserBasket
    {
        #region Properties

        private List<ObjectId> _ArticleId;

        public List<ObjectId> ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private List<ObjectId> _StoreId;

        public List<ObjectId> StoreId
        {
            get { return _StoreId; }
            set { _StoreId = value; }
        }

        private ObjectId _id;

        public ObjectId Id
        {
            get { return _id; }
            set { _id = value; }
        }

        #endregion Properties

        #region Events

        public List<UserBasket> LoadArticleByUserId(string userid)
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            List<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", userid))).ToList();
            // & (Builders<UserBasket>.Filter.In("ArticleId", ArticleId))).ToList();
            return usersBasket;
        }

        public void UpdateBasketForArticles()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.ArticleId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();
            if (usersBasket.Count > 0)
            {
                if (usersBasket[0].ArticleId != null)
                {
                    bool exist = false;
                    for (int i = 0; i < usersBasket[0].ArticleId.Count; i++)
                    {
                        if (usersBasket[0].ArticleId[i] == ArticleId[0])
                        {
                            exist = true;
                            break;
                        }
                    }
                    if (!exist)
                    {
                        usersBasket[0].ArticleId.Add(ArticleId[0]);
                        var update = Builders<UserBasket>.Update.Set("ArticleId", usersBasket[0].ArticleId);
                        collection.UpdateOne(Builders<UserBasket>.Filter.Eq("_id", Id), update);
                    }
                }
                else
                {
                    usersBasket[0].ArticleId = new List<ObjectId>();
                    usersBasket[0].ArticleId.Add(ArticleId[0]);
                    var update = Builders<UserBasket>.Update.Set("ArticleId", usersBasket[0].ArticleId);
                    collection.UpdateOne(Builders<UserBasket>.Filter.Eq("_id", Id), update);
                }
            }

        }

        public void UpdateStore()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.StoreId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();
            if (usersBasket.Count > 0)
            {
                if (usersBasket[0].StoreId != null)
                {
                    bool exist = false;
                    for (int i = 0; i < usersBasket[0].StoreId.Count; i++)
                    {
                        if (usersBasket[0].StoreId == StoreId)
                        {
                            exist = true;
                            break;
                        }
                    }
                    if (!exist)
                        usersBasket[0].ArticleId.Add(ArticleId[0]);
                }
            }
            var update = Builders<UserBasket>.Update.Set("StoreId", usersBasket[0].StoreId);
            collection.UpdateOne(Builders<UserBasket>.Filter.Eq("_id", Id), update);
        }

        internal List<ObjectId> LoadAllArticleBasket()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.ArticleId);
            var usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();

            List<ObjectId> articleList = new List<ObjectId>();

            if (usersBasket != null)
            {
                if (usersBasket[0].ArticleId != null)
                    if (usersBasket[0].ArticleId.Count > 0)
                    {
                        for (int i = 0; i < usersBasket[0].ArticleId.Count; i++)
                        {
                            articleList.Add(usersBasket[0].ArticleId[i]);
                        }
                    }
            }
            return articleList;
        }

        internal List<ObjectId> LoadArticleBasketByUserId()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.ArticleId);
            var usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();

            List<ObjectId> articleList = new List<ObjectId>();

            if (usersBasket != null & usersBasket.Count > 0)
            {
                if (usersBasket[0].ArticleId != null)
                    if (usersBasket[0].ArticleId.Count > 0)
                    {
                        for (int i = 0; i < usersBasket[0].ArticleId.Count; i++)
                        {
                            articleList.Add(usersBasket[0].ArticleId[i]);
                        }
                    }
            }
            return articleList;
        }

        internal List<ObjectId> LoadArticleBaskets()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.ArticleId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();

            List<ObjectId> articleList = new List<ObjectId>();

            if (usersBasket != null)
            {
                if (usersBasket[0].ArticleId != null)
                {
                    for (int i = 0; i < usersBasket[0].ArticleId.Count; i++)
                    {
                        articleList.Add(usersBasket[0].ArticleId[i]);
                    }
                }
            }
            return articleList;
        }


        internal List<Articles> LoadArticleDataByUserId()
        {
            IMongoCollection<Articles> collection_Articles = MongoConnection.instance.GetCollection<Articles>("Articles");
            List<Articles> articlesBasketnfo = new List<Articles>();
            articlesBasketnfo = collection_Articles.Find((Builders<Articles>.Filter.In("_id", ArticleId))).ToList();

            return articlesBasketnfo;
        }

        internal List<Stores> LoadStoreByUserId()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.StoreId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();

            List<Stores> articleList = new List<Stores>();
            List<Stores> temp = new List<Stores>();
            IMongoCollection<Stores> collection_Stores = MongoConnection.instance.GetCollection<Stores>("Stores");
            if (usersBasket != null)
            {
                if (usersBasket[0].ArticleId != null)
                {
                    for (int i = 0; i < usersBasket[0].ArticleId.Count; i++)
                    {
                        temp = collection_Stores.Find((Builders<Stores>.Filter.Eq("_id", usersBasket[0].StoreId[i]))).ToList();
                        if (temp.Count > 0)
                        {
                            articleList.Add(temp[0]);
                            temp = new List<Stores>();
                        }
                    }
                }
            }
            return articleList;
        }


        internal void DeleteBasketArticle()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");

            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.ArticleId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();
            if (usersBasket.Count > 0)
            {
                if (usersBasket[0].ArticleId != null)
                {
                    for (int i = 0; i < usersBasket[0].ArticleId.Count; i++)
                    {
                        if (usersBasket[0].ArticleId[i] == ArticleId[0])
                        {
                            usersBasket[0].ArticleId.Remove(ArticleId[0]);
                        }
                    }
                }
            }
            var update = Builders<UserBasket>.Update.Set("ArticleId", usersBasket[0].ArticleId);
            collection.UpdateOne(Builders<UserBasket>.Filter.Eq("_id", Id), update);
        }

        internal void DeleteBasketStore()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.StoreId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();
            if (usersBasket.Count > 0)
            {
                if (usersBasket[0].StoreId != null)
                {
                    for (int i = 0; i < usersBasket[0].StoreId.Count; i++)
                    {
                        if (usersBasket[0].StoreId[i] == StoreId[0])
                        {
                            usersBasket[0].StoreId.Remove(StoreId[0]);
                        }
                    }
                }
            }
            var update = Builders<UserBasket>.Update.Set("StoreId", usersBasket[0].StoreId);
            collection.UpdateOne(Builders<UserBasket>.Filter.Eq("_id", Id), update);
        }

        public void DeleteArticleBasketyUserId()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");

            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.ArticleId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();
            List<ObjectId> articles = new List<ObjectId>();

            var update = Builders<UserBasket>.Update.Set("ArticleId", articles);
            collection.UpdateOne(Builders<UserBasket>.Filter.Eq("_id", Id), update);
        }

        public void DeleteStoreBasketyUserId()
        {
            IMongoCollection<UserBasket> collection = MongoConnection.instance.GetCollection<UserBasket>("users");

            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.StoreId);
            IList<UserBasket> usersBasket = collection.Find((Builders<UserBasket>.Filter.Eq("_id", Id))).Project<UserBasket>(fields).ToList();
            List<ObjectId> articles = new List<ObjectId>();

            var update = Builders<UserBasket>.Update.Set("StoreId", StoreId);
            collection.UpdateOne(Builders<UserBasket>.Filter.Eq("_id", Id), update);
        }

        #endregion Events
    }

    public class UserManagementVIPRole
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }


        private string _City;
        [Display(Name = "شهر")]
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }


        [Required]
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "ایمیل")]
        [Remote("EmailExist", "Account", HttpMethod = "POST", ErrorMessage = "این آدرس ایمیل قبلا ثبت شده است. لطفا با ایمیل آدرس دیگری تلاش نمایید.")]
        //[ServerSideRemote("Account", "EmailExist")]
        public string Email { get; set; }

        [StringLength(100, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 3)]
        [Display(Name = "نام")]
        [DefaultValue(" ")]
        public string Name { get; set; }


        [StringLength(100, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 3)]
        [Display(Name = "نام خانوادگی")]
        [DefaultValue(" ")]
        public string LastName { get; set; }


        [Display(Name = "تاریخ ایجاد")]
        public string CreatedDate { get; set; }

        [Display(Name = "تاریخ انقضا")]
        public string ExpireDate { get; set; }
        public DateTime? VipExpireDateTime { get; set; }
        public string VipPlan { get; set; }


        public string[] Roles;
        public string[] _Roles
        {
            get { return _Roles; }
            set { _Roles = Roles; }
        }

        [Display(Name = "افزودن مدت زمان وی آی پی")]
        public int VipTime { get; set; }

        #endregion Porperties

        #region Events

        public List<UserManagementVIPRole> GetAllUsersFullInfo()
        {
            IMongoCollection<UserManagementVIPRole> collection = MongoConnection.instance.GetCollection<UserManagementVIPRole>("users");
            var fields = Builders<UserManagementVIPRole>.Projection.Include(p => p.Id).Include(p => p.Roles).Include(p => p.UserName).Include(p => p.CreatedDate)
                .Include(p => p.Email).Include(p => p.VipTime).Include(p => p.City).Include(p => p.ExpireDate);

            List<UserManagementVIPRole> reg = collection.Find((Builders<UserManagementVIPRole>.Filter.Empty)).Project<UserManagementVIPRole>(fields).ToList();

            return reg;
        }

        public UserManagementVIPRole GetUserRolessById()
        {
            IMongoCollection<UserManagementVIPRole> collection = MongoConnection.instance.GetCollection<UserManagementVIPRole>("users");
            var fields = Builders<UserManagementVIPRole>.Projection.Include(p => p.Id).Include(p => p.Roles);
            List<UserManagementVIPRole> userinfo = collection.Find((Builders<UserManagementVIPRole>.Filter.Eq("_id", Id))).Project<UserManagementVIPRole>(fields).ToList();
            if (userinfo.Count > 0)
                return userinfo[0];
            else
            {
                UserManagementVIPRole uvm = new UserManagementVIPRole();
                return uvm;
            }
        }

        public UserManagementVIPRole GetAllUsersFullInfoById()
        {
            IMongoCollection<UserManagementVIPRole> collection = MongoConnection.instance.GetCollection<UserManagementVIPRole>("users");
            var fields = Builders<UserManagementVIPRole>.Projection.Include(p => p.Id).Include(p => p.ExpireDate);

            List<UserManagementVIPRole> userinfo = collection.Find((Builders<UserManagementVIPRole>.Filter.Eq("_id", Id))).Project<UserManagementVIPRole>(fields).ToList();
            if (userinfo.Count > 0)
                return userinfo[0];
            else
            {
                UserManagementVIPRole uvm = new UserManagementVIPRole();
                return uvm;
            }
        }

        public void DeleteAcount()
        {
            IMongoCollection<UserManagementVIPRole> collection = MongoConnection.instance.GetCollection<UserManagementVIPRole>("users");
            collection.DeleteOne(Builders<UserManagementVIPRole>.Filter.Eq("_id", Id));
        }

        public void UpdateVipTime()
        {
            IMongoCollection<UserManagementVIPRole> collection = MongoConnection.instance.GetCollection<UserManagementVIPRole>("users");

            var fields = Builders<UserManagementVIPRole>.Projection.Include(p => p.Id).Include(p => p.Roles).Include(p => p.UserName).Include(p => p.CreatedDate)
               .Include(p => p.Email).Include(p => p.VipTime).Include(p => p.ExpireDate);

            UserManagementVIPRole userbyid = collection.Find((Builders<UserManagementVIPRole>.Filter.Eq("_id", Id))).Project<UserManagementVIPRole>(fields).ToList().FirstOrDefault();

            DateTime expiredate = DateTime.Parse(userbyid.ExpireDate);
            if (expiredate < DateTime.Now)
            {
                ExpireDate = DateTime.Now.AddDays(VipTime).ToString();
            }
            else
            {
                ExpireDate = expiredate.AddDays(VipTime).ToString();
            }

            var update = Builders<UserManagementVIPRole>.Update.Set("ExpireDate", ExpireDate);
            collection.UpdateOne(Builders<UserManagementVIPRole>.Filter.Eq("_id", Id), update);

        }

        #endregion Events
    }

    public class LoginViewModel
    {
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }

        [Display(Name = "مرا به خاطر بسپار")]
        public bool RememberMe { get; set; }

    }

    public class RegisterViewModel //: UserStore<TUser> where TUser : ApplicationUser
    {
        #region Properties 

        [Required]
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "ایمیل")]
        [Remote("EmailExist", "Account", HttpMethod = "POST", ErrorMessage = "این آدرس ایمیل قبلا ثبت شده است. لطفا با ایمیل آدرس دیگری تلاش نمایید.")]
        //[ServerSideRemote("Account", "EmailExist")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 3)]
        [Display(Name = "نام")]
        [DefaultValue(" ")]
        public string Name { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 3)]
        [Display(Name = "نام خانوادگی")]
        [DefaultValue(" ")]
        public string LastName { get; set; }


        [Required]
        [StringLength(40, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "تایید رمز عبور")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "تکرار رمز عبور منطبق با رمز عبور نمی باشد.")]
        public string ConfirmPassword { get; set; }


        [StringLength(100, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 3)]
        [Display(Name = "شهر")]
        [DefaultValue(" ")]
        public string City { get; set; }

        [StringLength(100, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 3)]
        [Display(Name = "آدرس")]
        [DefaultValue(" ")]
        public string Address { get; set; }

        [StringLength(100, ErrorMessage = " {0} باید لااقل {2} کارکتر داشته باشد.", MinimumLength = 3)]
        [Display(Name = "شماره تماس")]
        [DefaultValue("11111111")]
        public string Phone { get; set; }

        [Display(Name = "تصویر")]
        public ObjectId ImageId { get; set; }

        [Display(Name = "آدرس تصویر")]
        [DefaultValue(" ")]
        public string ImagePath { get; set; }

        [Display(Name = "تاریخ ایجاد")]
        public string CreatedDate { get; set; }

        [Display(Name = "تاریخ انقضا")]
        public string ExpireDate { get; set; }

        #endregion Properties

        #region Events

        public void updateUserImageProfile(ObjectId userid)
        {
            DateTime lastYear = new DateTime(DateTime.Now.Year - 1, DateTime.Now.Month, DateTime.Now.Day, 1, 1, 1);

            IMongoCollection<RegisterViewModel> collection = MongoConnection.instance.GetCollection<RegisterViewModel>("users");
            var update = Builders<RegisterViewModel>.Update.Set("ImagePath", ImagePath).Set("ImageId", ImageId)
                .Set("CreatedDate", DateTime.Now).Set("PhoneNumber", Phone).Set("Address", Address).Set("City", City)
                .Set("Name", Name).Set("LastName", LastName);//.Set("ExpireDate", lastYear)
            collection.UpdateOne(Builders<RegisterViewModel>.Filter.Eq("_id", userid), update);
        }


        public static ObjectId UploadFile(GridFSBucket fs, string filename, string path)
        {
            using (var s = File.OpenRead(path))
            {
                var t = Task.Run<ObjectId>(() =>
                {
                    return fs.UploadFromStreamAsync(filename, s);
                });
                return t.Result;
            }

        }

        public static byte[] DownloadFile(GridFSBucket fs, ObjectId id)//, string fileName)
        {
            byte[] x = fs.DownloadAsBytes(id);
            return x;
        }

        #endregion Events
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = " {0} باید لااقل {6} تعداد کارکتر داشته باشد.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تایید رمز عبور")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "تکرار رمز عبور منطبق با رمز عبور نمی باشد.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
    }

    public class UserRoles
    {
        private ObjectId _id;

        public ObjectId Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string[] Roles;
        public string[] _Roles
        {
            get { return _Roles; }
            set { _Roles = Roles; }
        }

        public UserRoles LoadDataByUserId()
        {
            IMongoCollection<UserRoles> collection = MongoConnection.instance.GetCollection<UserRoles>("users");
            var condition = Builders<UserRoles>.Filter.Eq("_id", Id);
            var fields = Builders<UserRoles>.Projection.Include(p => p.Roles);
            var results = collection.Find(condition).Project<UserRoles>(fields).ToList().AsQueryable();
            UserRoles userrolelist = results.FirstOrDefault();
            return userrolelist;

            //IList ur = collection.Find((Builders<UserRoles>.Filter.Eq("_id", Id))).ToList();
            //if (ur.Count > 0)
            //    return (UserRoles)ur[0];
            //else
            //    return null;

        }
    }

    public class VIPDropDown
    {
        #region Properties

        private VIPPlan _VipPlan;

        public VIPPlan VipPlan
        {
            get { return _VipPlan; }
            set { _VipPlan = value; }
        }

        private List<SelectListItem> _Plans;

        public List<SelectListItem> Plans
        {
            get { return _Plans; }
            set { _Plans = value; }
        }

        #endregion Properties
    }

}