﻿using System;
using System.Collections.Generic;
using System.Net;
using MongoDB.Bson;

namespace CGSilent.Models
{
    public class LayoutLoader
    {
        public static void CreateArticlesAndStoresData(bool isArticle, List<Articles> ArticleList, List<Stores> StoreList,
            out List<StoreWithLikeDislikeAndComments> ssCompeteList,
           out List<ArticleWithLikeDislikeAndComments> aaList, out ArticleListComplete articleList, out List<Stores> list_StoresForSlideShow)
        {
            
                ArticleList = Articles.LoadAllArticlesFromToday();
             
               // StoreList = Stores.LoadAllStoresFromToday();
            
            //---------------------------------------- Load Data For Groups ---------------
            
            //List<Articles> allArticles = Articles.LoadAllArticlesFromToday();
            Group group = new Group();
            ArticleGroups ag = new ArticleGroups();
            List<Group> g_List = group.LoadDataAll();
            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            #region Load Data For Advertizement And SlideShow
            //-------------------------------------- Load Data For Advertizement And SlideShow --------------------------------------------------
            List<Articles> SlideShowData = new List<Articles>();
            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist = new List<ArticleGroups>();
            ArticleGroupslist = articleGroup.LoadDataAll();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            SlideShowData = Articles.LoadArticlesByGroupId(new ObjectId("5a526a33e290b819dc102a35"));

            //for (int g = 0; g < allArticles.Count; g++)
            //{
            //    // articleGroup.ArticleId = (allArticles[g]).Id;
            //    for (int r = 0; r < ArticleGroupslist.Count; r++)
            //    {
            //        if (ArticleGroupslist[r].ArticleId == allArticles[g].Id)
            //        {
            //            if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
            //            {
            //                SlideShowData.Add(allArticles[g]);
            //            }
            //        }
            //    }
            //}
            #endregion Load Data For Advertizement And SlideShow
            //----------------------------------------- Load All Article And Store Data --------------------------- 
            aaList = FillArticlesFullData(ArticleList);
            ssCompeteList = FillStoresFullData(StoreList);
            #region Load Data Of Store For Main Layout
            list_StoresForSlideShow = LoadStoreDataForSlideShow();

            articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                //StoreList= ssCompeteList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = g_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            #endregion Load Data Of Store For Main Layout
        }

        private static List<Stores> LoadStoreDataForSlideShow()
        {
            Stores str = new Stores();
            List<Stores> list_Stores = new List<Stores>();
            list_Stores = str.loadTopTwentyStoriesForSlide();
            return list_Stores;
        }

        private static List<StoreWithLikeDislikeAndComments> FillStoresFullData(List<Stores> storeList)
        {
            LikeDislikeStore likedislike = new LikeDislikeStore();
            List<LikeDislikeStore> list_likedislike = likedislike.LoadDataAll();

            StoreGroups stg = new StoreGroups();
            List<StoreGroups> storeGroup_List = new List<StoreGroups>();
            storeGroup_List = stg.LoadDataAll();

            StoreComment comment = new StoreComment();
            List<StoreComment> comment_list = comment.LoadDataAll();
            List<RegisterViewModel> li_users = new List<RegisterViewModel>();

            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            List<StoreWithLikeDislikeAndComments> aaList = new List<StoreWithLikeDislikeAndComments>();
            if (storeList.Count > 0)
            {
                for (int i = 0; i < storeList.Count; i++)
                {
                    Stores st = storeList[i];

                    List<StoreGalary> List_Galary = new List<StoreGalary>();
                    StoreGalary agalary = new StoreGalary();
                    agalary.StoreId = st.Id;
                    List_Galary = agalary.LoadDataByStoreId();

                    List<ArticleGroups> articleGroups = new List<ArticleGroups>();

                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    for (int j = 0; j < list_likedislike.Count; j++)
                    {
                        if (list_likedislike[j].StoreId == st.Id)
                        {
                            likesCount = likesCount + list_likedislike[j].LikeCount;
                            dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                            if (list_likedislike[j].UserIp == ipaddress)
                            {
                                ip = list_likedislike[j].UserIp;
                            }
                            else { ip = ""; }
                        }
                    }
                    LikeDislikeStore ldarticle = new LikeDislikeStore
                    {
                        StoreId = storeList[i].Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    for (int j = 0; j < comment_list.Count; j++)
                    {
                        if (comment_list[j].StoreId != st.Id)
                        {
                            comment_list.RemoveAt(j);
                            j = j - 1;
                        }
                    }
                    commentsCount = comment_list.Count;
                    StoreWithLikeDislikeAndComments aa = new StoreWithLikeDislikeAndComments
                    {
                        Store = st,
                        LikeDislike = ldarticle,
                        StoreComments = comment_list,
                        Comment = comment,
                        StoreGalary = List_Galary
                    };
                    aaList.Add(aa);
                }
            }
            return aaList;
        }

        private static List<ArticleWithLikeDislikeAndComments> FillArticlesFullData(List<Articles> ArticleList)
        {
            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();

            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            articleGroup_List = ag.LoadDataAll();

            ArticleComment comment = new ArticleComment();
            List<ArticleComment> comment_list = comment.LoadDataAll();
            List<RegisterViewModel> li_users = new List<RegisterViewModel>();

            List<ArticleComment> articleComments = new List<ArticleComment>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            List<ArticleWithLikeDislikeAndComments> aaList = new List<ArticleWithLikeDislikeAndComments>();
            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = ArticleList[i];

                    List<ArticleGalary> List_Galary = new List<ArticleGalary>();
                    ArticleGalary agalary = new ArticleGalary();
                    agalary.ArticleId = al.Id;
                    List_Galary = agalary.LoadDataByArticleId();

                    List<ArticleGroups> articleGroups = new List<ArticleGroups>();
                    for (int j = 0; j < articleGroup_List.Count; j++)
                    {
                        if (articleGroup_List[j].ArticleId == al.Id)
                        {
                            articleGroups.Add(articleGroup_List[j]);
                        }
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    for (int j = 0; j < list_likedislike.Count; j++)
                    {
                        if (list_likedislike[j].ArticleId == al.Id)
                        {
                            likesCount = likesCount + list_likedislike[j].LikeCount;
                            dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                            if (list_likedislike[j].UserIp == ipaddress)
                            {
                                ip = list_likedislike[j].UserIp;
                            }
                            else { ip = ""; }
                        }
                    }
                    LikeDislikeArticle ldarticle = new LikeDislikeArticle
                    {
                        ArticleId = ArticleList[i].Id,
                        DislikeCount = dislikesCount,
                        LikeCount = likesCount,
                        UserIp = ip
                    };
                    for (int j = 0; j < comment_list.Count; j++)
                    {
                        if (comment_list[j].ArticleId == al.Id)
                        {
                            articleComments.Add(comment_list[j]);
                        }
                    }
                    commentsCount = articleComments.Count;
                    ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                    {
                        Article = al,
                        LikeDislike = ldarticle,
                        ArticleComments = articleComments,
                        Comment = comment,
                        ArticleGalary = List_Galary,
                        ArticleGroupsData = articleGroups
                    };
                    aaList.Add(aa);
                }
            }
            return aaList;
        }


        public static void CreateArticleListByGroupId(List<Articles> ArticleList, ObjectId groupid, ArticleListComplete articleList,
            List<ArticleWithLikeDislikeAndComments> aaList, out List<Stores> storeSlideShow)
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }
            List<Group> group_TreeView = new List<Group>();
            for (int r = 0; r < group_List.Count; r++)
            {
                if (group_List[r].Name != "تبلیغات" && group_List[r].Name != "اسلاید شو")
                {
                    group_TreeView.Add(group_List[r]);
                }
            }
            List<Articles> SlideShowData = new List<Articles>();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------
             
            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            ArticleGroups articleGroup = new ArticleGroups();
            List<ArticleGroups> ArticleGroupslist;
            ArticleGroupslist = articleGroup.LoadDataAll();

            ArticleGalary galary = new ArticleGalary();
            List<ArticleGalary> galaryList = galary.LoadDataAll();
            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();
            for (int g = 0; g < allArticles.Count; g++)
            {
                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if (ArticleGroupslist[r].ArticleId == allArticles[g].Id)
                    {
                        if (ArticleGroupslist[r].GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                        {
                            SlideShowData.Add(allArticles[g]);
                        }
                    }
                }
                for (int j = 0; j < galaryList.Count; j++)
                {
                    if (galaryList[j].ArticleId == allArticles[g].Id)
                        articleGalaryList.Add(galaryList[j]);
                }
            }
            LikeDislikeArticle likedislike = new LikeDislikeArticle();
            List<LikeDislikeArticle> list_likedislike = likedislike.LoadDataAll();
            ArticleGroups ag = new ArticleGroups();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            List<ArticleGroups> list_Group = ag.LoadDataAll();


            ArticleComment comment = new ArticleComment();
            List<ArticleComment> commentList = comment.LoadDataAll();
            List<ArticleComment> articleComments = new List<ArticleComment>();
            if (ArticleList.Count > 0)
            {
                for (int i = 0; i < ArticleList.Count; i++)
                {
                    Articles al = (Articles)ArticleList[i];
                    for (int j = 0; j < list_Group.Count; j++)
                    {
                        if (list_Group[j].ArticleId == al.Id)
                            articleGroup_List.Add(list_Group[j]);
                    }
                    int likesCount = 0;
                    int dislikesCount = 0;
                    int commentsCount = 0;
                    string ip = "0";
                    if (articleGroup_List.Count > 0)
                    {
                        for (int k = 0; k < articleGroup_List.Count; k++)
                        {
                            if (al.Id == articleGroup_List[k].ArticleId)
                            {
                                for (int j = 0; j < list_likedislike.Count; j++)
                                {
                                    if (list_likedislike[j].ArticleId == al.Id)
                                    {
                                        likesCount = likesCount + list_likedislike[j].LikeCount;
                                        dislikesCount = dislikesCount + list_likedislike[j].DislikeCount;
                                        if (list_likedislike[j].UserIp == ipaddress)
                                        {
                                            ip = list_likedislike[j].UserIp;
                                        }
                                        else { ip = ""; }
                                    }
                                }
                                LikeDislikeArticle ldarticle = new LikeDislikeArticle
                                {
                                    ArticleId = ArticleList[i].Id,
                                    DislikeCount = dislikesCount,
                                    LikeCount = likesCount,
                                    UserIp = ip
                                };
                                for (int j = 0; j < commentList.Count; j++)
                                {
                                    if (commentList[j].ArticleId == al.Id)
                                        articleComments.Add(commentList[j]);
                                }
                                commentsCount = articleComments.Count;
                                ArticleWithLikeDislikeAndComments aa = new ArticleWithLikeDislikeAndComments
                                {
                                    Article = al,
                                    LikeDislike = ldarticle,
                                    ArticleComments = articleComments,
                                    Comment = comment,
                                    ArticleGalary = articleGalaryList,
                                    ArticleGroupsData = articleGroup_List
                                };

                                aaList.Add(aa);
                            }
                        }
                    }
                }
            }
            articleList = new ArticleListComplete
            {
                ArticleList = aaList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = group_TreeView
            };
            storeSlideShow = LoadStoreDataForSlideShow();
        }

        public static void CreateStoreListForDetail(Stores store, out List<ArticleWithLikeDislikeAndComments> article, out StoreListComplete storeListFinal,
            out List<StoreGalary> storetailGalaryList, out List<Stores> al_RelatedArticles,
           out List<StoreWithLikeDislikeAndComments> storeList, out ArticleListComplete articleList, out List<Stores> storeSlideShow)
        {
            storeSlideShow = LoadStoreDataForSlideShow();
            article = new List<ArticleWithLikeDislikeAndComments>();
            storeList = new List<StoreWithLikeDislikeAndComments>();
            storetailGalaryList = new List<StoreGalary>();
            string strHostName = "";
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string ipaddress = Convert.ToString(ipEntry.AddressList[1]);

            LikeDislikeStore likedislike = new LikeDislikeStore();
            List<LikeDislikeStore> list_likedislike = likedislike.LoadDataAll();
             
            List<Articles> ArticleList = new List<Articles>();
            ArticleList = Articles.LoadDataAll();

            Group group = new Group();
            List<Group> g_List = group.LoadDataAll();
            List<Group> group_List = new List<Group>();
            List<ArticleGroups> articleGroup_List = new List<ArticleGroups>();
            ArticleGroups ag = new ArticleGroups();
            List<Group> list_Group = ag.LoadDataAllGroupsWithArticle();
            for (int i = 0; i < list_Group.Count; i++)
            {
                if (list_Group[i].Name == "تبلیغات" || list_Group[i].Name == "اسلاید شو")
                {
                    list_Group.RemoveAt(i);
                    i = i - 1;
                }
            }
            for (int i = 0; i < g_List.Count; i++)
            {
                group_List.Add(g_List[i]);
            }

            List<Articles> SlideShowData = new List<Articles>();

            //------------------------------ تبلیغات  ------------------
            Advertizements advertizement = new Advertizements();
            List<Advertizements> advertizementTopData = new List<Advertizements>();
            advertizementTopData = advertizement.LoadTopFourData();
            List<Advertizements> advertizementBottomData = new List<Advertizements>();
            advertizementBottomData = advertizement.LoadTopTwoData();
            //----------------------------------------------------------

            List<ArticleGalary> articleGalaryList = new List<ArticleGalary>();

            List<ArticleGroups> ArticleGroupslist;
             
            List<Articles> allArticles = Articles.LoadAllArticlesFromToday();

            ArticleGroups articleGroup = new ArticleGroups();
            for (int g = 0; g < allArticles.Count; g++)
            {
                articleGroup.ArticleId = (allArticles[g]).Id;
                ArticleGroupslist = articleGroup.LoadDataByArticleId();

                for (int r = 0; r < ArticleGroupslist.Count; r++)
                {
                    if ((ArticleGroupslist[r]).GroupId.ToString() == "5a526a33e290b819dc102a35")//SlideShow
                    {
                        SlideShowData.Add(allArticles[g]);
                    }
                }
                ArticleGalary galary = new ArticleGalary();
                galary.ArticleId = (allArticles[g]).Id;
                List<ArticleGalary> galaryList = galary.LoadDataByArticleId();
                if (galaryList.Count > 0)
                {
                    for (int j = 0; j < galaryList.Count; j++)
                    {
                        articleGalaryList.Add(galaryList[j]);
                    }
                }

            }

            #region ArticleData

            article = FillArticlesFullData(ArticleList);
            articleList = new ArticleListComplete
            {
                ArticleList = article,
                //StoreList= ssCompeteList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBottom = advertizementBottomData,
                GroupList = g_List,
                ArticleSlideShowList = SlideShowData,
                TreeGroup = list_Group
            };
            #endregion ArticleData

            #region StoreData

            al_RelatedArticles = new List<Stores>();
            if (store != null)
            {
                string[] str_RelatedStores = store.Tag.Split('،');

                al_RelatedArticles = store.LoadRelatedStores();
                List<StoreGroups> storeGroup_List = new List<StoreGroups>();
                StoreGroups sg = new StoreGroups();
                sg.StoreId = store.Id;
                storeGroup_List = sg.LoadDataByStoreId();

                int likesCount = 0;
                int dislikesCount = 0;
                int commentsCount = 0;
                string ip = "0";

                for (int j = 0; j < list_likedislike.Count; j++)
                {
                    if ((list_likedislike[j]).StoreId == store.Id)
                    {
                        likesCount = likesCount + (list_likedislike[j]).LikeCount;
                        dislikesCount = dislikesCount + (list_likedislike[j]).DislikeCount;
                        if ((list_likedislike[j]).UserIp == ipaddress)
                        {
                            ip = (list_likedislike[j]).UserIp;
                        }
                        else
                            ip = "";
                    }
                }
                LikeDislikeStore ldarticle = new LikeDislikeStore
                {
                    StoreId = store.Id,
                    DislikeCount = dislikesCount,
                    LikeCount = likesCount,
                    UserIp = ip
                };
                StoreComment comment = new StoreComment();
                comment.StoreId = store.Id;
                List<StoreComment> list = comment.LoadDataBystoreId();
                List<StoreComment> commentList = new List<StoreComment>();
                foreach (StoreComment a in list)
                {
                    commentList.Add(a);
                }
                commentsCount = list.Count;
                StoreGalary galary = new StoreGalary();
                galary.StoreId = store.Id;
                storetailGalaryList = galary.LoadDataByStoreId();
                StoreWithLikeDislikeAndComments aa = new StoreWithLikeDislikeAndComments
                {
                    Store = store,
                    LikeDislike = ldarticle,
                    StoreComments = commentList,
                    Comment = comment,
                    StoreGalary = storetailGalaryList
                };
                storeList.Add(aa);
            }
            storeListFinal = new StoreListComplete
            {
                StoreList = storeList,
                AdvertizementTop = advertizementTopData,
                AdvertizementBttom = advertizementBottomData,
                GroupList = group_List,
                ArticleSlideShowList = SlideShowData
            };
            #endregion StoreData

        }

    }
}