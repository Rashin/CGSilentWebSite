﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGSilent.Models
{
    public class SiteStatics
    {
        #region  Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _RoleName;

        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }

        private string _UserIdOrIp;

        public string UserIdOrIp
        {
            get { return _UserIdOrIp; }
            set { _UserIdOrIp = value; }
        }

        private string _PageName;

        public string PageName
        {
            get { return _PageName; }
            set { _PageName = value; }
        }



        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private ObjectId _ArticleId;

        public ObjectId ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private ObjectId _StoreId;

        public ObjectId StoreId
        {
            get { return _StoreId; }
            set { _StoreId = value; }
        }

        #endregion Properties


        #region Events
        public List<SiteStatics> LoadDataByPageName()
        {
            IMongoCollection<SiteStatics> collection = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics");
            List<SiteStatics> ss = collection.Find((Builders<SiteStatics>.Filter.Eq("PageName", PageName))).ToList();
            return ss;
        }

        public List<SiteStatics> LoadDataByDate(DateTime startdate, DateTime enddate)
        {
            IMongoCollection<SiteStatics> collection = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics");
            List<SiteStatics> ss = collection.Find((Builders<SiteStatics>.Filter.Empty)).Sort(Builders<SiteStatics>.Sort.Descending("CreatedDate")).ToList();
            for (int i = 0; i < ss.Count; i++)
            {
                if (DateTime.Parse(ss[i].CreatedDate) > enddate)
                {
                    ss.RemoveAt(i);
                    i = i - 1;
                    break;
                }
                if (DateTime.Parse(ss[i].CreatedDate) < startdate)
                {
                    ss.RemoveAt(i);
                    i = i - 1;
                    break;
                }
            }
            return ss;
        }


        public List<SiteStatics> LoadDataAll()
        {
            IMongoCollection<SiteStatics> collection = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics");
            List<SiteStatics> ss = collection.Find((Builders<SiteStatics>.Filter.Empty)).Sort(Builders<SiteStatics>.Sort.Descending("CreatedDate")).ToList();
            return ss;
        }

        public int LoadDataAllCount()
        {
            IMongoCollection<SiteStatics> collection = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics");
            int ss = collection.Find((Builders<SiteStatics>.Filter.Empty)).Sort(Builders<SiteStatics>.Sort.Descending("CreatedDate")).ToList().Count;
            return ss;
        }

        public ObjectId Insert()
        {
            IMongoCollection<SiteStatics> collection = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics");
            collection.InsertOne(this);
            return Id;
        }

        internal List<SiteStatics> LoadArticleViewByDateAndOrder()
        {
            IMongoCollection<SiteStatics> collection = MongoConnection.instance.GetCollection<SiteStatics>("SiteStatics");

            List<SiteStatics> list_sitestatic = collection.Find((Builders<SiteStatics>.Filter.Empty)).Sort(Builders<SiteStatics>.Sort.Descending("CreatedDate")).ToList();


            return list_sitestatic;
        }
        #endregion Events
    }
}