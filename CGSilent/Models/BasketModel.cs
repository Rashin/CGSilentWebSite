﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGSilent.Models
{
    public class BasketModel
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private ObjectId _UserId;

        public ObjectId UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }


        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private List<ObjectId> _ArticleId;

        public List<ObjectId> ArticleId
        {
            get { return _ArticleId; }
            set { _ArticleId = value; }
        }

        private List<string> _ArticleTitle;

        public List<string> ArticleTitle
        {
            get { return _ArticleTitle; }
            set { _ArticleTitle = value; }
        }

        private List<ObjectId> _StoreId;

        public List<ObjectId> StoreId
        {
            get { return _StoreId; }
            set { _StoreId = value; }
        }

        private List<string> _StoreTitle;

        public List<string> StoreTitle
        {
            get { return _StoreTitle; }
            set { _StoreTitle = value; }
        }

        private int _Status;

        public int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }


        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }


        private string _LastView;

        public string LastView
        {
            get { return _LastView; }
            set { _LastView = value; }
        }


        #endregion Properties

        #region Events

        public List<BasketModel> LoadAllNewBaskets()
        {
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");
            List<BasketModel> basket = collection.Find((Builders<BasketModel>.Filter.Empty)).ToList();
            return basket;
        }

        public List<BasketModel> LoadAllBaskets()
        {
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");
            List<BasketModel> basket = collection.Find((Builders<BasketModel>.Filter.Empty)).ToList();

            return basket;

        }
        public List<BasketModel> LoadBasketsByUserId()
        {
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");
            var userbasket = collection.Find((Builders<BasketModel>.Filter.Eq("UserId", UserId))).ToList();

            return userbasket;
        }

        public List<UserBasket> LoadBasketsdataByUserId()
        {
            IMongoCollection<UserBasket> collection2 = MongoConnection.instance.GetCollection<UserBasket>("Basket");
            var fields = Builders<UserBasket>.Projection.Include(p => p.Id).Include(p => p.ArticleId);
            var userbasket2 = collection2.Find((Builders<UserBasket>.Filter.Eq("_id", UserId))).ToList();
            return userbasket2;
        }

        public List<BasketModel> LoadBasketsById()
        {
            List<BasketModel> basket = new List<BasketModel>();
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");
            basket = collection.Find((Builders<BasketModel>.Filter.Eq("_id", Id))).ToList();

            return basket;
        }


        public List<BasketModel> UpdateBasketStatus()
        {
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");

            var update = Builders<BasketModel>.Update.Set("Status", Status);
            collection.UpdateOne(Builders<BasketModel>.Filter.Eq("_id", Id), update);
            List<BasketModel> baskets = collection.Find((Builders<BasketModel>.Filter.Empty)).ToList();
            return baskets;
        }

        internal void UpdateLastView()
        {
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");
            var update = Builders<BasketModel>.Update.Set("LastView", UpdateDate);
            collection.UpdateMany(Builders<BasketModel>.Filter.Empty, update);
        }

        public ObjectId Insert()
        {
            IMongoCollection<BasketModel> collection = MongoConnection.instance.GetCollection<BasketModel>("Basket");
            collection.InsertOne(this);
            return Id;
        }

        internal List<BasketModel> LoadAllDoneBaskets()
        {
            throw new NotImplementedException();
        }

        internal List<BasketModel> LoadAllNotDoneBaskets()
        {
            throw new NotImplementedException();
        }

        internal List<BasketModel> LoadAllDoingBaskets()
        {
            throw new NotImplementedException();
        }



        #endregion Events
    }
    public class BasketStatus
    {

    }

    public class BasketFullInfo
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _UserId;

        public string UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string _ArticleTitle;

        public string ArticleTitle
        {
            get { return _ArticleTitle; }
            set { _ArticleTitle = value; }
        }

        private string _Status;

        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        private DateTime _LastView;

        public DateTime LastView
        {
            get { return _LastView; }
            set { _LastView = value; }
        }

        private string _LinkAddress;

        public string LinkAddress
        {
            get { return _LinkAddress; }
            set { _LinkAddress = value; }
        }


        #endregion Properties

        #region Events
        public List<BasketFullInfo> LoadAllBaskets()
        {
            IMongoCollection<BasketFullInfo> collection = MongoConnection.instance.GetCollection<BasketFullInfo>("Basket");
            List<BasketFullInfo> basket = collection.Find((Builders<BasketFullInfo>.Filter.Eq("UserId", UserId))).ToList();

            return basket;
        }

        public List<BasketFullInfo> LoadUsersBasket()
        {
            List<BasketFullInfo> basket = new List<BasketFullInfo>();
            IMongoCollection<BasketFullInfo> collection = MongoConnection.instance.GetCollection<BasketFullInfo>("Basket");
         basket = collection.Find((Builders<BasketFullInfo>.Filter.Eq("UserId", UserId))).ToList();
            //basket = collection.Find((Builders<BasketFullInfo>.Filter.Empty)).ToList();

            return basket;
        }

        public List<BasketFullInfo> LoadBasketsByStatus()
        {
            List<BasketFullInfo> basket = new List<BasketFullInfo>();
            IMongoCollection<BasketFullInfo> collection = MongoConnection.instance.GetCollection<BasketFullInfo>("Basket");
            basket = collection.Find((Builders<BasketFullInfo>.Filter.Empty)).ToList();

            return basket;
        }
        #endregion Events
    }


    public class UserBasketInfo
    {
        private ObjectId _UserId;

        public ObjectId UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

    }
}