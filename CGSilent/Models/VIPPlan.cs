﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGSilent.Models
{
    public class VIPPlan
    {
        #region Properties

        private ObjectId _Id;

        public ObjectId Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Caption;

        public string Caption
        {
            get { return _Caption; }
            set { _Caption = value; }
        }

        private float _Price;

        public float Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        private int _DateLong;

        public int DateLong
        {
            get { return _DateLong; }
            set { _DateLong = value; }
        }


        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }


        private bool _IsDeleted;

        public bool IsDeleted
        {
            get { return _IsDeleted; }
            set { _IsDeleted = value; }
        }

        #endregion

        #region Evemnts
        public VIPPlan LoadDataById()
        {
            IMongoCollection<VIPPlan> collection = MongoConnection.instance.GetCollection<VIPPlan>("VIPPlan");
            List<VIPPlan> ss = collection.Find((Builders<VIPPlan>.Filter.Eq("_id", Id))).ToList();
            if (ss.Count > 0)
                return ss[0];
            else
                return null;
        }

        public List<VIPPlan> LoadDataAll()
        {
            IMongoCollection<VIPPlan> collection = MongoConnection.instance.GetCollection<VIPPlan>("VIPPlan");
            List<VIPPlan> ss = collection.Find((Builders<VIPPlan>.Filter.Empty)).Sort(Builders<VIPPlan>.Sort.Descending("vipPlanPrice")).ToList();
            return ss;
        }

        public List<VIPPlan> LoadDataActiveAll()
        {
            IMongoCollection<VIPPlan> collection = MongoConnection.instance.GetCollection<VIPPlan>("VIPPlan");
            List<VIPPlan> ss = collection.Find((Builders<VIPPlan>.Filter.Eq("IsDeleted", false))).ToList();
            return ss;
        }
        

        public ObjectId Insert()
        {
            IMongoCollection<VIPPlan> collection = MongoConnection.instance.GetCollection<VIPPlan>("VIPPlan");
            collection.InsertOne(this);
            return Id;
        }


        public void Update()
        {
            IMongoCollection<VIPPlan> collection = MongoConnection.instance.GetCollection<VIPPlan>("VIPPlan");
            // var update = Builders<Articles>.Update.Set("Articles", ViewCount);
            collection.ReplaceOne(Builders<VIPPlan>.Filter.Eq("_id", Id), this);
        }

        public void DeleteById()
        {
            IMongoCollection<VIPPlan> collection = MongoConnection.instance.GetCollection<VIPPlan>("VIPPlan");
            var update = Builders<VIPPlan>.Update.Set("IsDeleted", 1);
            collection.UpdateOne(Builders<VIPPlan>.Filter.Eq("_id", Id), update);
        }


        #endregion Events
    }
}