﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CGSilent.Startup))]
namespace CGSilent
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            ConfigureAuth(app); 
        }
         

    }
}
